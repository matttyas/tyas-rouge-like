var IdGenerator = {
  currentId: 0
};

IdGenerator.generateId = function() {
  IdGenerator.currentId += 1;
  return IdGenerator.currentId;
};
