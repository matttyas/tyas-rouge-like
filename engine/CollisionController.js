var CollisionController = {};

CollisionController.checkForCollision = function(actor, movement) {
  var tileX = Math.round(actor.x / CollisionController.engine.tileWidth);
  var tileY = Math.round(actor.y / CollisionController.engine.tileHeight);
  var tiles = CollisionController.currentMap.getCloseTiles(tileX, tileY, 2);
  var colliderActors = CollisionController.currentMap.getColliderActors(); //Get only close actors here.
  var enemies = CollisionController.currentMap.getEnemies();

  CollisionController.checkForCollisionWithObjects(tiles, actor, movement);
  CollisionController.checkForCollisionWithObjects(colliderActors, actor, movement);
  CollisionController.checkForCollisionWithObjects(enemies, actor, movement);
};

CollisionController.setEngine = function(engine) {
  CollisionController.engine = engine;
};

CollisionController.setRogueScene = function(rogueScene) {
  CollisionController.rogueScene = rogueScene;
};

CollisionController.setCurrentMap = function(currentMap) {
  CollisionController.currentMap = currentMap;
};

CollisionController.checkForCollisionWithObjects = function(objects, actor, movement) {
  var actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];
    if (!object) {
      console.log('debugya');
    }
    if (actor.id == object.id) continue; 
    var colliderObject = object.getColliderObject();
    if (!colliderObject) continue;

    var collision = colliderObject.checkForCollision(actorCollisionRect);

    if (collision.collision) {
      object.applyCollision(actor, collision, movement);
      actor.applyCollision(object, collision, movement);
      actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
    }
  }
};

CollisionController.buildActorCollisionRectangle = function(actor, movement) {
  if (!movement) movement = {x: 0, y: 0};
  if (!movement.x) movement.x = 0;
  if (!movement.y) movement.y = 0;
  return {x: actor.x + movement.x, y: actor.y + movement.y, width: actor.width, height: actor.height};
};

// if (movement.x < 0) {
//    movement.x = movement.x + Math.round(collision.xOverlap);
// } else {
//   movement.x = movement.x - Math.round(collision.xOverlap);
// }
