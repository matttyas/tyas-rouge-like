//ColliderObject hitbox positions relative to actor

function ColliderObject (actor, x, y, width, height) {
  this.actor = actor;
  this.initialise(x, y, width, height);
};

ColliderObject.prototype.initialise = function (x, y, width, height) {
  this.x = x;
  this.y = y;

  this.hitBoxes = [];

  if(width && height) {
    this.hitBoxes.push({x: x, y: y, width: width, height: height});
  }
};

ColliderObject.prototype.checkForCollision = function(checkRectangle) {
  for (var i = 0; i < this.hitBoxes.length; i++) {
    var collision = this.checkForCollisionWithHitBox(checkRectangle, this.hitBoxes[i]);
    if (collision.collision) return collision;
  }
  return {collision: false};
};

ColliderObject.prototype.checkForCollisionWithHitBox = function(checkRectangle, hitBox) {
  var x = this.actor.x + hitBox.x;
  var y = this.actor.y + hitBox.y;
  var hitBoxCheckRectangle = {x: x, y: y, width: hitBox.width, height: hitBox.height};
  return Physics.checkCollisionRects(checkRectangle, hitBoxCheckRectangle);
};

ColliderObject.prototype.addHitBox = function(newHitBox) {
  if (!newHitBox.x || !newHitBox.y || !newHitBox.width || !newHitBox.height) {
    return console.error('A hit box needs an x, y, width and height');
  }

  this.hitBoxes.push(newHitBox);
};

ColliderObject.prototype.getHitBoxes = function() {
  return this.hitBoxes;
};
