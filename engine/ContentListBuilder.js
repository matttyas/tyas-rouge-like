//Take RogueMap and convert to
var ContentListBuilder = {};

ContentListBuilder.buildListFromRogueMap = function(rogueMap) {
  var content = [];
  content = content.concat(ContentListBuilder.buildContentFromElementsList(rogueMap.decorations));
  content = content.concat(ContentListBuilder.buildContentFromElementsList(rogueMap.entrances));
  content = content.concat(ContentListBuilder.buildContentFromElementsList(rogueMap.enemies));
  content = content.concat(ContentListBuilder.buildContentFromTiles(rogueMap.tiles));
  return content;
};

ContentListBuilder.buildListFromRogueScene = function(rogueScene) {
  var content = [];
  content = content.concat(ContentListBuilder.buildContentFromElementsList(rogueScene.getHeroes()));
  return content;
};

ContentListBuilder.buildContentFromElementsList = function(elements) {
  if (!elements || elements.constructor != Array || elements.length == 0) return [];

  var content = [];

  for(var i = 0; i < elements.length; i++) {
    content.push({
      name: elements[i].getName(),
      content: elements[i].getTexturePath()
    });
  }

  content = ContentListBuilder.filterNonUniqueContent(content);

  return content;
};

ContentListBuilder.filterNonUniqueContent = function(content) {
	var namesInArray = [];
  return content.filter(function(item, pos) {
  	if (!item.name) return false;
  	if (namesInArray.indexOf(item.name) == -1) {
  		namesInArray.push(item.name);
  		return true;
  	}
    return false;
	});
};

ContentListBuilder.buildContentFromTiles = function(tiles) {
  if (!tiles || tiles.constructor != Array || tiles.length == 0) return [];
  var content = [];
  for (var i = 0; i < tiles.length; i++) {
    for (var j = 0; j < tiles[i].length; j++) {
      content.push({
        name: tiles[i][j].getName(),
        content: tiles[i][j].getTexturePath()
      });
    }
  }

  content = ContentListBuilder.filterNonUniqueContent(content);
  return content;
};

ContentListBuilder.getContentForItems = function() {
  return ContentListBuilder.buildContentFromElementsList(ItemContentLibrary.getRogueItems());
};
