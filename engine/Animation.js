function Animation(animationOrder, animationIndex) {
  this.animationOrder = animationOrder || [];
  this.animationIndex = animationIndex;
  this.timeBetweenFrames = 5;
  this.timeSinceFrameChange = 0;
  this.currentSpriteSheetRectangle = {x: 0, y: 0, width: 0, height: 0};
};

Animation.prototype.animate = function() {
  if (!this.checkForNewFrame()) return;
  this.animationIndex++;
  if (this.animationIndex == this.animationOrder.length) this.animationIndex = 0;

  this.currentFrame.x = this.animationOrder[this.animationIndex] * this.currentFrame.width;
};

Animation.prototype.checkForNewFrame = function() {
  if(this.animationOrder.length <= 1) return false;
  this.timeSinceFrameChange++;
  if(this.timeSinceFrameChange ==  this.timeBetweenFrames) {
    this.timeSinceFrameChange = 0;
    return true;
  }

  return false;
};

Animation.prototype.setFrameWidth = function() {
  this.currentFrame.width = width;
};

Animation.prototype.setFrameHeight = function() {
  this.currentFrame.height = height;
};

Animation.prototype.setFrameX = function(x) {
  this.currentFrame.x = x;
};

Animation.prototype.setFrameY = function(y) {
  this.currentFrame.y = y;
};

Animation.prototype.setCurrentFrame = function(rectangle) {
  this.currentFrame = rectangle;
}

Animation.prototype.getSpriteSheetRectangle = function() {
  return this.currentFrame;
};

Animation.prototype.setTimeBetweenFrames = function(timeBetweenFrames) {
  this.timeBetweenFrames = timeBetweenFrames;
};

Animation.prototype.setAnimationOrder = function(animationOrder) {
  this.animationOrder = animationOrder
};

Animation.prototype.setAnimationIndex = function(animationIndex) {
  this.animationIndex = animationIndex;
};
