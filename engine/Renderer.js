/*
  SPECIAL THANKS:

  Joe. - He gave me a piece of sandwhich when I needed it most.
  James - Who demanded more parallax scrolling.

*/

function Renderer(ctx, width, height) {
  this.ctx = ctx;
  this.width = width;
  this.height = height;
  this.currentScene = null;

  this.useGradientForBack = false;
  //this.gradientColors = ['#663399', '#F88017'];
  this.gradientColors = ['rgb(0,0,0)', 'rgb(93, 64, 123)'];
  this.backColor = 'rgb(0,0,0)';
  this.hideScrollingBack = false;

  this.font = 'Arial';
  this.textSize = '12px';
  this.textColor = 'rgb(255, 0, 0)';
}

Renderer.prototype.render = function() {

  this.camera.update();
  this.clearBackground();

  if(this.currentScene.getLoaded()) {
    this.currentScene.render(this.buffer, this);
  } else {
    this.currentScene.renderLoadingScreen(this.buffer, this);
  }


  // if (this.currentScene) {
  //   for (var i = 0; i < this.currentScene.actors.length; i++) {
  //       this.drawActor(this.currentScene.actors[i], true);
  //   }
  // }

  this.ctx.drawImage(this.buffer, 0, 0);
}

Renderer.prototype.clearBackground = function() {
  if (this.useGradientForBack) {
    var grd = this.buffer_context.createLinearGradient(0, 0, 0, this.height);
    grd.addColorStop(0, this.gradientColors[0]);
    grd.addColorStop(1, this.gradientColors[1]);
    this.buffer_context.fillStyle = grd;

  } else {
    this.buffer_context.fillStyle = this.backColor;
  }

  this.buffer_context.fillRect(0,0, this.width, this.height);

}

Renderer.prototype.drawActor = function(actor, blur) {

    if (actor.renderMethod == 'self') {
        actor.render(this.buffer_context, this, blur);
    }
    else if (actor.renderMethod == 'square') {
        this.buffer_context.fillStyle = actor.color || '#000000';
        this.buffer_context.fillRect(actor.x, actor.y, actor.width, actor.height);
    } else if (actor.renderMethod == 'circle') {

        if (actor.style && actor.style == 'stroke') {
            this.buffer_context.lineWidth = actor.lineWidth || 1;
            this.buffer_context.strokeStyle = actor.color || '#000000';
            this.buffer_context.beginPath();
          this.buffer_context.arc(actor.x, actor.y, actor.width, 0, 2 * Math.PI, false);
          this.buffer_context.stroke();
        } else {
            this.buffer_context.fillStyle = actor.color || '#000000';
            this.buffer_context.beginPath();
          this.buffer_context.arc(actor.x, actor.y, actor.width, 0, 2 * Math.PI, false);
          this.buffer_context.fill();
        }
    }

}

Renderer.prototype.setCurrentScene = function(scene) {
    this.currentScene = scene;
    this.currentScene.handleRenderer(this);
};

Renderer.prototype.blur = function(actor) {
    var oldX = actor.x;
    var oldY = actor.y;

    actor.x = actor.x - this.camera.movement.x / 2;
    actor.y = actor.y - this.camera.movement.y / 2;

    this.buffer_context.globalAlpha = 0.5;

    this.drawActor(actor, false);

    this.buffer_context.globalAlpha = 1;
    actor.x = oldX;
    actor.y = oldY;
}

Renderer.prototype.setUseGradientForBack = function (useGradientForBack) {
  this.useGradientForBack = useGradientForBack;
};

Renderer.prototype.setGradientColors = function (colors) {
  this.gradientColors = colors;
};

Renderer.prototype.useBackColor = function(color) {
  this.useGradientForBack = false;
  if (color) this.backColor = color;
};

Renderer.prototype.setHideScrollingBack = function(hideScrollingBack) {
  this.hideScrollingBack = hideScrollingBack;
};

//Naughty. But oh well :D
Renderer.prototype.useScrollingBack = function(scrollingBack) {
  this.currentScene.levelScrollingBackground = scrollingBack;
};

Renderer.prototype.setUseBackParticleSystem = function (useBackParticleSystem) {
  this.currentScene.useBackParticleSystem = useBackParticleSystem;
};

Renderer.prototype.drawTriangle = function(triangle, fillStyle) {
  this.buffer_context.fillStyle = fillStyle;
  this.buffer_context.beginPath();
  this.buffer_context.moveTo(triangle[0].x - this.camera.x, triangle[0].y - this.camera.y);
  for (var i = 0; i < triangle.length; i++) {
    if (i != 0) this.buffer_context.lineTo(triangle[i].x - this.camera.x, triangle[i].y - this.camera.y);
  }
  this.buffer_context.lineTo(triangle[0].x - this.camera.x, triangle[0].y - this.camera.y);
  this.buffer_context.fill();
  this.buffer_context.closePath();
};

Renderer.prototype.drawText = function(text, x, y) {
  this.buffer_context.fillStyle = this.textColor;
  this.buffer_context.font = this.textSize + " " + this.font;
  this.buffer_context.fillText(text, x, y);
};

Renderer.prototype.setFont = function(font) {
  this.font = font;
};

Renderer.prototype.setTextSize = function(textSize) {
  this.textSize = textSize + 'px';
};

Renderer.prototype.setTextColor = function (textColor) {
  this.textColor = textColor;
};

Renderer.prototype.drawLine = function (p1, p2) {
  this.buffer_context.beginPath();
  this.buffer_context.moveTo(p1.x, p1.y);
  this.buffer_context.lineTo(p2.x, p2.y);
  this.buffer_context.stroke();
  this.buffer_context.closePath();
};

Renderer.prototype.setLineWidth = function(lineWidth) {
  this.buffer_context.lineWidth = lineWidth;
};

Renderer.prototype.setStrokeStyle = function(strokeStyle) {
  this.buffer_context.strokeStyle = strokeStyle;
};

Renderer.prototype.strokeRect = function(x, y, width, height) {
  this.buffer_context.strokeRect(x, y, width, height);
};

Renderer.prototype.setFillStyle = function(fillStyle) {
  this.buffer_context.fillStyle = fillStyle;
};

Renderer.prototype.fillRect = function(x, y, width, height) {
  this.buffer_context.fillRect(x, y, width, height);
};

//Uses stroke style for outside box
Renderer.prototype.drawBoundedBox = function(x, y, width, height, strokeWidth) {
  var tempFillStyle = this.buffer_context.fillStyle;
  this.buffer_context.fillStyle = this.buffer_context.strokeStyle;

  this.fillRect(x,y,width,height);
  this.buffer_context.fillStyle = tempFillStyle;
  this.fillRect(x + strokeWidth, y + strokeWidth, width - (strokeWidth * 2), height - (strokeWidth * 2));
};

Renderer.prototype.scaleUpRectangle = function(rectangle, percentage) {
  var newWidth = ((rectangle.width / 100) * percentage) + rectangle.width;
  var newHeight = ((rectangle.height / 100) * percentage) + rectangle.height;
  var newX = rectangle.x - (((rectangle.width / 100) * percentage) / 2);
  var newY = rectangle.y - (((rectangle.height / 100) * percentage) / 2);

  return ({x: newX, y: newY, width: newWidth, height: newHeight});
};

Renderer.prototype.setBackColor = function(backColor) {
  if(typeof backColor != 'string') return console.error('Back Color must be a string');
  this.backColor = backColor;
};

Renderer.prototype.setCamera = function(camera) {
  if (!camera) return console.error('Camera must be passed in correctly');
  this.camera = camera;
};

Renderer.prototype.getCamera = function() {
  return this.camera;
};

Renderer.prototype.initialiseCanvas = function() {
  var canvas = document.getElementById('game');
  this.buffer = document.createElement('canvas');
  this.buffer.width = canvas.width;
  this.buffer.height = canvas.height;
  this.buffer_context = this.buffer.getContext('2d');
};

Renderer.prototype.drawImage = function(image, x, y) {
  this.buffer_context.drawImage(image, x, y);
};

Renderer.prototype.drawSpriteSheet = function(image, x, y, ix, iy, iwidth, iheight) {
  this.buffer_context.drawImage(image, ix, iy, iwidth, iheight, x, y, iwidth, iheight);
}

Renderer.prototype.setGlobalAlpha = function(globalAlpha) {
  this.buffer_context.globalAlpha = globalAlpha;
};

Renderer.prototype.renderBar = function(rectangle, partValue, value, color, backColor) {
  this.setFillStyle(backColor);
  this.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

  this.setFillStyle(color);

  var fillAmount = partValue / value;
  var width = (rectangle.width - 2) * fillAmount;

  this.fillRect(rectangle.x + 1, rectangle.y + 1, width, rectangle.height - 2);
};
