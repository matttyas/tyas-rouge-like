var keyMapper = {};

keyMapper.mapKeys = function(keys) {
  var mappedKeys = {};
  for (var key in keys) {
    var mappedKey = keyMapper.mapKey(key / 1);
    if (mappedKey) mappedKeys[mappedKey] = true;
  }

  return mappedKeys;
};

keyMapper.mapKey = function(keycode) {
  if(typeof keycode != 'number') {
    console.error('Invalid input to mapKey');
    return null;
  }

  var mappedKey = null;
  var keyMapping = {
    9: 'tab',
    13: 'enter',
    16: 'shift',
    17: 'ctrl',
    18: 'alt',
    20: 'caps',
    27: 'escape',
    32: 'space',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down',
    48: '0',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
    54: '6',
    55: '7',
    56: '8',
    57: '9',
    65: 'a',
    66: 'b',
    67: 'c',
    68: 'd',
    69: 'e',
    70: 'f',
    71: 'g',
    72: 'h',
    73: 'i',
    74: 'j',
    75: 'k',
    76: 'l',
    77: 'm',
    78: 'n',
    79: 'o',
    80: 'p',
    81: 'q',
    82: 'r',
    83: 's',
    84: 't',
    85: 'u',
    86: 'v',
    87: 'w',
    88: 'x',
    89: 'y',
    90: 'z'
  };

  mappedKey = keyMapping[keycode];
  if (!mappedKey && mappedKey !== '0') {
    console.error('Keycode: ' + keycode + ' is unsupported in keyMapper');
    return null;
  }

  return mappedKey;
};
