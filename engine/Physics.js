Physics = {

}

Physics.checkCollisionRects = function(object1, object2) {
    var object1Right = object1.x + object1.width;
    var object1RightY = object1.y + object1.height;

    var object2Right = object2.x + object2.width;
    var object2RightY = object2.y +object2.height;

    var xOverlap = Math.max(0, Math.min(object1Right, object2Right) - Math.max(object1.x, object2.x));
    var yOverlap = Math.max(0, Math.min(object1RightY, object2RightY) - Math.max(object1.y, object2.y));

    if (xOverlap > 0 && yOverlap > 0 ) {
        return {collision: true, xOverlap: xOverlap, yOverlap: yOverlap};
    } else {
        return {collision: false};
    }
}

Physics.advancedCollision = function(object1, object2) {
  //Make four lines
  var lines = [];
  lines.push( { start: {x: object2.x, y: object2.y}, end: {x: object2.x + object2.width, y: object2.y} }); //top
  lines.push( { start: {x: object2.x + object2.width, y: object2.y}, end: {x: object2.x + object2.width, y: object2.y + object2.height} }); //right
  lines.push( { start: {x: object2.x + object2.width, y: object2.y + object2.height}, end: {x: object2.x, y: object2.y + object2.height} }); //bottom
  lines.push( { start: {x: object2.x, y: object2.y}, end: {x: object2.x, y: object2.y + object2.height} }); //left

  var collisions = [];
  for (var i = 0; i < lines.length; i++) {
    if (Physics.checkLineRectangleIntersection(lines[i], object1)) {
      var xOverlap = Math.max(0, Math.min(object1.x + object1.width, object2.x + object2.width) - Math.max(object1.x, object2.x));
      var yOverlap = Math.max(0, Math.min(object1.y + object1.height, object2.y + object2.height) - Math.max(object1.y, object2.y));

      var collision = {collision: true, xOverlap: xOverlap, yOverlap: yOverlap, horizontal: true};
      if (lines[i].start.x != lines[i].end.x) {
        collision.horizontal = false;
      }
      //if (i == 0 || i == 2) {
        //collision.horizontal = false;
      //}

      collisions.push(collision);
    };

  }
  if (collisions.length == 0) {
    return false;
  } else {
    //trim out dodgy bits. get lines with most overlap for horizontal and vertical and keep those.
    if (collisions.length > 1) {
      var horizontalCollisions = [];
      var verticalCollisions = [];
      for (var i = 0; i < collisions.length; i++) {
        if (collisions[i].horizontal) {
          horizontalCollisions.push(collisions[i]);
        } else {
          verticalCollisions.push(collisions[i]);
        }
      }
      if (horizontalCollisions.length > 1) {
        if (horizontalCollisions[0].xOverlap > horizontalCollisions[1]) {
          horizontalCollisions = [horizontalCollisions[0]];
        } else {
          horizontalCollisions = [horizontalCollisions[1]];
        }
      }
      if (verticalCollisions.length > 1) {
        if (verticalCollisions[0].xOverlap > verticalCollisions[1]) {
          verticalCollisions = [verticalCollisions[0]];
        } else {
          verticalCollisions = [verticalCollisions[1]];
        }
      }
      collisions = horizontalCollisions.concat(verticalCollisions);
    }
  }
  return collisions;
  //Check collision for all
  //return collision and if vertical or horizontal
};

Physics.checkLineRectangleIntersection = function(line, rectangle) {
  var minX = rectangle.x;
  var minY = rectangle.y;
  var maxX = rectangle.x + rectangle.width;
  var maxY = rectangle.y + rectangle.height;
  var x1 = line.start.x;
  var x2 = line.end.x;
  var y1 = line.start.y;
  var y2 = line.end.y;

    // Completely outside.
  if ((x1 <= minX && x2 <= minX) || (y1 <= minY && y2 <= minY) || (x1 >= maxX && x2 >= maxX) || (y1 >= maxY && y2 >= maxY))
      return false;

  var m = (y2 - y1) / (x2 - x1);

  var y = m * (minX - x1) + y1;
  if (y > minY && y < maxY) return true;

  y = m * (maxX - x1) + y1;
  if (y > minY && y < maxY) return true;

  var x = (minY - y1) / m + x1;
  if (x > minX && x < maxX) return true;

  x = (maxY - y1) / m + x1;
  if (x > minX && x < maxX) return true;

  return false;

};

Physics.distanceBetweenPoints = function(p1, p2) {
  if(!p1 || !p2) {
    var debug = true;
  }
  var xDiff = p1.x - p2.x;
  var yDiff = p1.y - p2.y;

  return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
};

Physics.calculateMovementVector = function(p1, p2, speed) {
  var directionVector = {x: p2.x - p1.x, y: p2.y - p1.y};
  var xProportion = directionVector.x / (Math.abs(directionVector.x) + Math.abs(directionVector.y));
  var yProportion = directionVector.y / (Math.abs(directionVector.x) + Math.abs(directionVector.y));

  return {x: xProportion * speed, y: yProportion * speed};
}
