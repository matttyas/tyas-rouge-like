/*
    SceneManager.js

    Handles switching between scenes.
*/

function SceneManager(engine) {
    this.allScenes = [];
  this.sceneNames = [];
  this.transitionTimer = 0;
  this.newSceneName = '';
  this.engine = engine;
}

//Store all different scenes in game keyed on name.
SceneManager.prototype.buildSceneList = function(scenes) {
    for (var i = 0; i < scenes.length; i++) {
        this.allScenes[scenes[i].getName()] =  scenes[i];
    this.sceneNames.push(scenes[i].getName());
    }
};

//Scene state should be stored in allScenes due to byRef behaviour of js.
SceneManager.prototype.setCurrentScene = function( sceneName, transition) {
  if (transition) {
    this.transitionTimer = 60;
    this.newSceneName = sceneName;
    this.engine.globals.transition = true;
    if (this.engine.renderer) this.engine.renderer.startTransition(transition, this.transitionTimer);
    return;
  }
    this.engine.currentScene = this.allScenes[sceneName];
  
  //this.engine.audioPlayer.stopMusic();
  if (this.engine.renderer) this.engine.renderer.setCurrentScene(this.engine.currentScene);
};

SceneManager.prototype.getSceneNames = function() {
  return this.sceneNames;
};

SceneManager.prototype.getTransitionTimer = function() {
  return this.transitionTimer;
};

SceneManager.prototype.endTransition = function() {
  this.engine.globals.transition = false;
  this.transitionTimer = 0;
  this.setCurrentScene(this.newSceneName);
  this.newScene = '';
};

SceneManager.prototype.reduceTransitionTimer = function() {
  this.transitionTimer--;
  if (this.transitionTimer <= 0) this.endTransition();
}
