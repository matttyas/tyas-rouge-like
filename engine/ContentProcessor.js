var ContentProcessor = {};

ContentProcessor.processContentForActors = function(actors, content) {
  if (!actors || actors.constructor != Array || actors.length == 0) return null;
  for (var i = 0; i < actors.length; i++) {
    var actor = actors[i];
    var actorName = actor.getName();
    if (content[actorName]) {
      actor.processContent(content[actorName]);
    }
  }
};
