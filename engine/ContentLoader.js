var ContentLoader = {
  
};

ContentLoader.load = function(contentToLoad, contentOwner) {
  var loaded = 0;
  var content = {};

	if(contentToLoad.length == 0) loaded = 1;

	if (contentToLoad) {
    async.map(contentToLoad, function (item, callback) {
      try {
        var imageObj = new Image();
        imageObj.onload = function() {
          loaded += 1 / contentToLoad.length;
          content[item.name] = imageObj;
          callback(null, item.content);
        };
        imageObj.onerror = function() {
          console.log('failed to load: ' + item.content);
          callback('Load failure');
        };
      } catch (e) {
        console.log(e);
        console.log('Could not load' + item.name);
      }

      imageObj.src = item.content;

    }, function(err, results) {
      contentOwner.processContent(content);
    });
	} else {
		loaded = 1;
	}
};
