//Class representing a map in this here Rogue Like. It will hold tile data,
//and data detailing difficulty, style etc of the map. It converts a raw
//set of map data into a playable rogue map.

function RogueMap (mapData, engine, currentScene) {
  this.clearColor = 'rgb(0,0,0)'; //the color to clear the background of the renderer with
  this.mapData = mapData;
  this.engine = engine;
  this.currentScene = currentScene;
  this.entrances = [];
  this.doors = [];
  this.initialise();
  this.processedContent = false;
  this.adjacentMaps = [];
  this.projectiles = [];
};

RogueMap.prototype.initialise = function() {
  RogueMapBuilder.buildMap(this, this.mapData, this.currentScene, this.engine);
};

RogueMap.prototype.update = function() {
  for (var i = 0; i < this.projectiles.length; i++) {
    this.projectiles[i].update();
  }

  for (var i = 0; i < this.enemies.length; i++) {
    this.enemies[i].update();
  }
};

//Draws the map. Renders the tiles first
RogueMap.prototype.render = function (ctx, renderer) {
  renderer.setBackColor(this.clearColor);

  for (var i = 0; i < this.tiles.length; i++) {
    for (var j = 0; j < this.tiles[i].length; j++) {
      this.tiles[i][j].render(renderer);
    }
  }

  this.renderDecorations(renderer);
  this.renderEntrances(ctx, renderer);
  this.renderDoors(ctx, renderer);
  this.renderProjectiles(renderer);
  this.renderEnemies(renderer);
  if(DEBUG) this.debugRender(renderer);
};

RogueMap.prototype.renderEntrances = function(ctx, renderer) {
  for (var i = 0; i < this.entrances.length; i++) {
    this.entrances[i].render(renderer);
  }
};

RogueMap.prototype.renderDoors = function(ctx, renderer) {
  for (var i = 0; i < this.doors.length; i++) {
    this.doors[i].render(ctx, renderer);
  }
};

RogueMap.prototype.renderDecorations = function(renderer) {
  for (var i = 0; i < this.decorations.length; i++) {
    this.decorations[i].render(renderer);
  }
};

RogueMap.prototype.renderProjectiles = function(renderer) {
  for (var i = 0; i < this.projectiles.length; i++) {
    this.projectiles[i].render(renderer);
  }
};

RogueMap.prototype.renderEnemies = function(renderer) {
  for (var i = 0; i < this.enemies.length; i++) {
    this.enemies[i].render(renderer);
  }
};


RogueMap.prototype.debugRender = function(renderer) {
  for (var i = 0; i < this.rooms.length; i++) {
    this.rooms[i].render(renderer);
  };
};

RogueMap.prototype.debugRenderRoom = function(renderer, room) {

};

RogueMap.prototype.calculateRoomTopLeft = function(room) {

};

RogueMap.prototype.getEntrances = function() {
  return this.entrances;
};

RogueMap.prototype.getEntrance = function(classification) {
  if (!classification) return console.error("No classification for getCurrentMap");
  for (var i = 0; i < this.entrances.length; i++) {
    if (this.entrances[i].getClassification() == classification) {
      return this.entrances[i];
    }
  }
  return null;
};

RogueMap.prototype.getEntranceWithId = function(id) {
  for (var i = 0; i < this.entrances.length; i++) {
    var entrance = this.entrances[i];
    if (entrance.getId() == id) return entrance;
  }

  return null;
};

RogueMap.prototype.getCloseTiles = function(x, y, distance) {
  var leftMost = x - distance;
  if (leftMost < 0) leftMost = 0;

  var rightMost = x + distance;
  if (rightMost > this.tiles.length - 1) rightMost = this.tiles.length - 1;

  var upperMost = y - distance;
  if (upperMost < 0) upperMost = 0;

  var bottomMost = y + distance;
  if (bottomMost > this.tiles[0].length - 1) bottomMost = this.tiles[0].length - 1;

  var closeTiles = [];

  for (var y = upperMost; y <= bottomMost; y++) {
    for (var x = leftMost; x <= rightMost; x++) {
      closeTiles.push(this.tiles[x][y]);
    }
  }

  return closeTiles;
};

RogueMap.prototype.getInspectableObjects = function() {
  var inspectableObjects = [];
  inspectableObjects = inspectableObjects.concat(this.entrances);
  inspectableObjects = inspectableObjects.concat(this.doors);
  return inspectableObjects;
};

//Returns all objects the player could collide with that arent tiles
RogueMap.prototype.getColliderActors = function() {
  var colliderActors = [];
  colliderActors = colliderActors.concat(this.doors);
  return colliderActors;
};

RogueMap.prototype.setCurrentScene = function(currentScene) {
  this.currentScene = currentScene;
};

RogueMap.prototype.setId = function(id) {
  this.id = id;
};

RogueMap.prototype.getId = function() {
  return this.id;
};

RogueMap.prototype.getProcessedContent = function() {
  return this.processedContent;
};

RogueMap.prototype.getEnemies = function() {
  return this.enemies;
};

RogueMap.prototype.processContent = function(content) {
  ContentProcessor.processContentForActors(this.decorations, content);
  ContentProcessor.processContentForActors(this.entrances, content);
  ContentProcessor.processContentForActors(this.enemies, content);
  this.processContentForTiles(this.tiles, content);

  this.processedContent = true;
};

RogueMap.prototype.processContentForTiles = function(tiles, content) {
  if (!tiles || tiles.constructor != Array || tiles.length == 0) return null;
  for (var i = 0; i < tiles.length; i++) {
    ContentProcessor.processContentForActors(tiles[i], content);
  }
};

RogueMap.prototype.getAdjacentMaps = function() {
  if(this.adjacentMaps.length > 0) return this.adjacentMaps;

  this.adjacentMaps = this.buildAdjacentMaps();
  return this.adjacentMaps;
};

RogueMap.prototype.buildAdjacentMaps = function() {
  var mapIds = this.buildAdjacentMapIds();

  return this.currentScene.getRogueMapsFromIds(mapIds);
};

RogueMap.prototype.buildAdjacentMapIds = function() {
  var mapIds = [];
  for (var i = 0; i < this.entrances.length; i++) {
    mapIds.push(this.entrances[i].getTargetMapId());
  }

  return this.filterNonUniqueIds(mapIds);
};

RogueMap.prototype.filterNonUniqueIds = function(mapIds) {
  var uniqueIds = [];
  for (var i = 0; i < mapIds.length; i++) {
    if (uniqueIds.indexOf(mapIds[i]) == -1 ) uniqueIds.push(mapIds[i]);
  }

  return uniqueIds;
};

RogueMap.prototype.addProjectile = function(projectile) {
  this.projectiles.push(projectile);
};
