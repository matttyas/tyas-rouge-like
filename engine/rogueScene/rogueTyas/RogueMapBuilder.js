//Takes a Map and a new RogueMap and initialises all component parts of that RogueMap.

var RogueMapBuilder = {};

RogueMapBuilder.buildMap = function(rogueMap, mapData, rogueScene, engine) {

  rogueMap.tiles = RogueMapBuilder.initialiseTiles( mapData, engine);
  RogueMapBuilder.calculateAndSetBoundaries(rogueMap, engine);
  rogueMap.entrances = RogueMapBuilder.initialiseEntrances(mapData, rogueScene, engine);
  rogueMap.doors = RogueMapBuilder.initialiseDoors(mapData, engine);
  rogueMap.decorations = RogueMapBuilder.initialiseDecorations(mapData, engine);
  rogueMap.rooms = RogueMapBuilder.initialiseRooms(mapData, engine);
  rogueMap.enemies = RogueMapBuilder.initialiseEnemies(mapData, engine, rogueScene);
  rogueMap.id = mapData.id;
};

RogueMapBuilder.initialiseTiles = function(mapData, engine) {
  var tiles = [];

  for (var x = 0; x < mapData.tiles.length; x++) {
    var tileArray = [];
    for (var y = 0; y < mapData.tiles[x].length; y++) {
      //Will need to do a lot more interpretation here.
      var newTile = new RogueTile(x * engine.tileWidth, y * engine.tileHeight, engine.tileWidth, engine.tileHeight);
      newTile.buildRogueTile(mapData.tiles[x][y]);

      tileArray.push(newTile);
    }
    tiles.push(tileArray);
  }

  return tiles;
};

RogueMapBuilder.calculateAndSetBoundaries = function(rogueMap, engine) {
  rogueMap.left = 0;
  rogueMap.top = 0;
  rogueMap.bottom = rogueMap.tiles[0].length * engine.tileHeight;
  rogueMap.right = rogueMap.tiles.length * engine.tileWidth;
};

RogueMapBuilder.initialiseEntrances = function(mapData, rogueScene, engine) {
  var entrances = [];
  var entrancesData = mapData.getEntrances();
  for (var i = 0; i < entrancesData.length; i++) {
    entrances.push(RogueMapBuilder.buildEntrance(entrancesData[i], rogueScene, engine));
  }

  return entrances;
};

RogueMapBuilder.buildEntrance = function(entranceData, rogueScene, engine) {
  var entranceX = entranceData.getX() * engine.tileWidth;
  var entranceY = entranceData.getY() * engine.tileHeight;
  var newEntrance = {};

  if (entranceData.classification == 'Stairs Down') {
    newEntrance = new RogueStairsDown(entranceX, entranceY, entranceData);
  } else if (entranceData.classification == 'Stairs Up') {
    newEntrance = new RogueStairsUp(entranceX, entranceY, entranceData);
  } else {
    newEntrance = new RogueEntrance(entranceX, entranceY, entranceData);
  }

  newEntrance.setCurrentScene(rogueScene);
  return newEntrance;
};

RogueMapBuilder.initialiseDoors = function(mapData, engine) {
  var doors = [];
  var doorsData = mapData.getDoors();

  for (var i = 0; i < doorsData.length; i++) {
    doors.push(RogueMapBuilder.buildDoor(doorsData[i], engine));
  }

  return doors;
};

RogueMapBuilder.buildDoor = function(doorData, engine) {
  var doorPosition = RogueMapBuilder.calculateDoorPosition(doorData, engine);
  var direction = doorData.getDirection();
  if (direction == 'horizontal') return new RogueHorizontalDoor(doorPosition, doorData);
  if (direction == 'vertical') return new RogueVerticalDoor(doorPosition, doorData);
  return new RogueDoor(doorPosition, doorData);
};

//Returns a position halfway accross a tile on the axis the door is not on.
//RogueDoor will need to compensate by half its width for central door placement.
RogueMapBuilder.calculateDoorPosition = function(doorData, engine) {
  if (!doorData || !engine) return console.error('Missing Door Data or Engine in calculateDoorPosition');
  var direction = doorData.getDirection();
  var x = doorData.getX();
  var y = doorData.getY();

  var position = {x: 0, y: 0};

  if (direction == 'horizontal') {
    position.x = x * engine.tileWidth;
    position.y = (y * engine.tileHeight) + (engine.tileHeight / 2);
  } else if (direction == 'vertical') {
    position.x = (x * engine.tileWidth) + (engine.tileWidth / 2);
    position.y = y * engine.tileHeight;
  }

  return position;
};

RogueMapBuilder.initialiseDecorations = function(mapData, engine) {
  var decorations = [];
  var decorationsData = mapData.getDecorations();

  for (var i = 0; i < decorationsData.length; i++) {
    decorations.push(RogueMapBuilder.buildDecoration(decorationsData[i], engine));
  }

  return decorations;
};

RogueMapBuilder.buildDecoration = function(decorationData, engine) {
  var decorationPosition = RogueMapBuilder.calculateDecorationPosition(decorationData, engine);
  return new RogueDecoration(decorationPosition, decorationData);
};

RogueMapBuilder.calculateDecorationPosition = function(decorationData, engine) {
  var x = decorationData.getTileX();
  var y = decorationData.getTileY();

  var positionX = (x * engine.tileWidth);
  var positionY = (y * engine.tileHeight);

  return {x: positionX, y: positionY};
};

RogueMapBuilder.initialiseEnemies = function(mapData, engine, rogueScene) {
  var enemies = [];
  var enemiesData = mapData.getEnemies();

  for (var i = 0; i < enemiesData.length; i++) {
    enemies.push(RogueMapBuilder.buildEnemy(enemiesData[i], engine, rogueScene));
  }

  return enemies;
};

RogueMapBuilder.buildEnemy = function(enemyData, engine, rogueScene) {
  var enemyPosition = RogueMapBuilder.calculateEnemyPosition(enemyData, engine);
  var enemyClass = enemyData.getProperty('class');
  var enemy = new enemyClass(enemyPosition, enemyData);
  enemy.setFoes(rogueScene.getHeroes());
  return enemy;
};

RogueMapBuilder.calculateEnemyPosition = function(enemyData, engine) {
  var x = enemyData.getTileX();
  var y = enemyData.getTileY();

  var positionX = (x * engine.tileWidth);
  var positionY = (y * engine.tileHeight);

  return {x: positionX, y: positionY};
};

RogueMapBuilder.initialiseRooms = function(mapData, engine) {
  var rooms = [];
  var roomsData = mapData.getRooms();
  for (var i = 0; i < roomsData.length; i++) {
    rooms.push(RogueMapBuilder.buildRoom(roomsData[i], engine));
  }

  return rooms;
};

RogueMapBuilder.buildRoom = function(roomData, engine) {
  var roomPosition = RogueMapBuilder.calculateRoomPosition(roomData, engine);
  return new RogueRoom(roomPosition, roomData);
};

RogueMapBuilder.calculateRoomPosition = function(roomData, engine) {
  var x = roomData.getX() * engine.tileWidth;
  var y = roomData.getY() * engine.tileHeight;

  return {x: x, y: y};
};
