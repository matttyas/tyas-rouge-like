var EncounterGenerator = {};

EncounterGenerator.generateEncounters = function(map) {
  var encounters = [];
  var rooms = map.getRooms();
  if (rooms.length == 1) return encounters;
  var roomIndex = Math.round(Math.random() * (rooms.length - 1));
  if (roomIndex == 0) roomIndex = 1;
  encounters.push(EncounterGenerator.generateEncounter(rooms[roomIndex]));
  return encounters;
};

EncounterGenerator.generateEncounter = function(room) {
  var center = room.getRoundedCenter();
  var enemyPosition = {x: center.x, y: center.y + 1};
  var enemy = EnemyLibrary.getRandomNewEnemy(enemyPosition);
  return [enemy];
};
