//Room Content Generator will fetch the appropriate libraries of furniture / decorations
//and pass them to the FurnitureGenerator and the DecorationGenerator

//Room Content can be rougly split into furniture and decorations
//Furnityure may still be able to be moved or destroyed and will probably
//form the basis of many puzzles. But it should never be assumed they can be moved,
//unless the means to do this are provided

var RoomContentGenerator = {

};

RoomContentGenerator.generateAllRoomContent = function(map) {
  var rooms = map.getRooms();
  var furnitureGenerator = RoomContentGenerator.getFurnitureGenerator();
  var decorationGenerator = RoomContentGenerator.getDecorationGenerator();
  for (var i = 0; i < rooms.length; i++) {
    RoomContentGenerator.generateRoomContent(rooms[i], map, furnitureGenerator, decorationGenerator);
  }
};

RoomContentGenerator.generateRoomContent = function(room, map, furnitureGenerator, decorationGenerator) {
  var furnitureSet = RoomContentGenerator.getFurnitureSet();
  var decorationSet = RoomContentGenerator.getDecorationSet();

  room.furniture = furnitureGenerator.generateFurniture(room, map, furnitureSet);
  room.decorations = decorationGenerator.generateDecorations(room, map.getTiles(), decorationSet);

  map.furniture = map.furniture.concat(room.furniture);
  map.decorations = map.decorations.concat(room.decorations);
};

//These occupy space, so should be limited in number in the room
RoomContentGenerator.calculateMaximumFurniture = function(roomArea) {
  return Math.round(roomArea / 10);
};

//Will get a set of furniture from the library appropriate for a specific room
//For now it just returns an array of furniture objects
RoomContentGenerator.getFurnitureSet = function() {
  return [
    new LibraryContent(0, { name: "Chair"}),
    new LibraryContent(1, { name: "Book Shelf"}),
    new LibraryContent(2, { name: 'Crate'})
  ];
};

RoomContentGenerator.getDecorationSet = function() {
  return [
    new LibraryContent(0, { name: 'Red Rug', width: 2, height: 1, tags: ['floor', 'wide']}),
    new LibraryContent(0, { name: 'Red Rug Vertical', width: 1, height: 2, tags: ['floor', 'tall']}),
    new LibraryContent(1, { name: 'Blood Stain', width: 1, height: 1, tags: ['floor', 'randomOnly']}),
    new LibraryContent(1, { name: 'Cross', width: 1, height: 1, tags: ['floor']}),
    new LibraryContent(1, { name: 'Mosaic 1', width: 1, height: 1, tags: ['floor']}),
    new LibraryContent(1, { name: 'Grave', width: 1, height: 1.4, tags: ['floor']}),
    new LibraryContent(1, { name: 'Face', width: 1, height: 1, tags: ['floor']}),
    new LibraryContent(1, { name: 'Hour Glass Flag', width: 1, height: 1, tags: ['topWall']}),
    new LibraryContent(1, { name: 'Cell Window', width: 1, height: 1, tags: ['topWall']}),
    new LibraryContent(1, { name: 'Column', width: 1, height: 1.84, tags: ['topWall']}),
    new LibraryContent(1, { name: 'Drain', width: 1, height: 1, tags: ['topWall']}),

  ];
};

RoomContentGenerator.getFurnitureGenerator = function() {
  return FurnitureGenerator;
};

RoomContentGenerator.getDecorationGenerator = function() {
  return LayoutBasedDecorationGenerator;
};
