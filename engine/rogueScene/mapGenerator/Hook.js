function Hook (x, y, axis) {
  this.x = x;
  this.y = y;
  this.axis = axis;
  this.inUse = false; //Indicates if hook is used for corridor
};

Hook.prototype.getX = function() {
  return this.x;
};

Hook.prototype.getY = function() {
  return this.y;
};

//Returns the axis a corridor should be travelling on to attach to this.
Hook.prototype.getAxis = function() {
  return this.axis;
};

Hook.prototype.getPosition = function() {
  return {x: this.x, y: this.y};
};

Hook.prototype.getInUse = function() {
  return this.inUse;
};

Hook.prototype.setInUse = function(inUse) {
  this.inUse = inUse;
};
