var RoomGenerator = {
  NUMBERROOMS: 5
};

RoomGenerator.generateRooms = function(map) {
  var numberRooms = RoomGenerator.calculateNumberRooms();
  var rooms = [];
  var numberAttempts = 10; //Number of attempts to create a room that does not intersect with others. If hit then consider map full.

  for (var i = 0; i < numberRooms; i++) {
    var currentAttempts = 0;
    while (currentAttempts < numberAttempts) {
      var newRoom = RoomGenerator.generateRoom(map);
      newRoom.cleanHooks(map);

      if (RoomGenerator.checkRoomValid(newRoom, map, rooms)) {
        newRoom.setId(i);
        rooms.push(newRoom);
        break;
      }
      if (rooms.length > 0) currentAttempts += 1;
    }
  }

  return rooms;
};

//Determines how many rooms we will try and fit on this floor
RoomGenerator.calculateNumberRooms = function() {
  return RoomGenerator.NUMBERROOMS;
};

RoomGenerator.generateRoom = function(map) {
  var roomX = Math.round(Math.random() * map.getWidth());
  var roomY = Math.round(Math.random() * map.getHeight());
  var roomWidth = Math.round(Math.random() * MapGenerator.maxRoomWidth);
  var roomHeight = Math.round(Math.random() * MapGenerator.maxRoomHeight);

  if (roomWidth < 3) roomWidth = 3;
  if (roomHeight < 3) roomHeight = 3;
  //CHECK MATH FOR MIN - MAX BOUND OF RANDOM

  return new Room(roomX, roomY, roomWidth, roomHeight);
};

RoomGenerator.checkRoomValid = function(room, map, rooms) {
  if (RoomProcessor.checkRoomsCollision(rooms, room)) return false;
  if (room.getHooks().length == 0) return false;
  if (!RoomGenerator.checkRoomInBoundaries(room, map)) return false;

  return true;
};

RoomGenerator.checkRoomInBoundaries = function(room, map) {

  //console.log(map);
  var boundaries = [
    {x: 0, y: 0, width: map.width, height: 2}, //top
    {x: 0, y: 0, width: 2, height: map.height}, //left
    {x: 0, y: map.height - 1, width: map.width, height: 2}, //bottom
    {x: map.width - 1, y: 0, width: 2, height: map.height} //right
  ];

  for (var i = 0; i < boundaries.length; i++) {
    if (Physics.checkCollisionRects(room, boundaries[i]).collision) return false;
  }

  return true;
};
