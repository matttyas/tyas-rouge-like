var LayoutBasedDecorationGenerator = {

};

LayoutBasedDecorationGenerator.generateDecorations = function(room, tiles, decorationSet) {
  var decorations = [];
  var layout = new Layout();
  layout.pickDecorations(decorationSet);
  decorations = layout.generateDecorations(room, tiles);
  if (DEBUG) room.setPatterns(layout.getPatterns());
  return decorations;
};
