var DoorGenerator = {};

DoorGenerator.generateDoors = function(map) {
  var doors = [];
  var corridors = map.getCorridors();

  if(!corridors || corridors.length == 0) return;

  for (var i = 0; i < corridors.length; i++) {
    var corridor = corridors[i];
    var newStartDoor = DoorGenerator.generateDoor(corridor.startHook, map);
    if (newStartDoor) DoorGenerator.checkForDuplicateAndPush(doors, newStartDoor);

    var newEndDoor = DoorGenerator.generateDoor(corridor.endHook, map);
    if (newEndDoor) DoorGenerator.checkForDuplicateAndPush(doors, newEndDoor);
  }

  map.setDoors(doors);
};

DoorGenerator.generateDoor = function(position, map) {
  if (!map.checkTilePassable(position.x, position.y)) return null;
  var direction = DoorGenerator.decideHorizontalOrVertical(position, map);
  if(!direction) return null;

  return new Door(position, direction);
};

DoorGenerator.decideHorizontalOrVertical = function(position, map) {
  var passableStates = DoorGenerator.getPassableTileStates(position, map);

  if (DoorGenerator.checkDoorShouldBeHorizontal(passableStates)) {
    return 'horizontal';
  }

  if (DoorGenerator.checkDoorShouldBeVertical(passableStates)) {
    return 'vertical';
  }

  return null;
};

DoorGenerator.getPassableTileStates = function(position, map) {
  var passableStates = {};

  passableStates.top  = map.checkTilePassable(position.x, position.y - 1);
  passableStates.left = map.checkTilePassable(position.x - 1, position.y);
  passableStates.bottom = map.checkTilePassable(position.x, position.y + 1);
  passableStates.right = map.checkTilePassable(position.x + 1, position.y);

  return passableStates;
};

DoorGenerator.checkDoorShouldBeHorizontal = function(passableStates) {
  return (passableStates['top'] && passableStates['bottom'] && !passableStates['left'] && !passableStates['right']);
};

DoorGenerator.checkDoorShouldBeVertical = function(passableStates) {
  return (!passableStates['top'] && !passableStates['bottom'] && passableStates['left'] && passableStates['right']);
};

DoorGenerator.checkForDuplicateAndPush = function(doors, door) {
  if(!door.position) return console.error('Door must have a position in checkForDuplicateAndPush');
  for (var i = 0; i < doors.length; i++) {
    var currentDoor = doors[i];
    if (door.position.x == currentDoor.position.x && door.position.y == currentDoor.position.y) return;
  }

  doors.push(door);
};
