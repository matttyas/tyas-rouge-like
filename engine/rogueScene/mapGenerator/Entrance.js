function Entrance(x, y, classification) {
  this.x = x; //Which tile this entrance is on
  this.y = y;
  this.targetMapId = null;
  this.targetEntranceId = null;
  this.classification = classification; //The type of entrance  = this is. Stairs Up / Down / Mystical Portal to the stygian depths
};

Entrance.prototype.getX = function() {
  return this.x;
};

Entrance.prototype.getY = function() {
  return this.y;
};

Entrance.prototype.getClassification = function() {
  return this.classification;
};

Entrance.prototype.getId = function() {
  return this.id;
};

Entrance.prototype.setId = function(id) {
  this.id = id;
};

Entrance.prototype.setTargetMapId = function(targetMapId) {
  this.targetMapId = targetMapId;
};

Entrance.prototype.getTargetMapId = function() {
  return this.targetMapId;
};

Entrance.prototype.setTargetEntranceId = function(targetEntranceId) {
  this.targetEntranceId = targetEntranceId;
};

Entrance.prototype.getTargetEntranceId = function() {
  return this.targetEntranceId;
};
