function Door (position, direction) {
  this.initialise(position, direction);
}

Door.prototype.initialise = function(position, direction) {
  this.position = position;
  this.direction = direction;
};

Door.prototype.getDirection = function() {
  return this.direction;
};

Door.prototype.getX = function() {
  return this.position.x;
};

Door.prototype.getY = function() {
  return this.position.y;
};
