//Represents tile data. Not an actor. No render information just the properties
//of a tile
function Enemy (libraryContent, position) {
  if (position) {
    this.tileX = position.x;
    this.tileY = position.y;
  }

  this.properties = {};
  if (libraryContent) this.initialise(libraryContent);
};

Enemy.prototype.initialise = function(libraryContent) {
  this.properties = libraryContent.properties;
  this.properties['passable'] = false;
}

Enemy.prototype.setProperty = function(key, value) {
  this.properties[key] = value;
};

Enemy.prototype.getProperty = function(property) {
  return this.properties[property];
};

Enemy.prototype.getProperties = function() {
  return this.properties;
};

Enemy.prototype.getTileX = function() {
  return this.tileX;
};

Enemy.prototype.getTileY = function() {
  return this.tileY;
};

Enemy.prototype.setPosition = function(position) {
  this.tileX = position.x;
  this.tileY = position.y;
};

Enemy.prototype.generateNewInstance = function(position) {
  return new Enemy(this, position);
};
