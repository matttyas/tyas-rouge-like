function Corridor(startHook, endHook, sections) {
  this.startHook = startHook;
  this.endHook = endHook;
  this.sections = sections //Section is a straight line of tiles
};

Corridor.prototype.addSection = function(startPosition, endPosition, axis) {
  if (!startPosition.x || !startPosition.y || !endPosition.x || !endPosition.y || !axis) return console.error('Invalid Params in addSection');
  this.sections.push({start: startPosition, end:endPosition, axis: axis});
};

Corridor.prototype.getSections = function() {
  return this.sections;
};

Corridor.prototype.getStartPosition = function() {
  return {x: this.startHook.getX(), y: this.startHook.getY()};
};

Corridor.prototype.getEndPosition = function() {
  return {x: this.endHook.getX(), y: this.endHook.getY()};
};