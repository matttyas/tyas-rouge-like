function Room(x, y, width, height) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;

  this.center = {};
  this.center.x = this.x + (this.width / 2);
  this.center.y = this.y + (this.height / 2);

  //Array of points at which a corridor / room may be attached
  this.hooks = [];

  this.generateHooks();

  this.id = null;
};

Room.prototype.getX = function () {
  return this.x;
};

Room.prototype.getY = function () {
  return this.y;
};

Room.prototype.getHeight = function() {
  return this.height;
};

Room.prototype.getWidth = function () {
  return this.width;
};

Room.prototype.getArea = function() {
  if (!this.area) this.area = this.width * this.height;
  return this.area;
};

Room.prototype.getCenter = function() {
  return this.center;
};

Room.prototype.getRoundedCenter = function() {
  return {x: Math.floor(this.center.x), y: Math.floor(this.center.y)};
};

Room.prototype.setId = function(id) {
  this.id = id;
};

Room.prototype.getId = function() {
  return this.id;
};

//This method will add the points at which a room can have corridors / doors attached to it
//For now it's just the center points on each of the foor walls
Room.prototype.generateHooks = function() {
  //Top wall
  this.hooks.push( new Hook (Math.floor(this.center.x), this.y - 1, 'vertical'));
  //Left Wall
  this.hooks.push(new Hook (this.x - 1, Math.floor(this.center.y), 'horizontal'));
  //Bottom Wall
  this.hooks.push(new Hook (Math.floor(this.center.x), this.y + this.height, 'vertical'));
  //Right Wall
  this.hooks.push(new Hook (this.x + this.width, Math.floor(this.center.y), 'horizontal'));

};

Room.prototype.getHooks = function() {
  return this.hooks;
};

//Removes any hooks that are outside the map
Room.prototype.cleanHooks = function(map) {
  var tiles = map.getTiles();
  var cleanHooks = [];
  for (var i = 0; i < this.hooks.length; i++) {
    var hookX = this.hooks[i].getX();
    var hookY = this.hooks[i].getY();
    if (hookX < 0 || hookX > tiles.length) continue;
    if (hookY < 0 || hookY > tiles[0].length) continue;
    cleanHooks.push(this.hooks[i]);
  }

  if(cleanHooks.length == 0) {
    var a = 1;
  }

  this.hooks = cleanHooks;
};

//Goes through the hooks and returns any that are marked as inUse
Room.prototype.getExits = function() {
  var exits = [];
  for (var i = 0; i < this.hooks.length; i++) {
    if (this.hooks[i].getInUse()) exits.push(this.hooks[i]);
  }
  return exits;
};

//Should return all tiles in a room that have the classification Top Vertical Wall
Room.prototype.getTopWallPositions = function(tiles) {
  var topWallPositions = [];
  var right = this.x + this.width;
  var j = this.y - 1; //The wall above the top of the room.
  for (var i = this.x; i < right; i++) {
    if (tiles[i] && tiles[i][j]) topWallPositions.push({x: i, y: j});
  }

  return topWallPositions;
};

Room.prototype.getTopWallTiles = function(tiles) {
  var topWallTiles = [];
  var right = this.x + this.width;
  var j = this.y - 1; //The wall above the top of the room.
  for (var i = this.x; i < right; i++) {
    if (tiles[i] && tiles[i][j]) topWallTiles.push(tiles[i][j]);
  }

  return topWallTiles;
};

Room.prototype.setPatterns = function(patterns) {
  this.patterns = patterns;
};
