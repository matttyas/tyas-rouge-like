var TileClassifier = {

};

TileClassifier.classifyImpassableTiles = function(map, secondPass) {
  var tiles = map.getTiles();
  for (var i = 0; i < tiles.length; i++) {
    for (var j = 0; j < tiles[i].length; j++) {
      if (!tiles[i][j].getProperty('passable')) {
        if (secondPass) {
          TileClassifier.secondPassClassifyImpassableTile(tiles, i, j);
        } else {
          TileClassifier.classifyImpassableTile(tiles, i, j);
        }

      }
    }
  }
};

TileClassifier.classifyImpassableTile = function(tiles, x, y) {
  var tile = tiles[x][y];
  if (!tile || tile.getProperty('passable')) return console.error('Tile to check does not exist or is not impassable');

  var leftImpassable = TileClassifier.checkIfTileImpassable(tiles, x - 1, y);
  var topImpassable = TileClassifier.checkIfTileImpassable(tiles, x, y - 1);
  var rightImpassable = TileClassifier.checkIfTileImpassable(tiles, x + 1, y);
  var bottomImpassable = TileClassifier.checkIfTileImpassable(tiles, x, y + 1);

  var impassableData = {
    leftImpassable: leftImpassable,
    topImpassable: topImpassable,
    rightImpassable: rightImpassable,
    bottomImpassable: bottomImpassable
  };

  var classification = TileClassifier.calculateClassification(impassableData);
  tile.setProperty('classification', classification);
};

TileClassifier.checkIfTileImpassable = function(tiles, i, j) {
  if (j < 0) return true;
  if (i < 0) return true;
  if (j >= tiles.length) return true;
  if (i >= tiles[0].length) return true;

  return !(tiles[i][j].getProperty('passable'));
};

TileClassifier.calculateClassification = function(impassableData) {

  if (!impassableData.leftImpassable && !impassableData.topImpassable && !impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Pillar';
  if (!impassableData.rightImpassable && !impassableData.leftImpassable && impassableData.topImpassable && impassableData.bottomImpassable) return 'Vertical Wall';
  if (!impassableData.topImpassable && !impassableData.bottomImpassable && impassableData.leftImpassable && impassableData.rightImpassable) return 'Horizontal Wall';

  //Room Walls

  if (impassableData.leftImpassable && impassableData.topImpassable && !impassableData.rightImpassable && impassableData.bottomImpassable) return 'Left Wall';
  if (impassableData.leftImpassable && impassableData.topImpassable && impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Top Wall Tall';
  if (!impassableData.leftImpassable && impassableData.topImpassable && impassableData.rightImpassable && impassableData.bottomImpassable) return 'Right Wall';
  if (impassableData.leftImpassable && !impassableData.topImpassable && impassableData.rightImpassable && impassableData.bottomImpassable) return 'Bottom Wall';

  //Ends of narrow walls

  if (!impassableData.leftImpassable && !impassableData.topImpassable && !impassableData.rightImpassable && impassableData.bottomImpassable) return 'Top Vertical Wall';
  if (!impassableData.leftImpassable && impassableData.topImpassable && !impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Bottom Vertical Wall Tall';
  if (impassableData.leftImpassable && !impassableData.topImpassable && !impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Right Horizontal Wall';
  if (!impassableData.leftImpassable && !impassableData.topImpassable && impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Left Horizontal Wall';

  //Corners of blocks

  if (!impassableData.leftImpassable && impassableData.topImpassable && impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Bottom Left Tall';
  if (impassableData.leftImpassable && impassableData.topImpassable && !impassableData.rightImpassable && !impassableData.bottomImpassable) return 'Bottom Right Tall';
  if (impassableData.leftImpassable && !impassableData.topImpassable && !impassableData.rightImpassable && impassableData.bottomImpassable) return 'Top Right';
  if (!impassableData.leftImpassable && !impassableData.topImpassable && impassableData.rightImpassable && impassableData.bottomImpassable) return 'Top Left';

};

TileClassifier.secondPassClassifyImpassableTile = function(tiles, x, y) {
  if(tiles[x][y].getProperty('classification')) {
    return;
  }

  var leftClassification = TileClassifier.getClassification(tiles, x-1, y);
  
};

TileClassifier.secondPassCalculateClassification = function(classifcationData) {

};

TileClassifier.getClassification = function() {

};
