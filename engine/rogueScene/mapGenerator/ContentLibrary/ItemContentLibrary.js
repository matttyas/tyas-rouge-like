var ItemContentLibrary = {};

ItemContentLibrary.getContentData = function() {

};

ItemContentLibrary.initialise = function() {
  this.generateRogueItems();
}

//The raw data. No gameplay information, no content paths.
ItemContentLibrary.getItemLibraryData = function() {
  return [
    new LibraryContent( 0, {name: 'Throwing Knife', type: 'Projectile', class: Projectile})
  ];
};

//Gets the data need for items during map generation
ItemContentLibrary.getItems = function() {
  if (ItemContentLibrary.items) return ItemContentLibrary.items;
  ItemContentLibrary.items = ItemContentLibrary.generateItems();
  return ItemContentLibrary.items;
};

ItemContentLibrary.generateItems = function() {
  var itemLibraryData = ItemContentLibrary.getItemLibraryData();
  var items = [];

  for (var i = 0; i < itemLibraryData.length; i++) {
    items.push(new Item(itemLibraryData[i].properties));
  }

  return items;
};

ItemContentLibrary.getRogueItems = function() {
  if (ItemContentLibrary.rogueItems) return ItemContentLibrary.rogueItems;
  ItemContentLibrary.rogueItems = ItemContentLibrary.generateRogueItems();
  return ItemContentLibrary.rogueItems;
};

//Returns the full RogueItems that can be used in game.
ItemContentLibrary.generateRogueItems = function() {
  var rogueItems = [];
  var items = ItemContentLibrary.getItems();
  for (var i = 0; i < items.length; i++) {
    rogueItems.push(new RogueItem(items[i]));
  }

  ItemContentLibrary.generateRogueItemsLib(rogueItems);

  return rogueItems;
};

//Generates an object with a reference to each rogueItem on it. For easy retrieval of items
ItemContentLibrary.generateRogueItemsLib = function(rogueItems) {
  var rogueItemsLib = {};
  for (var i = 0; i < rogueItems.length; i++) {
    rogueItemsLib[rogueItems[i].getName()] = rogueItems[i];
  }

  ItemContentLibrary.rogueItemsLib = rogueItemsLib;
}

ItemContentLibrary.processContent = function(content) {
  for (var i = 0; i < ItemContentLibrary.rogueItems.length; i++) {
    var rogueItem = ItemContentLibrary.rogueItems[i];
    var rogueItemName = rogueItem.getName();
    if(content[rogueItemName]) {
      rogueItem.processContent(content[rogueItemName]);
    }
  }
};

ItemContentLibrary.getItem = function(itemName) {
  var rogueItem = ItemContentLibrary.rogueItemsLib[itemName];
  if (!rogueItem) return console.error('Item: ' + itemName + ' does not exist in database');
  return rogueItem;
};
