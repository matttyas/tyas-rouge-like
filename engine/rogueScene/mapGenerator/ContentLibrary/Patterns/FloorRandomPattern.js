function FloorRandomPattern () {
  this.addSelector(['floor', 'randomOnly'], [], 2, 2);
  this.name = 'Floor Random Pattern';
}

FloorRandomPattern.prototype = new Pattern();

FloorRandomPattern.prototype.generateDecorations = function(room, tiles) {
  return DecorationGenerator.generateDecorations(room, tiles, this.decorations);
};
