function FloorCentralYPattern () {
  this.addSelector(['floor'], ['randomOnly'], 1, 2);
  this.name = "Floor Central Y Pattern";
}

FloorCentralYPattern.prototype = new Pattern();

FloorCentralYPattern.prototype.generateDecorations = function(room) {
  var generatedDecorations = [];

  var decorationHeight = this.decorations[0].getProperty('height');
  var roomHeight = room.getHeight();
  var topRoom = room.getY();
  var distanceBetweenDecorations = 1;
  var center = room.getRoundedCenter();
  var newX = center.x;
  var newY = topRoom;

  while(newY <= (topRoom + roomHeight) - decorationHeight) {
    generatedDecorations.push(new Decoration(this.decorations[0], {x: newX, y: newY}));
    newY += distanceBetweenDecorations + decorationHeight;
  }

  return generatedDecorations;
};
