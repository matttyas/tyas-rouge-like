function FloorAllEntrancesPattern () {
  this.addSelector(['floor', 'wide'], [], 2, 1);
  this.addSelector(['floor', 'tall'], [], 1, 2);
  this.name = 'Floor All Entrances Pattern';
}

FloorAllEntrancesPattern.prototype = new Pattern();

FloorAllEntrancesPattern.prototype.generateDecorations = function(room) {
  var generatedDecorations = [];

  var exits = room.getExits();
  for (var i = 0; i < exits.length; i++) {
    var newPosition = exits[i].getPosition();

    if (exits[i].getAxis() == 'vertical') {
      generatedDecorations.push(new Decoration(this.decorations[1], newPosition));
    } else {
      generatedDecorations.push(new Decoration(this.decorations[0], newPosition));
    }
  }

  return generatedDecorations;
  //return new Decoration(this.decorations[0], room.getCenter());
};
