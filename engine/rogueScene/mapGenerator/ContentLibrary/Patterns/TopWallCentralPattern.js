function TopWallCentralPattern () {
  this.addSelector(['topWall'], [], 3, 2);
  this.name = 'Top Wall Central Pattern';
}

TopWallCentralPattern.prototype = new Pattern();

TopWallCentralPattern.prototype.generateDecorations = function(room, tiles) {
  var topWallPositions = room.getTopWallPositions(tiles);
  var topWallTiles = room.getTopWallTiles(tiles);
  var centralIndex = Math.round((topWallPositions.length - 1) / 2);
  var centralPosition = topWallPositions[centralIndex];

  if(topWallPositions.length == 0 || topWallTiles.length == 0) return []; //Top wall out of bounds
  if(topWallTiles[centralIndex].getProperty('passable') == true) return []; //Center is passable so can't hang a decoration on it!
  if(this.decorations[0].getProperty('height') > 1) centralPosition.y -= (this.decorations[0].getProperty('height') - 1);
  return [ new Decoration(this.decorations[0], centralPosition )];
};
