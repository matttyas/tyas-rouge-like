function Layout () {
  this.initialise();
};

Layout.prototype.initialise = function() {
  this.patterns = [];
  var numberPatterns = Math.round(Math.random() * 2);

  for (var i = 0; i < numberPatterns; i++) {
    var pattern = PatternLibrary.pickRandomPattern();
    this.patterns.push(pattern);
  }
};

//Gets the selection criteria for each decoration slot in each pattern
Layout.prototype.pickDecorations = function(decorationSet) {
  for (var i = 0; i < this.patterns.length; i++) {
    this.patterns[i].pickSuitableDecorations(decorationSet);
  }
};

Layout.prototype.getRequiredSelectors = function() {
  return this.requiredSelectors;
};

Layout.prototype.generateDecorations = function(room, tiles) {
  var decorations = [];
  for (var i = 0; i < this.patterns.length; i++) {
    decorations = decorations.concat(this.patterns[i].generateDecorations(room, tiles));
  }

  return decorations;
};

Layout.prototype.getPatterns = function() {
  return this.patterns;
};
