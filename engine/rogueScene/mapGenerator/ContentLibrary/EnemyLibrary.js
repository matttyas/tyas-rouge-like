var EnemyLibrary = {};

EnemyLibrary.getContentData = function() {

};

EnemyLibrary.initialise = function() {
  this.getEnemies();
}

//The raw data. No gameplay information, no content paths.
EnemyLibrary.getEnemyLibraryData = function() {
  return [
    new LibraryContent( 0, {name: 'Bat', class: Bat})
  ];
};

//Gets the data need for enemies during map generation
EnemyLibrary.getEnemies = function() {
  if (EnemyLibrary.enemies) return EnemyLibrary.enemies;
  EnemyLibrary.enemies = EnemyLibrary.generateEnemies();
  return EnemyLibrary.enemies;
};

EnemyLibrary.generateEnemies = function() {
  var enemyLibraryData = EnemyLibrary.getEnemyLibraryData();
  var enemies = [];

  for (var i = 0; i < enemyLibraryData.length; i++) {
    enemies.push(new Enemy(enemyLibraryData[i]));
  }

  return enemies;
};

EnemyLibrary.getEnemy = function(enemyName) {
  var rogueEnemy = EnemyLibrary.rogueEnemiesLib[enemyName];
  if (!rogueEnemy) return console.error('Enemy: ' + enemyName + ' does not exist in database');
  return rogueEnemy;
};

EnemyLibrary.getRandomNewEnemy = function(position) {
  var index = Math.round(Math.random() * (EnemyLibrary.enemies.length - 1));
  return EnemyLibrary.enemies[index].generateNewInstance(position);
};
