/*
  Writing this filter to be effecient will probably be one of the harder challenges
  of this project. Now fortunately this is limited to generation which means there
  is less of a time pressure. That's no excuse for shoddy coding but it means I'm
  not in huge rush to get it perfect.

  That said I would like to return to this at a future date and give this another
  go. For now I'm just going to get it working.


  FORMAT FOR SELECTOR:

  // var selector = {
  //   must: ['array', 'of', 'necessary', 'strings'],
  //   mustNot: ['array', 'of', 'undesired', 'strings'],
  //   maxWidth: 5,
  //   maxHeight: 3
  // });

*/

var GreatFilter = {

};

GreatFilter.pick = function(selector, objects) {
  var checkOrder = GreatFilter.generateCheckOrder(objects);

  for(var i = 0; i < checkOrder.length; i++) {
    var checkIndex = checkOrder[i];
    if (GreatFilter.checkObjectMatchesSelector(selector, objects[checkIndex].properties)) {
      return objects[checkIndex];
    }
  }

  return null;
};

GreatFilter.generateCheckOrder = function(objects) {
  var checkOrder = [];
  var i = 0;
  while (checkOrder.length < objects.length) {
    checkOrder.push(i);


    if (i >= 2) {
      var index = Math.round(Math.random() * (checkOrder.length - 1));
      var tempStore = checkOrder[index];
      checkOrder[index] = checkOrder[i];
      checkOrder[i] = tempStore;
    }
    i++;
  }

  return checkOrder;
};

GreatFilter.checkObjectMatchesSelector = function(selector, object) {
  if (selector.maxWidth < object.width) return false;
  if (selector.maxHeight < object.height) return false;

  if(!object.tags) return false; //No tags then object is never used.

  var mustTagsPresent = 0;
  for (var i = 0; i < object.tags.length; i++) {
    if(selector.must && selector.must.indexOf(object.tags[i]) > -1) mustTagsPresent += 1;
    if(selector.mustNot && selector.mustNot.indexOf(object.tags[i]) > -1) return false;
  }

  if (selector.must && mustTagsPresent != selector.must.length) return false;
  return true;
};
