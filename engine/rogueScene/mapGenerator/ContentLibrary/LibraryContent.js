//This represents an entry in the content library.
//It provides raw data from which a MapGenerator Object can be generated.
//It contains no map based data, and will simply be a set of strings and booleans
//Denoting property values

function LibraryContent (id, properties) {
  this.id = id;
  this.properties = properties;
};

LibraryContent.prototype.getProperty = function(property) {
  return this.properties[property];
};

LibraryContent.prototype.getProperties = function() {
  return this.properties;
};

LibraryContent.prototype.getId = function() {
  return this.id;
};

LibraryContent.prototype.getName = function() {
  return this.properties[name];
};
