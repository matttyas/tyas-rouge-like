function Pattern () {
  this.selectors = [];
  this.decorations = [];
  this.lockX = false;
  this.lockY = false;
};

Pattern.prototype.getSelectors = function() {
  return this.selectors;
};

Pattern.prototype.addSelector = function(must, mustNot, maxWidth, maxHeight) {
  this.selectors.push({
    must: must,
    mustNot: mustNot,
    maxWidth: maxWidth,
    maxHeight: maxHeight
  });
};


Pattern.prototype.pickSuitableDecorations = function(decorationSet) {
  this.decorations = [];
  for (var i = 0; i < this.selectors.length; i++) {
    this.decorations.push(GreatFilter.pick(this.selectors[i], decorationSet));
  }
};

Pattern.prototype.getName = function() {
  return this.name || 'Unnamed Pattern';
};

Pattern.prototype.generateDecorations = function() {
  return [];
};
