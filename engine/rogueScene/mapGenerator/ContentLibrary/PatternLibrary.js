var PatternLibrary = {

};

PatternLibrary.initialise = function() {
  PatternLibrary.patterns = [];

  var topWallCentralPattern = new TopWallCentralPattern();
  PatternLibrary.patterns.push(topWallCentralPattern);

  var floorCentralYPattern = new FloorCentralYPattern();
  PatternLibrary.patterns.push(floorCentralYPattern);

  var floorAllEntrancesPattern = new FloorAllEntrancesPattern();
  PatternLibrary.patterns.push(floorAllEntrancesPattern);

  var floorRandomPattern = new FloorRandomPattern();
  PatternLibrary.patterns.push(floorRandomPattern);
};

PatternLibrary.pickRandomPattern = function() {
  var index = Math.round(Math.random() * (PatternLibrary.patterns.length - 1));
  return PatternLibrary.patterns[index];
};
