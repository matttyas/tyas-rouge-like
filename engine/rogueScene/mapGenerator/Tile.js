//Represents tile data. Not an actor. No render information just the properties
//of a tile
function Tile () {
  this.properties = {
    passable: false
  }

  this.label = '';
};

Tile.prototype.setProperty = function(key, value) {
  this.properties[key] = value;
};

Tile.prototype.getProperty = function(property) {
  return this.properties[property];
};

Tile.prototype.getProperties = function() {
  return this.properties;
};

Tile.prototype.setLabel = function(label) {
  this.label = label;
};

Tile.prototype.getLabel = function () {
  return this.label;
};
