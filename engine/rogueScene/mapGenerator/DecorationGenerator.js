var DecorationGenerator = {

};

DecorationGenerator.generateDecorations = function(room, tiles, decorationSet) {
  var decorations = [];
  var numberDecorations = DecorationGenerator.calculateNumberDecorations(room);
  for (var i = 0; i < numberDecorations; i++) {
    var decoration = DecorationGenerator.generateDecoration(room, decorationSet);
    decorations.push(decoration);
  }
  return decorations;
};

DecorationGenerator.calculateNumberDecorations = function(room) {
  var roomArea = room.getArea();
  var miniumDecorations = DecorationGenerator.calculateMinimumDecorations(roomArea);
  var maximumDecorations = DecorationGenerator.calculateMaximumDecorations(roomArea);
  var numberDecorations = Math.round(Math.random() * maximumDecorations);
  if (numberDecorations < miniumDecorations) numberDecorations = miniumDecorations;

  return numberDecorations;
};

//Should probably limit the number of decorations. Perhaps. Hmm.
DecorationGenerator.calculateMaximumDecorations = function(roomArea) {
  return Math.round((roomArea / 10) + 2);
};

//Not all rooms need decorations.
DecorationGenerator.calculateMinimumDecorations = function() {
  return 0;
};

DecorationGenerator.generateDecoration = function(room, decorationSet) {
  var decorationContent = DecorationGenerator.pickRandomDecorationContent(decorationSet);
  var position = DecorationGenerator.calculatePositionForDecoration(room, decorationContent);
  return DecorationGenerator.buildDecoration(decorationContent, position);
};

DecorationGenerator.pickRandomDecorationContent = function(decorationSet) {
  var index = Math.round(Math.random() * (decorationSet.length - 1));
  return decorationSet[index];
};

DecorationGenerator.calculatePositionForDecoration = function(room, decorationContent) {
  var x = room.getX() + Math.round(Math.random() * (room.getWidth() - decorationContent.getProperty('width')));
  var y = room.getY() + Math.round(Math.random() * (room.getHeight() - decorationContent.getProperty('height')));

  return {x: x, y: y};
};

DecorationGenerator.checkPositionValid = function(room, decorationContent, position) {

}

DecorationGenerator.buildDecoration = function(decorationContent, position) {
  return new Decoration(decorationContent, position);
};
