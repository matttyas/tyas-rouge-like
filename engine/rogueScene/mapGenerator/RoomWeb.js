//A structure that holds objects linked by proximity.
//It should be possible to ascertain whether a room can be reached by another room by travelling along the paths between rooms
function RoomWeb () {
  this.nodes = {}; //Both indexed on id
  this.paths = [];
};

RoomWeb.prototype.addNode = function(node) {
  this.nodes[node.id] = {
    node: node
  }
};

RoomWeb.prototype.addPathsFromNode = function(nodeId, connectNodes) {
  for (var i = 0; i < connectNodes.length; i++) {
    if (this.checkPathExists(nodeId, connectNodes[i].getId())) continue;

    this.addPath(nodeId, connectNodes[i].getId());
  }
};

RoomWeb.prototype.addPath = function(id1, id2) {
  this.paths.push({start: id1, end: id2});
};

RoomWeb.prototype.checkPathExists = function(id1, id2) {
  var pathsForId1 = this.getPathsForId(id1);
  for (var i = 0; i < pathsForId1.length; i++) {
    if (pathsForId1.start == id2 || pathsForId1.end == id2) return true;
  }

  return false;
};

RoomWeb.prototype.getPaths = function() {
  return this.paths;
};

RoomWeb.prototype.getPathsForId = function(id) {
  var pathsForId = [];
  for (var i = 0; i < this.paths.length; i++) {
    if (this.paths[i].start == id || this.paths[i].end == id) pathsForId.push(this.paths[i]);
  }

  return pathsForId;
};

//Mostly used for debugging specific corridor generation
RoomWeb.prototype.getPath = function(start, end) {
  for (var i = 0; i < this.paths.length; i++) {
    if (this.paths[i].start == start && this.paths[i].end == end) return this.paths[i];
  }

  return null;
};
