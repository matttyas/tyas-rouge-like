//Room Processor will contain the methods needed to ensure good room placement
//and connectivity via corridors. For now it will be also contain non generator
//specific methods although these may be moved later to make for easier
//use of different map generators.

var RoomProcessor = {};

//Returns true if the checkRoom collides with any other room AND that there is
//at least a one tile gap between rooms.

RoomProcessor.checkRoomsCollision = function(rooms, checkRoom) {
  var roomArea = {
    x: checkRoom.x - 1,
    y: checkRoom.y - 1,
    height: checkRoom.height + 2,
    width: checkRoom.width +2
  };

  if (!rooms || rooms.length == 0) return false;

  for (var i = 0; i < rooms.length; i++) {
    var collision = Physics.checkCollisionRects(rooms[i], roomArea);
    if (collision.collision) {
      return true;
    }
  }

  return false;
};

//Returns the n closest rooms to the selected room
RoomProcessor.getClosestRooms = function(rooms, checkRoom, n) {
  if (!checkRoom || !rooms || rooms.length == 0 || n <= 0) console.error("Invalid params for getClosestRooms");
  var copyOfRooms = rooms.slice();

  copyOfRooms.sort(function (roomA, roomB) {
    var checkRoomCenter = checkRoom.getCenter();
    return Physics.distanceBetweenPoints(roomA.getCenter(), checkRoomCenter) - Physics.distanceBetweenPoints(roomB.getCenter(), checkRoomCenter);
  });

  //Closest room is always this room
  return copyOfRooms.slice(1, n + 1);
};