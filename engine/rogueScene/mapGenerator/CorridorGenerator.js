var CorridorGenerator = {

}

//Builds corridors between webs as detailed in the paths object on the roomweb
//Then puts the corridors on the map
//Sections generated in a straightest line priciple. No winding paths here. May need to change that.

CorridorGenerator.generateCorridors = function(map, roomWeb) {
  var rooms = map.rooms;
  var paths = roomWeb.getPaths();
  if (! paths) paths = [];
  var corridors = [];

  for (var i = 0; i < paths.length; i++) {
    var corridor = CorridorGenerator.generateCorridor(paths[i], map);
    corridors.push(corridor);
  }

  map.corridors = corridors;
};

//Generates a single corridor between two points.
//We generate the actual corridor sections once every pass.
//The more passes the wider the corridor will be
CorridorGenerator.generateCorridor = function(path, map, passes) {
  var hooks = CorridorGenerator.getStartAndEndHooks(path, map);
  if (!passes) var passes = 1;
  var sections = [];
  while(passes > 0) {
    sections = sections.concat(CorridorGenerator.buildSections(hooks.startHook, hooks.endHook));
    passes--;
  }

  CorridorGenerator.setHooksInUse(hooks);

  return new Corridor(hooks.startHook, hooks.endHook, sections);
};

CorridorGenerator.getStartAndEndHooks = function(path, map) {
  var startRoom = map.getRoomWithId(path.start);
  var endRoom = map.getRoomWithId(path.end);
  //THESE METHODS ARE THE ONES TO CHANGE TO GET NEW GENERATION
  var startHook = CorridorGenerator.getClosestCorridorHook(startRoom, endRoom.getCenter());
  var endHook = CorridorGenerator.getClosestCorridorHook(endRoom, startHook);

  return {startHook: startHook, endHook: endHook};
};

//Returns the n closest rooms to the selected room
CorridorGenerator.getClosestCorridorHook = function(room, position) {
  if (!position || !room) console.error("Invalid params for getClosestCorridorHook!");

  var hooks = room.getHooks().slice();

  hooks.sort(function (hookA, hookB) {
    return Physics.distanceBetweenPoints(hookA.getPosition(), position) - Physics.distanceBetweenPoints(hookB.getPosition(), position);
  });

  return hooks[0];
};

CorridorGenerator.setHooksInUse = function(hooks) {
  hooks.startHook.setInUse(true);
  hooks.endHook.setInUse(true);
};

//Takes the start and end hooks and builds them.
CorridorGenerator.buildSections = function(startHook, endHook) {
  var currentAxis = startHook.getAxis();
  var currentTile = {x: startHook.getX(), y: startHook.getY()};
  var sections = [];

  if (currentTile.x == endHook.getX() && currentTile.y == endHook.getY()) {
    return [ {startPosition: currentTile, endPosition: currentTile, axis: 'horizontal' }];
  }

  while (currentTile.x != endHook.getX() || currentTile.y != endHook.getY()) {
    var newSection = {};
    if(currentAxis == 'horizontal') {
      newSection = CorridorGenerator.generateHorizontalSection(currentTile, endHook);
      currentAxis = 'vertical';
    } else if (currentAxis == 'vertical') {
      newSection = CorridorGenerator.generateVerticalSection(currentTile, endHook);
      currentAxis = 'horizontal';
    }

    sections.push(newSection);
    //console.log(newSection);
    currentTile = newSection.endPosition;
  }

  return sections;
};

CorridorGenerator.generateHorizontalSection = function(startPosition, endHook) {
  var maxLength = Math.abs(endHook.x - startPosition.x);
  var minLength = 1;

  var length = Math.round(Math.random() * maxLength);
  if (length < minLength) length = minLength;

  if (endHook.x < startPosition.x) length *= -1;

  var newSectionEndPosition = {x: startPosition.x + length, y: startPosition.y};
  var newSection = {startPosition: startPosition, endPosition: newSectionEndPosition, axis: 'horizontal'};

  //Is this the last section. If so finish corridor
  if (endHook.axis == 'horizontal' && newSection.startPosition.y == endHook.y) {
    newSection.endPosition.x = endHook.x;
    return newSection;
  }

  //Otherwise make sure section does not go beyond end boundaries
  CorridorGenerator.capSection(newSection, startPosition, endHook);

  return newSection;
};

//Only get here when we aren't in the final section so need to leave enough room to finish on later section
CorridorGenerator.capSection = function(section, startPosition, endHook) {
  if(section.axis == 'horizontal') {
    if (startPosition.x > endHook.x) { //Going left, need to check we haven't gone too far left
      if (section.endPosition.x < endHook.x + 1) {
        (endHook.axis == 'vertical') ? section.endPosition.x = endHook.x : section.endPosition.x = endHook.x + 1;
      }
    } else { //Going right, check not too far right
      if (section.endPosition.x > endHook.x - 1) {
        (endHook.axis == 'vertical') ? section.endPosition.x = endHook.x : section.endPosition.x = endHook.x - 1;
      }
    }
  } else if (section.axis == 'vertical') {
    if (startPosition.y > endHook.y) { //Corridor going up. Don't go too far down
      if (section.endPosition.y < endHook.y + 1) {
        (endHook.axis == 'horizontal') ? section.endPosition.y = endHook.y : section.endPosition.y = endHook.y + 1;
      }
    } else { //Corridor going down. Don't go too far up.
      if (section.endPosition.y > endHook.y - 1) {
        (endHook.axis == 'horizontal') ? section.endPosition.y = endHook.y : section.endPosition.y = endHook.y - 1;
      }
    }
  }
};

CorridorGenerator.generateVerticalSection = function(startPosition, endHook) {
  var maxLength = Math.abs(endHook.y - startPosition.y);
  var minLength = 1;

  var length = Math.round(Math.random() * maxLength);
  if (length < minLength) length = minLength;

  if (endHook.y < startPosition.y) length *= -1;

  var newSectionEndPosition = {x: startPosition.x, y: startPosition.y + length};

  var newSection = {startPosition: startPosition, endPosition: newSectionEndPosition, axis: 'vertical'};

    //Is this the last section. If so finish corridor
  if (endHook.axis == 'vertical' && newSection.startPosition.x == endHook.x) {
    newSection.endPosition.y = endHook.y;
    return newSection;
  }

  CorridorGenerator.capSection(newSection, startPosition, endHook);

  return newSection;
};
