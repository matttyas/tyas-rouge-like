//Represents tile data. Not an actor. No render information just the properties
//of a tile
function Furniture (libraryContent, tileX, tileY) {
  if (tileX) this.tileX = tileX;
  if (tileY) this.tileY = tileY;

  this.properties = {};
  if (libraryContent) this.initialise(libraryContent);
};

Furniture.prototype.initialise = function(libraryContent) {
  this.properties = libraryContent;
  this.properties['passable'] = false;
}

Furniture.prototype.setProperty = function(key, value) {
  this.properties[key] = value;
};

Furniture.prototype.getProperty = function(property) {
  return this.properties[property];
};

Furniture.prototype.getProperties = function() {
  return this.properties;
};
