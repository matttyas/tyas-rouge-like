//Represents tile data. Not an actor. No render information just the properties
//of a tile
function Decoration (libraryContent, position) {
  if (position) {
    this.tileX = position.x;
    this.tileY = position.y;
  }

  this.properties = {};
  if (libraryContent) this.initialise(libraryContent);
};

Decoration.prototype.initialise = function(libraryContent) {
  this.properties = libraryContent.properties;
  this.properties['passable'] = false;
}

Decoration.prototype.setProperty = function(key, value) {
  this.properties[key] = value;
};

Decoration.prototype.getProperty = function(property) {
  return this.properties[property];
};

Decoration.prototype.getProperties = function() {
  return this.properties;
};

Decoration.prototype.getTileX = function() {
  return this.tileX;
};

Decoration.prototype.getTileY = function() {
  return this.tileY;
};
