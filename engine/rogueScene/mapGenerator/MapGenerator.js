var MapGenerator = {
  maxRoomWidth: 15,
  maxRoomHeight: 15,
  roomWeb: new RoomWeb()
};

MapGenerator.reset = function() {
  MapGenerator.roomWeb = new RoomWeb();
};

//This will generate a map, and eventually an entire dungeon based on a provided
//seedString. I think the seed string would be something like "flooded cultist
//temple. Each provided word would have its own effect on the generated map"
//seed is a number identifying the random seed used to generate a given map
MapGenerator.generateMap = function() {

  MapGenerator.reset();

  var roomGenerator = MapGenerator.getRoomGenerator();
  var corridorGenerator = MapGenerator.getCorridorGenerator();
  var doorGenerator = MapGenerator.getDoorGenerator();
  var roomContentGenerator = MapGenerator.getRoomContentGenerator();
  var encounterGenerator = MapGenerator.getEncounterGenerator();
  var map = MapGenerator.generateBase();
  MapGenerator.generateRooms(roomGenerator, map);
  MapGenerator.generateCorridors(corridorGenerator, map);
  MapGenerator.generateDoors(doorGenerator, map);
  MapGenerator.generateRoomContent(roomContentGenerator, map);
  MapGenerator.generateEnemies(encounterGenerator, map);
  TileClassifier.classifyImpassableTiles(map);
  TileClassifier.classifyImpassableTiles(map, true);
  return map;
};

MapGenerator.generateBase = function() {
  return new Map(20,20);
};

MapGenerator.getRoomGenerator = function() {
  return RoomGenerator;
}

MapGenerator.getCorridorGenerator = function() {
  return CorridorGenerator;
};

MapGenerator.getDoorGenerator = function() {
  return DoorGenerator;
};

MapGenerator.getRoomContentGenerator = function() {
  return RoomContentGenerator;
};

MapGenerator.getEncounterGenerator = function() {
  return EncounterGenerator;
};

MapGenerator.generateRooms = function(roomGenerator, map) {
  map.setRooms(roomGenerator.generateRooms(map));
  MapGenerator.applyRoomsToMap(map);
  MapGenerator.generateRoomLinks(map);

  //Room features?
  MapGenerator.addEntranceAndExit(map);
}
//Generates an object that represents the links between the rooms
MapGenerator.generateRoomLinks = function(map) {
  var rooms = map.rooms;

  for (var i = 0; i < rooms.length; i++) {
    var closestRooms = RoomProcessor.getClosestRooms(rooms, rooms[i], 2);
    MapGenerator.roomWeb.addNode(rooms[i]);
    MapGenerator.roomWeb.addPathsFromNode(rooms[i].getId(), closestRooms);
  }
};

//Takes a map with attached rooms and "paints" them onto the base tiles
MapGenerator.applyRoomsToMap = function(map) {
  var rooms = map.rooms;
  for (var i = 0; i < map.rooms.length; i++) {
    MapGenerator.applyRoomToMap(map.rooms[i], map);
  }
};

//Goes through a room and applies its features to the map
MapGenerator.applyRoomToMap = function(room, map) {
  //Go through each position in the room and make the relevent tile on the map
  //passable.
  var roomWidth = room.getWidth();
  var roomHeight = room.getHeight();
  var roomX = room.getX();
  var roomY = room.getY();

  var tiles = map.getTiles();

  for (var x = 0; x < roomWidth; x++) {
    var tileX = roomX + x;

    if (tileX >= tiles.length ) continue; //If we are outside the bounds of the map continue

    for (var y = 0; y < roomHeight; y++) {
      var tileY = roomY + y;
      if (tileY >= tiles[y].length) break; //If we are outside the vertical bounds of the map exit
      tiles[tileX][tileY].setProperty('passable', true);
      tiles[tileX][tileY].setProperty('classification', 'CaveFloor');
    }
  }

  MapGenerator.applyRoomHooksToTiles(room.getHooks(), tiles);
  if (DEBUG) MapGenerator.applyRoomLabelToTile(room, tiles);
};

MapGenerator.applyRoomHooksToTiles = function(hooks, tiles) {
  //Paint on hooks for debug purposes
  for (var i = 0; i < hooks.length; i++) {
    var hookX = hooks[i].getX();
    var hookY = hooks[i].getY();

    if(hookX >= tiles.length || hookY >= tiles[0].length) continue;
    if(hookX < 0 || hookY < 0) return;

    tiles[hookX][hookY].setProperty('hook', true);
  }
};


MapGenerator.applyRoomLabelToTile = function(room, tiles) {
  var center = room.getCenter();
  var label = 'Room ' + room.getId();
  var centerY = Math.floor(center.y);
  var centerX = Math.floor(center.x);

  if (tiles[centerX] && tiles[centerX][centerY]) {
    tiles[centerX][centerY].setLabel(label);
  }
};

MapGenerator.generateCorridors = function(corridorGenerator, map) {
  corridorGenerator.generateCorridors(map, MapGenerator.roomWeb);
  MapGenerator.applyCorridorsToMap(map);
};

MapGenerator.applyCorridorsToMap = function(map) {
  var corridors = map.corridors;
  for (var i = 0; i < corridors.length; i++) {
    MapGenerator.applyCorridorToMap(corridors[i], map);
  }
};

MapGenerator.applyCorridorToMap = function(corridor, map) {
  //Go through all sections then apply each to map
  var sections = corridor.getSections();
  for (var i = 0; i < sections.length; i++) {
    MapGenerator.applySectionToMap(sections[i], map);
  }
};

MapGenerator.applySectionToMap = function(section, map) {
  var axis = section.axis;
  if (axis == 'horizontal') {
    MapGenerator.applyHorizontalSectionToMap(section, map);
  } else if(axis == 'vertical') {
    MapGenerator.applyVerticalSectionToMap(section, map);
  }
};

MapGenerator.applyHorizontalSectionToMap = function(section, map) {
  var startX = (section.startPosition.x < section.endPosition.x) ? section.startPosition.x : section.endPosition.x;
  var endX = (section.startPosition.x > section.endPosition.x) ? section.startPosition.x : section.endPosition.x;
  var y = section.startPosition.y;

  var tiles = map.getTiles();

  for (var x = startX; x <= endX; x++) {
     if(!tiles[x] || !tiles[x][y]) continue;
    tiles[x][y].setProperty('passable', true);
    tiles[x][y].setProperty('classification', 'CaveFloor');
  }
};

MapGenerator.applyVerticalSectionToMap = function(section, map) {
  var startY = (section.startPosition.y < section.endPosition.y) ? section.startPosition.y : section.endPosition.y;
  var endY = (section.startPosition.y > section.endPosition.y) ? section.startPosition.y : section.endPosition.y;
  var x = section.startPosition.x;

  var tiles = map.getTiles();

  for (var y = startY; y <= endY; y++) {
    if(!tiles[x] || !tiles[x][y]) continue;
    tiles[x][y].setProperty('passable', true);
    tiles[x][y].setProperty('classification', 'CaveFloor');
  }
};

MapGenerator.generateEnemies = function(encounterGenerator, map) {
  var encounters = encounterGenerator.generateEncounters(map);
  var enemies = [];
  for (var i = 0; i < encounters.length; i++) {
    var encounter = encounters[i];
    for (var j = 0; j < encounter.length; j++) {
      enemies.push(encounter[j]);
    }
  }
  map.setEnemies(enemies);
};

MapGenerator.setEngine = function(engine) {
  MapGenerator.engine = engine;
};

MapGenerator.setMaxRoomWidth = function(maxRoomWidth) {
  MapGenerator.maxRoomWidth = maxRoomWidth;
};

MapGenerator.setMaxRoomHeight = function(maxRoomHeight) {
  MapGenerator.maxRoomHeight = maxRoomHeight;
};

MapGenerator.addEntranceAndExit = function(map) {
  var rooms = map.getRooms();
  var startRoom = rooms[0];
  var endRoom = rooms[rooms.length - 1];

  var startCenter = startRoom.getRoundedCenter();

  var endCenter = endRoom.getRoundedCenter();
  var positionForEnd = {x: endCenter.x, y: endCenter.y};
  if (rooms.length == 1) {
    positionForEnd.x += 1;
    positionForEnd.y += 1;
  }
  //var tiles = map.getTiles();

  var stairsUp = new Entrance(startCenter.x, startCenter.y,'Stairs Up');
  stairsUp.setId(0);

  var stairsDown = new Entrance(positionForEnd.x, positionForEnd.y,'Stairs Down');
  stairsDown.setId(1);

  map.addEntrance(stairsUp);
  map.addEntrance(stairsDown);

};

MapGenerator.generateDoors = function(doorGenerator, map) {
  doorGenerator.generateDoors(map);
};

MapGenerator.generateRoomContent = function(roomContentGenerator, map) {
  roomContentGenerator.generateAllRoomContent(map);
};
