//"EVERYONE! Into the fungeon!"
//"... I hate you."

var DungeonGenerator = {
  NUMBERFLOORS: 100
};

DungeonGenerator.generateFloors = function() {

  var floors = [];
  var numberFloors = DungeonGenerator.calculateNumberFloors();

  while(floors.length < numberFloors) {
    var newFloor = MapGenerator.generateMap();
    newFloor.setId(floors.length);
    floors.push(newFloor);
  }

  this.linkFloors(floors);
  return floors;
};


DungeonGenerator.calculateNumberFloors = function() {
  return DungeonGenerator.NUMBERFLOORS;
};

DungeonGenerator.linkFloors = function(floors) {
  if (!floors) return console.error('No floors/maps have been generated so this is probably busted!');

  //Last floor
  for(var i = 0; i < floors.length; i++) {
    var previousFloor = floors[i - 1];
    var currentFloor = floors[i];
    var nextFloor = floors[i + 1];

    this.linkFloor(currentFloor, previousFloor, nextFloor);
  }
};

DungeonGenerator.linkFloor = function(currentFloor, previousFloor, nextFloor) {
  var entrance = currentFloor.getEntrances()[0];
  var exit = currentFloor.getEntrances()[1];

  if (previousFloor) {
    var previousFloorEntrances = previousFloor.getEntrances();
    entrance.setTargetMapId(previousFloor.getId());
    entrance.setTargetEntranceId(previousFloorEntrances[1].getId());
  }

  if (nextFloor) {
    var nextFloorEntrances = nextFloor.getEntrances();
    exit.setTargetMapId(nextFloor.getId());
    exit.setTargetEntranceId(nextFloorEntrances[0].getId());
  }
}
