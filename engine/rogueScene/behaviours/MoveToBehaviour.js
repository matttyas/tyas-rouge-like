function MoveToBehaviour() {

}

MoveToBehaviour.prototype = new Behaviour();

MoveToBehaviour.prototype.start = function() {
  this.state.setPriorityBehaviour(this);
  this.setTargetLocation();
};

MoveToBehaviour.prototype.run = function() {
  this.actor.moveTowards(this.targetLocation);
  if(this.actor.collidedWithEnvironment) this.end();
  if(this.actor.closeTo(this.targetLocation)) this.end();
};


//Utterly random. Need to do a random weighted pick or better yet pick a desired location and
//try to pathfind to it.
MoveToBehaviour.prototype.setTargetLocation = function() {
  var actorX = this.actor.x;
  var actorY = this.actor.y;
  var x = actorX - 1000 + Math.round(Math.random() * 2000);
  var y = actorY - 1000 + Math.round(Math.random() * 2000);

  this.targetLocation = {x: x, y: y};
  // var start = {x: actorX, y: actorY};
  // var end = {x: x, y; y};
  //
  // var line = {start: start, end: end};
  // var lineCollision = CollisionController.checkLineForCollisionWithObjets(line);
  //
  // if (lineCollision.collision) {
  //   var endX = end.x - xOverlap;
  //   var endY = end.y - yOverlap;
  // } else {
  //   this.targetLocation = end;
  // }
};
