/*
  This is the parent behaviour class. It will be inherited from with classes
  that enable AIs to exhibit behaviour's and take actions.
*/
function Behaviour() {
  this.isConstant = false; //A constant Behaviour runs in parallel with other behaviours at all times.
};

Behaviour.prototype.initialise = function() {

};

Behaviour.prototype.start = function() {

};

Behaviour.prototype.run = function() {

};

Behaviour.prototype.end = function() {
  if (this.nextBehaviour) {
    this.nextBehaviour.start();
  } else {
    this.start();
  }
};

Behaviour.prototype.shouldRun = function() {
  return true;
};

Behaviour.prototype.setNextBehaviour = function(nextBehaviour) {
  this.nextBehaviour = nextBehaviour;
};

Behaviour.prototype.setNextState = function(nextState) {
  this.nextState = nextState;
};

Behaviour.prototype.setState = function(state) {
  this.state = state;
};

Behaviour.prototype.setActor = function(actor) {
  this.actor = actor;
};

Behaviour.prototype.setConstantBehaviour = function(constantBehaviour) {
  this.constantBehaviour = constantBehaviour;
};
