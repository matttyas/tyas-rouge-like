function RestBehaviour(timeToRestFor) {
  this.timeToRestFor = timeToRestFor || 300; //Time to wait in frames..
  this.restingTime = 0;
}

RestBehaviour.prototype = new Behaviour();

RestBehaviour.prototype.start = function() {
  this.state.setPriorityBehaviour(this);
  this.restingTime = 0;
};

RestBehaviour.prototype.run = function() {
  if(this.restingTime == this.timeToRestFor) {
    return this.end();
  }

  this.actor.rest();
  this.restingTime += 1;
};

RestBehaviour.prototype.end = function() {
  this.actor.stopResting();
  Behaviour.prototype.end.call(this);
};
