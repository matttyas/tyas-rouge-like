function MoveToSpecificPointBehaviour() {

}

MoveToSpecificPointBehaviour.prototype = new MoveToBehaviour();

MoveToSpecificPointBehaviour.prototype.run = function() {
  this.setTargetLocation();
  if (!this.targetLocation) return this.end();
  this.actor.moveTowards(this.targetLocation);
  if(this.actor.closeTo(this.targetLocation)) this.end();
};


//Heads towards the acotrs current target location
MoveToSpecificPointBehaviour.prototype.setTargetLocation = function() {
  this.actor.lookForTargets();
  this.targetLocation = this.actor.getTargetLocation();
  if (!this.targetLocation) this.end(); //Actor got away??
};

MoveToSpecificPointBehaviour.prototype.end = function() {
  if (this.nextBehaviour) {
    this.nextBehaviour.start();
  } else if (this.nextState) {
    this.nextState.becomeCurrentState();
  } else {
    this.start();
  }
};
