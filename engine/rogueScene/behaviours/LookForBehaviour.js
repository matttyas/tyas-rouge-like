function LookForBehaviour() {
  this.isConstant = true;
}

LookForBehaviour.prototype = new Behaviour();

LookForBehaviour.prototype.run = function() {
  this.actor.lookForTargets();

  if(this.actor.getCurrentTarget()) {
    if (this.nextState) {
      this.nextState.becomeCurrentState();
    } else {
      console.error('Look for behaviour has no next state');
    }
  }
};
