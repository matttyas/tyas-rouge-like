//Map - Map will hold the raw data that represents a map, entirely seperate
//from any presentational data i.e. coordinates, widths/heights
//and data detailing difficulty, style etc of the map.

function Map (width, height) {
  this.width = width; //Width of map in tiles
  this.height = height; //Height of map in tiles
  this.tiles = [];
  this.rooms = [];
  this.corridors = [];
  this.entrances = [];
  this.furniture = [];
  this.decorations = [];
  this.doors = [];
  this.id = null;
  this.outside = false;
  this.initialise();
};

Map.prototype.initialise = function() {
  for (var x = 0; x < this.width; x++) {
    var row = [];
    for (var y = 0; y < this.height; y++) {
      row.push(new Tile());
    }
    this.tiles.push(row);
  }
};

Map.prototype.setTiles = function (tiles) {
  this.tiles = tiles;
};

Map.prototype.getTiles = function() {
  return this.tiles;
};

Map.prototype.getWidth = function() {
  return this.width;
};

Map.prototype.getHeight = function() {
  return this.height;
};

Map.prototype.getRoomWithId = function(id) {
  for (var i = 0; i < this.rooms.length; i++) {
    if (this.rooms[i].id == id) return this.rooms[i];
  }

  return null;
};

Map.prototype.setRooms = function(rooms) {
  this.rooms = rooms;
};

Map.prototype.getRooms = function() {
  return this.rooms;
};

Map.prototype.setCorridors = function(corridors) {
  this.corridors = corridors;
};

Map.prototype.getCorridors = function() {
  return this.corridors;
};

Map.prototype.setDoors = function(doors) {
  this.doors = doors;
};

Map.prototype.getDoors = function() {
  return this.doors;
};

Map.prototype.addEntrance = function(entrance) {
  this.entrances.push(entrance);
};

Map.prototype.setEntrances = function(entrances) {
  this.entrances = entrances;
};

Map.prototype.getEntrances = function() {
  return this.entrances;
};

Map.prototype.getDecorations = function() {
  return this.decorations;
};

Map.prototype.setId = function(id) {
  this.id = id;
};

Map.prototype.getId = function() {
  return this.id;
};

Map.prototype.getTile = function(x, y) {
  if (x < 0 || x >= this.tiles.length) return null;
  if (y < 0 || y >= this.tiles[0].length) return null;
  return this.tiles[x][y];
};

//This is not for collision detection really. It simply tells us if a tile is pasable.
//Not when inside tiles outside the boundary are impassable for now, outside they are passable.
Map.prototype.checkTilePassable = function(x, y) {
  var tile = this.getTile(x, y);

  if (!tile) {
    if (this.outside) {
      return true;
    } else {
      return false;
    }
  }

  return tile.getProperty('passable');
};

Map.prototype.setEnemies = function(enemies) {
  this.enemies = enemies;
};

Map.prototype.getEnemies = function() {
  return this.enemies;
};
