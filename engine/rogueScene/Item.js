function Item (properties) {
  if (properties) this.initialise(properties);
};

Item.prototype.processContent = function(content) {
  this.spritesheet = content;
};

Item.prototype.initialise = function (properties) {
  this.properties = properties;
  // this.name = properties.getProperty('name');
  //this.texturePath = 'content/items/' + this.name + '.png'; //I see no reason not to do a spritesheet for this
};

Item.prototype.getName = function() {
  return this.properties.name;
};

Item.prototype.getProperty = function(property) {
  return this.properties[property];
};
