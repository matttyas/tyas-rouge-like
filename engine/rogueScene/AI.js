function AI(actor) {
  this.states = {};
  this.currentState = null;
  this.actor = actor;
  this.initialise();
};

AI.prototype.initialise = function() {

};

AI.prototype.act = function() {
  if (!this.currentState) return;
  this.currentState.run();
};

AI.prototype.addState = function(state) {
  var stateName = state.getName();
  if(!stateName) return console.error('Can not add an AI state without it having a name');
  this.states[stateName] = state;
  state.setAI(this);
  state.setActor(this.actor);
};

AI.prototype.setCurrentState = function(stateName) {
  if(!stateName) return console.error('Need name to set current AI state');
  if(!this.states[stateName]) return console.error('Attempted to switch to nonexistant state');

  this.currentState = this.states[stateName];
}
//So the majority of the "thinking" of the AI will be done in the states themselves.
//Certain states will call into certain other states and will have the actions which
//they execute in order to show certain behaviours.
