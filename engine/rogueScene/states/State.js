function State(name) {
  this.behaviours = [];
  this.priorityBehaviour = null;
  this.name = name;
};

State.prototype.initialise = function() {

};

//This should be called anytime an npc is expected to think / do anything
State.prototype.run = function() {
  this.runConstantBehaviours(); //These behaviours should always run in a state (think looking for an enemy)
  if (this.priorityBehaviour) return this.priorityBehaviour.run();

  for (var i = 0; i < this.behaviours.length; i++) {
    var behaviour = this.behaviours[i];
    if(behaviour.shouldRun() && ! (behaviour.isConstant)) behaviour.run();
  }
};

State.prototype.runConstantBehaviours = function() {
  //IF SUPER PRIORITY THEN PUNCH OUT OF EVEN THIS (FLEEING ETC)
  for (var i = 0; i < this.behaviours.length; i++) {
    var behaviour = this.behaviours[i];
    if(behaviour.isConstant) behaviour.run();
  }
}

State.prototype.addBehaviour = function(behaviour) {
  this.behaviours.push(behaviour);
  behaviour.setState(this);
};

State.prototype.switchToState = function(stateName) {
  this.ai.setCurrentState(stateName);
};

State.prototype.setAI = function(ai) {
  this.ai = ai;
};

State.prototype.setActor = function(actor) {
  for (var i = 0; i < this.behaviours.length; i++) {
    this.behaviours[i].setActor(actor);
  }

  this.actor = actor;
};

//If the AI wants to flee the flee behaviour could take priority. All else would
//be ignored for a while
State.prototype.setPriorityBehaviour  = function(priorityBehaviour) {
  this.priorityBehaviour = priorityBehaviour;
};

State.prototype.removePriorityBehaviour = function() {
  this.priorityBehaviour = null;
};

State.prototype.getName = function() {
  return this.name;
};

State.prototype.becomeCurrentState = function() {
  this.ai.setCurrentState(this.name);
};
