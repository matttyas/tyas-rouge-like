DEBUG = false;

function Engine () {
  this.canvas=document.createElement("canvas");
  this.canvas.id = 'game';
  this.ctx=this.canvas.getContext("2d");
  var aspectRatio = window.innerWidth / window.innerHeight;

  this.canvas.width = 1200;
  this.canvas.height = 750;

  this.tileWidth = 100;
  this.tileHeight = 100;

  this.screenWidth = this.canvas.width;
  this.screenHeight = this.canvas.height;

  this.previousKeysDown = [];
  this.keysDown = [];

  this.stats = new Stats();
  this.stats.setMode(0); // 0: fps, 1: ms

  // Align top-left

  this.debug = false;

  this.stats.domElement.style.position = 'absolute';
  this.stats.domElement.style.left = '0px';
  this.stats.domElement.style.top = '60px';

  document.body.appendChild(this.canvas);
  if (this.debug) document.body.appendChild( this.stats.domElement );

  //Instantiate Scenes
  //var seed = 611851.6766000539;

  //var seed = 888310.6771390885;
  if (!seed) var seed = Math.random() * 1000000;
  Math.seedrandom(seed) ;
  console.log("Map Seed: " + seed);

  PatternLibrary.initialise();
  ItemContentLibrary.initialise();
  EnemyLibrary.initialise();

  this.rogueScene = new RogueScene(this);
  CollisionController.setEngine(this);
  CollisionController.setRogueScene(this.rogueScene);
  this.sceneManager = new SceneManager(this);
  this.sceneManager.buildSceneList([this.rogueScene])
  this.sceneManager.setCurrentScene('RogueScene');

  this.renderer = new Renderer(this.ctx, this.canvas.width, this.canvas.height);
  this.renderer.setCamera(new Camera());
  this.renderer.initialiseCanvas();
 // this.world = new World();

  this.renderer.setCurrentScene(this.currentScene);
}

Engine.prototype.update = function() {

  if(this.currentScene.getLoaded() < 1) {
    ContentLoader.load(this.currentScene.getContentToLoad(), this.currentScene);
  } else {
    this.currentScene.handleKeyboard(this.keysDown, this.previousKeysDown);
    this.currentScene.update();
  }
  engine.previousKeysDown = this.cloneKeysDown();
  this.renderer.render();
};

    //Clones the keys that are down at this particular moment
Engine.prototype.cloneKeysDown = function() {
  var tempKeysDown = {};
  for(var key in engine.keysDown) {
    tempKeysDown[key] = true;
  }
  return tempKeysDown;
}

var engine = new Engine();

//EVENT LISTENERS

addEventListener("keydown", function (e) {
  engine.keysDown[keyMapper.mapKey(e.keyCode)] = true;
}, false);


addEventListener("keyup", function (e) {
  delete engine.keysDown[keyMapper.mapKey(e.keyCode)];
}, false);


addEventListener("click", function(e) {
  var canvasRectangle = engine.canvas.getBoundingClientRect();

  var relX = e.clientX - canvasRectangle.left;
  var relY = e.clientY - canvasRectangle.top;

  if (engine.renderer.camera) {
      relX = relX + engine.renderer.camera.x;
      relY = relY + engine.renderer.camera.y;
  }

  engine.currentScene.handleClick(relX, relY);
}, false);



//THE GAME LOOP
var animFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        null ;

if(animFrame !== null) {
  var recursiveAnim = function() {
          engine.stats.begin();
    engine.update();
    engine.stats.end();
    animFrame( recursiveAnim );
  };

  animFrame( recursiveAnim );
} else {
  var ONE_FRAME_TIME = 1000.0 / 60.0 ;
  setInterval( main, ONE_FRAME_TIME );
}
