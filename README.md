Tyas Rogue Like
===============

This is two projects in one really:

1. A map generator that I will use to explore the possibilities of procedural generation. Initially just for dungeons, but later worlds and even quests. At the moment it is random, but the aim is for it to generate "interesting" spaces and structures to explore. See giantbombs interview: Delving into the History of Roguelikes.

2. A basic real time(?) Rogue Like that will allow me to explore the maps generated. I'd like to have 8 directional movement ala zelda then layer dark souls esque combat but in 2d. For now a simple exploration engine with simple combat and basic map obfuscation through fog of war should suffice.


To Run
------

To run the game open index.js.

To run the tests open tests/unit/index.js


Controls
--------

WASD - Movement
Space - Inspect door or stairs / Select menu item
QE - Throw knife
F - Debug key currently set to damage player!


Tests
-----

I added in unit tests around commit 10. Coverage is not 100% but will be on any altered or new code. This is mainly to combat the introduction of bugs but also because writing tests nearly always highlights areas in which functions can be refactored or simplified.
