//Rogue Like scene. I have minimal preconceptions of what a rogue like is and intend for
//this one to get a little crazy
//Author: Matthew Tyas
//Date: 11/08/2015

function RogueScene(engine) {
  this.name = 'RogueScene';
  this.playerCount = 1;
  this.players = [];
  this.engine = engine;
  this.currentMenu = null;
  this.currentEntrances = [];
  this.heroInputState = 'Hero';
  this.menuInputState = 'Menu';
  this.editInputState = 'Editor';
  this.selectedMenuOption = 0;
  this.mainMenuOptions = [];
  this.currentMap = {};
  this.loadingText = 'Loading...';
  this.loaded = false;
  this.initialise();
}

RogueScene.prototype = new Scene();

RogueScene.prototype.initialise = function() {
  this.hero = new Hero(0,0);
  this.hero.setRogueScene(this);

  this.heroes = [this.hero];
  //this.world = this.engine.world.buildWorld(this.engine, this);
  this.editor = new Editor();

  //719979.629619047 - No Exit Seed. Investigate!!!
  this.initialiseDungeon();
  this.contentToLoad = ContentListBuilder.buildListFromRogueMap(this.currentMap);
  this.contentToLoad = this.contentToLoad.concat(ContentListBuilder.getContentForItems());
  this.contentToLoad = this.contentToLoad.concat(ContentListBuilder.buildListFromRogueScene(this));
  (DEBUG) ? this.useEditMode() : this.dontUseEditMode();
};

RogueScene.prototype.initialiseDungeon = function() {
  //66074.9867092818
  var seed = 543875.6002113223;
  var dungeonData = DungeonGenerator.generateFloors(seed);
  this.dungeon = [];

  for (var i = 0; i < dungeonData.length; i++) {
    this.dungeon.push( new RogueMap(dungeonData[i], this.engine, this));
  }
  this.setCurrentMap(this.dungeon[0]);
  this.currentFloor = 0;
};

RogueScene.prototype.dontUseEditMode = function() {
  this.inputState = this.heroInputState;
  this.editMode = false;
};

RogueScene.prototype.useEditMode = function() {
  this.inputState = this.editInputState;
  this.editMode = true;
  this.editor.setCurrentScene(this);
};

//Called once a frame by the engine.
RogueScene.prototype.update = function() {
  this.currentMap.update();

  for (var i = 0; i < this.heroes.length; i++) {
    this.heroes[i].update();
  }
};


RogueScene.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
  switch (this.inputState) {
    case this.editInputState:
      this.editor.handleKeyboard(keysDown, previousKeysDown);
      break;
    case this.heroInputState:
      this.hero.handleKeyboard(keysDown, previousKeysDown);
      break;
    case this.menuInputState:
      this.currentMenu.handleKeyboard(keysDown, previousKeysDown);
      break;
  }
};

RogueScene.prototype.debugActor = function(actor) {
  console.log('Actor Name: ' + actor.name);
  console.log('x: ' + actor.x);
  console.log('y: ' + actor.y);
  console.log('width: ' + actor.width);
  console.log('height: ' + actor.height);
  console.log('right: ' + (actor.x + actor.width));
  console.log('bottom:  ' + (actor.y + actor.height));
  if (actor.canMove) console.log('canMove: true');
};

RogueScene.prototype.render = function(ctx, renderer) {
  if (this.currentMap) this.currentMap.render(ctx, renderer);
  if (this.hero) this.hero.render(renderer);
  if(this.currentMenu) this.currentMenu.render(ctx, renderer);
};

//Give camera reference to editor
RogueScene.prototype.handleRenderer = function(renderer) {
  this.editor.setCamera(renderer.camera);
  this.hero.setEngine(this.engine);
  this.hero.setCurrentMap(this.currentMap);

  if(!this.editMode) {
    var camera = this.engine.renderer.getCamera();
    camera.setFollowDistance(this.engine.canvas.width / 2, this.engine.canvas.height / 2);
    camera.follow(this.hero);
  }
};

RogueScene.prototype.goDownAFloor = function() {
  if(this.currentFloor + 1 < this.dungeon.length) {
    this.currentFloor += 1;
    this.setCurrentMap(this.dungeon[this.currentFloor]);
  }
};

RogueScene.prototype.goUpAFloor = function() {
  if (this.currentFloor - 1 >= 0 ) {
    this.currentFloor -= 1;
    this.setCurrentMap(this.dungeon[this.currentFloor]);
  }
};

RogueScene.prototype.setCurrentMap = function(map, entrance) {
  if (!entrance) entrance = map.getEntrance("Stairs Up");
  this.currentMap = map;
  this.setUpHeroOnMap(this.currentMap, entrance);

  if (!this.currentMap.getProcessedContent()) {
    this.loaded = false;
  }
  var adjacentMaps = this.currentMap.getAdjacentMaps();
  this.loadAdjacentMapsContent(adjacentMaps);

  CollisionController.setCurrentMap(this.currentMap);
};

RogueScene.prototype.setUpHeroOnMap = function(currentMap, entrance) {
  this.hero.setX(entrance.getX());
  this.hero.setY(entrance.getY());
  this.hero.setCurrentMap(currentMap);
};

RogueScene.prototype.loadAdjacentMapsContent = function(adjacentMaps) {
  for (var i = 0; i < adjacentMaps.length; i++) {
    if(!adjacentMaps[i].getProcessedContent()) {
      var adjacentMapContent = ContentListBuilder.buildListFromRogueMap(adjacentMaps[i]);
      ContentLoader.load(adjacentMapContent, adjacentMaps[i]);
    }
  }
};

RogueScene.prototype.showInspectMenu = function(target) {
  this.currentMenu = new InspectMenu(target);
  this.currentMenu.setCurrentScene(this);
  this.checkInspectMenuValid();
};

RogueScene.prototype.checkInspectMenuValid = function() {
  if (this.currentMenu.hasNoActions()) {
    this.currentMenu = null;
  } else {
    this.inputState = this.menuInputState;
  }
};

RogueScene.prototype.closeCurrentMenu = function() {
  this.currentMenu = null;
  this.inputState = this.heroInputState;
};

RogueScene.prototype.goToMap = function(mapId, entranceId) {
  if((!mapId && mapId !== 0) || (!entranceId && entranceId !== 0)) return console.error('MapId Or EntranceId not set ')
  var map = this.getMapWithId(mapId);
  var entrance = map.getEntranceWithId(entranceId);

  this.setCurrentMap(map, entrance);
};

RogueScene.prototype.getMapWithId = function(mapId) {
  for (var i = 0; i < this.dungeon.length; i++) {
    var map = this.dungeon[i];
    if (map.getId() == mapId) return map;
  }
  return null;
};


RogueScene.prototype.renderLoadingScreen = function(ctx, renderer) {
  renderer.clearBackground();
  this.renderLoadingText(ctx, renderer);
};

RogueScene.prototype.renderLoadingText = function(ctx, renderer) {
  renderer.setTextSize(30);
  renderer.setTextColor('rgb(255,255,255)');
  renderer.drawText(this.loadingText, renderer.width - 165, renderer.height - 35);
};

RogueScene.prototype.processContent = function(content) {
  ItemContentLibrary.processContent(content);
  this.currentMap.processContent(content);
  ContentProcessor.processContentForActors(this.heroes, content);

  this.loaded = true;
};

RogueScene.prototype.getRogueMapsFromIds = function(mapIds) {
  if (!mapIds || mapIds.constructor != Array || mapIds.length == 0) return [];
  var maps = [];

  for (var i = 0; i < mapIds.length; i++) {
    var map = this.getMapWithId(mapIds[i]);
    if (map) maps.push(map);
  }

  return maps;
};

RogueScene.prototype.addProjectile = function(projectile) {
  this.currentMap.addProjectile(projectile);
};
//GETTERS

RogueScene.prototype.getLoaded = function() {
  return this.loaded;
};

RogueScene.prototype.getContentToLoad = function() {
  return this.contentToLoad;
};

RogueScene.prototype.getCurrentMap = function() {
  return this.getCurrentMap();
}

RogueScene.prototype.getHeroes = function() {
  return this.heroes;
};
