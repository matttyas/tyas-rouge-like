var assert = chai.assert;

describe('Actor', function() {
  describe('takeAction', function() {
    it('maps an display action string to the actions function then calls that function', function() {
      var actor = new Actor();
      var actionString = 'Open Chest';
      var actionMappings = { 'Open Chest': 'openChest'};
      actor.actionMappings = actionMappings;
      actor.openChest = function() {};
      sinon.spy(actor, 'openChest');

      actor.takeAction(actionString);

      assert.equal(actor.openChest.callCount, 1);
    });
  });

  describe('alterMovementForCollision', function() {
    it('takes a collision and current movement and updates this.movement.x if movement has a negative x value', function() {
      var movement = {x: -5, y: 0};
      var collision = {xOverlap: 3};
      var actor = new Actor();
      actor.movement = {x: -5, y: 5};

      var expected = {x: -2, y: 5};
      actor.alterMovementForCollision(collision, movement);
      assert.deepEqual(actor.movement, expected);
    });

    it('does the same but subtracts the value of the collision xOverlap if the momvent was positive', function() {
      var movement = {x: 5, y: 0};
      var collision = {xOverlap: 2};
      var actor = new Actor();
      actor.movement = {x: 5, y: 5};

      var expected = {x: 3, y: 5};
      actor.alterMovementForCollision(collision, movement);
      assert.deepEqual(actor.movement, expected);
    });

    it('takes a collision and current movement and updates this.movement.y if movement has a negative y value', function() {
      var movement = {x: 0, y: -5};
      var collision = {yOverlap: 1};
      var actor = new Actor();
      actor.movement = {x: 5, y: -5};

      var expected = {x: 5, y: -4};
      actor.alterMovementForCollision(collision, movement);
      assert.deepEqual(actor.movement, expected);
    });

    it('does the same but subtracts the value of the collision yOverlap if the momvent was positive', function() {
      var movement = {x: 0, y: 5};
      var collision = {yOverlap: 4};
      var actor = new Actor();
      actor.movement = {x: 5, y: 5};

      var expected = {x: 5, y: 1};
      actor.alterMovementForCollision(collision, movement);
      assert.deepEqual(actor.movement, expected);
    });

    it('does nothing to this.movement.x or y if they have 0 / unassigned values on movement', function() {
      var movement = {x: 0, y: 0};
      var collision = {yOverlap: 1};


      var actor = new Actor();
      actor.movement = {x: 5, y: -5};

      var expected = {x: 5, y: -5};
      actor.alterMovementForCollision(collision, movement);
      assert.deepEqual(actor.movement, expected);
    });
  });

  describe('moveTowards', function() {
    beforeEach(function() {
      sinon.stub(CollisionController, 'checkForCollision');
      sinon.stub(Physics, 'calculateMovementVector');
    });

    afterEach(function() {
      CollisionController.checkForCollision.restore();
      Physics.calculateMovementVector.restore();
    });

    it('sets collidedWithEnvironment to false before checking for collision with the environment', function() {
      var actor = new Actor();
      actor.collidedWithEnvironment = true;
      var position = {x: 0, y: 0};
      Physics.calculateMovementVector.returns({x: 0, y: 0});
      actor.moveTowards(position);

      assert.equal(actor.collidedWithEnvironment, false);
    });

    it('gets the correct xMovement and yMovement using Physics.calculateMovementVector', function() {
      var actor = new Actor();
      actor.x = 0;
      actor.y = 0;
      Physics.calculateMovementVector.returns({x: 7, y: 3});

      var targetPosition = {x: 14, y: 6};
      actor.speed = 10;

      actor.moveTowards(targetPosition);
      assert.ok(Physics.calculateMovementVector.calledWith({x: 0, y: 0}, targetPosition, actor.speed));
    });

    it('moves on x and y axis by amount returned by Phsics.calculateMovementVector', function() {

    })
  });

  describe('closeTo', function() {
    beforeEach(function() {
      sinon.stub(Physics, 'distanceBetweenPoints');
    });

    afterEach(function() {
      Physics.distanceBetweenPoints.restore();
    });

    it('returns true if the distance between two points is less than the actors speed', function() {
      var distance = 5;
      var actorSpeed = 10;
      var targetPosition = {'a': 'position'};
      var actor = new Actor();
      actor.speed = actorSpeed;

      Physics.distanceBetweenPoints.returns(distance);
      assert.ok(actor.closeTo(targetPosition));
    });

    it('returns true even if the distance between two points is negative (and is less than the actors speed)', function() {
      var distance = -5;
      var actorSpeed = 10;
      var targetPosition = {'a': 'position'};
      var actor = new Actor();
      actor.speed = actorSpeed;

      Physics.distanceBetweenPoints.returns(distance);
      assert.ok(actor.closeTo(targetPosition));
    });

    it('returns false if the distance between two points is bigger than speed', function() {
      var distance = 15;
      var actorSpeed = 10;
      var targetPosition = {'a': 'position'};
      var actor = new Actor();
      actor.speed = actorSpeed;

      Physics.distanceBetweenPoints.returns(distance);
      assert.ok(!actor.closeTo(targetPosition));
    });

    it('returns false if the distance between the two points is negative but absolutely bigger than speed', function() {
      var distance = -15;
      var actorSpeed = 10;
      var targetPosition = {'a': 'position'};
      var actor = new Actor();
      actor.speed = actorSpeed;

      Physics.distanceBetweenPoints.returns(distance);
      assert.ok(!actor.closeTo(targetPosition));
    });
  });

  describe('takeDamage', function() {
    beforeEach(function() {
      sinon.spy(Math, 'round');
      sinon.stub(Actor.prototype, 'die');
    });

    afterEach(function() {
      Math.round.restore();
      Actor.prototype.die.restore();
    });

    it('reduces the actors hp by the damage received', function() {
      var actor = new Actor();
      actor.hp = 100;
      actor.takeDamage(10);
      assert.equal(actor.hp, 90);
    });

    it('does nothing if no damage is passed in', function() {
      var actor = new Actor();
      actor.hp = 100;
      actor.takeDamage();
      assert.equal(Math.round.callCount, 0);
      assert.equal(actor.hp, 100);
    });

    it('does nothing if the players hp is < -100000', function() {
      var actor = new Actor();
      actor.hp = -900000;
      actor.takeDamage(150);
      assert.equal(Math.round.callCount, 0);
      assert.ok(!actor.dead);
    });

    it('sets the player to be dead and calls die if hp drops to below 0', function() {
      var actor = new Actor();
      actor.hp = 5;
      actor.takeDamage(10);
      assert.ok(actor.dead);
      assert.equal(actor.die.callCount, 1);
    });
  })
});
