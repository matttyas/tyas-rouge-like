var assert = chai.assert;

describe('Rogue Tile', function() {

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(RogueTile.prototype, 'renderUnrevealed');
      sinon.stub(Actor.prototype, 'renderTexture');
      sinon.stub(RogueTile.prototype, 'renderHidden');
      sinon.stub(RogueTile.prototype, 'debugRender');
    });

    afterEach(function() {
      RogueTile.prototype.renderUnrevealed.restore();
      Actor.prototype.renderTexture.restore();
      RogueTile.prototype.renderHidden.restore();
      RogueTile.prototype.debugRender.restore();
    });

    it('only calls renderUnrevealed if the tile is not revealed', function() {
      DEBUG = false;
      var renderer = {camera: {x: 5, y: 10}};
      var rogueTile = new RogueTile();
      rogueTile.revealed = false;
      rogueTile.texture = 'texture';

      rogueTile.render(renderer);
      assert.equal(rogueTile.renderUnrevealed.callCount, 1);
      assert.ok(rogueTile.renderUnrevealed.calledWith(renderer));
      assert.equal(rogueTile.renderTexture.callCount, 0);
    });

    it('renders the texture if rogueTile.texture is set', function() {
      DEBUG = false;
      var renderer = {camera: {x: 5, y: 10}};
      var rogueTile = new RogueTile();
      rogueTile.revealed = true;
      rogueTile.texture = 'texture';

      rogueTile.render(renderer);
      assert.equal(rogueTile.renderUnrevealed.callCount, 0);
      assert.equal(rogueTile.renderTexture.callCount, 1);
      assert.ok(rogueTile.renderTexture.calledWith(renderer));
    });

    it('renders a solid color rect if the texture is not set', function() {
      DEBUG = false;
      var renderer = {
        camera: {x: 5, y: 10},
        setFillStyle: function() {},
        fillRect: function() {}
      };

      sinon.spy(renderer, 'setFillStyle');
      sinon.spy(renderer, 'fillRect');

      var rogueTile = new RogueTile();
      rogueTile.color = 'rgb(255,0,0)';
      rogueTile.x = 15;
      rogueTile.y = 20;
      rogueTile.width = 50;
      rogueTile.height = 50;

      rogueTile.revealed = true;

      var expectedX = rogueTile.x - renderer.camera.x;
      var expectedY = rogueTile.y - renderer.camera.y;

      rogueTile.render(renderer);

      assert.equal(rogueTile.renderTexture.callCount, 0);
      assert.equal(renderer.setFillStyle.callCount, 1);
      assert.ok(renderer.setFillStyle.calledWith(rogueTile.color));
      assert.ok(renderer.fillRect.calledWith(expectedX, expectedY, rogueTile.width, rogueTile.height));
    });

    it('calls render hidden if the tile has been revealed but is now hidden (fog of war)', function() {
      DEBUG = false;
      var renderer = {camera: {x: 5, y: 10}};
      var rogueTile = new RogueTile();
      rogueTile.revealed = true;
      rogueTile.texture = 'texture';
      rogueTile.hidden = true;

      rogueTile.render(renderer);
      assert.equal(rogueTile.renderUnrevealed.callCount, 0);
      assert.equal(rogueTile.renderHidden.callCount, 1);
      assert.ok(rogueTile.renderHidden.calledWith(renderer));
    });

    it('uses the renderer to draw a border around the tile if renderBorder is set', function() {
      DEBUG = false;
      var renderer = {
        camera: {x: 5, y: 10},
        setStrokeStyle: function() {},
        strokeRect: function() {}
      };

      sinon.spy(renderer, 'setStrokeStyle');
      sinon.spy(renderer, 'strokeRect');

      var rogueTile = new RogueTile();
      rogueTile.borderColor = 'rgb(0,0,0)';
      rogueTile.renderBorder = true;
      rogueTile.texture = 'texture';
      rogueTile.x = 15;
      rogueTile.y = 20;
      rogueTile.width = 50;
      rogueTile.height = 50;

      rogueTile.revealed = true;

      var expectedX = rogueTile.x - renderer.camera.x;
      var expectedY = rogueTile.y - renderer.camera.y;

      rogueTile.render(renderer);

      assert.equal(renderer.setStrokeStyle.callCount, 1);
      assert.ok(renderer.setStrokeStyle.calledWith(rogueTile.color));
      assert.ok(renderer.strokeRect.calledWith(expectedX, expectedY, rogueTile.width, rogueTile.height));
    });

    it('calls debugRender if the global DEBUG is set', function() {
      var renderer = {camera: {x: 5, y: 10}};
      var rogueTile = new RogueTile();
      rogueTile.revealed = true;
      rogueTile.texture = 'texture';

      DEBUG = true;

      rogueTile.render(renderer);
      assert.equal(rogueTile.debugRender.callCount, 1);
      assert.ok(rogueTile.debugRender.calledWith(renderer));

      DEBUG = false;
    });
  });

  describe('setProperties', function() {
    it('sets the name and texturePath of rogueTile correctly if there is a classification property', function() {
      var classification = 'passable';
      var properties = {
        classification: classification
      };

      var rogueTile = new RogueTile();
      rogueTile.setProperties(properties);

      assert.equal(rogueTile.name, classification);
      assert.equal(rogueTile.texturePath, 'content/dungeon/tiles/passable.png');
    });
  });
});
