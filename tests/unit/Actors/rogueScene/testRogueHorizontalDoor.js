var assert = chai.assert;

describe('Rogue Horizontal Door', function() {
  describe('useOpenRender', function() {
    beforeEach(function() {
      sinon.stub(RogueHorizontalDoor.prototype, 'initialise');
    });

    afterEach(function() {
      RogueHorizontalDoor.prototype.initialise.restore();
    });

    it('sets width to OPENWIDTH and updates its colliderObject', function() {
      var rogueHorizontalDoor = new RogueHorizontalDoor({x: 1, y: 1}, {});
      var hitBoxes = [{width: 50}];
      rogueHorizontalDoor.colliderObject = {getHitBoxes: function() { return hitBoxes; }};
      rogueHorizontalDoor.useOpenRender();

      assert.equal(rogueHorizontalDoor.colliderObject.getHitBoxes()[0].width, rogueHorizontalDoor.OPENWIDTH);
    });
  });

  describe('useCloseRender', function() {
    beforeEach(function() {
      sinon.stub(RogueHorizontalDoor.prototype, 'initialise');
    });

    afterEach(function() {
      RogueHorizontalDoor.prototype.initialise.restore();
    });
    it('sets width to CLOSEDWIDTH and updates its colliderObject', function() {
      var rogueHorizontalDoor = new RogueHorizontalDoor({x: 1, y: 1}, {});
      var hitBoxes = [{width: 50}];
      rogueHorizontalDoor.colliderObject = {getHitBoxes: function() { return hitBoxes; }};
      rogueHorizontalDoor.useCloseRender();

      assert.equal(rogueHorizontalDoor.colliderObject.getHitBoxes()[0].width, rogueHorizontalDoor.CLOSEDWIDTH);
    });
  });
});
