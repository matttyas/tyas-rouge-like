var assert = chai.assert;

describe('Rogue Decoration', function() {
  describe('initialise', function() {
    it ('sets color to equal decorationData property color, if set', function() {
      var color = 'rgb(5,5,5)';
      var decorationData = { getProperty: function(property) { return color}};
      sinon.spy(decorationData, 'getProperty');

      var rogueDecoration = new RogueDecoration(null, decorationData);
      assert.ok(decorationData.getProperty.calledWith('color'));
      assert.equal(rogueDecoration.color, color);
    });

    it ('leaves the color alone if it is not a property in decorationData', function() {
      var decorationData = { getProperty: function(property) { return null}};
      sinon.spy(decorationData, 'getProperty');

      var rogueDecoration = new RogueDecoration(null, decorationData);

      assert.equal(rogueDecoration.color, rogueDecoration.DEFAULTCOLOR);
    });

    it('sets name from and builds path from mandatory property name', function() {
      var name = 'CaptainSillyPants';
      var decorationData = { getProperty: function(property) { return name}};
      sinon.spy(decorationData, 'getProperty');
      var expectedPath = 'content/dungeon/CaptainSillyPants.png';

      var rogueDecoration = new RogueDecoration(null, decorationData);

      assert.ok(decorationData.getProperty.calledWith('name'));
      assert.equal(rogueDecoration.name, name);
      assert.equal(rogueDecoration.texturePath, expectedPath);
    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(RogueDecoration.prototype, 'initialise');
      sinon.stub(Actor.prototype, 'renderTexture');
    });

    afterEach(function() {
      RogueDecoration.prototype.initialise.restore();
      Actor.prototype.renderTexture.restore();
    });

    it('calls Actor.renderTexture if texture is set', function() {
      var rogueDecoration = new RogueDecoration();
      var texture = {'a': 'texture'};
      var renderer = 'renderer';
      rogueDecoration.texture = texture;
      rogueDecoration.render(renderer);

      assert.equal(rogueDecoration.renderTexture.callCount, 1);
      assert.ok(rogueDecoration.renderTexture.calledWith(renderer));
    });

    it('otherwise sets the fill color and renders a rectangle in that color at the correct place in the level', function() {
      var rogueDecoration = new RogueDecoration();
      rogueDecoration.x = 200;
      rogueDecoration.y = 300;
      rogueDecoration.width = 100;
      rogueDecoration.height = 50;
      rogueDecoration.color = 'rgb(255,0,0)';

      var cameraX = 100;
      var cameraY = 150;

      var camera = {
        getX: function() { return cameraX },
        getY: function() { return cameraY }
      };

      var renderer = {
        setFillStyle: function() {},
        fillRect: function() {},
        getCamera: function() { return camera}
      };

      sinon.spy(renderer, 'setFillStyle');
      sinon.spy(renderer, 'fillRect');

      var expectedCameraX = rogueDecoration.x - cameraX;
      var expectedCameraY = rogueDecoration.y - cameraY;

      rogueDecoration.render(renderer);

      assert.ok(renderer.setFillStyle.calledWith(rogueDecoration.color));
      assert.ok(renderer.fillRect, expectedCameraX, expectedCameraY, rogueDecoration.width, rogueDecoration.height);
    });
  });
});
