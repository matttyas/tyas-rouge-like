var assert = chai.assert;

describe('Hero', function() {
  describe('initialise', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'setLastDirection');
      sinon.stub(Hero.prototype, 'initialiseStats');
      sinon.stub(IdGenerator, 'generateId');
    });

    afterEach(function() {
      Hero.prototype.setLastDirection.restore();
      Hero.prototype.initialiseStats.restore();
      IdGenerator.generateId.restore();
    });

    it('calls set last direction to up to prime the animation system', function() {
      var hero = new Hero(10, 10);

      assert.ok(hero.setLastDirection.calledWith('up'));
    });

    it('calls initialiseStats to setup the players stats', function() {
      var hero = new Hero(10, 10);
      assert.equal(hero.initialiseStats.callCount, 1);
    });

    it('sets the positions of players slots to the left of the screen if player1', function() {
      var expectedSlotLeftX = 20;
      var expectedSlotRightX = 90;

      var hero = new Hero(10, 10);

      assert.ok(hero.player1);
      assert.equal(expectedSlotLeftX, hero.SLOTLEFTX);
      assert.equal(expectedSlotRightX, hero.SLOTRIGHTX);
    });

    it('otherwises sets them to the right of the screen', function() {
      var expectedSlotLeftX = 1050;
      var expectedSlotRightX = 1120;

      var hero = new Hero(10, 10, 'player2');

      assert.ok(!hero.player1);
      assert.equal(expectedSlotLeftX, hero.SLOTLEFTX);
      assert.equal(expectedSlotRightX, hero.SLOTRIGHTX);
    });

    it('gets an id from the IdGenerator', function() {
      var id = 5;
      IdGenerator.generateId.returns(id);
      var hero = new Hero(10, 10);
      assert.equal(IdGenerator.generateId.callCount, 1);
      assert.equal(hero.id, id);
    });
  });

  describe ('handleKeyboard', function() {
    beforeEach (function() {
      sinon.stub(Hero.prototype, 'handleHorizontalMovementKeys');
      sinon.stub(Hero.prototype, 'handleVerticalMovementKeys');
      sinon.stub(Hero.prototype, 'move');
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.handleHorizontalMovementKeys.restore();
      Hero.prototype.handleVerticalMovementKeys.restore();
      Hero.prototype.move.restore();
      Hero.prototype.initialise.restore();
    });

    it ('passes keys to sub methods for movement, then passes movement data to move function', function() {
      var keysDown = { 'keys': 'down'};
      var previousKeysDown = { 'previous': 'keysDown'};
      var movementX = 1;
      var movementY = -1;

      Hero.prototype.handleHorizontalMovementKeys.returns(movementX);
      Hero.prototype.handleVerticalMovementKeys.returns(movementY);

      var hero = new Hero();
      hero.handleKeyboard(keysDown, previousKeysDown);

      assert.ok(Hero.prototype.handleHorizontalMovementKeys.calledWith(keysDown, previousKeysDown));
      assert.ok(Hero.prototype.handleVerticalMovementKeys.calledWith(keysDown, previousKeysDown));
      assert.ok(Hero.prototype.move.calledWith(movementX, movementY));
    });

    it ('gets the hero to start spriting if the shift key is held down', function () {
      var keysDown = { 'shift': true};
      var hero = new Hero();

      assert.ok(!hero.isSprinting());
      hero.handleKeyboard(keysDown, {});
      assert.ok(hero.isSprinting());
    });

    it('sets spriting to false if the spriting key is not down', function() {
      var hero = new Hero();

      assert.ok(!hero.isSprinting());
      hero.handleKeyboard({}, {});
      assert.ok(!hero.isSprinting());
    });

    it('uses the item in the left hand if there is an item equiped there and the key is down', function() {
      var rogueScene = {'a rakish': 'scene'};

      var hero = new Hero();
      hero.LEFTITEMKEY = 'q';
      hero.rogueScene = rogueScene;


      var leftHandEquipment = {use: function() {}};
      sinon.spy(leftHandEquipment, 'use');

      hero.equipment = {
        leftHand: leftHandEquipment
      };

      var keysDown = { 'q': true};
      var previousKeysDown = {};

      hero.handleKeyboard(keysDown, previousKeysDown);

      assert.ok(leftHandEquipment.use.calledWith(rogueScene, hero));
    });

    it('uses the item in the right hand if there is an item equiped there and the key is down', function() {
      var rogueScene = {'a rakish': 'scene'};

      var hero = new Hero();
      hero.RIGHTITEMKEY = 'e';
      hero.rogueScene = rogueScene;


      var rightHandEquipment = {use: function() {}};
      sinon.spy(rightHandEquipment, 'use');

      hero.equipment = {
        rightHand: rightHandEquipment
      };

      var keysDown = { 'e': true};
      var previousKeysDown = {};

      hero.handleKeyboard(keysDown, previousKeysDown);

      assert.ok(rightHandEquipment.use.calledWith(rogueScene, hero));
    });
  });

  describe('handleHorizontalMovementKeys', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'setLastDirection');
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.setLastDirection.restore();
      Hero.prototype.initialise.restore();
    });

    it ('returns a movementX of 0 if none of the horizontal movement keys are down', function() {
      var hero = new Hero();
      var movementX = hero.handleHorizontalMovementKeys({}, {});
      assert.equal(movementX, 0);
    });

    it ('returns a movement of -1 if the a key is down', function() {
      var keysDown = { 'a': true};
      var hero = new Hero();
      var movementX = hero.handleHorizontalMovementKeys(keysDown, {});
      assert.equal(movementX, -1);
      assert.ok(hero.setLastDirection.calledWith('left'));
    });

    it ('returns a movement of 1 if the d key is down', function() {
      var keysDown = { 'd': true};
      var hero = new Hero();
      var movementX = hero.handleHorizontalMovementKeys(keysDown, {});
      assert.equal(movementX, 1);
      assert.ok(hero.setLastDirection.calledWith('right'));
    });
  });

  describe('handleVerticalMovementKeys', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'setLastDirection');
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.setLastDirection.restore();
      Hero.prototype.initialise.restore();
    });

    it ('returns a movementY of 0 if none of the vertical movement keys are down', function() {
      var hero = new Hero();
      var movementY = hero.handleVerticalMovementKeys({}, {});
      assert.equal(movementY, 0);
    });

    it ('returns a movement of -1 if the w key is down', function() {
      var keysDown = { 'w': true};
      var hero = new Hero();
      var movementY = hero.handleVerticalMovementKeys(keysDown, {});
      assert.equal(movementY, -1);
      assert.ok(hero.setLastDirection.calledWith('up'));
    });

    it ('returns a movement of 1 if the s key is down', function() {
      var keysDown = { 's': true};
      var hero = new Hero();
      var movementY = hero.handleVerticalMovementKeys(keysDown, {});
      assert.equal(movementY, 1);
      assert.ok(hero.setLastDirection.calledWith('down'));
    });
  });

  describe ('move', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'normalize');
      sinon.stub(Hero.prototype, 'initialise');
      sinon.stub(CollisionController, 'checkForCollision');
    });

    afterEach(function() {
      Hero.prototype.normalize.restore();
      Hero.prototype.initialise.restore();
      CollisionController.checkForCollision.restore();
    });

    it ('Takes a movementX and movementY applies modifiers for spriting', function() {
      Hero.prototype.normalize.returns( {x: 0.5, y: -0.5});

      var hero = new Hero();
      hero.x = 0;
      hero.y = 0;

      hero.moveSpeed = 2;
      hero.sprinting = true;
      hero.sprintSpeedModifier = 2;

      var movementX = 1;
      var movementY = -1;

      hero.move(movementX, movementY);

      var expectedMovement = {x: 1, y: -1};
      assert.ok(hero.normalize.calledWith(expectedMovement));

      assert.equal(hero.x, 2);
      assert.equal(hero.y, -2);
    });

    it('calls checkForCollision with both the x and y movements', function() {
      Hero.prototype.normalize.returns( {x: 0.5, y: -0.5});

      var hero = new Hero();
      hero.x = 0;
      hero.y = 0;

      hero.moveSpeed = 2;
      hero.sprinting = true;
      hero.sprintSpeedModifier = 2;

      var movementX = 1;
      var movementY = -1;

      var expectedXMovement = {x: 2, y: 0};
      var expectedYMovement = {x: 0, y: -2};
      hero.move(movementX, movementY);

      assert.ok(CollisionController.checkForCollision.calledWith(hero, expectedXMovement));
      assert.ok(CollisionController.checkForCollision.calledWith(hero, expectedYMovement));

    });

    it('rounds movement so that player x and y is always a round number', function() {
      Hero.prototype.normalize.returns({x: 0.5, y: 0.5});
      sinon.spy(Math, 'round');

      var hero = new Hero();
      hero.move(1,1);

      var finalMoveX = 1.5;
      var finalMoveY = 1.5;

      assert.ok(Math.round.calledWith(finalMoveX));
      assert.ok(Math.round.calledWith(finalMoveY));

      Math.round.restore();
    });
  });

  describe('inspect', function() {
      beforeEach(function() {
        sinon.stub(Hero.prototype, 'checkForInspectableObjects');
        sinon.stub(Hero.prototype, 'initialise');
      });

      afterEach(function() {
        Hero.prototype.checkForInspectableObjects.restore();
        Hero.prototype.initialise.restore();
      });

      it('gets inspectable objects from the map then calls checkForInspectableObjects with those objects', function() {
        var hero = new Hero(0,0);
        var inspectableObjects = [{}];
        var currentMap = { getInspectableObjects: function() {return inspectableObjects;}};
        hero.currentMap = currentMap;

        sinon.spy(currentMap, 'getInspectableObjects');

        hero.inspect();

        assert.ok(hero.checkForInspectableObjects.calledWith(inspectableObjects));
        assert.equal(currentMap.getInspectableObjects.callCount, 1);
      });
  });

  describe('checkInspectCollisionRectangle', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.initialise.restore();
    });

    it('returns a rectangle above the player if lastDirection was up', function() {
      var hero = new Hero(10,10);
      hero.width = 10;
      hero.height = 10;
      hero.lastDirection = 'up';
      var collisionRectangle = hero.getInspectCollisionRectangle();
      var expectedRectangle = {x: 10, y: 0, width: 10, height: 10};

      assert.deepEqual(collisionRectangle, expectedRectangle);
    });

    it('returns a rectangle left of the player if lastDirection was left', function() {
      var hero = new Hero(10,10);
      hero.width = 10;
      hero.height = 10;
      hero.lastDirection = 'left';

      var collisionRectangle = hero.getInspectCollisionRectangle();
      var expectedRectangle = {x: 0, y: 10, width: 10, height: 10};

      assert.deepEqual(collisionRectangle, expectedRectangle);
    });

    it('returns a rectangle below the player if lastDirection was down', function() {
      var hero = new Hero(10,10);
      hero.width = 10;
      hero.height = 10;
      hero.lastDirection = 'down';

      var collisionRectangle = hero.getInspectCollisionRectangle();
      var expectedRectangle = {x: 10, y: 20, width: 10, height: 10};

      assert.deepEqual(collisionRectangle, expectedRectangle);
    });

    it('returns a rectangle right the player if lastDirection was right', function() {
      var hero = new Hero(10,10);
      hero.width = 10;
      hero.height = 10;
      hero.lastDirection = 'right';

      var collisionRectangle = hero.getInspectCollisionRectangle();
      var expectedRectangle = {x: 20, y: 10, width: 10, height: 10};

      assert.deepEqual(collisionRectangle, expectedRectangle);
    });
  });

  describe('checkForInspectableObjects', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'getInspectCollisionRectangle');
      sinon.stub(Hero.prototype, 'initialise');
      sinon.stub(Physics, 'checkCollisionRects');
      sinon.stub(RogueScene.prototype, 'showInspectMenu');
      sinon.stub(RogueScene.prototype, 'initialise');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      Hero.prototype.getInspectCollisionRectangle.restore();
      Hero.prototype.initialise.restore();
      Physics.checkCollisionRects.restore();
      RogueScene.prototype.showInspectMenu.restore();
      RogueScene.prototype.initialise.restore();
      console.error.restore();
    });

    it('calls showInspectMenu for the first object the playerCollisionObject collides with', function() {
      var hero = new Hero(10,10);
      hero.rogueScene = new RogueScene();
      var collisionObjects = [{}];
      var collision = {collision: true};
      var playerCollisionRectangle = { player: 'collisionRectangle'};

      Physics.checkCollisionRects.returns(collision);
      hero.getInspectCollisionRectangle.returns(playerCollisionRectangle);
      hero.checkForInspectableObjects(collisionObjects);

      assert.equal(hero.getInspectCollisionRectangle.callCount, 1);
      assert.ok(Physics.checkCollisionRects.calledWith(playerCollisionRectangle, collisionObjects[0]));
      assert.ok(hero.rogueScene.showInspectMenu.calledWith(collisionObjects[0]));
    });

    it('does not call showInspectMenu if the inspect collision object misses everything', function() {
      var hero = new Hero(10,10);
      hero.rogueScene = new RogueScene();
      var collisionObjects = [{}];
      var collision = {collision: false};
      var playerCollisionRectangle = { player: 'collisionRectangle'};

      Physics.checkCollisionRects.returns(collision);
      hero.getInspectCollisionRectangle.returns(playerCollisionRectangle);
      hero.checkForInspectableObjects(collisionObjects);

      assert.equal(hero.rogueScene.showInspectMenu.callCount, 0);

    });

    it('only inspects the first object if there are multiple collisions', function() {
      var hero = new Hero(10,10);
      hero.rogueScene = new RogueScene();
      var collisionObjects = [{}, {}];
      var collision = {collision: true};
      var playerCollisionRectangle = { player: 'collisionRectangle'};

      Physics.checkCollisionRects.returns(collision);
      hero.getInspectCollisionRectangle.returns(playerCollisionRectangle);
      hero.checkForInspectableObjects(collisionObjects);

      assert.equal(hero.rogueScene.showInspectMenu.callCount, 1);

    });

    it('does not break if the collisionObjects array is invalid', function() {
      var hero = new Hero(10,10);
      hero.rogueScene = new RogueScene();
      var collisionObjects = null;
      hero.checkForInspectableObjects(collisionObjects);

      assert.ok(console.error.calledWith('Invalid collision objects passed to checkForInspectableObjects'));
    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'renderEquipmentSlots');
      sinon.stub(Hero.prototype, 'renderHealthBar');
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.renderEquipmentSlots.restore();
      Hero.prototype.renderHealthBar.restore();
      Hero.prototype.initialise.restore();
    });

    it('gets the camera position and uses that to calcute the heros on screen position', function() {
      var camera = {
        getX: function() { return 100;},
        getY: function() { return 150;}
      };

      sinon.spy(camera, 'getX');
      sinon.spy(camera, 'getY');

      var renderer = {
        getCamera: function() {return camera},
        drawSpriteSheet: function() {}
      };

      var currentAnimation = {
        getSpriteSheetRectangle: function() {return {};}
      };

      sinon.spy(renderer, 'getCamera');

      var hero = new Hero(20, 30);
      hero.spriteSheetRectangle = {x: 0, y: 0, width: 10, height: 10};
      hero.currentAnimation = currentAnimation;
      hero.render(renderer);

      assert.equal(renderer.getCamera.callCount, 1);
    });

    it('calculates the right render position for the hero and calls drawSpriteSheet with that x and y and the current frame position from the animation', function() {
      var camera = {
        getX: function() { return 100;},
        getY: function() { return 150;}
      };

      sinon.spy(camera, 'getX');
      sinon.spy(camera, 'getY');

      var renderer = {
        getCamera: function() {return camera},
        drawSpriteSheet: function() {}
      };

      sinon.spy(renderer, 'drawSpriteSheet');

      var currentFrame = {x: 0, y: 0, width: 75, height: 75};
      var currentAnimation = {
        getSpriteSheetRectangle: function() {return currentFrame;}
      };

      sinon.spy(currentAnimation, 'getSpriteSheetRectangle');

      var expectedX = 20 - 100;
      var expectedY = 30 - 150;

      var hero = new Hero(20, 30);
      hero.currentAnimation = currentAnimation;

      hero.texture = {'a': 'texture'};
      hero.render(renderer);

      assert.ok(renderer.drawSpriteSheet.calledWith(hero.texture,expectedX, expectedY, currentFrame.x, currentFrame.y, currentFrame.width, currentFrame.height));
      assert.ok(currentAnimation.getSpriteSheetRectangle.callCount, 1);
    });
  });

  describe('renderEquipmentSlots', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.initialise.restore();
    });

    it('sets the stroke and fill styles to SLOTBORDERCOLOR and SLOTCOLOR', function() {
      var slotBorderColor = 'Slot Border Color';
      var slotColor = 'SLot Color';

      var renderer = {
        setStrokeStyle: function() {},
        setFillStyle: function() {},
        drawBoundedBox: function() {},
        setGlobalAlpha: function() {}
      };

      sinon.spy(renderer, 'setStrokeStyle');
      sinon.spy(renderer, 'setFillStyle');

      var hero = new Hero();
      hero.SLOTBORDERCOLOR = slotBorderColor;
      hero.SLOTCOLOR = slotColor;

      hero.renderEquipmentSlots(renderer);

      assert.ok(renderer.setStrokeStyle.calledWith(slotBorderColor));
      assert.ok(renderer.setFillStyle.calledWith(slotColor));
    });

    it('renders two bounded boxes at SLOT positions', function() {
      var slotLeftX = 50;
      var slotRightX = 100;
      var slotY = 500;

      var renderer = {
        setStrokeStyle: function() {},
        setFillStyle: function() {},
        drawBoundedBox: function() {},
        setGlobalAlpha: function() {}
      };

      sinon.spy(renderer, 'drawBoundedBox');

      var hero = new Hero();

      hero.SLOTLEFTX = slotLeftX;
      hero.SLOTRIGHTX = slotRightX;
      hero.SLOTY = slotY;

      hero.renderEquipmentSlots(renderer);

      assert.ok(renderer.drawBoundedBox.calledWith(slotLeftX, slotY, hero.SLOTWIDTH, hero.SLOTHEIGHT));
      assert.ok(renderer.drawBoundedBox.calledWith(slotRightX, slotY, hero.SLOTWIDTH, hero.SLOTHEIGHT));
    });

    it('sets globalAlpha of renderer to 0.5, then restores it to 1.0 after the boxes have been rendered', function() {
      var renderer = {
        setStrokeStyle: function() {},
        setFillStyle: function() {},
        drawBoundedBox: function() {},
        setGlobalAlpha: function() {}
      };

      sinon.stub(renderer, 'setGlobalAlpha');

      var hero = new Hero();

      hero.renderEquipmentSlots(renderer);

      assert.ok(renderer.setGlobalAlpha.calledWith(0.5));
      assert.ok(renderer.setGlobalAlpha.calledWith(1.0));
    });

    it('renders the equipment icons of the gear in right hand if they are eqipped', function() {
      var slotLeftX = 50;
      var slotRightX = 100;
      var slotY = 500;
      var renderer = {
        setStrokeStyle: function() {},
        setFillStyle: function() {},
        drawBoundedBox: function() {},
        setGlobalAlpha: function() {}
      };

      var rightHandEquipment = {
        renderAsIcon: function() {}
      };

      sinon.spy(rightHandEquipment, 'renderAsIcon');

      var hero = new Hero();
      hero.equipment = {
        'leftHand': null,
        'rightHand': rightHandEquipment
      };
      hero.SLOTLEFTX = slotLeftX;
      hero.SLOTRIGHTX = slotRightX;
      hero.SLOTY = slotY;

      hero.renderEquipmentSlots(renderer);

      assert.ok(rightHandEquipment.renderAsIcon.calledWith(renderer, slotRightX, slotY));
    });

    it('renders the equipment icons of the gear in left hand if they are eqipped', function() {
      var slotLeftX = 50;
      var slotRightX = 100;
      var slotY = 500;
      var renderer = {
        setStrokeStyle: function() {},
        setFillStyle: function() {},
        drawBoundedBox: function() {},
        setGlobalAlpha: function() {}
      };

      var leftHandEquipment = {
        renderAsIcon: function() {}
      };

      sinon.spy(leftHandEquipment, 'renderAsIcon');

      var hero = new Hero();
      hero.equipment = {
        'leftHand': leftHandEquipment,
        'rightHand': null
      };
      hero.SLOTLEFTX = slotLeftX;
      hero.SLOTRIGHTX = slotRightX;
      hero.SLOTY = slotY;

      hero.renderEquipmentSlots(renderer);

      assert.ok(leftHandEquipment.renderAsIcon.calledWith(renderer, slotLeftX, slotY));
    });
  });

  describe('renderHealthBar', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.initialise.restore();
    });

    it('calls renderBar with the rectangle for it, hp, maxHp, the block color and the background color', function() {
      var renderer = {renderBar: function() {}};
      sinon.spy(renderer, 'renderBar');

      var healthRectangle = {'a': 'rectangle'};
      var hp = 50;
      var maxHp = 100;
      var hpColor = 'ACOLOR';
      var slotBorderColor = 'COLOR';

      var hero = new Hero();
      hero.healthRectangle = healthRectangle;
      hero.hp = hp;
      hero.maxHp = maxHp;
      hero.hpColor = hpColor;
      hero.SLOTBORDERCOLOR = slotBorderColor;

      hero.renderHealthBar(renderer);

      assert.ok(renderer.renderBar.calledWith(healthRectangle, hp, maxHp, hpColor, slotBorderColor));
    });
  });



  describe('takeHit', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'initialise');
      sinon.stub(Hero.prototype, 'move');
      sinon.stub(Actor.prototype, 'takeDamage');
    });

    afterEach(function() {
      Hero.prototype.initialise.restore();
      Hero.prototype.move.restore();
      Actor.prototype.takeDamage.restore();
    });

    it('lastDirection was up then move down on taking a hit', function() {
      var hero = new Hero();
      hero.lastDirection = 'up';
      hero.takeHit();
      assert.ok(hero.move.calledWith(0, 1));
    });

    it('lastDirection was right then move left on taking a hit', function() {
      var hero = new Hero();
      hero.lastDirection = 'right';
      hero.takeHit();
      assert.ok(hero.move.calledWith(-1, 0));
    });

    it('lastDirection was down then move up on taking a hit', function() {
      var hero = new Hero();
      hero.lastDirection = 'down';
      hero.takeHit();
      assert.ok(hero.move.calledWith(0, -1));
    });

    it('lastDirection was left then move right on taking a hit', function() {
      var hero = new Hero();
      hero.lastDirection = 'left';
      hero.takeHit();
      assert.ok(hero.move.calledWith(1, 0));
    });

    it('makes the player take damage equal to the damage passed in', function() {
      var hero = new Hero();
      var damage = 15;
      hero.takeHit(damage);

      assert.ok(hero.takeDamage.calledWith(damage));
    });
  });
});
