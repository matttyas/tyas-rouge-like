var assert = chai.assert;

describe('Projectile', function() {
  describe('initialise', function() {
    beforeEach(function() {
      sinon.stub(Projectile.prototype, 'calculateSpeedAndDirection');
    });

    afterEach(function() {
      Projectile.prototype.calculateSpeedAndDirection.restore();
    });

    it('calls calculateSpeedAndDirection and passes it the user of the throwingKnife', function() {
      var user = {'the thrower': 'of the knife'};
      var projectile = new Projectile({}, user);

      assert.ok(projectile.calculateSpeedAndDirection.calledWith(user));
    });
  });

  describe('move', function() {
    beforeEach(function() {
      sinon.stub(CollisionController, 'checkForCollision');
      sinon.stub(Projectile.prototype, 'initialise');
    });

    afterEach(function() {
      CollisionController.checkForCollision.restore();
      Projectile.prototype.initialise.restore();
    });

    it('checks for potential collisions with both the x and y movements', function() {
      var movementX = 5;
      var movementY = 10;

      var projectile = new Projectile();
      projectile.movementX = movementX;
      projectile.movementY = movementY;
      projectile.speed = 5;
      projectile.move();

      assert.ok(CollisionController.checkForCollision.calledWith(projectile, {x: movementX * projectile.speed, y: 0}));
      assert.ok(CollisionController.checkForCollision.calledWith(projectile, {x:0, y: movementY * projectile.speed}));
    });

    it('moves x and y by the values of this.movmentX and this.movementY * this.speed if not colliding', function() {
      var movementX = 1;
      var movementY = 1;

      var projectile = new Projectile();
      projectile.movementX = movementX;
      projectile.movementY = movementY;
      projectile.speed = 5;
      projectile.x = 15;
      projectile.y = 20;

      projectile.move();
      assert.equal(projectile.x, 15 + movementX * projectile.speed);
      assert.equal(projectile.y, 20 + movementY * projectile.speed);
    });
  });

  describe('calculateSpeedAndDirection', function() {
    beforeEach(function() {
      sinon.stub(Projectile.prototype, 'initialise');
    });

    afterEach(function() {
      Projectile.prototype.initialise.restore();
    });

    it('gets the direction of the user', function() {
      var user = {
        getLastDirection: function() {}
      };
      sinon.spy(user, 'getLastDirection');

      var projectile = new Projectile();
      projectile.calculateSpeedAndDirection(user);

      assert.equal(user.getLastDirection.callCount, 1);
    });

    it('sets the spriteSheetRectangle to projectile.UPSPRITE if direction is up and movement to an up direction', function() {
      var user = {
        getLastDirection: function() {return 'up'}
      };

      var projectile = new Projectile();
      projectile.UPSPRITE = {'up': 'sprite'};

      projectile.calculateSpeedAndDirection(user);

      var expectedMovement = {x: 0, y: -1};
      assert.deepEqual(projectile.spriteSheetRectangle, projectile.UPSPRITE);
      assert.deepEqual(projectile.movement, expectedMovement);
    });

    it('sets the spriteSheetRectangle to projectile.RIGHTSPRITE if direction is right and movement to a rightwards direction', function() {
      var user = {
        getLastDirection: function() {return 'right'}
      };

      var projectile = new Projectile();
      projectile.RIGHTSPRITE = {'right': 'sprite'};

      projectile.calculateSpeedAndDirection(user);

      var expectedMovement = {x: 1, y: 0};
      assert.deepEqual(projectile.spriteSheetRectangle, projectile.RIGHTSPRITE);
      assert.deepEqual(projectile.movement, expectedMovement);
    });

    it('sets the spriteSheetRectangle to projectile.DOWNSPRITE if direction is down and movement to a downwards direction', function() {
      var user = {
        getLastDirection: function() {return 'down'}
      };

      var projectile = new Projectile();
      projectile.DOWNSPRITE = {'down': 'sprite'};

      projectile.calculateSpeedAndDirection(user);

      var expectedMovement = {x: 0, y: 1};
      assert.deepEqual(projectile.spriteSheetRectangle, projectile.DOWNSPRITE);
      assert.deepEqual(projectile.movement, expectedMovement);
    });

    it('sets the spriteSheetRectangle to projectile.LEFTSPRITE if direction is left and movement to a leftwards direction', function() {
      var user = {
        getLastDirection: function() {return 'left'}
      };

      var projectile = new Projectile();
      projectile.LEFTSPRITE = {'left': 'sprite'};

      projectile.calculateSpeedAndDirection(user);

      var expectedMovement = {x: -1, y: 0};
      assert.deepEqual(projectile.spriteSheetRectangle, projectile.LEFTSPRITE);
      assert.deepEqual(projectile.movement, expectedMovement);
    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(Projectile.prototype, 'initialise');
    });

    afterEach(function() {
      Projectile.prototype.initialise.restore();
    });

    it('it draws a specific part of the projectiles sprite sheet', function() {
      var camera = {
        getX: function() { return 5;},
        getY: function() { return 10;}
      };

      var renderer = {
        getCamera: function() { return camera},
        drawSpriteSheet: function() {}
      };

      sinon.spy(renderer, 'drawSpriteSheet');
      sinon.spy(renderer, 'getCamera');

      var texture = {'a': 'texture'};
      var spriteSheetRectangle = {x: 5, y: 10, width: 15, height: 20};

      var projectile = new Projectile();
      projectile.x = 0;
      projectile.y = 0;
      projectile.texture = texture;
      projectile.spriteSheetRectangle = spriteSheetRectangle;

      projectile.render(renderer);
      assert.equal(renderer.getCamera.callCount, 1);
      assert.ok(renderer.drawSpriteSheet.calledWith(projectile.texture, -5, -10, projectile.spriteSheetRectangle.x, projectile.spriteSheetRectangle.y, projectile.spriteSheetRectangle.width, projectile.spriteSheetRectangle.height));
    });
  });

  describe('applyCollision', function() {
    beforeEach(function() {
      sinon.stub(Projectile.prototype, 'initialise');
      sinon.stub(Projectile.prototype, 'alterMovementForCollision');
    });

    afterEach(function() {
      Projectile.prototype.initialise.restore();
      Projectile.prototype.alterMovementForCollision.restore();
    });

    it ('calls take hit on the actor it has hit, passing in its damage', function() {
      var projectile = new Projectile();
      var damage = 15;
      projectile.damage = damage;

      var actor = {
        takeHit: function() {},
        id: 8
      };

      sinon.spy(actor, 'takeHit');

      projectile.applyCollision(actor, {}, {});

      assert.ok(actor.takeHit.calledWith(damage));
    });

    it('does not hit the actor which projected it', function() {
      var projectile = new Projectile();
      var damage = 15;
      var userId = 3;
      projectile.damage = damage;
      projectile.userId = 3;

      var actor = {
        takeHit: function() {},
        id: userId
      };

      sinon.spy(actor, 'takeHit');

      projectile.applyCollision(actor, {}, {});

      assert.equal(actor.takeHit.callCount, 0);
    });

    it('calls alterMovementForCollision if we have not collided with the enivronment already', function() {
      var projectile = new Projectile();
      projectile.collidedWithEnvironment = false;
      var actor = {takeHit: function() {}, id: 15};
      projectile.id = 1;

      projectile.applyCollision(actor, {}, {});

      assert.equal(projectile.alterMovementForCollision.callCount, 1);
    });

    it('does not call alterMovementForCollision if it has already been called', function() {
      var projectile = new Projectile();
      projectile.collidedWithEnvironment = true;
      var actor = {takeHit: function() {}, id: 15};
      projectile.id = 1;

      projectile.applyCollision(actor, {}, {});

      assert.equal(projectile.alterMovementForCollision.callCount, 0);
    });
  });
});
