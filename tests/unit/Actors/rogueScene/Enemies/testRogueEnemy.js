var assert = chai.assert;

describe('Rogue Enemy', function() {
  describe('initialise', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy.prototype, 'initialiseStats');
      sinon.stub(RogueEnemy.prototype, 'initialiseAnimations');
      sinon.stub(RogueEnemy.prototype, 'setupAI');
      sinon.stub(IdGenerator, 'generateId');
    });

    afterEach(function() {
      RogueEnemy.prototype.initialiseStats.restore();
      RogueEnemy.prototype.initialiseAnimations.restore();
      RogueEnemy.prototype.setupAI.restore();
      IdGenerator.generateId.restore();
    });

    it('gets the enemy name from enemyData then uses that in it\'s texture path', function() {
      var name = 'Atolm Dragon';
      var enemyData = {
        getProperty: function() {return name;}
      };
      sinon.spy(enemyData, 'getProperty');

      var rogueEnemy = new RogueEnemy(null, enemyData);

      assert.ok(enemyData.getProperty.calledWith('name'));
      assert.equal(rogueEnemy.name, name);
      assert.equal(rogueEnemy.texturePath, 'content/enemies/' + name + '.png');
    });

    it('calls initialiseStats to setup the stats for the current enemy', function() {
      var enemyData = {
        getProperty: function() {}
      };

      var rogueEnemy = new RogueEnemy(null, enemyData);
      assert.equal(rogueEnemy.initialiseStats.callCount, 1);
    });

    it('calls initialiseAnimations to build the set of animations for the current enemy', function() {
      var enemyData = {
        getProperty: function() {}
      };

      var rogueEnemy = new RogueEnemy(null, enemyData);
      assert.equal(rogueEnemy.initialiseAnimations.callCount, 1);
    });

    it('calls setupAI to create the ai for the current enemy', function() {
      var enemyData = {
        getProperty: function() {}
      };

      var rogueEnemy = new RogueEnemy(null, enemyData);
      assert.equal(rogueEnemy.setupAI.callCount, 1);
    });

    it('gets an id from the IdGenerator', function() {
      var id = 5;
      IdGenerator.generateId.returns(id);
      var enemyData = {
        getProperty: function() {}
      };

      var rogueEnemy = new RogueEnemy(null, enemyData);
      assert.equal(IdGenerator.generateId.callCount, 1);
      assert.equal(rogueEnemy.id, id);
    })
  });

  describe('initialiseStats', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy.prototype, 'initialise');
    });

    afterEach(function() {
      RogueEnemy.prototype.initialise.restore();
    });

    it('sets the maxHp and hp of the enemy', function() {
      var rogueEnemy = new RogueEnemy();
      rogueEnemy.initialiseStats();

      assert.ok(rogueEnemy.hp);
      assert.ok(rogueEnemy.maxHp);
    });

    it('sets up a block of stats for the enemy', function() {
      var rogueEnemy = new RogueEnemy();
      rogueEnemy.initialiseStats();

      assert.ok(rogueEnemy.stats);
    });
  });

  describe('update', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy.prototype, 'initialise');
    });

    afterEach(function() {
      RogueEnemy.prototype.initialise.restore();
    });

    it('runs the currentAnimation, then sets the current spriteSheetRectangle from it if one is set', function() {
      var currentFrame = {'a': 'rectangle'};
      var currentAnimation = {
        animate: function() {},
        getSpriteSheetRectangle: function() {return currentFrame;}
      };

      var ai = {
        act: function() {}
      };

      sinon.spy(currentAnimation, 'animate');
      sinon.spy(currentAnimation, 'getSpriteSheetRectangle');

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.currentAnimation = currentAnimation;
      rogueEnemy.ai = ai;

      rogueEnemy.update();

      assert.equal(currentAnimation.animate.callCount, 1);
      assert.equal(currentAnimation.getSpriteSheetRectangle.callCount, 1);
      assert.deepEqual(rogueEnemy.spriteSheetRectangle, currentFrame);
    });

    it('stops the ai from acting if dontUpdate is set', function() {
      var ai = {
        act: function() {}
      };

      sinon.spy(ai, 'act');

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.ai = ai;
      rogueEnemy.dontUpdate = true;

      rogueEnemy.update();
      assert.equal(ai.act.callCount, 0);
    });

    it('otherwise tells the ai to act', function() {
      var ai = {
        act: function() {}
      };

      sinon.spy(ai, 'act');

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.ai = ai;
      rogueEnemy.dontUpdate = false;

      rogueEnemy.update();
      assert.equal(ai.act.callCount, 1);
    });
  });

  describe('die', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy.prototype, 'initialise');
    });

    afterEach(function() {
      RogueEnemy.prototype.initialise.restore();
    });

    it('sets don\'t update to true and sets the currentAnimation to be deathAnimation', function() {
      var deathAnimation = {'death': 'animation'};

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.dontUpdate = false;
      rogueEnemy.deathAnimation = deathAnimation;

      rogueEnemy.die();

      assert.ok(rogueEnemy.dontUpdate);
      assert.deepEqual(rogueEnemy.currentAnimation, rogueEnemy.deathAnimation);
    });
  });

  describe('lookForTargets', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy.prototype, 'initialise');
      sinon.stub(Physics, 'distanceBetweenPoints');
    });

    afterEach(function() {
      RogueEnemy.prototype.initialise.restore();
      Physics.distanceBetweenPoints.restore();
    });

    it('goes through each foe the rogueEnemy has and gets the distance between them and itself', function() {
      var foe1 = {x: 10, y: 15};
      var foe2 = {x: 15, y: 20};
      var foes = [foe1, foe2];

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.foes = foes;

      rogueEnemy.lookForTargets();

      assert.equal(Physics.distanceBetweenPoints.callCount, foes.length);
      assert.ok(Physics.distanceBetweenPoints.calledWith({x: rogueEnemy.x, y: rogueEnemy.y}, {x: foe1.x, y: foe1.y}));
      assert.ok(Physics.distanceBetweenPoints.calledWith({x: rogueEnemy.x, y: rogueEnemy.y}, {x: foe2.x, y: foe2.y}));
    });

    it('sets the closest foe in range to be its current target', function() {
      var foe1 = {x: 100, y: 100};
      var foe2 = {x: 100, y: 200};
      var foes = [foe1, foe2];

      Physics.distanceBetweenPoints.onCall(0).returns(200);
      Physics.distanceBetweenPoints.onCall(1).returns(300);

      var rogueEnemy = new RogueEnemy();
      rogueEnemy.x = 0;
      rogueEnemy.y = 0;
      rogueEnemy.foes = foes;
      rogueEnemy.detectionRange = 500;

      rogueEnemy.lookForTargets();

      assert.deepEqual(rogueEnemy.currentTarget, foe1);
    });
  });
});
