var assert = chai.assert;

describe('Rogue Entrance', function() {

  describe('initialise', function() {
    it('sets classification to equal entranceData.getClassification()', function() {
      var classification = 'a'
      var entranceData = {
        getClassification: function() { return classification;},
        getId: function() {},
        getTargetMapId: function() {},
        getTargetEntranceId: function() {}
      };

      var rogueEntrance = new RogueEntrance(0, 0, entranceData);

      assert.equal (rogueEntrance.getClassification(), classification);
    });

    it ('sets id to equal entranceData.getId()', function() {
      var id = 'id';
      var entranceData = {
        getClassification: function() {},
        getId: function() {return id;},
        getTargetMapId: function() {},
        getTargetEntranceId: function() {}
      };

      var rogueEntrance = new RogueEntrance(0, 0, entranceData);

      assert.equal (rogueEntrance.getId(), id);
    });

    it ('sets targetMapId to equal entranceData.getTargetMapId()', function() {
      var targetMapId = 'targetMapId';
      var entranceData = {
        getClassification: function() {},
        getId: function() {},
        getTargetMapId: function() {return targetMapId;},
        getTargetEntranceId: function() {}
      };

      var rogueEntrance = new RogueEntrance(0, 0, entranceData);

      assert.equal (rogueEntrance.getTargetMapId(), targetMapId);
    });

    it ('sets targetEntranceId to equal entranceData.getTargetEntranceId', function() {
      var targetEntranceId = 'targetEntranceId';
      var entranceData = {
        getClassification: function() {},
        getId: function() {},
        getTargetMapId: function() {},
        getTargetEntranceId: function() {return targetEntranceId;}
      };

      var rogueEntrance = new RogueEntrance(0, 0, entranceData);

      assert.equal (rogueEntrance.getTargetEntranceId(), targetEntranceId);
    });

    it('sets name equal to entranceData.getClassification and then uses that to build the right path', function() {
      var classification = 'classification';
      var entranceData = {
        getClassification: function() {return classification;},
        getId: function() {},
        getTargetMapId: function() {},
        getTargetEntranceId: function() {}
      };

      sinon.spy(entranceData, 'getClassification');

      var rogueEntrance = new RogueEntrance(0,0, entranceData);

      assert.equal(entranceData.getClassification.callCount, 1);
      assert.equal(rogueEntrance.name, classification);
      assert.equal(rogueEntrance.texturePath, 'content/dungeon/Entrances/classification.png');
    });
  });

  describe('exitArea', function() {
    beforeEach(function() {
      sinon.stub(console, 'log');
      sinon.stub(RogueEntrance.prototype, 'initialise');
    });

    afterEach(function() {
      console.log.restore();
      RogueEntrance.prototype.initialise.restore();
    });

    it('tells the current scene to go to the entrance with targetEntranceId on the map with targetMapId', function() {
      var currentScene = { goToMap: function() {}};
      sinon.spy(currentScene, 'goToMap');
      var targetMapId = 1000;
      var targetEntranceId = 1;

      var rogueEntrance = new RogueEntrance({});
      rogueEntrance.targetMapId = 1000;
      rogueEntrance.targetEntranceId = 1;
      rogueEntrance.currentScene = currentScene;

      rogueEntrance.exitArea();

      assert.ok(currentScene.goToMap.calledWith(targetMapId, targetEntranceId));
    });

    it('if either of targetMapId or targetEntranceId are unset it returns a log', function() {
      var rogueEntrance = new RogueEntrance({});
      rogueEntrance.exitArea();

      rogueEntrance.targetMapId = 5;
      rogueEntrance.exitArea();

      rogueEntrance.targetMapId = undefined;
      rogueEntrance.targetEntranceId = 1;
      rogueEntrance.exitArea();

      assert.equal(console.log.callCount, 3);
    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(RogueEntrance.prototype, 'initialise');
      sinon.stub(Actor.prototype, 'renderTexture');
    });

    afterEach(function() {
      RogueEntrance.prototype.initialise.restore();
      Actor.prototype.renderTexture.restore();
    });

    it('calls Actor.renderTexture if texture is set', function() {
      var rogueEntrance = new RogueEntrance();
      var texture = {'a': 'texture'};
      var renderer = 'renderer';
      rogueEntrance.texture = texture;
      rogueEntrance.render(renderer);

      assert.equal(rogueEntrance.renderTexture.callCount, 1);
      assert.ok(rogueEntrance.renderTexture.calledWith(renderer));
    });

    it('otherwise sets the fill color and renders a rectangle in that color at the correct place in the level', function() {
      var rogueEntrance = new RogueEntrance();
      rogueEntrance.x = 200;
      rogueEntrance.y = 300;
      rogueEntrance.width = 100;
      rogueEntrance.height = 50;
      rogueEntrance.color = 'rgb(255,0,0)';

      var cameraX = 100;
      var cameraY = 150;

      var camera = {
        getX: function() { return cameraX },
        getY: function() { return cameraY }
      };

      var renderer = {
        setFillStyle: function() {},
        fillRect: function() {},
        getCamera: function() { return camera}
      };

      sinon.spy(renderer, 'setFillStyle');
      sinon.spy(renderer, 'fillRect');

      var expectedCameraX = rogueEntrance.x - cameraX;
      var expectedCameraY = rogueEntrance.y - cameraY;

      rogueEntrance.render(renderer);

      assert.ok(renderer.setFillStyle.calledWith(rogueEntrance.color));
      assert.ok(renderer.fillRect, expectedCameraX, expectedCameraY, rogueEntrance.width, rogueEntrance.height);
    });
  });
});
