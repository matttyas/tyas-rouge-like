var assert = chai.assert;

describe('Inspect Menu', function() {
  beforeEach(function() {
    sinon.stub(InspectMenu.prototype, 'initialisePosition');
  });

  afterEach(function() {
    InspectMenu.prototype.initialisePosition.restore();
  });

  describe('initialise', function() {
    it('it sets inspectActions by getting them from the target', function() {
      var inspectActions = ['inspect', 'actions'];
      var target = { getInspectActions: function () {return inspectActions}};
      sinon.spy(target, 'getInspectActions');
      var inspectMenu = new InspectMenu(target);

      assert.equal(target.getInspectActions.callCount, 1);

      var expected = ['inspect', 'actions', 'Cancel'];
      assert.deepEqual(inspectMenu.inspectActions, expected);
    });

    it('It calls initialisePosition to set initial positions + dimensions', function() {
      var target = { getInspectActions: function () {return []}};
      sinon.spy(target, 'getInspectActions');
      var inspectMenu = new InspectMenu(target);

      assert.equal(inspectMenu.initialisePosition.callCount, 1);
    });
  });

  describe('handleInput', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'closeSelf');
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'increaseSelectedActionIndex');
      sinon.stub(InspectMenu.prototype, 'decreaseSelectedActionIndex');
      sinon.stub(InspectMenu.prototype, 'selectAction');
    });

    afterEach(function() {
      InspectMenu.prototype.closeSelf.restore();
      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.increaseSelectedActionIndex.restore();
      InspectMenu.prototype.decreaseSelectedActionIndex.restore();
      InspectMenu.prototype.selectAction.restore();
    });

    it('Calls closeSelf if the closeKey is down', function() {
      var inspectMenu = new InspectMenu();
      var keysDown = {};
      keysDown[inspectMenu.closeKey] = true;
      inspectMenu.handleKeyboard(keysDown, {});
      assert.equal(inspectMenu.closeSelf.callCount, 1);
    });

    it('Calls increaseSelectedActionIndex if the upKey is pressed', function() {
      var inspectMenu = new InspectMenu();
      var keysDown = {};
      keysDown[inspectMenu.upKey] = true;
      inspectMenu.handleKeyboard(keysDown, {});
      assert.equal(inspectMenu.increaseSelectedActionIndex.callCount, 1);
    });

    it('Calls decreaseSelectedActionIndex if the downKey is pressed', function() {
      var inspectMenu = new InspectMenu();
      var keysDown = {};
      keysDown[inspectMenu.downKey] = true;
      inspectMenu.handleKeyboard(keysDown, {});
      assert.equal(inspectMenu.decreaseSelectedActionIndex.callCount, 1);
    });

    it('Calls selectAction if the selectKey is pressed', function() {
      var inspectMenu = new InspectMenu();
      var keysDown = {};
      keysDown[inspectMenu.selectKey] = true;
      inspectMenu.handleKeyboard(keysDown, {});
      assert.equal(inspectMenu.selectAction.callCount, 1);
    });
  });

  describe('closeSelf', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
    });

    it('tells it\'s parent scene to close this', function() {
      var currentScene = {closeCurrentMenu: function() {}};
      sinon.spy(currentScene, 'closeCurrentMenu');

      var inspectMenu = new InspectMenu();
      inspectMenu.setCurrentScene(currentScene);
      inspectMenu.closeSelf();

      assert.equal(currentScene.closeCurrentMenu.callCount, 1);
    });

    it('returns an error if currentScene is not set and this is called', function() {
      sinon.stub(console, 'error');

      var inspectMenu = new InspectMenu();
      inspectMenu.closeSelf();

      assert.ok(console.error.calledWith('Trying to close menu without having scene set!'));
      console.error.restore();
    });
  });

  describe('increaseSelectedActionIndex', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'hasNoActions');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.hasNoActions.restore();
      console.error.restore();
    });

    it('increases selectedActionIndex by one', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = ['a', 'b', 'c'];
      inspectMenu.selectedActionIndex = 0;
      inspectMenu.hasNoActions.returns(false);

      inspectMenu.increaseSelectedActionIndex();
      assert.equal(inspectMenu.selectedActionIndex, 1);
    });

    it('Wraps around if the new selectedActionIndex is equal to the length of the actions array', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = ['a', 'b', 'c'];
      inspectMenu.selectedActionIndex = 2;
      inspectMenu.hasNoActions.returns(false);

      inspectMenu.increaseSelectedActionIndex();
      assert.equal(inspectMenu.selectedActionIndex, 0);
    });

    it('Returns an error if the InspectMenu has no actions', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.hasNoActions.returns(true);

      inspectMenu.increaseSelectedActionIndex();
      assert.ok(console.error.calledWith('No actions added. Cannot change selectedActionIndex'));
    });
  });

  describe('decreaseSelectedActionIndex', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'hasNoActions');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.hasNoActions.restore();
      console.error.restore();
    });

    it('decreases selectedActionIndex by one', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = ['a', 'b', 'c'];
      inspectMenu.selectedActionIndex = 2;
      inspectMenu.hasNoActions.returns(false);

      inspectMenu.decreaseSelectedActionIndex();
      assert.equal(inspectMenu.selectedActionIndex, 1);
    });

    it('Wraps around if the new selectedActionIndex is less than zero', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = ['a', 'b', 'c'];
      inspectMenu.selectedActionIndex = 0;
      inspectMenu.hasNoActions.returns(false);

      inspectMenu.decreaseSelectedActionIndex();
      assert.equal(inspectMenu.selectedActionIndex, inspectMenu.inspectActions.length - 1);
    });

    it('Returns an error if the InspectMenu has no actions', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.hasNoActions.returns(true);

      inspectMenu.decreaseSelectedActionIndex();
      assert.ok(console.error.calledWith('No actions added. Cannot change selectedActionIndex'));
    });
  });

  describe('selectAction', function(){
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'closeSelf');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.closeSelf.restore();
    });

    it('calls close self if the selected action is the last in inspectActions', function() {
      var target = {takeAction: function() {}};
      sinon.spy(target, 'takeAction');

      var actions = ['a', 'b', 'Cancel'];
      var inspectMenu = new InspectMenu();

      inspectMenu.selectedActionIndex = actions.length - 1;
      inspectMenu.inspectActions = actions;
      inspectMenu.target = target;

      inspectMenu.selectAction();

      assert.equal(inspectMenu.closeSelf.callCount, 1);
      assert.equal(target.takeAction.callCount, 0);
    });

    it('closes the menu and calls takeAction on the target and passes it the action to take otherwise', function() {
      var target = { takeAction: function() {}};
      sinon.spy(target, 'takeAction');

      var actions = ['a', 'b', 'Cancel'];
      var inspectMenu = new InspectMenu();
      var actionIndex = 1;

      inspectMenu.selectedActionIndex = actionIndex;
      inspectMenu.inspectActions = actions;
      inspectMenu.target = target;

      inspectMenu.selectAction();

      assert.ok(target.takeAction.calledWith(inspectMenu.inspectActions[actionIndex]));
      assert.equal(inspectMenu.closeSelf.callCount, 1);
    });
  });

  describe('hasNoActions', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
    });

    it('returns true if inspectActions is empty', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = [];
      assert.ok(inspectMenu.hasNoActions());
    });

    it('returns true if there is at least one action', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.inspectActions = [ 'action' ];
      assert.ok(!inspectMenu.hasNoActions());
    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'renderBackPanel');
      sinon.stub(InspectMenu.prototype, 'renderInspectActions');
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.renderBackPanel.restore();
      InspectMenu.prototype.renderInspectActions.restore();
      InspectMenu.prototype.initialise.restore();
    });

    it('renders the back panel of the menu', function() {
      var renderer = { render: 'er'};
      var ctx = {};

      var inspectMenu = new InspectMenu();
      inspectMenu.render(ctx, renderer);

      assert.ok(inspectMenu.renderBackPanel.calledWith(ctx, renderer));
    });

    it('calls render inspect actions', function() {
      var renderer = { render: 'er'};
      var ctx = {};

      var inspectMenu = new InspectMenu();
      inspectMenu.render(ctx, renderer);

      assert.ok(inspectMenu.renderInspectActions.calledWith(ctx, renderer));
    });
  });

  describe('renderBackPanel', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
    });

    it('sets the render fillStyle to backColor then calls renderer fillRect', function() {
      var renderer = {
        setFillStyle: function() {},
        fillRect: function() {}
      };
      sinon.spy(renderer, 'setFillStyle');
      sinon.spy(renderer, 'fillRect');

      var inspectMenu = new InspectMenu();
      inspectMenu.renderBackPanel({}, renderer);

      assert.ok(renderer.setFillStyle.calledWith(inspectMenu.backColor));
      assert.ok(renderer.fillRect.calledWith(inspectMenu.x, inspectMenu.y, inspectMenu.width, inspectMenu.height));
    });
  });

  describe('renderInspectActions', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'renderSelectCursor');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.renderSelectCursor.restore();
    });

    it('calls draw text once for each inspectAction the InspectMenu has', function() {
      var renderer = {
        setTextColor: function() {},
        setTextSize: function() {},
        drawText: function() {}
      };

      sinon.spy(renderer, 'setTextColor');
      sinon.spy(renderer, 'setTextSize');
      sinon.spy(renderer, 'drawText');

      var inspectMenu = new InspectMenu();
      var inspectActions = ['act', 'ion'];

      inspectMenu.inspectActions = inspectActions;

      inspectMenu.renderInspectActions({}, renderer);

      assert.ok(renderer.setTextColor.calledWith(inspectMenu.textColor));
      assert.ok(renderer.setTextSize.calledWith(inspectMenu.textSize));
      assert.equal(renderer.drawText.callCount, inspectActions.length);
    });
  });

  describe('renderSelectCursor', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.initialise.restore();
    });

    it('calculates where to draw the cursor based on the selectedActionIndex', function() {
      var inspectMenu = new InspectMenu();
      inspectMenu.x = 5;
      inspectMenu.y = 5;
      inspectMenu.width = 20;
      inspectMenu.selectedActionIndex = 1;

      var expectedX = inspectMenu.x + 5;
      var expectedY = inspectMenu.y + 5 + (inspectMenu.selectedActionIndex * 35);
      var expectedWidth = inspectMenu.width - 10;
      var expectedHeight = 30;

      var renderer = {
        setFillStyle: function() {},
        fillRect: function() {}
      };

      sinon.spy(renderer, 'setFillStyle');
      sinon.spy(renderer, 'fillRect');

      inspectMenu.renderSelectCursor({}, renderer);

      assert.ok(renderer.setFillStyle.calledWith(inspectMenu.highlightColor));
      assert.ok(renderer.fillRect.calledWith(expectedX, expectedY, expectedWidth, expectedHeight));

    });
  });
});
