var assert = chai.assert;

describe('Rogue Item', function() {
  describe('initialise', function() {
     it('sets its name from the properties', function() {
       var name = 'Item Name';
       var itemData = {getProperty: function() { return name}};
       sinon.spy(itemData, 'getProperty');

       var rogueItem = new RogueItem(itemData);
       assert.ok(itemData.getProperty.calledWith('name'));
       assert.equal(rogueItem.name, name);
     });

     it('sets texturePath to equal content/items/itemData.name.png', function() {
       var name = 'Item Name';
       var itemData = {getProperty: function() { return name}};
       var rogueItem = new RogueItem(itemData);

       assert.equal(rogueItem.texturePath, 'content/items/Item Name.png');
     });

     it('gets the property class and sets it as this.class on the RogueItem', function() {
       var aClass = 'ItemClass';
       var itemData = {getProperty: function() { return aClass}};
       sinon.spy(itemData, 'getProperty');

       var rogueItem = new RogueItem(itemData);
       assert.ok(itemData.getProperty.calledWith('class'));
       assert.equal(rogueItem.class, aClass);
     });
  });

  describe('use', function() {
    beforeEach(function() {
      sinon.stub(Projectile.prototype, 'initialise');
      sinon.stub(Projectile.prototype, 'processContent');
      sinon.stub(RogueItem.prototype, 'initialise');
    });

    afterEach(function() {
      Projectile.prototype.initialise.restore();
      Projectile.prototype.processContent.restore();
      RogueItem.prototype.initialise.restore();
    });

    it('if item is of type Projectile it creates an instance of the class this.class and adds it to the rogueScene', function() {
      var rogueItem = new RogueItem();
      var texture = {'a': 'texture'};
      var rogueScene = {
        addProjectile: function() {}
      };
      sinon.spy(rogueScene, 'addProjectile');

      var user = {'the': 'user'};

      rogueItem.class = Projectile;
      rogueItem.type = 'Projectile';
      rogueItem.texture = texture;

      rogueItem.use(rogueScene, user);
      assert.equal(Projectile.prototype.initialise.callCount, 1);
      assert.ok(Projectile.prototype.initialise.calledWith(rogueScene, user));
      assert.ok(Projectile.prototype.processContent.calledWith(rogueItem.texture));
      assert.equal(rogueScene.addProjectile.callCount, 1);
    });
  });
});
