var assert = chai.assert;

describe('Rogue Vertical Door', function() {
  describe('useOpenRender', function() {
    beforeEach(function() {
      sinon.stub(RogueVerticalDoor.prototype, 'initialise');
    });

    afterEach(function() {
      RogueVerticalDoor.prototype.initialise.restore();
    });

    it('sets width to OPENHEIGHT and updates its colliderObject', function() {
      var rogueVerticalDoor = new RogueVerticalDoor({x: 1, y: 1}, {});
      var hitBoxes = [{width: 50}];
      rogueVerticalDoor.colliderObject = {getHitBoxes: function() { return hitBoxes; }};
      rogueVerticalDoor.useOpenRender();

      assert.equal(rogueVerticalDoor.colliderObject.getHitBoxes()[0].height, rogueVerticalDoor.OPENHEIGHT);
    });
  });

  describe('useCloseRender', function() {
    beforeEach(function() {
      sinon.stub(RogueVerticalDoor.prototype, 'initialise');
    });

    afterEach(function() {
      RogueVerticalDoor.prototype.initialise.restore();
    });
    
    it('sets width to CLOSEDHEIGHT and updates its colliderObject', function() {
      var rogueVerticalDoor = new RogueVerticalDoor({x: 1, y: 1}, {});
      var hitBoxes = [{width: 50}];
      rogueVerticalDoor.colliderObject = {getHitBoxes: function() { return hitBoxes; }};
      rogueVerticalDoor.useCloseRender();

      assert.equal(rogueVerticalDoor.colliderObject.getHitBoxes()[0].height, rogueVerticalDoor.CLOSEDHEIGHT);
    });
  });
});
