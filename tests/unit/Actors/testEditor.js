var assert = chai.assert;

describe('Editor', function() {
  describe('handleKeyboard', function() {
    beforeEach(function() {
      sinon.stub(Editor.prototype, 'handleCameraInput');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      Editor.prototype.handleCameraInput.restore();
      console.error.restore();
    });

    it('When inputState is set to cameraInputState it passes keyboard input to the camera', function() {
      var editor = new Editor();
      editor.inputState = editor.cameraInputState;
      editor.handleKeyboard({}, {});
      assert.equal(editor.handleCameraInput.callCount, 1);
    });

    it('tells the currentScene to go up a floor when the q key is pressed', function() {
      var keysDown = {'q': true};
      var currentScene = { goUpAFloor: function() {}};
      sinon.spy(currentScene, 'goUpAFloor');

      var editor = new Editor();
      editor.currentScene = currentScene;
      editor.handleKeyboard(keysDown, {});

      assert.equal(currentScene.goUpAFloor.callCount, 1);
    });

    it('tells the currentScene to go down a floor when the e key is pressed', function() {
      var keysDown = {'e': true};
      var currentScene = { goDownAFloor: function() {}};
      sinon.spy(currentScene, 'goDownAFloor');

      var editor = new Editor();
      editor.currentScene = currentScene;
      editor.handleKeyboard(keysDown, {});

      assert.equal(currentScene.goDownAFloor.callCount, 1);
    });

    it ('throws an error if currentScene is not set and either q or e is pressed', function() {
      var keysDownE = {'e': true};
      var keysDownQ = {'q': true};
      var editor = new Editor();
      editor.handleKeyboard(keysDownE, {});
      editor.handleKeyboard(keysDownQ, {});
      assert.equal(console.error.callCount, 2);
    });
  });
});
