var assert = chai.assert;

describe('Collider Object', function() {
  describe('initialise', function() {
    it('if it gets a width and height it adds its first hitbox', function() {
      var x = 10;
      var y = 20;
      var width = 30;
      var height = 40;

      var expectedHitBox = {x: x, y: y, width: width, height: height};
      var colliderObject = new ColliderObject({ }, x, y, width, height);

      assert.equal(colliderObject.hitBoxes.length, 1);
      assert.deepEqual(colliderObject.hitBoxes[0], expectedHitBox);
    });

    it('if it gets no width or no height it does not add a hitbox', function() {
      var colliderObject = new ColliderObject({ }, 10, 20);
      assert.equal(colliderObject.hitBoxes.length, 0);
    });
  });

  describe('addHitBox', function() {
    beforeEach(function() {
      sinon.stub(console, 'error');
      sinon.stub(ColliderObject.prototype, 'initialise');
    });

    afterEach(function() {
      console.error.restore();
      ColliderObject.prototype.initialise.restore();
    });

    it('adds a new hit box if x, y, width and height are set', function() {
      var hitBox = {x: 5, y: 10, width: 15, height: 20};
      var colliderObject = new ColliderObject();
      colliderObject.hitBoxes = [];

      colliderObject.addHitBox(hitBox);
      var expectedHitBoxes = [ hitBox ];
      assert.deepEqual(colliderObject.hitBoxes, expectedHitBoxes);
    });

    it('returns an error if any of x, y, width and height aren\'t set', function() {
      var hitBox = {y: 10, width: 15, height: 20};
      var colliderObject = new ColliderObject();

      colliderObject.addHitBox(hitBox);

      hitBox = {x: 5, width: 15, height: 20};
      colliderObject.addHitBox(hitBox);

      hitBox = {x: 5, y: 10, height: 20};
      colliderObject.addHitBox(hitBox);

      hitBox = {x: 5, y: 10, width: 15};
      colliderObject.addHitBox(hitBox);

      assert.equal(console.error.callCount, 4);
    });
  });

  describe('checkForCollision', function() {
    beforeEach(function() {
      sinon.stub(ColliderObject.prototype, 'initialise');
      sinon.stub(ColliderObject.prototype, 'checkForCollisionWithHitBox');
    });

    afterEach(function() {
      ColliderObject.prototype.initialise.restore();
      ColliderObject.prototype.checkForCollisionWithHitBox.restore();
    });

    it('checks for a collision with all hitboxes with the passed in colliderObject and returns {collision: false} if there are none', function() {
      var checkRectangle = {'other': 'collisionObject'};
      var hitBoxes = ['1', '2', '3'];
      var expectedCollision = { collision: false};

      var colliderObject = new ColliderObject();
      colliderObject.hitBoxes = hitBoxes;
      colliderObject.checkForCollisionWithHitBox.returns({collision: false});

      var collision = colliderObject.checkForCollision(checkRectangle);

      assert.equal(colliderObject.checkForCollisionWithHitBox.callCount, hitBoxes.length);
      assert.ok(colliderObject.checkForCollisionWithHitBox.calledWith(checkRectangle, hitBoxes[0]));
      assert.deepEqual(collision, expectedCollision);
    });

    it('returns only the first successful collision with a hitbox', function() {
      var hitBoxes = ['a', 'b', 'c'];
      var hitboxCollision = { collision: true };
      var colliderObject = new ColliderObject();
      colliderObject.hitBoxes = hitBoxes;

      colliderObject.checkForCollisionWithHitBox.returns({collision: false});
      colliderObject.checkForCollisionWithHitBox.onCall(1).returns(hitboxCollision);

      var collision = colliderObject.checkForCollision({});
      assert.equal(colliderObject.checkForCollisionWithHitBox.callCount, 2);
      assert.deepEqual(collision, hitboxCollision);
    });
  });

  describe('checkForCollisionWithHitBox', function() {
    beforeEach(function() {
      sinon.stub(Physics, 'checkCollisionRects');
      sinon.stub(ColliderObject.prototype, 'initialise');
    });

    afterEach(function() {
      Physics.checkCollisionRects.restore();
      ColliderObject.prototype.initialise.restore();
    });

    it('calculates the position of the hitbox by adding actor position', function() {
      var actor = { x: 20, y: 20};
      var hitBox = {x: 5, y: 10, width: 20, height: 30};
      var checkRectangle = {x: 5, y: 5, width: 10, height: 10};

      var colliderObject = new ColliderObject();
      colliderObject.actor = actor;

      var expectedHitBoxCheckRectangle = {x: 25, y: 30, width: 20, height: 30};
      var collision = colliderObject.checkForCollisionWithHitBox(checkRectangle, hitBox);
      assert.ok(Physics.checkCollisionRects.calledWith(checkRectangle, expectedHitBoxCheckRectangle));

    });
  });
});
