var assert = chai.assert;

describe('KeyMapper', function() {
  describe('mapKeys - maps keycodes to the respective keys', function() {
    beforeEach(function() {
      sinon.stub(keyMapper, 'mapKey');
      keyMapper.mapKey.onCall(0).returns('w');
      keyMapper.mapKey.onCall(1).returns('a');

    });

    afterEach(function() {
      keyMapper.mapKey.restore();
    });

    it('takes an object and runs mapKey for every property in the object then returns correctly mapped keys', function() {
      var keysDown = {87: true, 65: true};
      var expectedKeys = {'w': true, 'a': true};

      var mappedKeys = keyMapper.mapKeys(keysDown);

      assert.equal(keyMapper.mapKey.callCount, 2);
      assert.deepEqual(expectedKeys, mappedKeys);
    });

    it('does not push erroneous data into the mappedKeys array', function() {
      keyMapper.mapKey.onCall(2).returns(null);
      var keysDown = {87: true, 65: true, 'asdadf': true};
      var expectedKeys = {'w': true, 'a': true};

      assert.deepEqual(expectedKeys, keyMapper.mapKeys(keysDown));
    });

    it('does not break if it recieves an empty array', function() {
      assert.deepEqual(keyMapper.mapKeys({}), {});
    });
  });

  describe('test', function() {
    it('test', function() {
      console.log(keyMapper.mapKeys([87, 88]));
    });
  });

  describe('mapKey', function() {
    it('logs an error if it gets garbage and returns null', function() {
      sinon.stub(console, 'error');
      keyMapper.mapKey('asdfdf');
      keyMapper.mapKey(null);
      var mappedKey = keyMapper.mapKey(undefined);

      assert.strictEqual(mappedKey, null);
      assert.equal(console.error.callCount, 3);
      console.error.restore();
    });

    it('maps letter keys to expected values', function() {
      assert.equal('a', keyMapper.mapKey(65));
      assert.equal('b', keyMapper.mapKey(66));
      assert.equal('c', keyMapper.mapKey(67));
      assert.equal('d', keyMapper.mapKey(68));
      assert.equal('e', keyMapper.mapKey(69));
      assert.equal('f', keyMapper.mapKey(70));
      assert.equal('g', keyMapper.mapKey(71));
      assert.equal('h', keyMapper.mapKey(72));
      assert.equal('i', keyMapper.mapKey(73));
      assert.equal('j', keyMapper.mapKey(74));
      assert.equal('k', keyMapper.mapKey(75));
      assert.equal('l', keyMapper.mapKey(76));
      assert.equal('m', keyMapper.mapKey(77));
      assert.equal('n', keyMapper.mapKey(78));
      assert.equal('o', keyMapper.mapKey(79));
      assert.equal('p', keyMapper.mapKey(80));
      assert.equal('q', keyMapper.mapKey(81));
      assert.equal('r', keyMapper.mapKey(82));
      assert.equal('s', keyMapper.mapKey(83));
      assert.equal('t', keyMapper.mapKey(84));
      assert.equal('u', keyMapper.mapKey(85));
      assert.equal('v', keyMapper.mapKey(86));
      assert.equal('w', keyMapper.mapKey(87));
      assert.equal('x', keyMapper.mapKey(88));
      assert.equal('y', keyMapper.mapKey(89));
      assert.equal('z', keyMapper.mapKey(90));
    });

    it('maps number keys to expected values', function() {
      assert.equal('0', keyMapper.mapKey(48));
      assert.equal('1', keyMapper.mapKey(49));
      assert.equal('2', keyMapper.mapKey(50));
      assert.equal('3', keyMapper.mapKey(51));
      assert.equal('4', keyMapper.mapKey(52));
      assert.equal('5', keyMapper.mapKey(53));
      assert.equal('6', keyMapper.mapKey(54));
      assert.equal('7', keyMapper.mapKey(55));
      assert.equal('8', keyMapper.mapKey(56));
      assert.equal('9', keyMapper.mapKey(57));
    });

    it('maps space, return, shift, tab, escape and enter to expected values', function() {
      assert.equal('tab', keyMapper.mapKey(9));
      assert.equal('enter', keyMapper.mapKey(13));
      assert.equal('shift', keyMapper.mapKey(16));
      assert.equal('escape', keyMapper.mapKey(27));
      assert.equal('space', keyMapper.mapKey(32));
    });

    it ('maps ctrl, alt, caps, and arrow keys correctly', function() {
      assert.equal('ctrl', keyMapper.mapKey(17));
      assert.equal('alt', keyMapper.mapKey(18));
      assert.equal('caps', keyMapper.mapKey(20));
      assert.equal('left', keyMapper.mapKey(37));
      assert.equal('up', keyMapper.mapKey(38));
      assert.equal('right', keyMapper.mapKey(39));
      assert.equal('down', keyMapper.mapKey(40));
    });

    it('returns null if it gets an unknown keycode and outputs an error', function() {
      sinon.stub(console, 'error');
      assert.strictEqual(null, keyMapper.mapKey(100001));
      assert.ok(console.error.calledWith('Keycode: 100001 is unsupported in keyMapper'));
      console.error.restore();
    });
  })
});