var assert = chai.assert;

describe('Content Processor', function() {
  describe('processContentForActors', function() {

    it('calls process content on each actor that has content set in the content object', function() {
      var actor1 = {
        'getName': function() {return 'actor1'},
        'processContent': function() {}
      };

      var actor2 = {
        'getName': function() {return 'actor2'},
        'processContent': function() {}
      };

      sinon.spy(actor1, 'processContent');
      sinon.spy(actor2, 'processContent');

      var actors = [actor1, actor2];

      var content = {
        'actor1': { 'some': 'content'}
      };

      ContentProcessor.processContentForActors(actors, content);

      assert.equal(actor1.processContent.callCount, 1);
      assert.ok(actor1.processContent.calledWith(content.actor1));
      assert.equal(actor2.processContent.callCount, 0);
    });

    it ('returns with null if the actor array is empty or of an incorrect type', function() {
      assert.strictEqual(ContentProcessor.processContentForActors(null, {}), null);
      assert.strictEqual(ContentProcessor.processContentForActors([], {}), null);
    });
  });
});
