var assert = chai.assert;

describe('Move To Specific Location Behaviour', function() {
  beforeEach(function() {
    sinon.stub(Behaviour.prototype, 'initialise');
  });

  afterEach(function() {
    Behaviour.prototype.initialise.restore();
  });

  describe('run', function() {
    beforeEach(function() {
      sinon.stub(MoveToSpecificPointBehaviour.prototype, 'setTargetLocation');
      sinon.stub(MoveToSpecificPointBehaviour.prototype, 'end');
    });

    afterEach(function() {
      MoveToSpecificPointBehaviour.prototype.setTargetLocation.restore();
      MoveToSpecificPointBehaviour.prototype.end.restore();
    });

    it('calls setTargetLocation every time it is run to get a constantly updated target location', function() {
      var actor = {
        closeTo: function() { return false},
        moveTowards: function() {}
      };

      var moveToSpecificPointBehaviour = new MoveToSpecificPointBehaviour();
      moveToSpecificPointBehaviour.actor = actor;

      moveToSpecificPointBehaviour.run();

      assert.equal(moveToSpecificPointBehaviour.setTargetLocation.callCount, 1);
    });

    it('constantly moves towards a new location', function() {
      var actor = {
        closeTo: function() { return false},
        moveTowards: function() {}
      };
      sinon.spy(actor, 'moveTowards');

      var targetLocation = {'a': 'targetLocation'};

      var moveToSpecificPointBehaviour = new MoveToSpecificPointBehaviour();
      moveToSpecificPointBehaviour.actor = actor;
      moveToSpecificPointBehaviour.targetLocation = targetLocation;

      moveToSpecificPointBehaviour.run();

      assert.equal(actor.moveTowards.callCount, 1);
      assert.ok(actor.moveTowards.calledWith(moveToSpecificPointBehaviour.targetLocation));
    });

    it('calls its end function if the actor is close to the targetLocation', function() {
      var actor = {
        closeTo: function() { return true},
        moveTowards: function() {}
      };
      sinon.spy(actor, 'closeTo');

      var moveToSpecificPointBehaviour = new MoveToSpecificPointBehaviour();
      moveToSpecificPointBehaviour.actor = actor;
    });
  });

  describe('setTargetLocation', function() {
    it ('does something', function() {
      assert.ok(false);
    })
  });

  describe('end', function() {
    it ('does something', function() {
      assert.ok(false);
    })
  })
});
