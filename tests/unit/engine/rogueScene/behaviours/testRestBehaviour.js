var assert = chai.assert;

describe('Rest Behaviour', function() {

  describe('start', function() {
    beforeEach(function() {
      sinon.stub(Behaviour.prototype, 'initialise');
    });

    afterEach(function() {
      Behaviour.prototype.initialise.restore();
    });

    it('takes priority when it starts up', function() {
      var state = {setPriorityBehaviour: function() {}};
      sinon.spy(state, 'setPriorityBehaviour');

      var restBehaviour = new RestBehaviour();
      restBehaviour.state = state;

      restBehaviour.start();

      assert.ok(state.setPriorityBehaviour.calledWith(restBehaviour));
    });

    it('sets restingTime to 0, indicating we are at the start of a resting period', function() {
      var state = {setPriorityBehaviour: function() {}};
      var restBehaviour = new RestBehaviour();
      restBehaviour.state = state;
      restBehaviour.restingTime = 500;

      restBehaviour.start();
      assert.equal(restBehaviour.restingTime, 0);
    });
  });

  describe('run', function() {
    beforeEach(function() {
      sinon.stub(Behaviour.prototype, 'initialise');
      sinon.stub(RestBehaviour.prototype, 'end');
    });

    afterEach(function() {
      Behaviour.prototype.initialise.restore();
      RestBehaviour.prototype.end.restore();
    });

    it('waitingTime constantly increments', function() {
      var actor = { rest: function() {}};
      var restBehaviour = new RestBehaviour(50);
      restBehaviour.restingTime = 0;
      restBehaviour.actor = actor;
      restBehaviour.run();

      assert.equal(restBehaviour.restingTime, 1);
    });

    it('should tell the actor it is resting', function() {
      var actor = { rest: function() {}};
      sinon.spy(actor, 'rest');

      var restBehaviour = new RestBehaviour(100);
      restBehaviour.restingTime = 5;
      restBehaviour.actor = actor;

      restBehaviour.run();

      assert.equal(actor.rest.callCount, 1);
      //assert.equal(actor.resting.callCount, 1);
    });

    it('calls end when restingTime == timeToRestFor', function() {
      var restBehaviour = new RestBehaviour();
      restBehaviour.restingTime = 50;
      restBehaviour.timeToRestFor = 50;

      restBehaviour.run();
      assert.equal(restBehaviour.end.callCount, 1);
    });
  });
});
