var assert = chai.assert;

describe('Behaviour', function() {
  describe('end', function() {
    beforeEach (function() {
      sinon.stub(Behaviour.prototype, 'start');
    });

    afterEach(function() {
      Behaviour.prototype.start.restore();
    });

    it('calls the next behaviour if one is set', function() {
      var nextBehaviour = {start: function() {}};
      sinon.spy(nextBehaviour, 'start');

      var behaviour = new Behaviour();
      behaviour.nextBehaviour = nextBehaviour;

      behaviour.end();
      assert.equal(nextBehaviour.start.callCount, 1);
    });

    it('calls start of itself otherwise', function() {
      var behaviour = new Behaviour();
      behaviour.end();
      assert.equal(behaviour.start.callCount, 1);
    });
  });
});
