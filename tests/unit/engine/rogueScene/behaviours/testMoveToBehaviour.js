var assert = chai.assert;

describe('Move To Behaviour', function() {
  beforeEach(function() {
    sinon.stub(Behaviour.prototype, 'initialise');
  });

  afterEach(function() {
    Behaviour.prototype.initialise.restore();
  });

  describe('start', function() {
    beforeEach(function() {
      sinon.stub(MoveToBehaviour.prototype, 'setTargetLocation');
    });

    afterEach(function() {
      MoveToBehaviour.prototype.setTargetLocation.restore();
    });

    it('takes priority behaviour', function() {
      var state = {setPriorityBehaviour: function() {}};
      sinon.spy(state, 'setPriorityBehaviour');

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.state = state;

      moveToBehaviour.start();

      assert.ok(state.setPriorityBehaviour.calledWith(moveToBehaviour));
    });

    it('sets the target location for the current move', function() {
      var state = {setPriorityBehaviour: function() {}};
      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.state = state;

      moveToBehaviour.start();
      assert.equal(moveToBehaviour.setTargetLocation.callCount, 1);
    });
  });

  describe('run', function() {
    beforeEach(function() {
      sinon.stub(MoveToBehaviour.prototype, 'end');
      sinon.stub(MoveToBehaviour.prototype, 'setTargetLocation');
    });

    afterEach(function() {
      MoveToBehaviour.prototype.end.restore();
      MoveToBehaviour.prototype.setTargetLocation.restore();
    });

    it('it tells the actor to move towards the target location', function() {
      var targetLocation = {'target': 'location'};
      var actor = {
        moveTowards: function() {},
        closeTo: function() { return false;}
      };
      sinon.spy(actor, 'moveTowards');

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.actor = actor;
      moveToBehaviour.targetLocation = targetLocation;

      moveToBehaviour.run();

      assert.ok(moveToBehaviour.actor.moveTowards.calledWith(targetLocation));
    });

    it('if the actor collided with the environment end behaviour - FOR NOW', function() {
      var actor = {
        moveTowards: function() { this.collidedWithEnvironment = true},
        closeTo: function() { return false;}
      };

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.actor = actor;

      moveToBehaviour.run();
      assert.equal(moveToBehaviour.end.callCount, 1);
    });

    it('if the actor is close enough to the target location we call the end', function() {
      var targetLocation = {'target': 'location'};
      var actor = {
        moveTowards: function() {},
        closeTo: function() { return true;}
      };
      sinon.spy(actor, 'closeTo');

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.actor = actor;
      moveToBehaviour.targetLocation = targetLocation;

      moveToBehaviour.run();

      assert.ok(actor.closeTo.calledWith(targetLocation));
      assert.equal(moveToBehaviour.end.callCount, 1);
    });
  });

  describe('setTargetLocation', function() {
    beforeEach(function() {
      sinon.stub(Math, 'random');
      sinon.spy(Math, 'round');
    });

    afterEach(function() {
      Math.random.restore();
      Math.round.restore();
    });

    it ('picks a random location within 1000 of x', function() {
      var actor = new Actor();
      actor.x = 1000;
      Math.random.returns(0.5);

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.actor = actor;
      moveToBehaviour.setTargetLocation();

      assert.ok(Math.round.calledWith(0.5 * 2000));
      assert.equal(moveToBehaviour.targetLocation.x, 1000);
    });

    it('picks a random location within 1000 of y', function() {
      var actor = new Actor();
      actor.y = 500;
      Math.random.returns(0.25);

      var moveToBehaviour = new MoveToBehaviour();
      moveToBehaviour.actor = actor;
      moveToBehaviour.setTargetLocation();

      assert.ok(Math.round.calledWith(0.25 * 2000));
      assert.equal(moveToBehaviour.targetLocation.y, 0);
    });
  });
});
