var assert = chai.assert;

describe('Look For Behaviour', function() {

  describe('run', function() {
    beforeEach(function() {
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      console.error.restore();
    });

    it('tells its actor to look for targets every time it runs', function() {
      var actor = {
        lookForTargets: function() {},
        getCurrentTarget: function () {return false;}
      };

      sinon.spy(actor, 'lookForTargets');

      var lookForBehaviour = new LookForBehaviour();
      lookForBehaviour.actor = actor;
      lookForBehaviour.run();

      assert.equal(actor.lookForTargets.callCount, 1);
    });

    it('if the actor finds a target we begin the next state', function() {
      var actor = {
        lookForTargets: function() {},
        getCurrentTarget: function () {return true;}
      };

      var nextState = {
        becomeCurrentState: function() {}
      };

      sinon.spy(nextState, 'becomeCurrentState');

      var lookForBehaviour = new LookForBehaviour();
      lookForBehaviour.actor = actor;
      lookForBehaviour.nextState = nextState;

      lookForBehaviour.run();

      assert.ok(nextState.becomeCurrentState.callCount, 1);
    });

    it('if the next state is not set then we throw an error', function() {
      var actor = {
        lookForTargets: function() {},
        getCurrentTarget: function () {return true;}
      };

      var lookForBehaviour = new LookForBehaviour();
      lookForBehaviour.actor = actor;

      lookForBehaviour.run();

      assert.ok(console.error.calledWith('Look for behaviour has no next state'));
    });
  });
});
