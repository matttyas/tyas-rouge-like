var assert = chai.assert;

describe('Dungeon Generator', function() {
  describe ('generateFloors', function() {
    beforeEach (function() {
      sinon.stub(DungeonGenerator, 'calculateNumberFloors');
      sinon.stub(DungeonGenerator, 'linkFloors');
      sinon.stub(MapGenerator, 'generateMap');
    });

    afterEach (function() {
      DungeonGenerator.calculateNumberFloors.restore();
      DungeonGenerator.linkFloors.restore();
      MapGenerator.generateMap.restore();
    });

    it('calls generate a dungeon with a number of floors equal to calculateNumberFloors', function() {
      var numberFloors = 5;
      DungeonGenerator.calculateNumberFloors.returns(numberFloors);
      MapGenerator.generateMap.returns({setId: function() {}});
      var floors = DungeonGenerator.generateFloors();


      assert.equal(floors.length, numberFloors);
    });

    it('calls link floors once the floors have been generated', function() {
      var numberFloors = 3;

      MapGenerator.generateMap.returns({setId: function() {}});
      DungeonGenerator.calculateNumberFloors.returns(numberFloors);
      var floors = DungeonGenerator.generateFloors();

      assert.ok(DungeonGenerator.linkFloors.calledWith(floors));
    });

    it('sets the id of each map to the floor number starting at 0', function() {
      var numberFloors = 3;
      var map = {setId: function() {}};
      sinon.spy(map, 'setId');

      MapGenerator.generateMap.returns(map);
      DungeonGenerator.calculateNumberFloors.returns(numberFloors);
      var floors = DungeonGenerator.generateFloors();

      assert.ok(map.setId.calledWith(0));
      assert.ok(map.setId.calledWith(1));
      assert.ok(map.setId.calledWith(2));
      assert.equal(map.setId.callCount, 3);
    });
  });

  describe('linkFloors', function() {
    beforeEach(function() {
      sinon.stub(DungeonGenerator, 'linkFloor');
    });

    afterEach(function() {
      DungeonGenerator.linkFloor.restore();
    });

    it('Passes the current floor, undefined and undefined to link floor if there is only 1 floor', function() {
      var floors = [0];
      DungeonGenerator.linkFloors(floors);
      assert.ok(DungeonGenerator.linkFloor.calledWith(0, undefined, undefined));
    });

    it('Passes the current floor, the previous floor and the next floor for each middle floor in a dungeon', function() {
      var floors = [0, 1, 2, 3];
      DungeonGenerator.linkFloors(floors);
      assert.ok(DungeonGenerator.linkFloor.calledWith(1, 0, 2));
      assert.ok(DungeonGenerator.linkFloor.calledWith(2, 1, 3));
    });

    it('Passes the current floor and the previous floor to linkFloor for the final floor', function() {
      var floors = [0, 1, 2];
      DungeonGenerator.linkFloors(floors);
      assert.ok(DungeonGenerator.linkFloor.calledWith(2, 1, undefined));
    });
  });

  describe('linkFloor', function() {
    beforeEach(function() {
      sinon.spy(Entrance.prototype, 'getId');
      sinon.spy(Entrance.prototype, 'setTargetMapId');
      sinon.spy(Entrance.prototype, 'setTargetEntranceId');
      sinon.stub(Map.prototype, 'initialise');
    });

    afterEach(function() {
      Entrance.prototype.getId.restore();
      Entrance.prototype.setTargetMapId.restore();
      Entrance.prototype.setTargetEntranceId.restore();
      Map.prototype.initialise.restore();
    });

    it('links the first entrance of a floor to the previous and the second entrance of the floor to the next', function() {
      var entrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      entrances[0].id = 0;
      entrances[1].id = 1;

      var currentFloor = new Map();
      currentFloor.setEntrances(entrances);

      var previousEntrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      previousEntrances[0].id = 2;
      previousEntrances[1].id = 3;

      var previousFloor = new Map();
      previousFloor.id = 100;
      previousFloor.setEntrances(previousEntrances);

      var nextEntrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      nextEntrances[0].id = 4;
      nextEntrances[1].id = 5;

      var nextFloor = new Map();
      nextFloor.id = 200;
      nextFloor.setEntrances(nextEntrances);

      DungeonGenerator.linkFloor(currentFloor, previousFloor, nextFloor);

      assert.ok(entrances[0].setTargetEntranceId.calledWith(3));
      assert.ok(entrances[0].setTargetMapId.calledWith(100));
      assert.ok(entrances[1].setTargetEntranceId.calledWith(4));
      assert.ok(entrances[1].setTargetMapId.calledWith(200));
    });

    it('links the first floor of the dungeon only to the next', function() {
      var entrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      entrances[0].id = 0;
      entrances[1].id = 1;

      var currentFloor = new Map();
      currentFloor.setEntrances(entrances);

      var nextEntrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      nextEntrances[0].id = 4;
      nextEntrances[1].id = 5;

      var nextFloor = new Map();
      nextFloor.id = 200;
      nextFloor.setEntrances(nextEntrances);

      DungeonGenerator.linkFloor(currentFloor, undefined, nextFloor);

      assert.equal(entrances[0].getTargetEntranceId(), undefined);
      assert.equal(entrances[0].getTargetMapId(), undefined);
      assert.ok(entrances[1].setTargetEntranceId.calledWith(4));
      assert.ok(entrances[1].setTargetMapId.calledWith(200));
    });

    it('links the last floor of the dungeon only to the floor before', function() {
      var entrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      entrances[0].id = 0;
      entrances[1].id = 1;

      var currentFloor = new Map();
      currentFloor.setEntrances(entrances);

      var previousEntrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      previousEntrances[0].id = 2;
      previousEntrances[1].id = 3;

      var previousFloor = new Map();
      previousFloor.id = 100;
      previousFloor.setEntrances(previousEntrances);

      DungeonGenerator.linkFloor(currentFloor, previousFloor, undefined);

      assert.equal(entrances[1].getTargetEntranceId(), undefined);
      assert.equal(entrances[1].getTargetMapId(), undefined);
      assert.ok(entrances[0].setTargetEntranceId.calledWith(3));
      assert.ok(entrances[0].setTargetMapId.calledWith(100));
    });

    it('links nothing to the floor if the previous and next floors are undefined', function() {
      var entrances = [ new Entrance (5, 5, 'en'), new Entrance(10, 10, 'ex')];
      entrances[0].id = 0;
      entrances[1].id = 1;

      var currentFloor = new Map();
      currentFloor.setEntrances(entrances);

      DungeonGenerator.linkFloor(currentFloor, undefined, undefined);

      assert.equal(entrances[0].getTargetEntranceId(), undefined);
      assert.equal(entrances[0].getTargetMapId(), undefined);

      assert.equal(entrances[1].getTargetEntranceId(), undefined);
      assert.equal(entrances[1].getTargetMapId(), undefined);
    });
  });
});
