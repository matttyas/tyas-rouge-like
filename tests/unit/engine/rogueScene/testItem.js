var assert = chai.assert;

describe('Item', function() {
  describe('initialise', function() {
    it('sets properties on the new item if passed in', function() {
      var properties = {
        getProperty: function() {}
      };
      var item = new Item(properties);

      assert.deepEqual(item.properties, properties);
    });
  });
});
