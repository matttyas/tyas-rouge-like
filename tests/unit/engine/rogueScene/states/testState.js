var assert = chai.assert;

describe('State', function() {
  describe ('act', function() {

  });

  describe ('run', function() {
    beforeEach(function() {
      sinon.stub(State.prototype, 'initialise');
      sinon.stub(State.prototype, 'runConstantBehaviours');
    });

    afterEach(function() {
      State.prototype.initialise.restore();
      State.prototype.runConstantBehaviours.restore();
    });

    it('calls each behaviour to check if it should be run, then runs it if so', function() {
      var constantState1 = {
        shouldRun: function() { return true;},
        run: function() {}
      };
      var constantState2 = {
        shouldRun: function() { return true;},
        run: function() {}
      };
      var activeState1 = {
        shouldRun: function() { return false;},
        run: function() {}
      };

      sinon.spy(constantState1, 'run');
      sinon.spy(constantState2, 'run');
      sinon.spy(activeState1, 'run');

      var state = new State();
      state.behaviours = [constantState1, constantState2, activeState1];

      state.run();

      assert.equal(constantState1.run.callCount, 1);
      assert.equal(constantState2.run.callCount, 1);
      assert.equal(activeState1.run.callCount, 0);
    });

    it('calls only a single overriding behaviour if one behaviour has taken priority', function() {
      var constantState1 = {
        shouldRun: function() { return true;},
        run: function() {}
      };
      var constantState2 = {
        shouldRun: function() { return true;},
        run: function() {}
      };
      var activeState1 = {
        shouldRun: function() { return false;},
        run: function() {}
      };

      sinon.spy(constantState1, 'run');
      sinon.spy(constantState2, 'run');
      sinon.spy(activeState1, 'run');

      var state = new State();
      state.behaviours = [constantState1, constantState2, activeState1];
      state.priorityBehaviour = activeState1;
      state.run();

      assert.equal(constantState1.run.callCount, 0);
      assert.equal(constantState2.run.callCount, 0);
      assert.equal(activeState1.run.callCount, 1);
    });

    it('always calls runConstantBehaviours even if a single behaviour has priority', function() {
      var activeState1 = {
        shouldRun: function() { return false;},
        run: function() {}
      };

      var state = new State();
      state.behaviours = [activeState1];
      state.priorityBehaviour = activeState1;
      state.run();

      assert.ok(state.runConstantBehaviours.callCount, 1);
    });
  });

  describe('addBehaviour', function() {
    beforeEach( function() {
      sinon.stub(State.prototype, 'initialise');
    });

    afterEach( function() {
      State.prototype.initialise.restore();
    });

    it('adds the behaviour to the list of behaviours', function() {
      var behaviour = { setState: function() {}};

      var state = new State();
      state.addBehaviour(behaviour);

      assert.deepEqual(state.behaviours, [behaviour]);
    });

    it('sets the state on the behaviour that has been added', function() {
      var behaviour = { setState: function() {}};
      sinon.spy(behaviour, 'setState');

      var state = new State();
      state.addBehaviour(behaviour);

      assert.ok(behaviour.setState.calledWith(state));
    });
  });
});
