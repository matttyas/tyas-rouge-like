var assert = chai.assert;

describe('Rogue Map Builder', function() {
  describe('buildMap', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'initialiseEntrances');
      sinon.stub(RogueMapBuilder, 'calculateAndSetBoundaries');
      sinon.stub(RogueMapBuilder, 'initialiseTiles');
      sinon.stub(RogueMapBuilder, 'initialiseDoors');
      sinon.stub(RogueMapBuilder, 'initialiseDecorations');
      sinon.stub(RogueMapBuilder, 'initialiseRooms');
      sinon.stub(RogueMapBuilder, 'initialiseEnemies');
    });

    afterEach(function() {
      RogueMapBuilder.initialiseEntrances.restore();
      RogueMapBuilder.initialiseTiles.restore();
      RogueMapBuilder.calculateAndSetBoundaries.restore();
      RogueMapBuilder.initialiseDoors.restore();
      RogueMapBuilder.initialiseDecorations.restore();
      RogueMapBuilder.initialiseRooms.restore();
      RogueMapBuilder.initialiseEnemies.restore();
    });

    it ('initialises the tiles, entrances, doors, decorations, rooms and enemies', function() {
      RogueMapBuilder.buildMap({}, {}, {}, {});

      assert.equal(RogueMapBuilder.initialiseTiles.callCount, 1);
      assert.equal(RogueMapBuilder.initialiseEntrances.callCount, 1);
      assert.equal(RogueMapBuilder.initialiseDoors.callCount, 1);
      assert.equal(RogueMapBuilder.initialiseDecorations.callCount, 1);
      assert.equal(RogueMapBuilder.initialiseRooms.callCount, 1);
      assert.equal(RogueMapBuilder.initialiseEnemies.callCount, 1);
    });

    it('It calculates the boundaries', function() {
      RogueMapBuilder.buildMap({}, {}, {});
      assert.equal(RogueMapBuilder.calculateAndSetBoundaries.callCount, 1);
    });

    it('Sets the maps id to equal mapData.id', function() {
      var mapData = { id: '15'};
      var rogueMap = {};

      RogueMapBuilder.buildMap(rogueMap, mapData, {});
      assert.equal(rogueMap.id, '15');
    });
  });

  describe('initialiseTiles', function() {
    beforeEach(function() {
      sinon.stub(RogueTile.prototype, 'buildRogueTile');
    });

    afterEach(function() {
      RogueTile.prototype.buildRogueTile.restore();
    });

    it('takes mapData tiles and builds a 2d array of Tiles from them', function() {
      var tiles = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8]
      ];

      RogueTile.prototype.buildRogueTile.returns(0);

      var mapData = { tiles: tiles};
      var engine = { tileWidth: 10, tileHeight: 10};
      var expectedTiles = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
      ];

      var newTiles = RogueMapBuilder.initialiseTiles(mapData, engine);

      for (var x = 0; x < tiles.length; x++) {
        for (var y = 0; y < tiles[x].length; y++) {
          assert.ok(RogueTile.prototype.buildRogueTile.calledWith(tiles[x][y]));
        }
      }
    });
  });

  describe('calculateAndSetBoundaries', function() {
    it('uses rogueMap tiles and engine tile measurements to calculate where the boundaries of the current map should be', function() {
      var tiles = [
        [0, 2, 3],
        [4, 5, 7]
      ];

      var rogueMap = {
        tiles: tiles
      };

      var engine = {
        tileHeight: 50,
        tileWidth: 50
      };

      RogueMapBuilder.calculateAndSetBoundaries(rogueMap, engine);

      assert.equal(rogueMap.left, 0);
      assert.equal(rogueMap.top, 0);
      assert.equal(rogueMap.bottom, tiles[0].length * engine.tileHeight);
      assert.equal(rogueMap.right, tiles.length * engine.tileWidth);
    });
  });

  describe('initialiseEntrances', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'buildEntrance');
    });

    afterEach(function() {
        RogueMapBuilder.buildEntrance.restore();
    });

    it('takes each entrance in mapData, builds a RogueEntrance for it then returns an array of all RogueEntrances', function() {
      var entrances = [ 1, 2, 3];
      var mapData = { getEntrances: function() { return entrances; }};
      RogueMapBuilder.buildEntrance.returns('ENTRANCE');

      var rogueScene = 'rogueScene';
      var engine = 'engine';

      var rogueEntrances = RogueMapBuilder.initialiseEntrances(mapData, rogueScene, engine);

      assert.equal(RogueMapBuilder.buildEntrance.callCount, entrances.length);
      assert.ok(RogueMapBuilder.buildEntrance.calledWith(1, rogueScene, engine));
      assert.deepEqual(rogueEntrances, ['ENTRANCE', 'ENTRANCE', 'ENTRANCE']);
    });
  });

  describe('buildEntrance', function() {
    beforeEach(function() {
      sinon.stub(RogueEntrance.prototype, 'initialise');
      sinon.spy(RogueEntrance.prototype, 'setCurrentScene');
    });

    afterEach(function() {
      RogueEntrance.prototype.initialise.restore();
      RogueEntrance.prototype.setCurrentScene.restore();
    });

    it('Calculates the actual in game coordinates for the entrance and passes them to its constructor', function() {
      var engine = {tileWidth: 10, tileHeight: 10};
      var entranceData = {
        getX: function() { return 5; },
        getY: function() { return 7; }
      };

      var newEntrance = RogueMapBuilder.buildEntrance(entranceData, {}, engine);

      var expectedEntranceX = entranceData.getX() * engine.tileWidth;
      var expectedEntranceY = entranceData.getY() * engine.tileHeight;

      assert.equal(newEntrance.getX(), expectedEntranceX);
      assert.equal(newEntrance.getY(), expectedEntranceY);
    });

    it('sets the RogueMap\'s currentScene on the entrance', function() {
      var engine = {tileWidth: 10, tileHeight: 10};
      var entranceData = {
        getX: function() { return 5; },
        getY: function() { return 7; }
      };
      var rogueScene = 'rogueScene';
      var newEntrance = RogueMapBuilder.buildEntrance(entranceData, rogueScene, engine);
      assert.ok(newEntrance.setCurrentScene.calledWith(rogueScene));
    });
  });

  describe('initialiseDoors', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'buildDoor');
    });

    afterEach(function() {
      RogueMapBuilder.buildDoor.restore();
    });

    it('gets the doorsData from the mapData', function() {
      var mapData = { getDoors: function() {return []}};
      sinon.spy(mapData, 'getDoors');

      RogueMapBuilder.initialiseDoors(mapData, {});
      assert.equal(mapData.getDoors.callCount, 1);
    });

    it('builds each door in the doorsData then adds it to the RogueMap door array', function() {
      var doorsData = ['a', 'b', 'c'];
      var mapData = { getDoors: function() { return doorsData}};
      var engine = 'engine';

      RogueMapBuilder.initialiseDoors(mapData, engine);
      assert.equal(RogueMapBuilder.buildDoor.callCount, doorsData.length);
      assert.ok(RogueMapBuilder.buildDoor.calledWith('a', engine))
    });
  });

  describe('buildDoor', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'calculateDoorPosition');
      sinon.stub(RogueDoor.prototype, 'initialise');
      sinon.stub(RogueHorizontalDoor.prototype, 'initialise');
      sinon.stub(RogueVerticalDoor.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMapBuilder.calculateDoorPosition.restore();
      RogueDoor.prototype.initialise.restore();
      RogueHorizontalDoor.prototype.initialise.restore();
      RogueVerticalDoor.prototype.initialise.restore();
    });

    it('Gets the doors expected position then passes that to rogueDoor', function() {
      var doorData = {getDirection: function() { return 'direction' }};
      var doorPosition = {x: 30, y: 35};
      var engine = 'engine';

      RogueMapBuilder.calculateDoorPosition.returns(doorPosition);
      var door = RogueMapBuilder.buildDoor(doorData, engine);

      assert.ok(RogueMapBuilder.calculateDoorPosition.calledWith(doorData, engine));
      assert.ok(door.initialise.calledWith(doorData));
      assert.equal(door.x, doorPosition.x);
      assert.equal(door.y, doorPosition.y);
    });

    it('Returns a horizontal door if the doorData direction is horizontal', function() {
      var doorData = {getDirection: function() {return 'horizontal'}};
      var doorPosition = {x: 1, y: 1};
      var engine ='engine';

      RogueMapBuilder.calculateDoorPosition.returns(doorPosition);
      var door = RogueMapBuilder.buildDoor(doorData, engine);
      assert.equal(RogueHorizontalDoor.prototype.initialise.callCount, 1);
    });

    it('Returns a vertical door if the doorData direction is vertical', function() {
      var doorData = {getDirection: function() {return 'vertical'}};
      var doorPosition = {x: 1, y: 1};
      var engine ='engine';

      RogueMapBuilder.calculateDoorPosition.returns(doorPosition);
      var door = RogueMapBuilder.buildDoor(doorData, engine);
      assert.equal(RogueVerticalDoor.prototype.initialise.callCount, 1);
    });
  });

  describe('calculateDoorPosition', function() {
    it ('Gets the door direction, x and y from doorData', function() {
      var doorData = {
        getDirection: function() { return 'direction'},
        getX: function() { return 1; },
        getY: function() { return 1;}
      };

      sinon.spy(doorData, 'getDirection');
      sinon.spy(doorData, 'getX');
      sinon.spy(doorData, 'getY');

      RogueMapBuilder.calculateDoorPosition(doorData, {});

      assert.equal(doorData.getDirection.callCount, 1);
      assert.equal(doorData.getX.callCount, 1);
      assert.equal(doorData.getY.callCount, 1);
    });

    it('Sets the door x to doorData.x * engine.tileWidth when direction is horizontal', function() {
      var doorX = 10;
      var doorY = 10;

      var doorData = {
        getDirection: function() { return 'horizontal'; },
        getX: function() { return doorX; },
        getY: function() { return doorY; }
      };

      var engine = { tileWidth: 10, tileHeight: 10};
      var expectedX = 100;

      var position = RogueMapBuilder.calculateDoorPosition(doorData, engine);
      assert.equal(position.x, expectedX);
    });

    it('Sets the door y to doorData.y * engine.tileHeight + (engine.tileHeight / 2) when horizontal', function() {
      var doorX = 10;
      var doorY = 10;

      var doorData = {
        getDirection: function() { return 'horizontal'; },
        getX: function() { return doorX; },
        getY: function() { return doorY; }
      };

      var engine = { tileWidth: 10, tileHeight: 10};
      var expectedY = 105;

      var position = RogueMapBuilder.calculateDoorPosition(doorData, engine);
      assert.equal(position.y, expectedY);
    });

    it('Sets the door x to doorData.x * engine.tileWidth + (engine.tileWidth / 2) when vertical', function() {
      var doorX = 10;
      var doorY = 10;

      var doorData = {
        getDirection: function() { return 'vertical'; },
        getX: function() { return doorX; },
        getY: function() { return doorY; }
      };

      var engine = { tileWidth: 10, tileHeight: 10};
      var expectedX = 105;

      var position = RogueMapBuilder.calculateDoorPosition(doorData, engine);
      assert.equal(position.x, expectedX);
    });

    it('Sets the door y to doorData.y * engine.tileHeight when direction is vertical', function() {
      var doorX = 10;
      var doorY = 10;

      var doorData = {
        getDirection: function() { return 'vertical'; },
        getX: function() { return doorX; },
        getY: function() { return doorY; }
      };

      var engine = { tileWidth: 10, tileHeight: 10};
      var expectedY = 100;

      var position = RogueMapBuilder.calculateDoorPosition(doorData, engine);
      assert.equal(position.y, expectedY);
    });

    it('Returns an error if either doorData or engine is missing', function() {
      sinon.stub(console, 'error');

      RogueMapBuilder.calculateDoorPosition('doorData');
      RogueMapBuilder.calculateDoorPosition(null, 'engine');

      assert.equal(console.error.callCount, 2);
      assert.ok(console.error.calledWith('Missing Door Data or Engine in calculateDoorPosition'))
      console.error.restore();
    });
  });

  describe('initialiseDecorations', function () {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'buildDecoration');
    });

    afterEach(function() {
      RogueMapBuilder.buildDecoration.restore();
    });

    it('gets the decorations from the mapData', function() {
      var mapData = { getDecorations: function() {return []}};
      sinon.spy(mapData, 'getDecorations');

      RogueMapBuilder.initialiseDecorations(mapData, {});
      assert.equal(mapData.getDecorations.callCount, 1);
    });

    it ('calls buildDecoration a number of times equal to the length of the decorationsData', function() {
      var decorationsData = [1, 2, 3]
      var mapData = { getDecorations: function() { return decorationsData; }};

      RogueMapBuilder.initialiseDecorations(mapData, {});
      assert.equal(RogueMapBuilder.buildDecoration.callCount, decorationsData.length);
    });

    it('returns the array of decorations', function() {
      var decorationsData = [1, 2, 3]
      var mapData = { getDecorations: function() { return decorationsData; }};
      RogueMapBuilder.buildDecoration.returns('a');
      var decorations = RogueMapBuilder.initialiseDecorations(mapData, {});
      assert.deepEqual(decorations, ['a', 'a', 'a'])
    });
  });

  describe('buildDecoration', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'calculateDecorationPosition');
      sinon.stub(RogueDecoration.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMapBuilder.calculateDecorationPosition.restore();
      RogueDecoration.prototype.initialise.restore();
    });

    it ('calculates the position to place the decoration at and sets it on the new decoration', function() {
      var decorationPosition = {x: 'pos', y: 'ition'};
      var decorationData = {'decoration': 'data'};
      var engine = {'engine': true};

      RogueMapBuilder.calculateDecorationPosition.returns(decorationPosition);

      var rogueDecoration = RogueMapBuilder.buildDecoration(decorationData, engine);

      assert.ok(RogueMapBuilder.calculateDecorationPosition.calledWith(decorationData, engine));
      assert.equal(rogueDecoration.x, decorationPosition.x);
      assert.equal(rogueDecoration.y, decorationPosition.y);
    });
  });

  describe('calculateDecorationPosition', function() {
    it('gets tileX and tileY from the decoration data', function() {
      var decorationData = {
        getTileX: function() { return 5},
        getTileY: function() { return 10},
        getProperty: function(property) { return 4}
      };

      var engine = { tileWidth: 20, tileHeight: 20};
      sinon.spy(decorationData, 'getTileX');
      sinon.spy(decorationData, 'getTileY');
      sinon.spy(decorationData, 'getProperty');

      var decorationPosition = RogueMapBuilder.calculateDecorationPosition(decorationData, engine);

      assert.equal(decorationData.getTileX.callCount, 1);
      assert.equal(decorationData.getTileY.callCount, 1);
    });

    it('calculates the x position using engine width and the decorations x and width data', function() {
      var decorationData = {
        getTileX: function() { return 5},
        getTileY: function() { return 8},
        getProperty: function(property) { return 10}
      };
      var engine = {tileWidth: 10, tileHeight: 20};
      var expectedX = 50;

      var position = RogueMapBuilder.calculateDecorationPosition(decorationData, engine);
      assert.equal(position.x, expectedX);
    });

    it('calculates the y position using engine height and the decorations y and height data', function() {
      var decorationData = {
        getTileX: function() { return 5},
        getTileY: function() { return 8},
        getProperty: function(property) { return 10}
      };
      var engine = {tileWidth: 10, tileHeight: 20};
      var expectedY = 160;

      var position = RogueMapBuilder.calculateDecorationPosition(decorationData, engine);
      assert.equal(position.y, expectedY);
    });
  });

  describe('initialiseRooms', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'buildRoom');
    });

    afterEach(function() {
      RogueMapBuilder.buildRoom.restore();
    });

    it('gets the rooms from the mapData', function() {
      var mapData = { getRooms: function() {return []}};
      sinon.spy(mapData, 'getRooms');

      RogueMapBuilder.initialiseRooms(mapData, {});
      assert.equal(mapData.getRooms.callCount, 1);
    });

    it ('calls buildRoom a number of times equal to the length of the roomsData', function() {
      var roomsData = [1, 2, 3]
      var mapData = { getRooms: function() { return roomsData; }};

      RogueMapBuilder.initialiseRooms(mapData, {});
      assert.equal(RogueMapBuilder.buildRoom.callCount, roomsData.length);
    });

    it('returns the array of rooms', function() {
      var roomsData = [1, 2, 3]
      var mapData = { getRooms: function() { return roomsData; }};
      RogueMapBuilder.buildRoom.returns('a');
      var rooms = RogueMapBuilder.initialiseRooms(mapData, {});
      assert.deepEqual(rooms, ['a', 'a', 'a'])
    })
  });

  describe('buildRoom', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'calculateRoomPosition');
    });

    afterEach(function() {
      RogueMapBuilder.calculateRoomPosition.restore();
    });

    it ('calculates the position to place the room at and sets it on the new room', function() {
      var roomPosition = {x: 'pos', y: 'ition'};
      var roomData = {'decoration': 'data'};
      var engine = {'engine': true};

      RogueMapBuilder.calculateRoomPosition.returns(roomPosition);

      var rogueRoom = RogueMapBuilder.buildRoom(roomData, engine);

      assert.ok(RogueMapBuilder.calculateRoomPosition.calledWith(roomData, engine));
      assert.equal(rogueRoom.x, roomPosition.x);
      assert.equal(rogueRoom.y, roomPosition.y);
    });
  });

  describe('calculateRoomPosition', function() {
    it('gets x and y from the room data', function() {
      var roomData = {
        getX: function() { return 5},
        getY: function() { return 10},
      };

      var engine = { tileWidth: 20, tileHeight: 20};
      sinon.spy(roomData, 'getX');
      sinon.spy(roomData, 'getY');

      var roomPosition = RogueMapBuilder.calculateRoomPosition(roomData, engine);

      assert.equal(roomData.getX.callCount, 1);
      assert.equal(roomData.getY.callCount, 1);
    });

    it('calculates the x position using engine width and the decorations x and width data', function() {
      var roomData = {
        getX: function() { return 5},
        getY: function() { return 8},
      };
      var engine = {tileWidth: 10, tileHeight: 20};
      var expectedX = 50;

      var position = RogueMapBuilder.calculateRoomPosition(roomData, engine);
      assert.equal(position.x, expectedX);
    });

    it('calculates the y position using engine height and the decorations y and height data', function() {
      var roomData = {
        getX: function() { return 5},
        getY: function() { return 8}
      };

      var engine = {tileWidth: 10, tileHeight: 20};
      var expectedY = 160;

      var position = RogueMapBuilder.calculateRoomPosition(roomData, engine);
      assert.equal(position.y, expectedY);
    });
  });

  describe('initialiseEnemies', function() {
    beforeEach(function() {
      sinon.stub(RogueMapBuilder, 'buildEnemy');
    });

    afterEach(function() {
      RogueMapBuilder.buildEnemy.restore();
    });

    it('Calls build enemy for each enemy in map data, with its enemy data and the engine', function() {
      var enemy1 = {'an': 'enemy'};
      var enemy2 = {'another': 'enemy'};
      var enemies = [enemy1, enemy2];

      var mapData = {getEnemies: function() { return enemies;}};
      sinon.spy(mapData, 'getEnemies');

      var engine = {'the': 'engine'};
      var rogueScene = {'a': 'scene'};

      RogueMapBuilder.buildEnemy.returns({setFoes: function() {}});

      var genereatedEnemies = RogueMapBuilder.initialiseEnemies(mapData, engine, rogueScene);

      assert.equal(RogueMapBuilder.buildEnemy.callCount, enemies.length);
      assert.ok(RogueMapBuilder.buildEnemy.calledWith(enemy1, engine, rogueScene));
      assert.ok(RogueMapBuilder.buildEnemy.calledWith(enemy2, engine, rogueScene));
    });
  });

  describe('buildEnemy', function() {
    beforeEach(function() {
      sinon.stub(RogueEnemy, 'initialise');
    });

    afterEach(function() {
      RogueEnemy.initialise.restore();
    });
  });
});
