var assert = chai.assert;

describe('Map', function() {
  describe ('getRoomWithId', function() {

    beforeEach(function() {
      sinon.stub(Map.prototype, 'initialise');
    });

    afterEach(function() {
      Map.prototype.initialise.restore();
    });

    it('searches through all rooms and returns the one with the specified id', function() {
      var map = new Map();
      var room1 = { id: 'eyyyyyyyy'};
      var room2 = { id: 'winwardos'};
      map.rooms = [room1, room2];

      assert.deepEqual(map.getRoomWithId('winwardos'), room2);
    });

    it('returns null if no tile exists for that id', function() {
      var map = new Map();
      var room1 = { id: 'eyyyyyyyy'};
      var room2 = { id: 'winwardos'};
      map.rooms = [room1, room2];

      assert.equal(map.getRoomWithId('pizza'), null);
    });
  });

  describe('getTile', function() {
    beforeEach(function() {
      sinon.stub(Map.prototype, 'initialise');
    });

    afterEach(function() {
      Map.prototype.initialise.restore();
    });

    it('returns the tile at the specified (x, y) position', function() {
      var tiles = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ];

      var map = new Map();
      map.tiles = tiles;
      assert.equal(map.getTile(1, 1), 5);
    });

    it('returns null if the tile requested is less than the x boundary', function() {
      var tiles = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ];

      var map = new Map();
      map.tiles = tiles;
      assert.equal(map.getTile(-1, 1), null);
    });

    it('returns null if the tile requested is greater than the x boundary', function() {
      var tiles = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ];

      var map = new Map();
      map.tiles = tiles;
      assert.equal(map.getTile(3, 1), null);
    });

    it('returns null if the tile requested is greater than the y boundary', function() {
      var tiles = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ];

      var map = new Map();
      map.tiles = tiles;
      assert.equal(map.getTile(1, 3), null);
    });

    it('returns null if the tile requested is less than the y boundary', function() {
      var tiles = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ];

      var map = new Map();
      map.tiles = tiles;
      assert.equal(map.getTile(1, -1), null);
    });
  });

  describe('checkTilePassable', function() {
    beforeEach(function() {
      sinon.stub(Map.prototype, 'initialise');
      sinon.stub(Map.prototype, 'getTile');
    });

    afterEach(function() {
      Map.prototype.initialise.restore();
      Map.prototype.getTile.restore();
    });

    it ('returns true if the tile is both passable and within boundaries', function() {
      var tile = { getProperty: function() { return true; }};
      var map = new Map();
      map.getTile.returns(tile);

      assert.equal(map.checkTilePassable(5, 5), true);
    });

    it ('returns true if the requestedPosition is outside boundaries and this is an outside map', function() {
      var map = new Map();
      map.outside = true;
      map.getTile.returns(null);

      assert.equal(map.checkTilePassable(-1, 5), true);
    });

    it ('returns false if the tile is outside boundaries and this is an inside map', function() {
      var map = new Map();
      map.outside = false;
      map.getTile.returns(null);

      assert.equal(map.checkTilePassable(), false);
    });
  });
});
