var assert = chai.assert;

describe('========= AI =========', function() {
  describe ('act', function() {
    beforeEach(function() {
      sinon.stub(AI.prototype, 'initialise');
    });

    afterEach(function() {
      AI.prototype.initialise.restore();
    });

    it('exits immediately if currentState is not set', function() {
      var ai = new AI();
      var state = { run: function() {}};
      sinon.spy(state, 'run');

      ai.states = { 'state': state};
      assert.doesNotThrow( function() {
        ai.act();
      })

      assert.equal(state.run.callCount, 0);
    });

    it('calls run on the currentState', function() {
      var ai = new AI();
      var state = { run: function() {}};
      sinon.spy(state, 'run');
      ai.states = { 'state': state};

      ai.currentState = ai.states.state;
      ai.act();

      assert.equal(state.run.callCount, 1);
    });
  });

  describe('addState', function() {
    beforeEach(function() {
      sinon.stub(AI.prototype, 'initialise');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      AI.prototype.initialise.restore();
      console.error.restore();
    });

    it('adds the provided state to the state object', function() {
      var stateName = 'Utah';
      var state = {
        getName: function() {return stateName},
        setAI: function() {},
        setActor: function() {}
      };

      sinon.spy(state, 'getName');
      var ai = new AI();
      ai.addState(state);

      assert.equal(state.getName.callCount, 1);
      var expectedStates = {
        'Utah': state
      };

      assert.deepEqual(ai.states, expectedStates);
    });

    it('should set the ai and actor on the state it is adding', function() {
      var stateName = 'Nebraska';
      var state = {
        getName: function() {return stateName},
        setAI: function() {},
        setActor: function() {}
      };

      sinon.spy(state, 'setAI');
      sinon.spy(state, 'setActor');

      var actor = {'an': 'actor'};
      var ai = new AI(actor);
      ai.addState(state);

      assert.ok(state.setAI.calledWith(ai));
      assert.ok(state.setActor.calledWith(actor));
    });

    it('returns a console.error if the state provided does not have a name', function() {
      var state = {getName: function() {return undefined}};
      var ai = new AI();
      ai.addState(state);

      assert.ok(console.error.calledWith('Can not add an AI state without it having a name'));
    });
  });

  describe('setCurrentState', function() {
    beforeEach(function() {
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      console.error.restore();
    });

    it('takes a stateName and sets the state with that name to be the current one', function() {
      var stateName = 'Vermont';
      var state = {'a': 'state'};
      var states = {'Vermont': state};

      var ai = new AI();
      ai.states = states;

      ai.setCurrentState(stateName);
      assert.deepEqual(state, ai.currentState);
    });

    it('errors if it does not recieve a stateName', function() {
      var ai = new AI();

      ai.setCurrentState();
      assert.ok(console.error.calledWith('Need name to set current AI state'));
    });

    it('errors if the named state does not exist', function() {
      var ai = new AI();

      ai.setCurrentState('nonexistant state');
      assert.ok(console.error.calledWith('Attempted to switch to nonexistant state'));
    });
  });
});
