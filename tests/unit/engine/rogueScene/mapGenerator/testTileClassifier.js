var assert = chai.assert;

describe('Tile Classifier', function() {

  describe('classifyImpassableTiles', function() {
    beforeEach(function() {
      sinon.stub(TileClassifier, 'classifyImpassableTile');
      sinon.stub(TileClassifier, 'secondPassClassifyImpassableTile');
    });

    afterEach(function() {
      TileClassifier.classifyImpassableTile.restore();
      TileClassifier.secondPassClassifyImpassableTile.restore();
    });

    it('calls map.getTiles to get the tiles its going to process', function() {
      var map = {
        getTiles: function() { return [ [] ]}
      };

      sinon.spy(map, 'getTiles');

      TileClassifier.classifyImpassableTiles(map);
      assert.equal(map.getTiles.callCount, 1);
    });

    it('iterates over every tile in the 2d tile array and if impassable calls classifyImpassableTile with that tile', function() {
      var impassableTile = { getProperty: function() {return false}};
      var passableTile = {getProperty: function() {return true}};

      sinon.spy(impassableTile, 'getProperty');
      sinon.spy(passableTile, 'getProperty');

      var tiles = [
        [ impassableTile, impassableTile, impassableTile ],
        [ passableTile, impassableTile, impassableTile],
        [ impassableTile, passableTile, impassableTile]
      ];

      var map = { getTiles: function() { return tiles}};

      TileClassifier.classifyImpassableTiles(map);

      assert.equal(impassableTile.getProperty.callCount, 7);
      assert.equal(passableTile.getProperty.callCount, 2);
      assert.ok(passableTile.getProperty.calledWith('passable'));
      assert.equal(TileClassifier.classifyImpassableTile.callCount, 7)
      assert.ok(TileClassifier.classifyImpassableTile.calledWith(tiles, 0, 0));
    });

    it('calls secondPassClassifyImpassableTile if this is the secondPass', function() {
      var tile = { getProperty: function() {return false}};
      var tiles = [ [tile] ];
      var map = {
        getTiles: function() { return tiles}
      };

      sinon.spy(map, 'getTiles');

      TileClassifier.classifyImpassableTiles(map, true);
      assert.ok(TileClassifier.secondPassClassifyImpassableTile.calledWith(tiles, 0, 0));
    });
  });

  describe('classifyImpassableTile', function() {
    beforeEach(function() {
      sinon.stub(TileClassifier, 'checkIfTileImpassable');
      sinon.stub(TileClassifier, 'calculateClassification');
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      TileClassifier.checkIfTileImpassable.restore();
      TileClassifier.calculateClassification.restore();
      console.error.restore();
    });

    it('gets tile: tiles[x,y] from the tiles 2d array and checks it is impassable', function() {
      var tile = {
        getProperty: function() {return true;},
        setProperty: function() {}
      };
       sinon.spy(tile, 'getProperty');
       var tiles = [
         [ 1, 2, 3],
         [ 4, tile, 5]
       ];

       TileClassifier.classifyImpassableTile(tiles, 1, 1);
       assert.ok(tile.getProperty.calledWith('passable'));
    });

    it('returns an error if the tile is not set or the tile is passable', function() {
      var tiles = [ [] ];

      TileClassifier.classifyImpassableTile(tiles, 0, 0);
      assert.ok(console.error.calledWith('Tile to check does not exist or is not impassable'));

      tiles = [ [ {getProperty: function() { return true; }} ] ]
      TileClassifier.classifyImpassableTile(tiles, 0,0);

      assert.equal(console.error.callCount, 2);
    });

    it('checks if tile to left is passable', function() {
      var checkTile = {
        getProperty: function() {return false;},
        setProperty: function() {}
      };
      var tiles = [
        [1, 1, 1],
        [1, checkTile, 1]
      ];

      TileClassifier.classifyImpassableTile(tiles, 1, 1);

      assert.ok(TileClassifier.checkIfTileImpassable.calledWith(tiles, 1, 0));
    });

    it('checks if tile above is passable', function() {
      var checkTile = {
        getProperty: function() {return false;},
        setProperty: function() {}
      };
      var tiles = [
        [1, 1, 1],
        [1, checkTile, 1]
      ];

      TileClassifier.classifyImpassableTile(tiles, 1, 1);

      assert.ok(TileClassifier.checkIfTileImpassable.calledWith(tiles, 0, 1));
    });

    it('checks if tile to the right is passable', function() {
      var checkTile = {
        getProperty: function() {return false;},
        setProperty: function() {}
      };
      var tiles = [
        [1, 1, 1],
        [1, checkTile, 1]
      ];

      TileClassifier.classifyImpassableTile(tiles, 1, 1);

      assert.ok(TileClassifier.checkIfTileImpassable.calledWith(tiles, 1, 2));
    });

    it('checks if tile below is passable', function() {
      var checkTile = {
        getProperty: function() {return false;},
        setProperty: function() {}
      };
      var tiles = [
        [1, 1, 1],
        [1, checkTile, 1],
        [1, 1, 1]
      ];

      TileClassifier.classifyImpassableTile(tiles, 1, 1);

      assert.ok(TileClassifier.checkIfTileImpassable.calledWith(tiles, 2, 1));
    });

    it('passes that data to a function that returns the classification of the tile', function() {

      var checkTile = {
        getProperty: function() {return false;},
        setProperty: function() {}
      };
      var tiles = [
        [1, 1, 1],
        [1, checkTile, 1],
        [1, 1, 1]
      ];

      TileClassifier.checkIfTileImpassable.onCall(0).returns(true);
      TileClassifier.checkIfTileImpassable.onCall(1).returns(false);
      TileClassifier.checkIfTileImpassable.onCall(2).returns(false);
      TileClassifier.checkIfTileImpassable.onCall(3).returns(true);

      var expectedImpassableData = {
        leftImpassable: true,
        topImpassable: false,
        rightImpassable: false,
        bottomImpassable: true
      };

      TileClassifier.classifyImpassableTile(tiles, 1, 1);

      assert.ok(TileClassifier.calculateClassification.calledWith(expectedImpassableData));
    });
  });

  describe('checkIfTileImpassable', function() {
    it('returns true if the tile is out of bounds', function() {

      var tiles = [
        [1,1,1],
        [1,1,1],
        [1,1,1]
      ];

      //Left
      var impassable = TileClassifier.checkIfTileImpassable(tiles, 0, -1);
      assert.strictEqual(impassable, true);

      //Up
      impassable = TileClassifier.checkIfTileImpassable(tiles, -1, 0);
      assert.strictEqual(impassable, true);

      //Right
      impassable = TileClassifier.checkIfTileImpassable(tiles, 0, 3);
      assert.strictEqual(impassable, true);

      //Down
      impassable = TileClassifier.checkIfTileImpassable(tiles, -1, 0);
      assert.strictEqual(impassable, true);
    });

    it('otherwise it returns the inverse of the result of getProperty(passable)', function() {
      var passable = false;
      var tile = { getProperty: function() { return passable}};
      sinon.spy(tile, 'getProperty');

      var tiles = [
        [1,1,1],
        [1,1,tile],
        [1,1,1]
      ];

      var tileImpassable = TileClassifier.checkIfTileImpassable(tiles, 1, 2);
      assert.ok(tile.getProperty.calledWith('passable'));
      assert.equal(tileImpassable, !passable);
    });
  });

  describe('calculateClassification', function() {
    it('returns Pillar if all surrounding tiles are passable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: false,
        rightImpassable: false,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Pillar');
    });

    it ('returns Vertical Wall if the left and right are passable and the top and bottom are impassable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: true,
        rightImpassable: false,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Vertical Wall');
    });

    it ('returns Horizontal Wall if the top and bottom are passable and both the left and right are impassable', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: false,
        rightImpassable: true,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Horizontal Wall');
    });

    it('returns Left Wall if the top, bottom, and left tiles are impassable', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: true,
        rightImpassable: false,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Left Wall');
    });

    it('returns Top Wall if the top, left, and right tiles are impassable', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: true,
        rightImpassable: true,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Top Wall Tall');
    });

    it('returns Right Wall if the top, right, and bottom tiles are impassable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: true,
        rightImpassable: true,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Right Wall');
    });

    it('returns Bottom Wall if the bottom, left, and right tiles are impassable', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: false,
        rightImpassable: true,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Bottom Wall');
    });

    it('returns Top Vertical Wall if the bottom is impassable and the rest are passable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: false,
        rightImpassable: false,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Top Vertical Wall');
    });

    //TEST 300!!! Wooooooooooooo!

    it('returns Bottom Vertical Wall if the top is impassable and the rest are passable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: true,
        rightImpassable: false,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Bottom Vertical Wall Tall');
    });

    it('returns Right Horizontal Wall if the left is impassable and the rest are passable', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: false,
        rightImpassable: false,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Right Horizontal Wall');
    });

    it('returns Left Horizontal Wall if the right is impassable and the rest are passable', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: false,
        rightImpassable: true,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Left Horizontal Wall');
    });

    it('returns Bottom Left if the right and top are impassable and the left and bottom are not', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: true,
        rightImpassable: true,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Bottom Left Tall');
    });

    it('returns Bottom Right if the left and top are impassable and the right and bottom are not', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: true,
        rightImpassable: false,
        bottomImpassable: false
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Bottom Right Tall');
    });

    it('returns Top Right if the left and bottom are impassable and the right and top are not', function() {
      var impassableData = {
        leftImpassable: true,
        topImpassable: false,
        rightImpassable: false,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Top Right');
    });

    it('returns Top Left if the right and bottom are impassable and the left and top are not', function() {
      var impassableData = {
        leftImpassable: false,
        topImpassable: false,
        rightImpassable: true,
        bottomImpassable: true
      };

      assert.equal(TileClassifier.calculateClassification(impassableData), 'Top Left');
    });
  });

  describe('secondPassClassifyImpassableTile', function() {
    beforeEach(function() {
      sinon.stub(TileClassifier, 'secondPassCalculateClassification');
      sinon.stub(TileClassifier, 'getClassification');
    });

    afterEach(function() {
      TileClassifier.secondPassCalculateClassification.restore();
      TileClassifier.getClassification.restore();
    });

    it('returns early if the tile passed in already has a classification', function() {
      var tile = {getProperty: function() {return 'a classification';}};
      var tiles = [[tile]];
      sinon.spy(tile, 'getProperty');

      TileClassifier.secondPassClassifyImpassableTile(tiles, 0, 0);

      assert.equal(tile.getProperty.callCount, 1);
      assert.ok(tile.getProperty.calledWith('classification'));
      assert.equal(TileClassifier.getClassification.callCount, 0);
    });

    it('builds classification data for the tile to the left, bottom, right and above', function() {

    });

    it('passes that data to secondPassCalculateClassification', function() {

    });
  });
});
