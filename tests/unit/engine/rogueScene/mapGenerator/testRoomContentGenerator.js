var assert = chai.assert;

describe('Room Content Generator', function() {
  describe ('generateAllRoomContent', function() {

    beforeEach(function() {
      sinon.stub(RoomContentGenerator, 'generateRoomContent');
      sinon.stub(RoomContentGenerator, 'getFurnitureGenerator');
      sinon.stub(RoomContentGenerator, 'getDecorationGenerator');
    });

    afterEach(function() {
      RoomContentGenerator.generateRoomContent.restore();
      RoomContentGenerator.getFurnitureGenerator.restore();
      RoomContentGenerator.getDecorationGenerator.restore();
    });

    it('Gets the rooms from the map passed in to it', function() {
      var map = {
        getRooms: function() {return {}},
        getTiles: function() {return []}
      };

      sinon.spy(map, 'getRooms');

      RoomContentGenerator.generateAllRoomContent(map);
      assert.equal(map.getRooms.callCount, 1);
    });

    it('gets furniture and decoration generators then calls generateRoomContent on each room, providing both', function() {
      var room1 = {'room': '1'};
      var room2 = {'room': '2'};
      var rooms = [ room1, room2];
      var map = {
        getRooms: function() {return rooms}
      };
      var furnitureGenerator = {'furniture': 'Generator'};
      var decorationGenerator = {'decoration': 'Generator'};

      RoomContentGenerator.getFurnitureGenerator.returns(furnitureGenerator);
      RoomContentGenerator.getDecorationGenerator.returns(decorationGenerator);
      RoomContentGenerator.generateAllRoomContent(map);
      assert.ok(RoomContentGenerator.generateRoomContent.calledWith(room1, map, furnitureGenerator, decorationGenerator));
    });
  });

  describe('generateRoomContent', function() {
    beforeEach(function() {
      sinon.stub(RoomContentGenerator, 'getFurnitureSet');
      sinon.stub(RoomContentGenerator, 'getDecorationSet');
    });

    afterEach(function() {
      RoomContentGenerator.getFurnitureSet.restore();
      RoomContentGenerator.getDecorationSet.restore();
    });

    it('gets a furniture set and passes it on to the furnitureGenerator', function() {
      var furnitureSet = [ {'Set Of': 'Furniture'}];
      var map = {furniture: [], decorations: [], getTiles: function() {}};
      var room = {};
      var furnitureGenerator = { generateFurniture: function() {}};
      var decorationGenerator = { generateDecorations: function() { return []}};

      sinon.stub(furnitureGenerator, 'generateFurniture');
      furnitureGenerator.generateFurniture.returns([]);
      RoomContentGenerator.getFurnitureSet.returns(furnitureSet);

      RoomContentGenerator.generateRoomContent(room, map, furnitureGenerator, decorationGenerator);

      assert.equal(RoomContentGenerator.getFurnitureSet.callCount, 1);
      assert.ok(furnitureGenerator.generateFurniture.calledWith(room, map, furnitureSet));
    });

    it('gets a decoration set and tiles and passes them on to the decorationGenerator', function() {
      var decorationSet = [ { 'Set of': 'Decorations'}];
      var tiles = ['some', 'tiles'];
      var map = {furniture: [], decorations: [], getTiles: function() {return tiles}};
      var room = {};
      var furnitureGenerator = { generateFurniture: function() { return []}};
      var decorationGenerator = { generateDecorations: function() {}};

      sinon.stub(decorationGenerator, 'generateDecorations');
      decorationGenerator.generateDecorations.returns([]);
      RoomContentGenerator.getDecorationSet.returns(decorationSet);

      RoomContentGenerator.generateRoomContent(room, map, furnitureGenerator, decorationGenerator);

      assert.equal(RoomContentGenerator.getDecorationSet.callCount, 1);
      assert.ok(decorationGenerator.generateDecorations.calledWith(room, tiles, decorationSet));
    });

    it('gets both furniture and decorations and attaches them to the room', function() {
      var furniture = ['a', 'b'];
      var decorations = ['c', 'd'];
      var map = {furniture: [], decorations: [], getTiles: function() {}};
      var room = {};
      var furnitureGenerator = { generateFurniture: function() { return furniture}};
      var decorationGenerator = { generateDecorations: function() { return decorations}};

      RoomContentGenerator.generateRoomContent(room, map, furnitureGenerator, decorationGenerator);

      assert.deepEqual(room.furniture, furniture);
      assert.deepEqual(room.decorations, decorations);
    });

    it('it also adds furniture and decorations onto the maps list of furniture and decorations', function() {
      var furniture = ['c', 'd'];
      var decorations = ['g', 'h'];
      var mapFurniture = ['a', 'b'];
      var mapDecorations = ['e', 'f'];
      var map = {furniture: mapFurniture, decorations: mapDecorations, getTiles: function() {}};
      var room = {};
      var furnitureGenerator = { generateFurniture: function() { return furniture}};
      var decorationGenerator = { generateDecorations: function() { return decorations}};

      RoomContentGenerator.generateRoomContent(room, map, furnitureGenerator, decorationGenerator);

      assert.deepEqual(map.furniture, ['a', 'b', 'c', 'd']);
      assert.deepEqual(map.decorations, ['e', 'f', 'g', 'h']);
    })
  });

  describe('getFurnitureSet', function() {
    it('returns objects representing the available furniture', function() {
      var furnitureSet = RoomContentGenerator.getFurnitureSet();
      assert.ok(furnitureSet.constructor == Array);
    });
  });

  describe('getDecorationSet', function() {
    it('returns objects representing the available decorations', function() {
      var decorationSet = RoomContentGenerator.getDecorationSet();
      assert.ok(decorationSet.constructor == Array);
    });
  });

  describe('calculateMaximumFurniture', function() {
    it('returns one tenth the area of the room as the max number of RoomContents', function() {
      var area = 100;
      var maxCollidables = RoomContentGenerator.calculateMaximumFurniture(area);
      assert.equal(area/10, maxCollidables);
    });
  });
});
