var assert = chai.assert;

describe('Map Generator', function() {
  describe ('generateMap', function() {
    beforeEach (function() {
      sinon.stub(MapGenerator, 'reset');
      sinon.stub(MapGenerator, 'getRoomGenerator');
      sinon.stub(MapGenerator, 'getCorridorGenerator');
      sinon.stub(MapGenerator, 'getDoorGenerator');
      sinon.stub(MapGenerator, 'getRoomContentGenerator');
      sinon.stub(MapGenerator, 'getEncounterGenerator');
      sinon.stub(MapGenerator, 'generateBase');
      sinon.stub(MapGenerator, 'generateRooms');
      sinon.stub(MapGenerator, 'generateCorridors');
      sinon.stub(MapGenerator, 'generateDoors');
      sinon.stub(MapGenerator, 'generateRoomContent');
      sinon.stub(MapGenerator, 'generateEnemies');
      sinon.stub(TileClassifier, 'classifyImpassableTiles');
    });

    afterEach (function() {
      MapGenerator.reset.restore();
      MapGenerator.getRoomGenerator.restore();
      MapGenerator.getCorridorGenerator.restore();
      MapGenerator.getDoorGenerator.restore();
      MapGenerator.getRoomContentGenerator.restore();
      MapGenerator.getEncounterGenerator.restore();
      MapGenerator.generateBase.restore();
      MapGenerator.generateRooms.restore();
      MapGenerator.generateCorridors.restore();
      MapGenerator.generateDoors.restore();
      MapGenerator.generateRoomContent.restore();
      MapGenerator.generateEnemies.restore();
      TileClassifier.classifyImpassableTiles.restore();
    });

    it('Resets itself then generates a map using a base, roomGenerator and corridorGenerator', function() {
      MapGenerator.getCorridorGenerator.returns({});
      MapGenerator.getRoomGenerator.returns({});
      MapGenerator.getDoorGenerator.returns({});
      MapGenerator.getRoomContentGenerator.returns({});

      MapGenerator.generateMap();

      assert.equal(MapGenerator.reset.callCount, 1);
      assert.equal(MapGenerator.generateBase.callCount, 1);
      assert.equal(MapGenerator.generateRooms.callCount, 1);
      assert.equal(MapGenerator.generateCorridors.callCount, 1);
      assert.equal(MapGenerator.generateRoomContent.callCount, 1);
    });

    it('Also generates doors using a doorGenerator', function() {
      MapGenerator.getDoorGenerator.returns({});

      MapGenerator.generateMap();

      assert.equal(MapGenerator.generateDoors.callCount, 1);
    });

    it('Calls TileClassifier twice with the final map to more accurately classify the impassable tiles', function() {
      var map = {'a': 'map'};
      MapGenerator.generateBase.returns(map);

      MapGenerator.generateMap();
      assert.equal(TileClassifier.classifyImpassableTiles.callCount, 2);
      assert.ok(TileClassifier.classifyImpassableTiles.calledWith(map));
      assert.ok(TileClassifier.classifyImpassableTiles.calledWith(map, true));
    });

    it('calls generateEnemies with itself and an encounterGenerator, to populate the map with appropriate enemies', function() {
      var map = {'a': 'map'};
      var encounterGenerator = {'an': 'encounterGenerator'};

      MapGenerator.getEncounterGenerator.returns(encounterGenerator);
      MapGenerator.generateBase.returns(map);

      MapGenerator.generateMap();
      assert.equal(MapGenerator.getEncounterGenerator.callCount, 1);
      assert.ok(MapGenerator.generateEnemies.calledWith(encounterGenerator, map));
    });
  });

  describe('getRoomGenerator', function() {
    it('returns the roomGenerator the MapGenerator should use', function() {
      assert.ok(MapGenerator.getRoomGenerator());
    });
  });

  describe('getCorridorGenerator', function() {
    it('returns the corridorGenerator the MapGenerator should use', function() {
      assert.ok(MapGenerator.getCorridorGenerator());
    });
  });

  describe('getDoorGenerator', function() {
    it('returns the doorGenerator the MapGenerator should use', function() {
      assert.ok(MapGenerator.getDoorGenerator());
    });
  })

  describe('generateRooms', function() {
    beforeEach(function() {
      sinon.stub(MapGenerator, 'applyRoomsToMap');
      sinon.stub(MapGenerator, 'generateRoomLinks');
      sinon.stub(MapGenerator, 'addEntranceAndExit');
      sinon.stub(Map.prototype, 'setRooms');
    });

    afterEach(function () {
      MapGenerator.applyRoomsToMap.restore();
      MapGenerator.generateRoomLinks.restore();
      MapGenerator.addEntranceAndExit.restore();
      Map.prototype.setRooms.restore();
    });

    it('gets rooms from the room generator then applies them', function() {
      var rooms = ['rooms'];
      var roomGenerator = {
        generateRooms: function () { return ['rooms']; }
      };
      var map = new Map();
      MapGenerator.generateRooms(roomGenerator, map);

      assert.ok(map.setRooms.calledWith(rooms));
      assert.equal(MapGenerator.applyRoomsToMap.callCount, 1);
      assert.equal(MapGenerator.generateRoomLinks.callCount, 1);
      assert.equal(MapGenerator.addEntranceAndExit.callCount, 1);
    });
  });

  describe('generateCorridors', function() {
    beforeEach(function() {
      sinon.stub(MapGenerator, 'applyCorridorsToMap');
    });

    afterEach(function() {
      MapGenerator.applyCorridorsToMap.restore();
    });

    it('gets corridors from the corridor generator then applys them to the map', function() {
      MapGenerator.roomWeb = {};
      var corridorGenerator = { generateCorridors: function() {}};
      sinon.stub(corridorGenerator, 'generateCorridors');

      MapGenerator.generateCorridors(corridorGenerator, {});
    });
  });

  describe('addEntranceAndExit', function() {
    beforeEach(function() {
      sinon.stub(Map.prototype, 'getRooms');
    });

    afterEach(function() {
      Map.prototype.getRooms.restore();
    });

    it('adds stairs up in the first room', function() {
      var startRoom = new Room(1, 1, 1, 1);
      sinon.stub (startRoom, 'getRoundedCenter').returns({x: 1, y: 1});

      var endRoom = new Room(2, 2, 2, 2);
      sinon.stub (endRoom, 'getRoundedCenter').returns({x: 2, y: 2});

      Map.prototype.getRooms.returns([startRoom, endRoom]);

      var map = new Map();
      MapGenerator.addEntranceAndExit(map);

      var startEntrance = map.getEntrances()[0];
      assert.equal(startEntrance.getX(), 1);
      assert.equal(startEntrance.getY(), 1);
      assert.equal(startEntrance.getClassification(), 'Stairs Up');
    });

    it('adds stairs down in the last room', function() {
      var startRoom = new Room(1, 1, 1, 1);
      sinon.stub (startRoom, 'getRoundedCenter').returns({x: 1, y: 1});

      var endRoom = new Room(2, 2, 2, 2);
      sinon.stub (endRoom, 'getRoundedCenter').returns({x: 2, y: 2});

      Map.prototype.getRooms.returns([startRoom, endRoom]);

      var map = new Map();
      MapGenerator.addEntranceAndExit(map);

      var endEntrance = map.getEntrances()[1];
      assert.equal(endEntrance.getX(), 2);
      assert.equal(endEntrance.getY(), 2);
      assert.equal(endEntrance.getClassification(), 'Stairs Down');
    });

    it('moves the exit stairs by 1x and 1y if there is only one room', function() {
      var startRoom = new Room(1, 1, 10, 10);
      var endCenter = {x: 5, y: 5};
      sinon.stub(startRoom, 'getRoundedCenter').returns(endCenter);

      Map.prototype.getRooms.returns([startRoom]);

      var map = new Map();
      MapGenerator.addEntranceAndExit(map);

      var endEntrance = map.getEntrances()[1];
      assert.equal(endEntrance.getX(), endCenter.x + 1);
      assert.equal(endEntrance.getY(), endCenter.y + 1);
    });

    it('adds id of 0 to stairsUp and id of 1 to stairsDown', function() {
      var startRoom = new Room(1, 1, 1, 1);
      sinon.stub (startRoom, 'getRoundedCenter').returns({x: 1, y: 1});

      var endRoom = new Room(2, 2, 2, 2);
      sinon.stub (endRoom, 'getRoundedCenter').returns({x: 2, y: 2});

      Map.prototype.getRooms.returns([startRoom, endRoom]);

      var map = new Map();
      MapGenerator.addEntranceAndExit(map);

      var startEntrance = map.getEntrances()[0];
      assert.equal(startEntrance.getId(), 0);

      var endEntrance = map.getEntrances()[1];
      assert.equal(endEntrance.getId(), 1);
    });
  });

  describe('generateEnemies', function() {
    it('gets a list of encounters by calling generateEncounters', function() {
      var encounterGenerator = {generateEncounters: function() { return [];}};
      var map = { setEnemies: function() {}};

      sinon.spy(encounterGenerator, 'generateEncounters');
      MapGenerator.generateEnemies(encounterGenerator, map);

      assert.equal(encounterGenerator.generateEncounters.callCount, 1);
    });

    it('then goes through each encounter and adds every enemy in each to the list of enemies on the map', function() {
      var encounters = [
        ['enemy1', 'enemy2'],
        ['enemy3']
      ];

      var encounterGenerator = {generateEncounters: function() { return encounters;}};
      var map = { setEnemies: function() {}};

      sinon.spy(map, 'setEnemies');
      var expectedEnemies = ['enemy1', 'enemy2', 'enemy3'];

      MapGenerator.generateEnemies(encounterGenerator, map);
      assert.ok(map.setEnemies.calledWith(expectedEnemies));
    });
  });
});
