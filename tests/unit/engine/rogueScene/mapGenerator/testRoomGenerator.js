var assert = chai.assert;

describe('Room Generator', function() {
  describe ('generateRooms', function() {
    beforeEach (function() {
      sinon.stub(RoomGenerator, 'calculateNumberRooms');
      sinon.stub(RoomGenerator, 'checkRoomValid');
      sinon.stub(RoomGenerator, 'generateRoom');
    });

    afterEach (function() {
      RoomGenerator.calculateNumberRooms.restore();
      RoomGenerator.checkRoomValid.restore();
      RoomGenerator.generateRoom.restore();
    });

    it('calls generate room then returns array of all valid rooms', function() {
      RoomGenerator.calculateNumberRooms.returns(3);
      RoomGenerator.checkRoomValid.returns(true);
      RoomGenerator.generateRoom.returns({
        cleanHooks: function() {},
        setId: function() {}
      });

      var rooms = RoomGenerator.generateRooms({});
      assert.equal(rooms.length, 3);
    });

    it ('does not add invalid rooms, instead generates another', function() {
      RoomGenerator.calculateNumberRooms.returns(3);
      RoomGenerator.checkRoomValid.returns(true);
      RoomGenerator.checkRoomValid.onCall(2).returns(false);
      RoomGenerator.generateRoom.returns({
        cleanHooks: function() {},
        setId: function() {}
      });

      var rooms = RoomGenerator.generateRooms({});
      assert.equal(RoomGenerator.generateRoom.callCount, 4);

    });

    it('always adds at least one room, even if it takes more than 10 attempts to place it', function() {
      RoomGenerator.calculateNumberRooms.returns(1);
      RoomGenerator.checkRoomValid.returns(false);
      RoomGenerator.checkRoomValid.onCall(11).returns(true);
      RoomGenerator.generateRoom.returns({
        cleanHooks: function() {},
        setId: function() {}
      });

      var rooms = RoomGenerator.generateRooms({});
      assert.equal(rooms.length, 1);
    });
  });

  describe('calculateNumberRooms', function() {
    it('Returns a number indicating how many rooms will be generated for the current floor', function() {
      assert.ok(RoomGenerator.calculateNumberRooms() >= 0);
    });
  });

  describe('checkRoomValid', function() {
    it('returns true if the room doesnt collide with others, has hooks and is within the boundaries of the map', function() {

    });
  });
});
