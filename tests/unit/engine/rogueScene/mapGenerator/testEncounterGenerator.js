var assert = chai.assert;

describe('Encounter Generator', function() {
  describe ('generateEncounters', function() {
    beforeEach( function() {
      sinon.stub(EncounterGenerator, 'generateEncounter');
      sinon.stub(Math, 'random');
    });

    afterEach(function() {
      EncounterGenerator.generateEncounter.restore();
      Math.random.restore();
    });

    it ('picks a single random room then calls generate encounter with that room', function() {
      var rooms = ['room1', 'room2', 'room3'];
      var map = { getRooms: function() { return rooms }};
      sinon.spy (map, 'getRooms');
      var encounter = {'an': 'encounter'};
      Math.random.returns(1);

      EncounterGenerator.generateEncounter.returns(encounter);

      var encounters = EncounterGenerator.generateEncounters(map);
      assert.equal(EncounterGenerator.generateEncounter.callCount, 1);
      assert.equal(map.getRooms.callCount, 1);
      assert.ok(EncounterGenerator.generateEncounter.calledWith('room1') || EncounterGenerator.generateEncounter.calledWith('room2') || EncounterGenerator.generateEncounter.calledWith('room3'));
      assert.deepEqual(encounters, [encounter]);
    });

    it('returns without generating an encounter if there is only one room - FOR NOW', function() {
      var rooms = ['room1'];
      var map = { getRooms: function() { return rooms; }};

      var encounters = EncounterGenerator.generateEncounters(map);
      assert.equal(EncounterGenerator.generateEncounter.callCount, 0);
    });

    it('puts the enemy in the second room if the first room is returned at random', function() {
      var room2 = {'a': 'room'};
      var rooms = ['room1', room2];
      var map = { getRooms: function() { return rooms; }};

      Math.random.returns(0);

      EncounterGenerator.generateEncounter.returns({});

      var encounters = EncounterGenerator.generateEncounters(map);
      assert.ok(EncounterGenerator.generateEncounter.calledWith(room2));
    });
  });

  describe('generateEncounter', function() {
    beforeEach(function() {
      sinon.stub(EnemyLibrary, 'getRandomNewEnemy');
    });

    afterEach(function() {
      EnemyLibrary.getRandomNewEnemy.restore();
    });

    it('places a random enemy in the center of the room provided then returns an array with that enemy in it', function() {
      var center = {x: 10, y: 5};
      var enemy = {'an': 'enemy'};
      var room = {getRoundedCenter: function() { return center}};
      sinon.spy(room, 'getRoundedCenter');

      EnemyLibrary.getRandomNewEnemy.returns(enemy);
      var expectedPosition = {x: center.x, y: center.y + 1};
      var encounter = EncounterGenerator.generateEncounter(room);
      assert.equal(room.getRoundedCenter.callCount, 1);
      assert.ok(EnemyLibrary.getRandomNewEnemy.calledWith(expectedPosition));
      assert.ok(encounter.length, 1);
      assert.deepEqual(encounter[0], enemy);
    });
  });
});
