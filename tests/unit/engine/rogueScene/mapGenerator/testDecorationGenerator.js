var assert = chai.assert;

describe('Decoration Generator', function() {
  describe('generateDecorations', function() {
      beforeEach(function() {
        sinon.stub(DecorationGenerator, 'calculateNumberDecorations');
        sinon.stub(DecorationGenerator, 'generateDecoration');
      });

      afterEach(function() {
        DecorationGenerator.calculateNumberDecorations.restore();
        DecorationGenerator.generateDecoration.restore();
      });

      it('gets how many decorations to generate then calls generateDecorations that many times', function() {
        var numberDecorations = 5;
        DecorationGenerator.generateDecoration.returns({});
        DecorationGenerator.calculateNumberDecorations.returns(numberDecorations);

        var decorations = DecorationGenerator.generateDecorations({}, {}, []);
        assert.equal(DecorationGenerator.generateDecoration.callCount, numberDecorations);
      });
  });

  describe ('generateDecoration', function() {
    beforeEach(function() {
      sinon.stub(DecorationGenerator, 'pickRandomDecorationContent');
      sinon.stub(DecorationGenerator, 'calculatePositionForDecoration');
      sinon.stub(DecorationGenerator, 'buildDecoration');
    });

    afterEach(function() {
      DecorationGenerator.pickRandomDecorationContent.restore();
      DecorationGenerator.calculatePositionForDecoration.restore();
      DecorationGenerator.buildDecoration.restore();
    });

    it ('picks a piece of content from the decoration set', function() {
      var decorationSet = [ 'a', 'b', 'c'];
      var room = {'a': 'room'};
      DecorationGenerator.pickRandomDecorationContent.returns('a');
      var decoration = DecorationGenerator.generateDecoration(room, decorationSet);
      assert.equal(DecorationGenerator.pickRandomDecorationContent.callCount, 1);
    });

    it ('tries to place the new decoration appropriately', function() {
      var decorationContent = {'a': 'decoration'};
      var room = {'a': 'room'};
      DecorationGenerator.pickRandomDecorationContent.returns(decorationContent);
      var decoration = DecorationGenerator.generateDecoration(room, []);
      assert.ok(DecorationGenerator.calculatePositionForDecoration.calledWith(room, decorationContent));
    });

    it('builds a Decoration using the decoration content and the position', function() {
      var decorationContent = {'a': 'decoration'};
      var position = {'x': 1, 'y': 2};
      DecorationGenerator.pickRandomDecorationContent.returns(decorationContent);
      DecorationGenerator.calculatePositionForDecoration.returns(position);
      var decoration = DecorationGenerator.generateDecoration({}, []);
      assert.ok(DecorationGenerator.buildDecoration.calledWith(decorationContent, position));
    });
  });

  describe('calculateNumberDecorations', function() {
    beforeEach(function() {
      sinon.stub(Math, 'random');
      sinon.stub(DecorationGenerator, 'calculateMinimumDecorations');
      sinon.stub(DecorationGenerator, 'calculateMaximumDecorations');
    });

    afterEach(function() {
      Math.random.restore();
      DecorationGenerator.calculateMinimumDecorations.restore();
      DecorationGenerator.calculateMaximumDecorations.restore();
    });

    it('gets a random number of value less then or equal to maximumDecorations', function() {

      Math.random.returns(1)
      var maximumDecorations = 10;
      DecorationGenerator.calculateMaximumDecorations.returns(maximumDecorations);
      var miniumDecorations = 0;
      DecorationGenerator.calculateMinimumDecorations.returns(miniumDecorations);

      var room = { getArea: function() { return 9}};
      var numberDecorations = DecorationGenerator.calculateNumberDecorations(room);

      assert.equal(numberDecorations, maximumDecorations);
    });

    it ('gets the area of the room and passes it to calculateMaximumDecorations', function() {
      var area = 15;
      var room = {getArea: function() {return area}};
      sinon.spy(room, 'getArea');

      var maximumDecorations = 10;
      DecorationGenerator.calculateMaximumDecorations.returns(maximumDecorations);
      var miniumDecorations = 0;
      DecorationGenerator.calculateMinimumDecorations.returns(miniumDecorations);

      var numberDecorations = DecorationGenerator.calculateNumberDecorations(room);
      assert.ok(DecorationGenerator.calculateMaximumDecorations.calledWith(area));
    })
  });

  describe('calculateMaximumDecorations', function() {
    it('returns an integer defining the maximum number of decorations for a room', function() {
      var maxDecorations = DecorationGenerator.calculateMaximumDecorations(9);
      assert.equal(typeof maxDecorations, 'number');
    });
  });

  describe ('calculateMinimumDecorations', function() {
    it ('returns an integer defining the minimum number of decorations for a room', function() {
      var minDecorations = DecorationGenerator.calculateMinimumDecorations();
      assert.equal(typeof minDecorations, 'number');
    });
  });

  describe('pickRandomDecorationContent', function() {
    beforeEach(function() {
      sinon.spy(Math, 'round');
    });

    afterEach(function() {
      Math.round.restore();
    });

    it('picks a random index between 0 and the length of decorationSet - 1', function() {
      var decorationSet = [1, 2, 3, 4, 5];
      var decorationContent = DecorationGenerator.pickRandomDecorationContent(decorationSet);
      assert.ok(function() {
        for (var i = 0; i < decorationSet.length; i++) {
          if (decorationSet == decorationContent) return true;
        }
        return false;
      });
    })
  });

  describe('calculatePositionForDecoration', function() {
    it ('picks a random x position anywhere in the room, at most the width of the object away from the right wall', function() {
      var x = 5;
      var width = 10;
      var decorationWidth = 2;

      var room = {
        getX: function() {return x},
        getY: function() {return 8},
        getWidth: function() {return width},
        getHeight: function() {7},
      }

      var decorationContent = {
        getProperty: function() {return decorationWidth;}
      };

      var position = DecorationGenerator.calculatePositionForDecoration(room, decorationContent);
      assert.ok (position.x >= x);
      assert.ok (position.x <= x + width - decorationWidth);
    });

    it ('picks a random y position anywhere in the room at least decorationHeight away from the top wall', function() {
      var y = 10;
      var height = 27;
      var decorationHeight = 3;
      var room = {
        getX: function() {return 5},
        getY: function() {return y},
        getWidth: function() {return 11},
        getHeight: function() {return height}
      };

      var decorationContent = {
        getProperty: function() {return decorationHeight;}
      };

      var position = DecorationGenerator.calculatePositionForDecoration(room, decorationContent);
      assert.ok (position.y >= y);
      assert.ok (position.y <= y + height - decorationHeight);
    });
  });
});
