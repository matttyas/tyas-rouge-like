var assert = chai.assert;

describe('Item Content Library', function() {
  describe('getContentData', function() {

  });

  describe('getItemLibraryData', function() {
    it('returns the raw Item data from the library', function() {
      var itemLibraryData = ItemContentLibrary.getItemLibraryData();
      assert.ok(itemLibraryData.length > 0);
    });
  });

  describe('getItems', function() {
    beforeEach(function() {
      sinon.stub(ItemContentLibrary, 'generateItems');
    });

    afterEach(function() {
      ItemContentLibrary.generateItems.restore();
    });

    it('returns an array of item data that is used during game generation', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.generateItems.returns(generatedItems)
      var items = ItemContentLibrary.getItems();
      assert.deepEqual(items, generatedItems);
    });

    it('calls generate items if the items have not already been generated', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.generateItems.returns(generatedItems);
      ItemContentLibrary.items = undefined;
      var items = ItemContentLibrary.getItems();
      assert.equal(ItemContentLibrary.generateItems.callCount, 1);
      assert.deepEqual(items, generatedItems);
    });

    it('does not call generate items if they have already been generated', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.items = generatedItems;
      var items = ItemContentLibrary.getItems();
      assert.equal(ItemContentLibrary.generateItems.callCount, 0);
    });
  });

  describe('generateItems', function() {
    beforeEach( function() {
      sinon.stub(ItemContentLibrary, 'getItemLibraryData');
      sinon.stub(Item.prototype, 'initialise');
    });

    afterEach(function() {
      ItemContentLibrary.getItemLibraryData.restore();
      Item.prototype.initialise.restore();
    });

    it('calls getItemLibraryData to get all item data from the ItemLibrary', function() {
      ItemContentLibrary.getItemLibraryData.returns([]);
      ItemContentLibrary.generateItems();
      assert.equal(ItemContentLibrary.getItemLibraryData.callCount, 1);
    });

    it('then goes through each piece of LibraryContent and creates a new Item from it', function() {
      var itemLibraryData = [
        {properties: {'some': 'properties'}},
        {properties: {'more': 'properties'}}
      ];

      ItemContentLibrary.getItemLibraryData.returns(itemLibraryData);

      var items = ItemContentLibrary.generateItems();
      assert.ok(Item.prototype.initialise.calledWith(itemLibraryData[0].properties));
      assert.ok(Item.prototype.initialise.calledWith(itemLibraryData[1].properties));
    });
  });

  describe('getRogueItems', function() {
    beforeEach(function() {
      sinon.stub(ItemContentLibrary, 'generateRogueItems');
    });

    afterEach(function() {
      ItemContentLibrary.generateRogueItems.restore();
    });

    it('returns an array of item data that is used during game generation', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.generateRogueItems.returns(generatedItems)
      var rogueItems = ItemContentLibrary.getRogueItems();
      assert.deepEqual(rogueItems, generatedItems);
    });

    it('calls generate items if the items have not already been generated', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.generateRogueItems.returns(generatedItems);
      ItemContentLibrary.rogueItems = undefined;
      var items = ItemContentLibrary.getRogueItems();
      assert.equal(ItemContentLibrary.generateRogueItems.callCount, 1);
      assert.deepEqual(items, generatedItems);
    });

    it('does not call generate items if they have already been generated', function() {
      var generatedItems = ['generated', 'item', 'data'];
      ItemContentLibrary.items = generatedItems;
      var items = ItemContentLibrary.getRogueItems();
      assert.equal(ItemContentLibrary.generateRogueItems.callCount, 0);
    });
  });

  describe('generateRogueItems', function() {
    beforeEach(function() {
      sinon.stub(ItemContentLibrary, 'generateItems');
      sinon.stub(ItemContentLibrary, 'getItems');
      sinon.stub(RogueItem.prototype, 'initialise');
      sinon.stub(ItemContentLibrary, 'generateRogueItemsLib');
    });

    afterEach(function() {
      ItemContentLibrary.generateItems.restore();
      RogueItem.prototype.initialise.restore();
      ItemContentLibrary.getItems.restore();
      ItemContentLibrary.generateRogueItemsLib.restore();
    });

    it('goes through each of the ItemContentLibrary.items and creates a new RogueItem for it', function() {
      var items = [
        {properties: {'some': 'properties'}},
        {properties: {'more': 'properties'}}
      ];

      ItemContentLibrary.getItems.returns(items);
      var rogueItems = ItemContentLibrary.generateRogueItems();

      assert.ok(RogueItem.prototype.initialise.calledWith(items[0]));
      assert.ok(RogueItem.prototype.initialise.calledWith(items[1]));
    });

    it('then passes that rogueItems array to ItemContentLibrary.generateRogueItemsLib', function() {
      var items = [
        {properties: {'some': 'properties'}},
        {properties: {'more': 'properties'}}
      ];

      ItemContentLibrary.getItems.returns(items);
      var rogueItems = ItemContentLibrary.generateRogueItems();

      assert.ok(ItemContentLibrary.generateRogueItemsLib.callCount, 1);
    });
  });

  describe('generateRogueItemsLib', function() {
    it('Takes an array of rogueItems and builds an object that matches that array', function() {

      var rogueItem1 = {
        getName: function() { return'rogueItem1'}
      };

      var rogueItem2 = {
        getName: function() {return 'rogueItem2'}
      };

      sinon.spy(rogueItem1, 'getName');
      sinon.spy(rogueItem2, 'getName');

      var rogueItems = [rogueItem1, rogueItem2];

      ItemContentLibrary.generateRogueItemsLib(rogueItems);

      var expected = {
        'rogueItem1': rogueItem1,
        'rogueItem2': rogueItem2
      };

      assert.deepEqual(ItemContentLibrary.rogueItemsLib, expected);
    });
  });

  describe('processContent', function() {
    it('calls processContent on each rogueItem that has content set in the content object', function() {
      var rogueItem1 = {
        'getName': function() {return 'rogueItem1'},
        'processContent': function() {}
      };

      var rogueItem2 = {
        'getName': function() {return 'rogueItem2'},
        'processContent': function() {}
      };

      sinon.spy(rogueItem1, 'processContent');
      sinon.spy(rogueItem2, 'processContent');

      ItemContentLibrary.rogueItems = [rogueItem1, rogueItem2];

      var content = {
        'rogueItem1': {'some': 'content'}
      };

      ItemContentLibrary.processContent(content);

      assert.equal(rogueItem1.processContent.callCount, 1);
      assert.ok(rogueItem1.processContent.calledWith(content.rogueItem1));
      assert.equal(rogueItem2.processContent.callCount, 0);
    });
  });

  describe('getItem', function() {
    beforeEach(function() {
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      console.error.restore();
    });

    it('returns the item with the given name from rogueItemsLib', function() {
      var bacon = {'delicious': true};
      ItemContentLibrary.rogueItemsLib = { 'Bacon': bacon}

      var rogueItem = ItemContentLibrary.getItem('Bacon');
      assert.deepEqual(rogueItem, bacon);
    });

    it('but returns an error if it is not in the library', function() {
      var bacon = {'delicious': true};
      ItemContentLibrary.rogueItemsLib = { 'Bacon': bacon}

      var rogueItem = ItemContentLibrary.getItem('Sausages');
      assert.ok(console.error.calledWith('Item: Sausages does not exist in database'));
    });
  });
});
