var assert = chai.assert;

describe('Great Filter', function() {
  describe('pick', function() {
    beforeEach(function() {
      sinon.stub(GreatFilter, 'generateCheckOrder');
      sinon.stub(GreatFilter, 'checkObjectMatchesSelector');
    });

    afterEach( function() {
      GreatFilter.generateCheckOrder.restore();
      GreatFilter.checkObjectMatchesSelector.restore();
    });

    it ('generates a random order to check the objects against the selector', function() {
      var objects = ['array', 'of', 'objects'];
      GreatFilter.generateCheckOrder.returns([2, 0, 1]);
      var selectedObject = GreatFilter.pick({}, objects);

      assert.ok(GreatFilter.generateCheckOrder.calledWith(objects));
    });

    it('tests each object in the objects array and returns null if none match the selector', function() {
      var objects = [{properties: 'array'}, {properties: 'of'}, {properties: 'objects'}];
      var selector = {'the': 'selector'};

      GreatFilter.generateCheckOrder.returns([2, 0, 1]);
      GreatFilter.checkObjectMatchesSelector.returns(false);
      var selectedObject = GreatFilter.pick(selector, objects);

      assert.equal(GreatFilter.checkObjectMatchesSelector.callCount, objects.length);
      assert.ok(GreatFilter.checkObjectMatchesSelector.calledWith(selector, objects[0].properties));
      assert.ok(GreatFilter.checkObjectMatchesSelector.calledWith(selector, objects[1].properties));
      assert.ok(GreatFilter.checkObjectMatchesSelector.calledWith(selector, objects[2].properties));

      assert.equal(selectedObject, null);
    });

    it('returns the first object that does match the selector', function() {
      var objects = ['array', 'of', 'objects'];
      var selector = {'the': 'selector'};

      GreatFilter.generateCheckOrder.returns([2, 0, 1]);
      GreatFilter.checkObjectMatchesSelector.returns(false);
      GreatFilter.checkObjectMatchesSelector.onCall(1).returns(true);
      var selectedObject = GreatFilter.pick(selector, objects);

      assert.deepEqual(selectedObject, objects[0]);
    });
  });

  describe('generateCheckOrder', function() {
    beforeEach(function() {
      sinon.spy (Math, 'random');
    });

    afterEach(function() {
      Math.random.restore();
    });

    it('generates an array equal in length to the one passed to it', function() {
      var objects = ['a', 'b', 'c', 'd', 'e'];
      var checkOrder = GreatFilter.generateCheckOrder(objects);

      assert.equal(checkOrder.length, objects.length);
    });

    it('scrambles the order of the array by swapping a random element in the array with the latest one', function() {
      var objects = ['a', 'b', 'c', 'd', 'e'];
      var checkOrder = GreatFilter.generateCheckOrder(objects);
      assert.equal(Math.random.callCount, checkOrder.length - 2)
    });

  });

  describe('checkObjectMatchesSelector', function() { //Cheap checks first
    it('checks that the decorations width is less than or equal to the maxWidth', function() {
      var selector = {
        maxWidth: 1
      };

      var object = { width: 2};

      assert.ok(!GreatFilter.checkObjectMatchesSelector(selector, object));
    });

    it('checks that the decorations height is less than or equal to the maxHeight', function() {
      var selector = {
        maxHeight: 1
      };

      var object = { height: 2};

      assert.ok(!GreatFilter.checkObjectMatchesSelector(selector, object));
    });

    it('returns false if all tags in selector.must are not present', function() {
      var selector = {
        must: ['very', 'important']
      };

      var object = {
        tags: [ 'important', '...fairly']
      };

      assert.ok(!GreatFilter.checkObjectMatchesSelector(selector, object));
    });

    it('checks that the decoration does not have any tags in the must not list', function() {
      var selector = {
        mustNot: ['wall']
      };

      var object = {
        tags: ['wide', 'unique', 'wall']
      };

      assert.ok(!GreatFilter.checkObjectMatchesSelector(selector, object));
    });

    it('returns true if all checks pass', function() {
      var object = {
        width: 1,
        height: 1,
        tags: ['wall', 'ornate']
      };

      var selector = {
        maxWidth: 2,
        maxHeight: 1,
        must: ['wall'],
        mustNot: ['simple', 'ruined']
      };

      assert.ok(GreatFilter.checkObjectMatchesSelector(selector, object));
    });

  });
});
