var assert = chai.assert;

describe('Pattern', function() {
  describe('pickSuitableDecorations', function() {
    beforeEach(function() {
      sinon.stub (GreatFilter, 'pick');
    });

    afterEach(function() {
      GreatFilter.pick.restore();
    });

    it('Uses the GreatFilter to pick decorations based on its selectors', function() {
      var requiredDecoration = 'requiredDecoration';
      var decorationSet = ['a', 'b', 'c'];
      var selectedDecoration = 'selectedDecoration';

      GreatFilter.pick.returns(selectedDecoration);
      var pattern = new Pattern();
      pattern.selectors = ['1', '2', '3'];
      pattern.pickSuitableDecorations(decorationSet);

      assert.ok(GreatFilter.pick.calledWith(pattern.selectors[0], decorationSet));
      assert.ok(GreatFilter.pick.calledWith(pattern.selectors[1], decorationSet));
      //assert.ok(GreatFilter.pick.calledWith(pattern.selectors[2], decorationSet));

      //assert.deepEqual(pattern.decorations, ['selectedDecoration', 'selectedDecoration', 'selectedDecoration'])
    });
  });

  describe('generateDecorations', function() {
    it('returns an empty array as it is meant only to be overwritten by child elements', function() {
      var pattern = new Pattern();
      assert.deepEqual(pattern.generateDecorations({}), []);
    });
  });

  describe('getName', function() {
    it('returns the patterns name if it has been set', function() {
      var patternName = "Mike Pattern";
      var pattern = new Pattern();
      pattern.name = patternName;

      assert.equal(pattern.getName(), patternName);
    });
    it('returns Unnamed Pattern otherwise', function() {
      var pattern = new Pattern();
      assert.equal(pattern.getName(), 'Unnamed Pattern');
    })
  });
});
