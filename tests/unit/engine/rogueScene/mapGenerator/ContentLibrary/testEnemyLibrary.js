var assert = chai.assert;

describe('Enemy Library', function() {
  describe('getContentData', function() {

  });

  describe('getEnemyLibraryData', function() {
    it('returns the raw Enemy data from the library', function() {
      var enemyLibraryData = EnemyLibrary.getEnemyLibraryData();
      assert.ok(enemyLibraryData.length > 0);
    });
  });

  describe('getEnemies', function() {
    beforeEach(function() {
      sinon.stub(EnemyLibrary, 'generateEnemies');
    });

    afterEach(function() {
      EnemyLibrary.generateEnemies.restore();
    });

    it('returns an array of enemy data that is used during game generation', function() {
      var generatedEnemies = ['generated', 'enemy', 'data'];
      EnemyLibrary.generateEnemies.returns(generatedEnemies)
      var enemies = EnemyLibrary.getEnemies();
      assert.deepEqual(enemies, generatedEnemies);
    });

    it('calls generate enmies if the enemies have not already been generated', function() {
      var generatedEnemies = ['generated', 'enemy', 'data'];
      EnemyLibrary.generateEnemies.returns(generatedEnemies);
      EnemyLibrary.enemies = undefined;
      var enemies = EnemyLibrary.getEnemies();
      assert.equal(EnemyLibrary.generateEnemies.callCount, 1);
      assert.deepEqual(enemies, generatedEnemies);
    });

    it('does not call generate enemies if they have already been generated', function() {
      var generatedEnemies = ['generated', 'enemy', 'data'];
      EnemyLibrary.enemies = generatedEnemies;
      var enemies = EnemyLibrary.getEnemies();
      assert.equal(EnemyLibrary.generateEnemies.callCount, 0);
    });
  });

  describe('generateEnemies', function() {
    beforeEach( function() {
      sinon.stub(EnemyLibrary, 'getEnemyLibraryData');
      sinon.stub(Enemy.prototype, 'initialise');
    });

    afterEach(function() {
      EnemyLibrary.getEnemyLibraryData.restore();
      Enemy.prototype.initialise.restore();
    });

    it('calls getEnemyLibraryData to get all enemy data from the EnemyLibrary', function() {
      EnemyLibrary.getEnemyLibraryData.returns([]);
      EnemyLibrary.generateEnemies();
      assert.equal(EnemyLibrary.getEnemyLibraryData.callCount, 1);
    });

    it('then goes through each piece of LibraryContent and creates a new Enemy from it', function() {
      var enemyLibraryData = [
        {properties: {'some': 'properties'}},
        {properties: {'more': 'properties'}}
      ];

      EnemyLibrary.getEnemyLibraryData.returns(enemyLibraryData);

      var enemies = EnemyLibrary.generateEnemies();
      assert.ok(Enemy.prototype.initialise.calledWith(enemyLibraryData[0]));
      assert.ok(Enemy.prototype.initialise.calledWith(enemyLibraryData[1]));
    });
  });

  describe('getEnemy', function() {
    beforeEach(function() {
      sinon.stub(console, 'error');
    });

    afterEach(function() {
      console.error.restore();
    });

    it('returns the enemy with the given name from rogueEnemiesLib', function() {
      var bacon = {'delicious': true};
      EnemyLibrary.rogueEnemiesLib = { 'Bacon': bacon}

      var rogueEnemy = EnemyLibrary.getEnemy('Bacon');
      assert.deepEqual(rogueEnemy, bacon);
    });

    it('but returns an error if it is not in the library', function() {
      var bacon = {'delicious': true};
      EnemyLibrary.rogueEnemiesLib = { 'Bacon': bacon}

      var rogueEnemy = EnemyLibrary.getEnemy('Sausages');
      assert.ok(console.error.calledWith('Enemy: Sausages does not exist in database'));
    });
  });

  describe('getRandomNewEnemy', function() {
    beforeEach(function() {
      sinon.stub(Enemy.prototype, 'initialise');
      sinon.stub(Math, 'random');
    });

    afterEach(function() {
      Enemy.prototype.initialise.restore();
      Math.random.restore();
    });

    it('picks out a random Enemy from EnemyLibrary.enemies and creates a new instance of that class', function() {
      Math.random.returns(0);
      var enemy = {'an': 'enemy'};
      var enemyLibraryEntry = { generateNewInstance: function() { return enemy}};
      var position = {'a': 'position'};

      EnemyLibrary.enemies = [enemyLibraryEntry];

      sinon.spy(enemyLibraryEntry, 'generateNewInstance');

      var newEnemy = EnemyLibrary.getRandomNewEnemy(position);
      assert.deepEqual(newEnemy, enemy);
      assert.ok(enemyLibraryEntry.generateNewInstance.calledWith(position));
    });
  })
});
