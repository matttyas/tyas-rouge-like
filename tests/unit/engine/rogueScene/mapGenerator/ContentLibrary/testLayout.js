var assert = chai.assert;

describe('Layout', function() {

  describe('pickDecorations', function() {
    beforeEach(function() {
      sinon.stub(Layout.prototype, 'initialise');
    });

    afterEach(function() {
      Layout.prototype.initialise.restore();
    });

    it('tells each pattern to pick suitable decorations from the set provided', function() {
      var pattern1 = { pickSuitableDecorations: function() {}};
      var pattern2 = { pickSuitableDecorations: function() {}};
      var pattern3 = { pickSuitableDecorations: function() {}};
      var decorationSet = ['a', 'set', 'of', 'decorations'];

      sinon.spy(pattern1, 'pickSuitableDecorations');
      sinon.spy(pattern2, 'pickSuitableDecorations');
      sinon.spy(pattern3, 'pickSuitableDecorations');
      var patterns = [ pattern1, pattern2, pattern3];

      var layout = new Layout();
      layout.patterns = patterns;
      layout.pickDecorations(decorationSet);

      assert.ok(pattern1.pickSuitableDecorations.calledWith(decorationSet));
      assert.ok(pattern2.pickSuitableDecorations.calledWith(decorationSet));
      assert.ok(pattern3.pickSuitableDecorations.calledWith(decorationSet));
    });
  });

  describe('generateDecorations', function() {
    beforeEach(function() {
      sinon.stub(Layout.prototype, 'initialise');
    });

    afterEach(function() {
      Layout.prototype.initialise.restore();
    });

    it('calls generateDecorations on each pattern in Layout.patterns', function() {
      var pattern = { generateDecorations: function() {}};
      sinon.spy(pattern, 'generateDecorations');

      var patterns = [pattern, pattern];
      var layout = new Layout();
      layout.patterns = patterns;

      layout.generateDecorations();

      assert.equal(pattern.generateDecorations.callCount, 2);
    });
  });
});
