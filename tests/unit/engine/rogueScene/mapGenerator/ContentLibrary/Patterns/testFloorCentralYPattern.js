var assert = chai.assert;

describe('Floor Central Y Pattern', function() {
  beforeEach(function() {
    sinon.stub(FloorCentralYPattern.prototype, 'addSelector');
    sinon.stub(Decoration.prototype, 'initialise');
  });

  afterEach(function() {
    FloorCentralYPattern.prototype.addSelector.restore();
    Decoration.prototype.initialise.restore();
  });

  describe('generateDecorations', function() {
    it ('gets all room and decoration information needed to generate the pattern', function() {
      var centerTileY = 5;
      var roomHeight = 12;
      var roomY = 15;
      var room = {
        getRoundedCenter: function() { return centerTileY },
        getHeight: function() {return roomHeight},
        getY: function() {return roomY}
      };

      sinon.spy(room, 'getRoundedCenter');
      sinon.spy(room, 'getHeight');
      sinon.spy(room, 'getY');

      var decorations = [ {getProperty: function() { return 1;}} ];
      sinon.spy(decorations[0], 'getProperty');

      var floorCentralYPattern = new FloorCentralYPattern();
      floorCentralYPattern.decorations = decorations;

      floorCentralYPattern.generateDecorations(room);

      assert.equal(room.getRoundedCenter.callCount, 1);
      assert.ok(decorations[0].getProperty.calledWith('height'));
      assert.equal(room.getHeight.callCount, 1);
    });

    it('returns a column of decorations going from the top of the floor to the bottom, centrally', function() {
      var centerTileY = 5;
      var roomHeight = 12;
      var roomY = 15;
      var room = {
        getRoundedCenter: function() { return centerTileY },
        getHeight: function() {return roomHeight},
        getY: function() {return roomY}
      };

      var decorations = [ {getProperty: function() { return 2;}} ];

      var floorCentralYPattern = new FloorCentralYPattern();
      floorCentralYPattern.decorations = decorations;
      var generatedDecorations = floorCentralYPattern.generateDecorations(room);
      //2 = decoration height, 1 = gapBetweenDecorations
      assert.equal(generatedDecorations[0].tileY, roomY);
      assert.equal(generatedDecorations[1].tileY, roomY + (2 + 1));
      assert.equal(generatedDecorations[2].tileY, roomY + (2 * (2 + 1)));
      assert.equal(generatedDecorations[3].tileY, roomY + (3 * (2 + 1)));
    });
  });
});
