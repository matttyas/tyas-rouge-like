var assert = chai.assert;

describe('Floor All Entrances Pattern', function() {
  beforeEach(function() {
    sinon.stub(FloorAllEntrancesPattern.prototype, 'addSelector');
    sinon.stub(Decoration.prototype, 'initialise');
  });

  afterEach(function() {
    FloorAllEntrancesPattern.prototype.addSelector.restore();
    Decoration.prototype.initialise.restore();
  });

  describe('generateDecorations', function() {
    beforeEach(function() {

    });

    afterEach(function() {

    });

    it('gets the exits for the room', function() {
      var room = {getExits: function() {return []}};
      sinon.spy(room, 'getExits');

      var floorAllEntrancesPattern = new FloorAllEntrancesPattern();
      floorAllEntrancesPattern.generateDecorations(room);

      assert.equal(room.getExits.callCount, 1);
    });

    it('iterates over the entrances and gets the axis of the entrance, along with its position', function() {
      var exit = {
        getAxis: function() { return 'whatever';},
        getPosition: function() { return {};}
      };

      sinon.spy(exit, 'getAxis');
      sinon.spy(exit, 'getPosition');

      var exits = [exit, exit, exit, exit];
      var room = {getExits: function() {return exits}};

      var floorAllEntrancesPattern = new FloorAllEntrancesPattern();
      floorAllEntrancesPattern.generateDecorations(room);

      assert.equal(exit.getAxis.callCount, exits.length);
      assert.equal(exit.getPosition.callCount, exits.length);
    });

    it('adds decorations at the position of the exits', function() {
      var position = {x: 5, y: 5};
      var exit = {
        getAxis: function() { return 'whatever';},
        getPosition: function() { return position;}
      };

      var exits = [exit];

      var room = {getExits: function() {return exits}};

      var floorAllEntrancesPattern = new FloorAllEntrancesPattern();
      var generatedDecorations = floorAllEntrancesPattern.generateDecorations(room);

      assert.equal(generatedDecorations[0].tileX, position.x);
      assert.equal(generatedDecorations[0].tileY, position.y);
    });

    it('adds the first decoration if the exit is vertical and the second if horizontal', function() {
      var exit1 = {
        getAxis: function() { return 'vertical';},
        getPosition: function() { return {x: 15, y: 15}; }
      };

      var exit2 = {
        getAxis: function() { return 'horizontal';},
        getPosition: function() { return {x: 20, y: 10}; }
      };

      var decoration1 = { properties: {'unique': 'properties'}};
      var decoration2 = { properties: {'more': 'properties'}};

      var decorations = [decoration1, decoration2];

      var exits = [exit1, exit2];

      var room = {getExits: function() {return exits}};

      var floorAllEntrancesPattern = new FloorAllEntrancesPattern();
      floorAllEntrancesPattern.decorations = decorations;

      var generatedDecorations = floorAllEntrancesPattern.generateDecorations(room);
      assert.ok(generatedDecorations[0].initialise.calledWith(decoration2));
      assert.ok(generatedDecorations[1].initialise.calledWith(decoration1));
    });
  });
});
