var assert = chai.assert;

describe('Floor Random Pattern', function() {
  beforeEach(function() {
    sinon.stub(FloorRandomPattern.prototype, 'addSelector');
    sinon.stub(DecorationGenerator, 'generateDecorations');
  });

  afterEach(function() {
    FloorRandomPattern.prototype.addSelector.restore();
    DecorationGenerator.generateDecorations.restore();
  });

  describe('generateDecorations', function() {
    it ('takes room and tiles parameters and passes them along with its decorations to DecorationGenerator then returns the results', function() {
      var randomDecorations = ['an', 'odd', 'assortment'];
      var decorations = [ { 'some': 'decoration'}];
      var tiles = [['atile', 'the', 'hun']];
      var room = {'a': 'room'};
      DecorationGenerator.generateDecorations.returns(randomDecorations);

      var floorRandomPattern = new FloorRandomPattern();
      floorRandomPattern.decorations = decorations;

      var generatedDecorations = floorRandomPattern.generateDecorations(room, tiles);
      assert.ok(DecorationGenerator.generateDecorations.calledWith(room, tiles, decorations));
      assert.deepEqual(generatedDecorations, randomDecorations);
    });
  });
});
