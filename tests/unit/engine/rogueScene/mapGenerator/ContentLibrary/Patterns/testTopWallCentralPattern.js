var assert = chai.assert;

describe('Top Wall Central Pattern', function() {
  beforeEach(function() {
    sinon.stub(TopWallCentralPattern.prototype, 'addSelector');
    sinon.stub(Decoration.prototype, 'initialise');
  });

  afterEach(function() {
    TopWallCentralPattern.prototype.addSelector.restore();
    Decoration.prototype.initialise.restore();
  });

  describe('generateDecorations', function() {
    it('fetches an array of tile positions for the top wall of the room from the Room passed to it', function() {
      var room = {
        getTopWallPositions: function() { return []; },
        getTopWallTiles: function() {return []; }
      };
      sinon.spy(room, 'getTopWallPositions');
      sinon.spy(room, 'getTopWallTiles');

      var topWallCentralPattern = new TopWallCentralPattern();
      topWallCentralPattern.generateDecorations(room);

      assert.equal(room.getTopWallPositions.callCount, 1);
      assert.equal(room.getTopWallTiles.callCount, 1);
    });

    it('returns an array containing a single decoration placed in the centre of the top wall', function() {
      var decorations = [
        {
          getProperty: function() {return 1;},
          properties: {'something': 'important'}
        }
      ];
      sinon.spy(decorations[0], 'getProperty');

      var centralPosition = {x: 15, y: 20};
      var centreTile = {getProperty: function() {return false}};
      sinon.spy(centreTile, 'getProperty');

      var room = {
        getTopWallPositions: function() { return [0, centralPosition, 2]; },
        getTopWallTiles: function() {return [0, centreTile, 2];}
      };

      var topWallCentralPattern = new TopWallCentralPattern();
      topWallCentralPattern.decorations = decorations;

      var generatedDecorations = topWallCentralPattern.generateDecorations(room);

      assert.ok(centreTile.getProperty.calledWith('passable'));
      assert.ok(generatedDecorations[0].initialise.calledWith(decorations[0]));
      assert.equal(generatedDecorations[0].tileX, centralPosition.x);
      assert.equal(generatedDecorations[0].tileY, centralPosition.y);
      assert.ok(decorations[0].getProperty.calledWith('height'));
    });

    it('does not place a decoration if there is an entrance to the room there', function() {
      var decorations = [ {properties: {'something': 'important'}}];
      var centralPosition = {x: 15, y: 20};
      var centreTile = {getProperty: function() {return true}};
      sinon.spy(centreTile, 'getProperty');

      var room = {
        getTopWallPositions: function() { return [0, centralPosition, 2]; },
        getTopWallTiles: function() {return [0, centreTile, 2];}
      };

      var topWallCentralPattern = new TopWallCentralPattern();
      topWallCentralPattern.decorations = decorations;

      var generatedDecorations = topWallCentralPattern.generateDecorations(room);

      assert.ok(centreTile.getProperty.calledWith('passable'));
      assert.deepEqual([], generatedDecorations);
    });

    it('returns an empty array if getTopWallTiles or getTopWallPositions returns an empty array', function() {
      var room = {
        getTopWallPositions: function() { return []; },
        getTopWallTiles: function() {return []; }
      };

      var topWallCentralPattern = new TopWallCentralPattern();
      var generatedDecorations = topWallCentralPattern.generateDecorations(room);

      assert.deepEqual([], generatedDecorations);
    });
  });
});
