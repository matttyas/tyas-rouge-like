var assert = chai.assert;

describe('Room', function() {
  describe ('getExits', function() {
    beforeEach(function() {
      sinon.stub(Room.prototype, 'generateHooks');
    });

    afterEach(function(){
      Room.prototype.generateHooks.restore();
    });

    it ('returns an array of hooks', function() {
      var hook1 = { getInUse: function() { return true }};
      var hook2 = { getInUse: function() { return false }};
      var hook3 = { getInUse: function() { return true }};

      var expectedHooks = [hook1, hook3];
      var room = new Room(5,5,10,10);
      room.hooks = [hook1, hook2, hook3];

      var exits = room.getExits();
      assert.deepEqual(exits, expectedHooks);
    });

    it('checks if every hook is active', function() {
      var hook1 = { getInUse: function() { return true }};
      var hook2 = { getInUse: function() { return false }};
      var hook3 = { getInUse: function() { return true }};

      sinon.spy(hook1, 'getInUse');
      sinon.spy(hook2, 'getInUse');
      sinon.spy(hook3, 'getInUse');

      var room = new Room(5, 5, 10, 10);
      room.hooks = [hook1, hook2, hook3];

      var exits = room.getExits();
      assert.equal(hook1.getInUse.callCount, 1);
      assert.equal(hook2.getInUse.callCount, 1);
      assert.equal(hook3.getInUse.callCount, 1);
    });
  });

  describe('getArea', function() {
    beforeEach(function() {
      sinon.stub(Room.prototype, 'generateHooks');
    });

    afterEach(function(){
      Room.prototype.generateHooks.restore();
    });

    it('calculates area if not set then returns it', function() {
      var room = new Room();
      room.width = 3;
      room.height = 10;

      var area = room.getArea();
      assert.equal(room.area, room.width * room.height);
    });

    it('returns area if it is already set without calculating it', function() {
      var room = new Room();
      room.with = 3;
      room.height = 10;
      room.area = 'banana';

      var area = room.getArea();
      assert.equal(room.area, 'banana');
    });
  });

  describe('getTopWallPositions', function() {
    it('goes from top left of room to top right and adds each position from tiles to an array', function() {
      var room = new Room(0, 1, 4, 5);
      var tile1 = {'a': 'tile'};
      var tile2 = {'another': 'tile'};
      var tile3 = {'third': 'tile'};
      var tile4 = {'fourth': 'tile'};
      var tile5 = {'fifth': 'tile'};
      var tile6 = {'sixth': 'tile'};

      var tiles = [
        [tile1],
        [tile2],
        [tile3],
        [tile4],
        [tile5],
        [tile6]
      ];
      var expected = [ {x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 3, y: 0}];

      var topWallTiles = room.getTopWallPositions(tiles);

      assert.deepEqual(topWallTiles, expected);
    });

    it('returns an empty array if the wall would otherwise be out of bounds', function() {
      var room = new Room(0, 0, 4, 5);
      var tile1 = {'a': 'tile'};
      var tile2 = {'another': 'tile'};
      var tile3 = {'third': 'tile'};
      var tile4 = {'fourth': 'tile'};
      var tile5 = {'fifth': 'tile'};
      var tile6 = {'sixth': 'tile'};

      var tiles = [
        [tile1],
        [tile2],
        [tile3],
        [tile4],
        [tile5],
        [tile6]
      ];

      var expected = [];

      var topWallTiles = room.getTopWallPositions(tiles);

      assert.deepEqual(topWallTiles, expected);
    });
  });

  describe('getTopWallTiles', function() {
    it('goes to row one above top left of room to top right and adds each tile from tiles to an array', function() {
      var room = new Room(0, 1, 4, 5);
      var tile1 = {'a': 'tile'};
      var tile2 = {'another': 'tile'};
      var tile3 = {'third': 'tile'};
      var tile4 = {'fourth': 'tile'};
      var tile5 = {'fifth': 'tile'};
      var tile6 = {'sixth': 'tile'};

      var tiles = [
        [tile1],
        [tile2],
        [tile3],
        [tile4],
        [tile5],
        [tile6]
      ];
      var expected = [ tile1, tile2, tile3, tile4];

      var topWallTiles = room.getTopWallTiles(tiles);

      assert.deepEqual(topWallTiles, expected);
    });

    it('returns an empty array if the wall would otherwise be out of bounds', function() {

    });
  });
});
