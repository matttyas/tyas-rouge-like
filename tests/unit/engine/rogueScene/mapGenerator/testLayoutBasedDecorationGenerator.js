var assert = chai.assert;

describe('Layout Based Decoration Generator', function() {
  describe('generateDecorations', function() {
    beforeEach(function() {
      sinon.stub(Layout.prototype, 'initialise');
      sinon.stub(Layout.prototype, 'pickDecorations');
      sinon.stub(Layout.prototype, 'generateDecorations');
      sinon.stub(Layout.prototype, 'getPatterns');
    });

    afterEach(function() {
      Layout.prototype.initialise.restore();
      Layout.prototype.pickDecorations.restore();
      Layout.prototype.generateDecorations.restore();
      Layout.prototype.getPatterns.restore();
    });

    it('Creates a new layout then tells it to pick decorations from the decoration set', function() {
      DEBUG = false;
      var decorationSet = ['set', 'of', 'decorations'];
      LayoutBasedDecorationGenerator.generateDecorations({}, [], decorationSet);
      assert.equal(Layout.prototype.pickDecorations.callCount, 1);
      assert.ok(Layout.prototype.pickDecorations.calledWith(decorationSet));
    });

    it('then tells the layout to generateDecorations on that layout using tiles provided', function() {
      DEBUG = false;
      var decorationSet = ['set', 'of', 'decorations'];
      var tiles = ['some', 'tiles'];
      var room = {'a': 'room'};
      LayoutBasedDecorationGenerator.generateDecorations(room, tiles, decorationSet);
      assert.equal(Layout.prototype.generateDecorations.callCount, 1);
      assert.ok(Layout.prototype.generateDecorations.calledWith(room, tiles));
    });

    it('if DEBUG is set it gets the patterns from the layout and sets them on the room', function() {
      DEBUG = true;
      var room = {setPatterns: function() {}};
      sinon.spy(room, 'setPatterns');
      var patterns = ['array', 'of', 'patterns'];
      Layout.prototype.getPatterns.returns(patterns);
      LayoutBasedDecorationGenerator.generateDecorations(room, [], []);
      assert.ok(room.setPatterns.calledWith(patterns));
      DEBUG = false;
    });
  });
});
