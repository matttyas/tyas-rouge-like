var assert = chai.assert;

describe('Corridor Generator', function() {
  describe ('generateCorridors', function() {
    beforeEach (function() {
      sinon.stub(CorridorGenerator, 'generateCorridor');
      sinon.stub(RoomWeb.prototype, 'getPaths');
    });

    afterEach (function() {
      CorridorGenerator.generateCorridor.restore();
      RoomWeb.prototype.getPaths.restore();
    });

    it('calls generate corridor once for each of the paths in roomWeb', function() {
      var numberPaths = 3;

      var roomWeb = new RoomWeb();
      roomWeb.getPaths.returns([0, 1, 2]);

      var map = { rooms: [] };

      CorridorGenerator.generateCorridors(map, roomWeb);

      assert.equal(CorridorGenerator.generateCorridor.callCount, numberPaths);
      assert.equal(map.corridors.length, numberPaths);
    });

    it ('generates no paths if there are no paths in the roomWeb', function() {
      var numberPaths = 0;
      var roomWeb = new RoomWeb();
      roomWeb.getPaths.returns(undefined);

      var map = { rooms: []};

      CorridorGenerator.generateCorridors(map, roomWeb);

      assert.equal(CorridorGenerator.generateCorridor.callCount, numberPaths);
      assert.deepEqual(map.corridors, []);
    });
  });

  describe('generateCorridor', function() {
    beforeEach(function() {
      sinon.stub(CorridorGenerator, 'getStartAndEndHooks');
      sinon.stub(CorridorGenerator, 'buildSections');
      sinon.stub(CorridorGenerator, 'setHooksInUse');
    });

    afterEach(function() {
      CorridorGenerator.getStartAndEndHooks.restore();
      CorridorGenerator.buildSections.restore();
      CorridorGenerator.setHooksInUse.restore();
    });

    it('Generates a corridor, with a start hook, end hook and sections for the given path', function() {
      var startHook = 'startHook';
      var endHook = 'endHook';
      var path = 'path';
      var map = 'map';
      var sections = ['section'];

      CorridorGenerator.getStartAndEndHooks.returns({startHook: startHook, endHook: endHook});
      CorridorGenerator.buildSections.returns(sections);

      var corridor = CorridorGenerator.generateCorridor(path, map, 1);

      assert.ok(CorridorGenerator.getStartAndEndHooks.calledWith(path, map));
      assert.ok(CorridorGenerator.buildSections.calledWith(startHook, endHook));

      assert.equal(corridor.startHook, startHook);
      assert.equal(corridor.endHook, endHook);
      assert.deepEqual(corridor.sections, sections);
    });

    it('Builds sections a number of times equal to passes and builds array of all results', function() {
      var passes = 3;
      var sections = ['section'];
      var allSections = ['section', 'section2', 'section3'];


      CorridorGenerator.getStartAndEndHooks.returns({startHook: 'startHook', endHook: 'endHook'});
      CorridorGenerator.buildSections.returns(sections);
      CorridorGenerator.buildSections.onCall(1).returns(['section2']);
      CorridorGenerator.buildSections.onCall(2).returns(['section3']);

      var corridor = CorridorGenerator.generateCorridor('map', 'path', passes);

      assert.equal(CorridorGenerator.buildSections.callCount, 3);
      assert.deepEqual(corridor.sections, allSections);
    });

    it('Defaults passes to 1 if not provided', function() {
      CorridorGenerator.getStartAndEndHooks.returns({startHook: 'startHook', endHook: 'endHook'});
      CorridorGenerator.buildSections.returns([]);

      var corridor = CorridorGenerator.generateCorridor('map', 'path');

      assert.equal(CorridorGenerator.buildSections.callCount, 1);
    });

    it('Sets both hooks to be in use', function() {
      CorridorGenerator.getStartAndEndHooks.returns({startHook: 'startHook', endHook: 'endHook'});
      CorridorGenerator.buildSections.returns([]);

      var corridor = CorridorGenerator.generateCorridor('map', 'path');

      assert.equal(CorridorGenerator.setHooksInUse.callCount, 1);
    });
  });

  describe('getStartAndEndHooks', function() {
    beforeEach(function() {
      sinon.stub(Map.prototype, 'getRoomWithId');
      sinon.stub(CorridorGenerator, 'getClosestCorridorHook');
    });

    afterEach(function() {
      Map.prototype.getRoomWithId.restore();
      CorridorGenerator.getClosestCorridorHook.restore();
    });

    it('gets each room in the path, then gets the hook for each of those rooms', function() {
      var startRoom = 'startRoom';
      var endRoomCenter = 'endRoomCenter';
      var startHook = 'startHook';
      var endHook = 'endHook';
      var path = {start: 'start', end: 'end'};
      var endRoom = { getCenter: function(){ return endRoomCenter; } };


      CorridorGenerator.getClosestCorridorHook.onCall(0).returns(startHook);
      CorridorGenerator.getClosestCorridorHook.onCall(1).returns(endHook);
      Map.prototype.getRoomWithId.onCall(0).returns(startRoom);
      Map.prototype.getRoomWithId.onCall(1).returns(endRoom);

      var hooks = CorridorGenerator.getStartAndEndHooks(path, new Map());
      var expected = {startHook: startHook, endHook: endHook};

      assert.ok(Map.prototype.getRoomWithId.calledWith(path.start));
      assert.ok(Map.prototype.getRoomWithId.calledWith(path.end));
      assert.ok(CorridorGenerator.getClosestCorridorHook.calledWith(startRoom, endRoomCenter));
      assert.ok(CorridorGenerator.getClosestCorridorHook.calledWith(endRoom, startHook));
      assert.deepEqual(expected, hooks);
    });
  });

  describe('setHooksInUse', function() {
    it('Sets both start and end hooks to be inUse', function() {
      var startHook = { setInUse: function() {}};
      var endHook = { setInUse: function() {}};
      sinon.stub(startHook, 'setInUse');
      sinon.stub(endHook, 'setInUse');

      var hooks = {startHook: startHook, endHook: endHook};
      CorridorGenerator.setHooksInUse(hooks);

      assert.equal(startHook.setInUse.callCount, 1);
      assert.equal(endHook.setInUse.callCount, 1);
    });
  });

  describe('buildSections', function() {
    beforeEach( function() {
      sinon.stub(CorridorGenerator, 'generateHorizontalSection');
      sinon.stub(CorridorGenerator, 'generateVerticalSection');
    });

    afterEach( function() {
      sinon.stub(CorridorGenerator.generateHorizontalSection.restore());
      sinon.stub(CorridorGenerator.generateVerticalSection.restore());
    });

    it('generates a horizontal section between currentTile and endHook if the axis is horizontal', function() {
      var startHook = {
        getAxis: function() { return 'horizontal'},
        getX: function() { return 5},
        getY: function() { return 10}
      };

      var endHook = {
        getX: function() { return 10},
        getY: function() { return 10}
      };

      var horizontalSection = {endPosition: {x: 10, y: 10}};
      CorridorGenerator.generateHorizontalSection.returns(horizontalSection);

      var expectedSections = [horizontalSection];
      var expectedHorizontalTile = {x: 5, y: 10};

      var sections = CorridorGenerator.buildSections(startHook, endHook);

      assert.ok(CorridorGenerator.generateHorizontalSection.calledWith(expectedHorizontalTile, endHook));
      assert.deepEqual(sections, expectedSections);
    });

    //TEST 400!!!! BWADABABOOOO! BRADADABOOO! ~John Cena Suuuuuucccks...
    it('generates a vertical section between currentTile and endhook if the axis is vertical then sets current axis to horizontal', function() {
      var startHook = {
        getAxis: function() { return 'vertical'},
        getX: function() { return 10},
        getY: function() { return 5}
      };

      var endHook = {
        getX: function() { return 10},
        getY: function() { return 10}
      };

      var verticalSection = {endPosition: {x: 10, y: 10}};
      CorridorGenerator.generateVerticalSection.returns(verticalSection);

      var expectedSections = [verticalSection];
      var expectedVerticalTile = {x: 10, y: 5};

      var sections = CorridorGenerator.buildSections(startHook, endHook);

      assert.ok(CorridorGenerator.generateVerticalSection.calledWith(expectedVerticalTile, endHook));
      assert.deepEqual(sections, expectedSections);
    });

    it('generates a horizontal section then a vertical then a horizontal until finished', function() {
      var startHook = {
        getAxis: function() { return 'horizontal'},
        getX: function() { return 5},
        getY: function() { return 10}
      };

      var endHook = {
        getX: function() { return 20},
        getY: function() { return 20}
      };

      var horizontalSection = {endPosition: {x: 10, y: 10}};
      CorridorGenerator.generateHorizontalSection.returns(horizontalSection);

      var verticalSection = {endPosition: {x: 10, y: 15}};
      CorridorGenerator.generateVerticalSection.returns(verticalSection);

      var horizontalSection2 = {endPosition: {x: 20, y:15}};
      CorridorGenerator.generateHorizontalSection.onCall(1).returns(horizontalSection2);

      var verticalSection2 = {endPosition: {x: 20, y: 20}};
      CorridorGenerator.generateVerticalSection.onCall(1).returns(verticalSection2);

      var expectedSections = [horizontalSection, verticalSection, horizontalSection2, verticalSection2];
      var expectedHorizontalTile = {x: 5, y: 10};

      var sections = CorridorGenerator.buildSections(startHook, endHook);

      assert.ok(CorridorGenerator.generateHorizontalSection.calledWith(expectedHorizontalTile, endHook));
      assert.deepEqual(sections, expectedSections);
    });

    it('REGRESSION - It a generates a corridor one tile in size if start and end hook are in the same place', function() {
      var startHook = {
        getAxis: function() { return 'horizontal'},
        getX: function() { return 5},
        getY: function() { return 10}
      };

      var endHook = {
        getX: function() { return 5},
        getY: function() { return 10}
      };

      var expectedSection = {startPosition: {x: 5, y: 10}, endPosition: {x:5, y: 10}, axis:'horizontal'};
      var expectedSections = [ expectedSection ];

      var sections = CorridorGenerator.buildSections(startHook, endHook);
      assert.deepEqual(sections, expectedSections);
      assert.equal(CorridorGenerator.generateHorizontalSection.callCount, 0);
      assert.equal(CorridorGenerator.generateVerticalSection.callCount, 0);
    });
  });
});
