var assert = chai.assert;

describe('Door Generator', function() {
  describe ('generateDoors', function() {
    beforeEach(function() {
      sinon.stub(DoorGenerator, 'generateDoor');
      sinon.stub(DoorGenerator, 'checkForDuplicateAndPush');
    });

    afterEach(function() {
      DoorGenerator.generateDoor.restore();
      DoorGenerator.checkForDuplicateAndPush.restore();
    });

    it('trys to generate a door for each hook of every corridor', function() {
      var door = {'door': true};
      var corridor1 = {startHook: 'hook', endHook: 'endHook'};
      var corridor2 = {startHook: 'hook', endHook: 'endHook'};
      var corridors = [corridor1, corridor2];


      DoorGenerator.generateDoor.returns(door);

      var map = {
        getCorridors: function() { return corridors},
        setDoors: function() {}
      };
      sinon.spy(map, 'setDoors');
      DoorGenerator.generateDoors(map);

      assert.equal(DoorGenerator.generateDoor.callCount, corridors.length * 2);
      assert.equal(DoorGenerator.checkForDuplicateAndPush.callCount, corridors.length * 2);;
    });

    it('returns without adding doors if the map has no corridors', function() {
      var expectedDoors = 'doors';
      var map = { getCorridors: function() { return [] }, doors: expectedDoors};
      DoorGenerator.generateDoors(map);
      assert.equal(map.doors, expectedDoors);
    });
  });

  describe('generateDoor', function() {
    beforeEach( function() {
      sinon.stub(DoorGenerator, 'decideHorizontalOrVertical');
      sinon.stub(Door.prototype, 'initialise');
    });

    afterEach( function() {
      DoorGenerator.decideHorizontalOrVertical.restore();
      Door.prototype.initialise.restore();
    });

    it('checks the doors own position is passable. If not it returns null', function() {
      var map = {checkTilePassable: function() {return false}};
      sinon.spy(map, 'checkTilePassable');

      var newDoor = DoorGenerator.generateDoor({}, map);
      assert.equal(map.checkTilePassable.callCount, 1);
      assert.equal(newDoor, null);
    });

    it('figures out if the door should be horizontal or vertical', function() {
      var map = {checkTilePassable: function() {return true;}};
      var position = { 'pos': 'ition'};
      var newDoor = DoorGenerator.generateDoor(position, map);

      assert.ok(DoorGenerator.decideHorizontalOrVertical.calledWith(position, map));
    });

    it('figures out if the door is in a position that is not viable', function() {
      var map = { checkTilePassable: function() {return true;}};
      var invalidPosition = { 'invalid': 'position'};
      DoorGenerator.decideHorizontalOrVertical.returns(null);

      var newDoor = DoorGenerator.generateDoor(invalidPosition, map);
      assert.ok(DoorGenerator.decideHorizontalOrVertical.calledWith(invalidPosition, map));
      assert.equal(newDoor, null);
    });

    it('returns a horizontal door if viable', function() {
      var map = {checkTilePassable: function() {return true;}};
      var horizontalPosition = { 'horizontal': 'position'};
      var generatedDirection = 'horizontal';
      DoorGenerator.decideHorizontalOrVertical.returns(generatedDirection);

      var newDoor = DoorGenerator.generateDoor(horizontalPosition, map);

      assert.ok(DoorGenerator.decideHorizontalOrVertical.calledWith(horizontalPosition, map));
      assert.ok(newDoor.initialise.calledWith(horizontalPosition, generatedDirection));
    });

    it('returns a vertical door if viable', function() {
      var map = {checkTilePassable: function() {return true;}};
      var verticalPosition = { 'vertical': 'position'};
      var generatedDirection = 'vertical';
      DoorGenerator.decideHorizontalOrVertical.returns(generatedDirection);

      var newDoor = DoorGenerator.generateDoor(verticalPosition, map);

      assert.ok(DoorGenerator.decideHorizontalOrVertical.calledWith(verticalPosition, map));
      assert.ok(newDoor.initialise.calledWith(verticalPosition, generatedDirection));
    });
  });

  describe('decideHorizontalOrVertical', function() {
    beforeEach(function() {
      sinon.stub(DoorGenerator, 'getPassableTileStates');
      sinon.stub(DoorGenerator, 'checkDoorShouldBeHorizontal');
      sinon.stub(DoorGenerator, 'checkDoorShouldBeVertical');
    });

    afterEach(function() {
      DoorGenerator.getPassableTileStates.restore();
      DoorGenerator.checkDoorShouldBeHorizontal.restore();
      DoorGenerator.checkDoorShouldBeVertical.restore();
    });

    it('gets the passable states of the tiles around position then passes them to other functions', function() {
      var position = 'postion';
      var map = 'map';
      var passableStates = { 'passable': 'states'};

      DoorGenerator.getPassableTileStates.returns(passableStates);
      DoorGenerator.checkDoorShouldBeHorizontal.returns(false);

      DoorGenerator.decideHorizontalOrVertical(position, map);
      assert.ok(DoorGenerator.getPassableTileStates.calledWith(position, map));
      assert.ok(DoorGenerator.checkDoorShouldBeHorizontal.calledWith(passableStates));
      assert.ok(DoorGenerator.checkDoorShouldBeVertical.calledWith(passableStates));
    });

    it('returns horizontal if the door should be horizontal', function() {
      var position = 'position';
      var map = 'map';

      DoorGenerator.checkDoorShouldBeHorizontal.returns(true);
      var direction = DoorGenerator.decideHorizontalOrVertical(position, map);

      assert.equal(direction, 'horizontal');
    });

    it('returns vertical if the door should be vertical', function() {
      var position = 'position';
      var map = 'map';

      DoorGenerator.checkDoorShouldBeVertical.returns(true);
      DoorGenerator.checkDoorShouldBeHorizontal.returns(false);
      var direction = DoorGenerator.decideHorizontalOrVertical(position, map);

      assert.equal(direction, 'vertical');
    });

    it('returns null if a door should be neither horizontal or vertical', function() {
      var position = 'position';
      var map = 'map';

      DoorGenerator.checkDoorShouldBeVertical.returns(false);
      DoorGenerator.checkDoorShouldBeHorizontal.returns(false);
      var direction = DoorGenerator.decideHorizontalOrVertical(position, map);

      assert.equal(direction, null);
    });
  });

  describe('getPassableTileStates', function() {
    it('getsPassableTileStates for tiles to the top, left, bottom and right of position', function() {
      var map = {checkTilePassable: function(x, y) { return true;}};
      sinon.spy(map, 'checkTilePassable');
      var position = {x: 3, y: 3};
      var expectedPassableStates = {
        top: true,
        left: true,
        bottom: true,
        right: true
      };

      var passableStates = DoorGenerator.getPassableTileStates(position, map);
      assert.ok(map.checkTilePassable.calledWith(3,2));
      assert.ok(map.checkTilePassable.calledWith(2,3));
      assert.ok(map.checkTilePassable.calledWith(3,4));
      assert.ok(map.checkTilePassable.calledWith(4,3));

      assert.deepEqual(passableStates, expectedPassableStates)
    });
  });

  describe('checkDoorShouldBeHorizontal', function() {
    it('returns true if blocks to left and right impassable and block up / below are passable', function() {
      var passableStates = {
        'top': true,
        'left': false,
        'bottom': true,
        'right': false
      };

      assert.ok(DoorGenerator.checkDoorShouldBeHorizontal(passableStates));
    });

    it('returns false if either left or right is passable', function() {
      var passableStates = {
        'top': true,
        'left': true,
        'bottom': true,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeHorizontal(passableStates), false);

      passableStates = {
        'top': true,
        'left': false,
        'bottom': true,
        'right': true
      };
      assert.equal(DoorGenerator.checkDoorShouldBeHorizontal(passableStates), false);
    });

    it('returns false if either top or bottom is impassable', function() {
      var passableStates = {
        'top': false,
        'left': false,
        'bottom': true,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeHorizontal(passableStates), false);

      passableStates = {
        'top': true,
        'left': false,
        'bottom': false,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeHorizontal(passableStates), false);
    });
  });

  describe('checkDoorShouldBeVertical', function() {
    it('returns true if blocks to top and bottom are impassable and blocks left / right are passable', function() {
      var passableStates = {
        'top': false,
        'left': true,
        'bottom': false,
        'right': true
      };

      assert.ok(DoorGenerator.checkDoorShouldBeVertical(passableStates));
    });

    it('returns false if either top or bottom is passable', function() {
      var passableStates = {
        'top': true,
        'left': false,
        'bottom': false,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeVertical(passableStates), false);

      passableStates = {
        'top': false,
        'left': false,
        'bottom': true,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeVertical(passableStates), false);
    });

    it('returns false if either left or right is impassable', function() {
      var passableStates = {
        'top': false,
        'left': false,
        'bottom': false,
        'right': true
      };
      assert.equal(DoorGenerator.checkDoorShouldBeVertical(passableStates), false);

      passableStates = {
        'top': false,
        'left': true,
        'bottom': false,
        'right': false
      };
      assert.equal(DoorGenerator.checkDoorShouldBeVertical(passableStates), false);
    });

    describe('checkForDuplicateAndPush', function() {
      beforeEach(function() {
        sinon.stub(console, 'error');
      });

      afterEach(function() {
        console.error.restore();
      });

      it('checks if the door x and y are the same as ones already and doesnt push if it is', function() {
        var doors = [ {position: {x: 5, y: 5}} ];
        var door = { position: {x: 5, y: 5}};

        DoorGenerator.checkForDuplicateAndPush(doors, door);

        assert.equal(doors.length, 1);
      });

      it('adds the door to doors if either x or y doesnt match', function() {
        var doors = [ {position: {x: 5, y: 5}} ];
        var door = {position: {x: 5, y: 6}};
        var door2 = {position: {x: 6, y: 5}};

        DoorGenerator.checkForDuplicateAndPush(doors, door);
        DoorGenerator.checkForDuplicateAndPush(doors, door2);

        assert.equal(doors.length, 3);
      });

      it('calls console.error if position is not set on the new door', function() {
        var door = {};
        var doors = [{position: {x: 1, y: 1}}];

        DoorGenerator.checkForDuplicateAndPush(doors, door);

        assert.ok(console.error.calledWith('Door must have a position in checkForDuplicateAndPush'));
      });
    });
  });
});
