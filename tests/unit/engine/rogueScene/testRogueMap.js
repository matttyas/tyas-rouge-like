var assert = chai.assert;

describe('Rogue Map', function() {
  describe('initialise', function() {
    it('calls RogueMapBuilder.buildMap with itself, mapdata and the engine', function() {

    });
  });

  describe('render', function() {
    beforeEach(function() {
      sinon.stub(RogueMap.prototype, 'initialise');
      sinon.stub(RogueMap.prototype, 'renderDecorations');
      sinon.stub(RogueMap.prototype, 'renderEntrances');
      sinon.stub(RogueMap.prototype, 'renderDoors');
      sinon.stub(RogueMap.prototype, 'renderEnemies');
      sinon.stub(RogueMap.prototype, 'debugRender');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
      RogueMap.prototype.renderDecorations.restore();
      RogueMap.prototype.renderEntrances.restore();
      RogueMap.prototype.renderDoors.restore();
      RogueMap.prototype.renderEnemies.restore();
      RogueMap.prototype.debugRender.restore();
    });

    it('calls render for every tile in the 2d array tiles - EASY EFFICIENCY GAIN', function() {
      var tile = {render: function() {}};
      sinon.spy(tile, 'render');
      var renderer = {
        setBackColor: function() {}
      };

      var tiles = [
        [tile, tile, tile],
        [tile, tile, tile, tile],
        [tile, tile, tile]
      ];

      var rogueMap = new RogueMap();
      rogueMap.tiles = tiles;

      rogueMap.render({}, renderer);
      assert.ok(tile.render.calledWith(renderer));
      assert.equal(tile.render.callCount, 10);
    });

    it('sets the back color on the renderer then calls renderDecorations, renderEntrances and renderDoors', function() {
      var renderer = {
        setBackColor: function() {}
      };
      sinon.spy(renderer, 'setBackColor');

      var rogueMap = new RogueMap();
      rogueMap.tiles = [];
      rogueMap.render({}, renderer);

      assert.ok(renderer.setBackColor.calledWith(rogueMap.clearColor));
      assert.equal(rogueMap.renderDecorations.callCount, 1);
      assert.equal(rogueMap.renderEntrances.callCount, 1);
      assert.equal(rogueMap.renderDoors.callCount, 1);
      assert.equal(rogueMap.renderEnemies.callCount, 1);
    });

    it('calls debugRender if the global RENDER is set to true', function() {
      DEBUG = true;
      var renderer = {
        setBackColor: function() {}
      };
      var rogueMap = new RogueMap();
      rogueMap.tiles = [];
      rogueMap.render({}, renderer);
      assert.ok(rogueMap.debugRender.calledWith(renderer));

      DEBUG = false;
    });
  });

  describe('debugRender', function() {
    beforeEach(function() {
      sinon.stub(RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
    });

    it('calls render on each room rogueMap.rooms', function() {
      var room = {render: function() {}};
      sinon.spy(room, 'render');
      var rooms = [ room, room, room ];

      var renderer = {'the': 'renderer'};

      var rogueMap = new RogueMap();
      rogueMap.rooms = rooms;

      rogueMap.debugRender(renderer);
      assert.ok(room.render.calledWith(renderer));
      assert.equal(room.render.callCount, rooms.length);
    });
  });

  describe ('renderEntrances', function() {
    it('calls render on each entrance in entrances', function() {
      sinon.stub(RogueMap.prototype, 'initialise');
      var rogueMap = new RogueMap();
      RogueMap.prototype.initialise.restore();
      var entrance = { render: function() {}};
      sinon.stub(entrance, 'render');

      var entrances = [entrance, entrance, entrance];
      rogueMap.entrances = entrances;

      rogueMap.renderEntrances();

      assert.equal(entrance.render.callCount, entrances.length);
    });
  });

  describe ('renderDoors', function() {
    it('calls render on each door in doors', function() {
      sinon.stub(RogueMap.prototype, 'initialise');
      var rogueMap = new RogueMap();
      RogueMap.prototype.initialise.restore();
      var door = { render: function() {}};
      sinon.stub(door, 'render');

      var doors = [door, door, door];
      rogueMap.doors = doors;

      rogueMap.renderDoors();

      assert.equal(door.render.callCount, doors.length);
    });
  });

  describe ('renderDecorations', function() {
    it('calls render on each Decoration in decorations', function() {
      sinon.stub(RogueMap.prototype, 'initialise');
      var rogueMap = new RogueMap();
      RogueMap.prototype.initialise.restore();
      var decoration = { render: function() {}};
      sinon.stub(decoration, 'render');

      var decorations = [decoration, decoration, decoration];
      rogueMap.decorations = decorations;

      rogueMap.renderDecorations();

      assert.equal(decoration.render.callCount, decorations.length);
    });
  });

  describe('getCloseTiles', function() {
    it('returns the tiles up to distance away from tile[x][y]', function() {
      var x = 1;
      var y = 3;

      var tiles = [
        [ 0,  1,  2,  3,  4,  5],
        [ 6,  7,  8,  9, 10, 11],
        [12, 13, 14, 15, 16, 17]
      ];

      sinon.stub(RogueMap.prototype, 'initialise');
      var rogueMap = new RogueMap();
      RogueMap.prototype.initialise.restore();

      rogueMap.tiles = tiles;

      var expected = [ 2, 8, 14, 3, 9, 15, 4, 10, 16 ];

      assert.deepEqual(expected, rogueMap.getCloseTiles(x, y, 1));
    });
  });

  describe('getEntranceWithId', function() {
    beforeEach( function() {
      sinon.stub(RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
    });

    it('returns the first entrance with an id matching entranceId', function() {
      var rogueMap = new RogueMap();
      var entrance1 = { getId: function() { return 5}};
      sinon.spy(entrance1, 'getId');
      var entrance2 = { getId: function() { return 10}};
      sinon.spy(entrance2, 'getId');

      var entrances = [ entrance1, entrance2];
      rogueMap.entrances = entrances;

      var returnedEntrance = rogueMap.getEntranceWithId(10);
      assert.ok(entrance1.getId.callCount, 1);
      assert.ok(entrance2.getId.callCount, 1);
      assert.deepEqual(returnedEntrance, entrance2);
    });

    it('returns null if no maps match the id', function() {
      var rogueMap = new RogueMap();
      var entrance1 = { getId: function() { return 5}};
      sinon.spy(entrance1, 'getId');
      var entrance2 = { getId: function() { return 10}};
      sinon.spy(entrance2, 'getId');

      var entrances = [ entrance1, entrance2];
      rogueMap.entrances = entrances;

      var returnedEntrance = rogueMap.getEntranceWithId(15);

      assert.ok(entrance1.getId.callCount, 1);
      assert.ok(entrance2.getId.callCount, 1);
      assert.deepEqual(returnedEntrance, null);
    });
  });

  describe('getInspectableObjects', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
    });

    it('puts all entrances in inspectableObjects', function() {
      var entrances = ['a', 'b'];
      var rogueMap = new RogueMap();
      rogueMap.entrances = entrances;

      var inspectableObjects = rogueMap.getInspectableObjects();
      assert.deepEqual(inspectableObjects, entrances);
    });

    it('puts all doors in inspectableObjects', function() {
      var doors = ['c', 'd'];
      var rogueMap = new RogueMap();
      rogueMap.doors = doors;

      var inspectableObjects = rogueMap.getInspectableObjects();
      assert.deepEqual(inspectableObjects, doors);
    });
  });

  describe('getColliderActors', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
    });

    it('puts all doors in colliderActors', function() {
      var doors = ['c', 'd'];
      var rogueMap = new RogueMap();
      rogueMap.doors = doors;

      var colliderActors = rogueMap.getColliderActors();
      assert.deepEqual(colliderActors, doors);
    });
  });

  describe('processContent', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
      sinon.stub (ContentProcessor, 'processContentForActors');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
      ContentProcessor.processContentForActors.restore();
    });

    it ('calls processContentForActors with the decorations and content', function() {
      var rogueMap = new RogueMap();
      var decorations = ['decorations'];
      rogueMap.decorations = decorations;
      var content = {'some': 'content'};

      rogueMap.processContent(content);

      assert.ok(ContentProcessor.processContentForActors.calledWith(decorations, content));
    });

    it('calls processContentForActors with the entrances and content', function() {
      var rogueMap = new RogueMap();
      var entrances = ['entrances'];
      rogueMap.entrances = entrances;
      var content = {'some': 'content'};

      rogueMap.processContent(content);

      assert.ok(ContentProcessor.processContentForActors.calledWith(entrances, content));
    });

    it('sets processedContent to true after being run', function() {
      var rogueMap = new RogueMap();
      rogueMap.processContent({});
      assert.ok(rogueMap.processedContent);
    });
  });

  describe('processContentForTiles', function() {
    beforeEach(function() {
      sinon.stub(ContentProcessor, 'processContentForActors');
      sinon.stub (RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      ContentProcessor.processContentForActors.restore();
      RogueMap.prototype.initialise.restore();
    });

    it('calls processContentForActors for each tile array inside tiles', function() {
      var tileArray1 = 'tileArray1';
      var tileArray2 = 'tileArray1';
      var tileArray3 = 'tileArray1';
      var content = {'some': 'content'};

      var tiles = [tileArray1, tileArray2, tileArray3];

      var rogueMap = new RogueMap();
      rogueMap.processContentForTiles(tiles, content);

      assert.equal(ContentProcessor.processContentForActors.callCount, tiles.length);
      assert.ok(ContentProcessor.processContentForActors.calledWith(tileArray1, content));
      assert.ok(ContentProcessor.processContentForActors.calledWith(tileArray2, content));
      assert.ok(ContentProcessor.processContentForActors.calledWith(tileArray3, content));
    });
  });

  describe('getAdjacentMaps', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
      sinon.stub (RogueMap.prototype, 'buildAdjacentMaps');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
      RogueMap.prototype.buildAdjacentMaps.restore();
    });

    it('returns rogueMap.adjacentMaps if its length is > 0', function() {
      var adjacentMaps = [ 'adjacent', 'maps'];
      var rogueMap = new RogueMap();

      rogueMap.adjacentMaps = adjacentMaps;
      assert.equal(rogueMap.getAdjacentMaps(), adjacentMaps);
    });

    it('calls buildAdjacentMaps if the length of adjacentMaps is zero then returns the value of that', function() {
      var newAdjacentMaps = [ 'new', 'adjacent', 'maps'];
      var rogueMap = new RogueMap();
      rogueMap.buildAdjacentMaps.returns(newAdjacentMaps);
      var adjacentMaps = rogueMap.getAdjacentMaps();

      assert.deepEqual(adjacentMaps, newAdjacentMaps);
      assert.equal(rogueMap.buildAdjacentMaps.callCount, 1);
    });
  });

  describe('buildAdjacentMaps', function() {
    beforeEach(function() {
      sinon.stub(RogueMap.prototype, 'initialise');
      sinon.stub(RogueMap.prototype, 'buildAdjacentMapIds');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
      RogueMap.prototype.buildAdjacentMapIds.restore();
    });

    it('gets a list of mapIds then gets those maps from the currentScene', function() {
      var currentScene = {
        getRogueMapsFromIds: function() {}
      };

      var mapIds = ['id1', 'id2', 'id3'];
      sinon.spy(currentScene, 'getRogueMapsFromIds');

      var rogueMap = new RogueMap();
      rogueMap.buildAdjacentMapIds.returns(mapIds);
      rogueMap.currentScene = currentScene;

      rogueMap.buildAdjacentMaps();

      assert.equal(rogueMap.buildAdjacentMapIds.callCount, 1);
      assert.ok(currentScene.getRogueMapsFromIds.calledWith(mapIds));
    });
  });

  describe('buildAdjacentMapIds', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
      sinon.stub( RogueMap.prototype, 'filterNonUniqueIds');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
      RogueMap.prototype.filterNonUniqueIds.restore();
    });

    it('goes through all the entrances on the map builds a list of map ids they connect to an array', function() {
      var mapId1 = 15;
      var mapId2 = 20;

      var entrance1 = { getTargetMapId: function() {return mapId1;} };
      var entrance2 = { getTargetMapId: function() {return mapId2;} };

      sinon.spy(entrance1, 'getTargetMapId');
      sinon.spy(entrance2, 'getTargetMapId');

      var entrances = [ entrance1, entrance2];

      var rogueMap = new RogueMap();
      rogueMap.entrances = entrances;
      rogueMap.filterNonUniqueIds.returnsArg(0);

      var adjacentMaps = rogueMap.buildAdjacentMapIds();
      assert.deepEqual(adjacentMaps, [mapId1, mapId2]);

      assert.equal(entrance1.getTargetMapId.callCount, 1);
      assert.equal(entrance2.getTargetMapId.callCount, 1);
    });

    it('calls filterNonUniqueIds with all ids to filter out non-unique map ids', function() {
      var mapId1 = 15;
      var mapId2 = 20;
      var mapId3 = 15;

      var entrance1 = { getTargetMapId: function() {return mapId1;} };
      var entrance2 = { getTargetMapId: function() {return mapId2;} };
      var entrance3 = { getTargetMapId: function() {return mapId3;} };

      var entrances = [entrance1, entrance2, entrance3];

      var rogueMap = new RogueMap();
      rogueMap.entrances = entrances;
      rogueMap.filterNonUniqueIds.returns([mapId1, mapId2]);

      var adjacentMaps = rogueMap.buildAdjacentMapIds();
      assert.deepEqual(adjacentMaps, [mapId1, mapId2]);
      assert.ok(rogueMap.filterNonUniqueIds.calledWith([mapId1, mapId2, mapId3]));
    });
  });

  describe('filterNonUniqueIds', function() {
    beforeEach(function() {
      sinon.stub (RogueMap.prototype, 'initialise');
    });

    afterEach(function() {
      RogueMap.prototype.initialise.restore();
    });

    it('takes a array of ids and returns one without duplicates', function() {
      var ids = [ 1, 2, 2, 3, 4, 5, 5];
      var uniqueIds = [1, 2, 3, 4, 5];

      var filteredIds = new RogueMap().filterNonUniqueIds(ids);
      assert.deepEqual(filteredIds, uniqueIds);
    });
  });
});
