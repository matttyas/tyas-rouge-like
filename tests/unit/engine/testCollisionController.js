var assert = chai.assert;

describe('Collision Controller', function() {


  describe('checkForCollision', function() {
    beforeEach(function() {
      sinon.stub(CollisionController, 'checkForCollisionWithObjects');
    });

    afterEach(function() {
      CollisionController.checkForCollisionWithObjects.restore();
    });

    it('gets the tiles, collisonObjects and enemies from the current rogueMap in rogueScene', function() {
      var currentMap = {
        getCloseTiles: function() {return []},
        getColliderActors: function() {return []},
        getEnemies: function() {return []}
      };

      var actor = {x: 50, y: 100};
      var engine = {
        tileWidth: 10,
        tileHeight: 10
      };
      sinon.spy(currentMap, 'getCloseTiles');
      sinon.spy(currentMap, 'getColliderActors');
      sinon.spy(currentMap, 'getEnemies');

      CollisionController.currentMap = currentMap;
      CollisionController.engine = engine;

      CollisionController.checkForCollision(actor);

      var expectedTileX = 5;
      var expectedTileY = 10;
      assert.equal(currentMap.getCloseTiles.callCount, 1);
      assert.ok(currentMap.getCloseTiles.calledWith(expectedTileX, expectedTileY, 2));
      assert.equal(currentMap.getColliderActors.callCount, 1);
      assert.equal(currentMap.getEnemies.callCount, 1);
    });

    it('calls checkForCollisionWithObjects with the tiles, collisionObjects and enemies', function() {
      var colliderActors = ['collider', 'actors'];
      var tiles = ['close', 'tiles'];

      var currentMap = {
        getCloseTiles: function() {return tiles},
        getColliderActors: function() {return colliderActors},
        getEnemies: function() {return []}
      };

      var actor = {x: 5, y: 10};
      var movement = {'a': 'movement'};

      CollisionController.currentMap = currentMap;
      CollisionController.checkForCollision(actor, movement);

      assert.equal(CollisionController.checkForCollisionWithObjects.callCount, 3);
      assert.ok(CollisionController.checkForCollisionWithObjects.calledWith(tiles, actor, movement));
      assert.ok(CollisionController.checkForCollisionWithObjects.calledWith(colliderActors, actor, movement));
    });
  });

  describe('checkForCollisionWithObjects', function() {
    beforeEach(function() {
      sinon.stub(CollisionController, 'buildActorCollisionRectangle');
    });

    afterEach(function() {
      CollisionController.buildActorCollisionRectangle.restore();
    });

    it('calls buildActorCollisionRectangle to get the correct actorCollisionRect', function() {
      var actor = {'an': 'actor'};
      var movement = {'a': 'movement'};

      var object = {
        checkForCollision: function() {return {};},
        applyCollision: function() {},
        getColliderObject: function() {return null;}
      };

      var objects = [object];
      CollisionController.checkForCollisionWithObjects(objects, actor, movement);

      assert.ok(CollisionController.buildActorCollisionRectangle.calledWith(actor, movement));
    });

    it('does not check for collision if the object.getColliderObject returns null', function() {
      var actor = {'an': 'actor'};
      var movement = {'a': 'movement'};
      var actorCollisionRect = {'a': 'collisionRectangle'};
      var colliderObject = {checkForCollision: function() {}};
      var object = {
        applyCollision: function() {},
        getColliderObject: function() {return null}
      };

      sinon.spy(colliderObject, 'checkForCollision');
      var objects = [object, object, object];

      CollisionController.buildActorCollisionRectangle.returns(actorCollisionRect);
      CollisionController.checkForCollisionWithObjects(objects, actor, movement);

      assert.equal(colliderObject.checkForCollision.callCount, 0);
    });

    it('checks for collision between the actor and each objects collider object', function() {
      var actor = {id: 777};
      var movement = {'a': 'movement'};
      var actorCollisionRect = {'a': 'collisionRectangle'};
      var colliderObject = {checkForCollision: function() {return {}}};
      sinon.spy(colliderObject, 'checkForCollision');
      var object = {
        applyCollision: function() {},
        getColliderObject: function() {return colliderObject}
      };

      var objects = [object, object, object];

      CollisionController.buildActorCollisionRectangle.returns(actorCollisionRect);
      CollisionController.checkForCollisionWithObjects(objects, actor, movement);

      assert.equal(colliderObject.checkForCollision.callCount, objects.length);
    });

    it('calls apply collision on the object the actor has collided with if a collision has occured', function() {
      var actor = {
        applyCollision: function() {},
        id: 777
      };
      var movement = {'a': 'movement'};
      var actorCollisionRect = {'a': 'collisionRectangle'};
      var colliderObject = {checkForCollision: function() {}};
      sinon.stub(colliderObject, 'checkForCollision');
      var object = {
        applyCollision: function() {},
        getColliderObject: function() {return colliderObject;}
      };

      sinon.spy(object, 'applyCollision');
      colliderObject.checkForCollision.returns({});
      colliderObject.checkForCollision.onCall(1).returns({collision: true});

      var objects = [object, object, object];

      CollisionController.buildActorCollisionRectangle.returns(actorCollisionRect);
      CollisionController.checkForCollisionWithObjects(objects, actor, movement);

      assert.equal(object.applyCollision.callCount, 1);
      assert.ok(object.applyCollision.calledWith(actor, {collision: true}));
    });

    it('calls apply collision on the actor that has collided with the object as well', function() {
      var actor = {
        applyCollision: function() {},
        id: 777
      };
      sinon.spy(actor, 'applyCollision');

      var movement = {'a': 'movement'};
      var actorCollisionRect = {'a': 'collisionRectangle'};
      var colliderObject = {checkForCollision: function() {}};
      sinon.stub(colliderObject, 'checkForCollision');
      var object = {
        applyCollision: function() {},
        getColliderObject: function() {return colliderObject;}
      };

      sinon.spy(object, 'applyCollision');
      colliderObject.checkForCollision.returns({});
      colliderObject.checkForCollision.onCall(1).returns({collision: true});

      var objects = [object, object, object];

      CollisionController.buildActorCollisionRectangle.returns(actorCollisionRect);
      CollisionController.checkForCollisionWithObjects(objects, actor, movement);

      assert.equal(actor.applyCollision.callCount, 1);
      assert.ok(actor.applyCollision.calledWith(object, {collision: true}));
    });

    it('does not check for collision if the actor and object are the same thing (ids match)', function() {
      var actor = {
        applyCollision: function() {},
        id: 777
      };
      sinon.spy(actor, 'applyCollision');

      var movement = {'a': 'movement'};
      var actorCollisionRect = {'a': 'collisionRectangle'};
      var colliderObject = {checkForCollision: function() {}};
      sinon.spy(colliderObject, 'checkForCollision');
      var object = {
        applyCollision: function() {},
        id: 777,
        getColliderObject: function() {return colliderObject;}
      };

      var objects = [object];

      assert.equal(colliderObject.checkForCollision.callCount, 0);
    });
  });

  describe('buildActorCollisionRectangle', function() {
    it('takes actors position rectangle, adds movement and returns a rectangle with the results', function() {
      var actor = {x: 5, y: 10, width: 20, height: 30};
      var movement = {x: 7, y: 8};

      var expected = {x: 12, y: 18, width: 20, height: 30};
      var actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);

      assert.deepEqual(actorCollisionRect, expected);
    });

    it('correctly handles recieving no or an incomplete movement object', function() {
      var actor = {x: 5, y: 10, width: 20, height: 30};

      var actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
      assert.deepEqual(actorCollisionRect, actor);

      var movement = {};
      actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
      assert.deepEqual(actorCollisionRect, actor);

      movement = {x: 0};
      actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
      assert.deepEqual(actorCollisionRect, actor);

      movement = {y: 0};
      actorCollisionRect = CollisionController.buildActorCollisionRectangle(actor, movement);
      assert.deepEqual(actorCollisionRect, actor);
    });
  });
});
