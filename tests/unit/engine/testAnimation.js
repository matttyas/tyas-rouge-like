var assert = chai.assert;

describe('Animation', function() {
  describe('animate', function() {
    beforeEach( function() {
      sinon.stub(Animation.prototype, 'checkForNewFrame');
    });

    afterEach( function() {
      Animation.prototype.checkForNewFrame.restore();
    });

    it('returns without change if it is not time for a new frame', function() {
      Animation.prototype.checkForNewFrame.returns(false);

      var animation = new Animation();
      animation.animationIndex = 0;
      animation.animate();
      assert.equal(animation.animationIndex, 0);
    });

    it('increments the value of animationIndex otherwise', function() {
      Animation.prototype.checkForNewFrame.returns(true);
      var animation = new Animation();
      animation.currentFrame = {x: 0, y: 0, width: 20, height: 20};
      animation.animationOrder = [0, 1, 2, 1];
      animation.animationIndex = 1;

      animation.animate();
      assert.equal(animation.animationIndex, 2);
    });

    it('resets animationIndex to 0 if it is at the end of the animationOrder array', function() {
      Animation.prototype.checkForNewFrame.returns(true);
      var animation = new Animation();
      animation.currentFrame = {x: 0, y: 0, width: 20, height: 20};
      animation.animationOrder = [0, 1, 2, 1];
      animation.animationIndex = 3;

      animation.animate();
      assert.equal(animation.animationIndex, 0);
    });

    it('then calculates the x value for that frame and sets that on currentFrame', function() {
      Animation.prototype.checkForNewFrame.returns(true);
      var animation = new Animation();
      animation.currentFrame = {x: 0, y: 0, width: 20, height: 20};
      animation.animationOrder = [0, 1, 2, 1];
      animation.animationIndex = 1;

      animation.animate();
      assert.equal(animation.currentFrame.x, 40);
    });
  });

  describe('checkForNewFrame', function() {
    it('always returns false if there is only one frame in the animation', function() {
      var animation = new Animation([0]);
    });

    it('increments timeSinceFrameChange and returns false', function() {
      var animation = new Animation([0, 1]);
      animation.timeSinceFrameChange = 1;

      animation.checkForNewFrame();

      assert.equal(animation.timeSinceFrameChange, 2);
    });

    it('returns false if it is not time for a new frame', function() {
      var animation = new Animation([0, 1]);
      animation.timeSinceFrameChange = 1;

      assert.equal(animation.checkForNewFrame(), false);
    });

    it('resets timeSinceFrameChange and returns true if it is time for a new frame', function() {
      var animation = new Animation([0, 1]);
      animation.timeSinceFrameChange = 4;
      animation.timeBetweenFrames = 5;

      assert.equal(animation.checkForNewFrame(), true);

      assert.equal(animation.timeSinceFrameChange, 0);
    });
  });
});
