var assert = chai.assert;

describe('Content List Builder', function() {
  describe('buildListFromRogueMap', function() {
    beforeEach(function() {
    	sinon.stub(ContentListBuilder, 'buildContentFromElementsList');
      sinon.stub(ContentListBuilder, 'buildContentFromTiles');
    });

    afterEach(function() {
    	ContentListBuilder.buildContentFromElementsList.restore();
      ContentListBuilder.buildContentFromTiles.restore();
    });

    it('gets content from the rogueMap decorations and adds it to the contentList', function() {
    	var rogueMap = {
    		decorations: [ {'lots of': 'decorations'}]
    	};

    	var decorationsContent = [1, 2, 3];

      ContentListBuilder.buildContentFromElementsList.returns([]);
      ContentListBuilder.buildContentFromTiles.returns([]);
    	ContentListBuilder.buildContentFromElementsList.onCall(0).returns(decorationsContent);
    	var content = ContentListBuilder.buildListFromRogueMap(rogueMap);
    	assert.ok(ContentListBuilder.buildContentFromElementsList.calledWith(rogueMap.decorations));
    	assert.deepEqual(content, decorationsContent);
    });

    it('gets content from the rogueMap entrances and adds it to the contentList', function() {
      var rogueMap = {
    		entrances: [ {'lots of': 'entrances'}]
    	};

    	var entrancesContent = [1, 2, 3];

      ContentListBuilder.buildContentFromElementsList.returns([]);
      ContentListBuilder.buildContentFromTiles.returns([]);
    	ContentListBuilder.buildContentFromElementsList.onCall(1).returns(entrancesContent);
    	var content = ContentListBuilder.buildListFromRogueMap(rogueMap);
    	assert.ok(ContentListBuilder.buildContentFromElementsList.calledWith(rogueMap.entrances));
    	assert.deepEqual(content, entrancesContent);
    });

    it('gets content from the rogueMap enemies and adds it to the contentList', function() {
      var rogueMap = {
    		enemies: [ {'lots of': 'enemies'}]
    	};

    	var enemiesContent = [1, 2, 3];

      ContentListBuilder.buildContentFromElementsList.returns([]);
      ContentListBuilder.buildContentFromTiles.returns([]);
    	ContentListBuilder.buildContentFromElementsList.onCall(2).returns(enemiesContent);
    	var content = ContentListBuilder.buildListFromRogueMap(rogueMap);
    	assert.ok(ContentListBuilder.buildContentFromElementsList.calledWith(rogueMap.enemies));
    	assert.deepEqual(content, enemiesContent);
    });

    it('gets content from the rogueMap tiles and adds it to the contentList', function() {
      var rogueMap = {
    		tiles: [ {'lots of': 'tiles'}]
    	};

    	var tilesContent = [1, 2, 3];

      ContentListBuilder.buildContentFromElementsList.returns([]);
    	ContentListBuilder.buildContentFromTiles.returns(tilesContent);
    	var content = ContentListBuilder.buildListFromRogueMap(rogueMap);
    	assert.ok(ContentListBuilder.buildContentFromTiles.calledWith(rogueMap.tiles));
    	assert.deepEqual(content, tilesContent);
    });
  });

  describe('buildListFromRogueScene', function() {
    beforeEach(function() {
      sinon.stub(ContentListBuilder, 'buildContentFromElementsList');
    });

    afterEach(function() {
      ContentListBuilder.buildContentFromElementsList.restore();
    });

    it('gets the content from the rogueScene heroes and adds it to the contentList', function() {
      var heroes = ['array', 'of', 'heroes'];
      var rogueScene = {
        getHeroes: function() { return heroes; }
      };

      var heroesContent = ['array of', 'hero content'];
      sinon.spy(rogueScene, 'getHeroes');

      ContentListBuilder.buildContentFromElementsList.returns(heroesContent);
      var content = ContentListBuilder.buildListFromRogueScene(rogueScene);
      assert.ok(ContentListBuilder.buildContentFromElementsList.calledWith(heroes));
      assert.deepEqual(content, heroesContent);
    });
  });

  describe('buildContentFromElementsList', function() {
    beforeEach(function() {
      sinon.stub(ContentListBuilder, 'filterNonUniqueContent');
    });

    afterEach(function() {
      ContentListBuilder.filterNonUniqueContent.restore();
    });
    it('takes a list of elements and gets the name and texture path from each', function() {
    	var element = {
    		getName: function() { return 'name'; },
    		getTexturePath: function() { return 'a/content/path.png'}
    	};

    	sinon.spy(element, 'getName');
    	sinon.spy(element, 'getTexturePath');
      ContentListBuilder.filterNonUniqueContent.returnsArg(0);

      var elements = [element, element, element];
      ContentListBuilder.buildContentFromElementsList(elements);

      assert.equal(element.getName.callCount, 3);
      assert.equal(element.getTexturePath.callCount, 3);
    });

    it('returns a list of content suitable for processing by the ContentLoader', function() {
    	var name = 'name';
    	var contentPath = 'a/content/path.png';
    	var element = {
    		getName: function() { return name; },
    		getTexturePath: function() { return contentPath}
    	};

    	sinon.spy(element, 'getName');
    	sinon.spy(element, 'getTexturePath');
      ContentListBuilder.filterNonUniqueContent.returnsArg(0);

    	var elements = [element];
    	var expectedContent = [ { name: name, content: contentPath}];

    	var content = ContentListBuilder.buildContentFromElementsList(elements);
    	assert.deepEqual(content, expectedContent);
    });

    it('calls filterNonUniqueContent with the compiled list to remove non unique elements', function() {
      var name = 'name';
      var contentPath = 'a/content/path.png';
      var element = {
        getName: function() { return name; },
        getTexturePath: function() { return contentPath}
      };

      sinon.spy(element, 'getName');
      sinon.spy(element, 'getTexturePath');
      ContentListBuilder.filterNonUniqueContent.returnsArg(0);

      var elements = [element];
      var expectedContent = [ { name: name, content: contentPath}];

      var content = ContentListBuilder.buildContentFromElementsList(elements);
      assert.equal(ContentListBuilder.filterNonUniqueContent.callCount, 1);
    });

    it('returns an empty array if it recieves incorrect input', function() {
      var content = ContentListBuilder.buildContentFromElementsList();
      assert.deepEqual(content, []);

      var content = ContentListBuilder.buildContentFromElementsList(null);
      assert.deepEqual(content, []);

      var content = ContentListBuilder.buildContentFromElementsList(1);
      assert.deepEqual(content, []);

      var content = ContentListBuilder.buildContentFromElementsList(undefined);
      assert.deepEqual(content, []);

      var content = ContentListBuilder.buildContentFromElementsList({});
      assert.deepEqual(content, []);

      var content = ContentListBuilder.buildContentFromElementsList([]);
      assert.deepEqual(content, []);
    });
  });

  describe('filterNonUniqueContent', function() {
    it ('filters out non unique content items', function() {
      var content = [{name: 'name', contentPath: 'path'}, {name:'otherName', contentPath: 'otherNamePath'}, {name: 'name', contentPath: 'path'}];
      var expectedContent = [{name: 'name', contentPath: 'path'}, {name:'otherName', contentPath: 'otherNamePath'}];

      var filteredContent = ContentListBuilder.filterNonUniqueContent(content);
      assert.deepEqual(filteredContent, expectedContent);
    });

    it('filters out any item that doesnt have a name', function() {
      var content =  [{name: 'name', contentPath: 'path'}, {contentPath: 'otherNamePath'}];
      var expectedContent = [{name: 'name', contentPath: 'path'}];

      var filteredContent = ContentListBuilder.filterNonUniqueContent(content);
      assert.deepEqual(filteredContent, expectedContent);
    });
  });

  describe('buildContentFromTiles', function() {
    beforeEach(function() {
      sinon.stub(ContentListBuilder, 'filterNonUniqueContent');
    });

    afterEach(function() {
      ContentListBuilder.filterNonUniqueContent.restore();
    });

    it('goes through all elements in 2d array tiles and pushes content for each', function() {
      var tile = {
        getName: function() {},
        getTexturePath: function() {}
      };

      sinon.spy(tile, 'getName');
      sinon.spy(tile, 'getTexturePath');

      var tiles = [
        [ tile, tile],
        [ tile, tile]
      ];

      var content = ContentListBuilder.buildContentFromTiles(tiles);
      assert.equal(tile.getName.callCount, 4);
      assert.equal(tile.getTexturePath.callCount, 4);
    });
  });

  describe('getContentForItems', function() {
    beforeEach(function() {
      sinon.stub(ItemContentLibrary, 'getRogueItems');
      sinon.stub(ContentListBuilder, 'buildContentFromElementsList');
    });

    afterEach(function() {
      ItemContentLibrary.getRogueItems.restore();
      ContentListBuilder.buildContentFromElementsList.restore();
    });

    it('calls the ItemContentLibrary.getContent then passes that to buildContentFromElementsList and returns the result', function() {
      var items = ['some', 'items'];
      var builtItemContent = ['built', 'item', 'content'];

      ItemContentLibrary.getRogueItems.returns(items);
      ContentListBuilder.buildContentFromElementsList.returns(builtItemContent);
      var itemContent = ContentListBuilder.getContentForItems();
      assert.ok(ContentListBuilder.buildContentFromElementsList.calledWith(items));
      assert.deepEqual(itemContent, builtItemContent);
    });
  });
});
