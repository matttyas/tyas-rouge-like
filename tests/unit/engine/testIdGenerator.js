var assert = chai.assert;

describe('Id Generator', function() {
  describe('generateId', function() {
    it('Generates the next Id in the sequence, ensuring that all IDs are unique', function() {
      IdGenerator.currentId = 1;
      var id = IdGenerator.generateId();
      assert.equal(id, 2);
      assert.equal(IdGenerator.currentId, 2);
    });
  });
});
