var assert = chai.assert;
DEBUG = true;

describe('Rogue Scene', function() {
  describe('initialise', function() {
    beforeEach(function() {
      sinon.stub(RogueScene.prototype, 'initialiseDungeon');
      sinon.stub(RogueScene.prototype, 'useEditMode');
      sinon.stub(RogueScene.prototype, 'dontUseEditMode');
      sinon.stub(Hero.prototype, 'initialise');
      sinon.stub(ContentListBuilder, 'buildListFromRogueMap');
      ContentListBuilder.buildListFromRogueMap.returns([]);
      sinon.stub(ContentListBuilder, 'getContentForItems');
      ContentListBuilder.getContentForItems.returns([]);
    });

    afterEach(function() {
      RogueScene.prototype.initialiseDungeon.restore();
      RogueScene.prototype.useEditMode.restore();
      RogueScene.prototype.dontUseEditMode.restore();
      Hero.prototype.initialise.restore();
      ContentListBuilder.buildListFromRogueMap.restore();
      ContentListBuilder.getContentForItems.restore();
    });

    it('adds the editor', function() {
      var rogueScene = new RogueScene();
      assert.ok(rogueScene.editor);
    });

    it('calls initialiseDungeon', function() {
      var rogueScene = new RogueScene();
      assert.equal(rogueScene.initialiseDungeon.callCount, 1);
    });

    it('gets and sets the content the scene will need to load before the game can be played from the rogueMap', function() {
      var rogueScene = new RogueScene();
      assert.ok(ContentListBuilder.buildListFromRogueMap.calledWith(rogueScene.currentMap));
    });

    it('gets the item content from the ContentListBuilder and adds it to the contentToLoad', function() {
      var itemContent = ['some', 'item', 'content'];
      ContentListBuilder.getContentForItems.returns(itemContent);

      var rogueScene = new RogueScene();
      assert.equal(ContentListBuilder.getContentForItems.callCount, 1);
      assert.deepEqual(rogueScene.contentToLoad, itemContent);
    });

    it('Calls useEditMode if global DEBUG is on', function() {
      DEBUG = true;
      var rogueScene = new RogueScene();
      assert.equal(rogueScene.useEditMode.callCount, 1);
      assert.equal(rogueScene.dontUseEditMode.callCount, 0);
    });

    it('Calls dontUseEditMode if global DEBUG is on', function() {
      DEBUG = false;
      var rogueScene = new RogueScene();
      assert.equal(rogueScene.useEditMode.callCount, 0);
      assert.equal(rogueScene.dontUseEditMode.callCount, 1);
    });
  });

  describe('update', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it('calls update on the currentMap', function() {
      var currentMap = { update: function() {}};
      sinon.spy(currentMap, 'update');

      var rogueScene = new RogueScene();
      rogueScene.currentMap = currentMap;
      rogueScene.heroes = [];
      rogueScene.update();

      assert.equal(currentMap.update.callCount, 1);
    });

    it('calls update on each hero', function() {
      var currentMap = { update: function() {}};
      var hero = { update: function() {}};
      sinon.spy(hero, 'update');

      var heroes = [hero, hero];
      var rogueScene = new RogueScene();
      rogueScene.heroes = heroes;
      rogueScene.currentMap = currentMap;

      rogueScene.update();
      assert.equal(hero.update.callCount, 2);
    });
  })

  describe('handleKeyboard', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it ('when inputState set to editInputState it passes keyboard input to editor', function() {
      var editor = { handleKeyboard: function() {}};
      sinon.spy(editor, 'handleKeyboard');

      var rogueScene = new RogueScene();
      rogueScene.inputState = rogueScene.editInputState;
      rogueScene.editor = editor;

      rogueScene.handleKeyboard({}, {});

      assert.equal(editor.handleKeyboard.callCount, 1);
    });

    it ('when inputState set to heroInputState it passes keyboard input to hero', function() {
      var hero = { handleKeyboard: function() {}};
      sinon.spy(hero, 'handleKeyboard');

      var rogueScene = new RogueScene();
      rogueScene.inputState = rogueScene.heroInputState;
      rogueScene.hero = hero;

      rogueScene.handleKeyboard({}, {});

      assert.equal(hero.handleKeyboard.callCount, 1);
    });

    it ('when inputState set to menuInputState it passes keyboard input to currentMenu', function() {
      var currentMenu = { handleKeyboard: function() {}};
      sinon.spy(currentMenu, 'handleKeyboard');

      var rogueScene = new RogueScene();
      rogueScene.inputState = rogueScene.menuInputState;
      rogueScene.currentMenu = currentMenu;

      rogueScene.handleKeyboard({}, {});

      assert.equal(currentMenu.handleKeyboard.callCount, 1);
    });
  });

  describe('render', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it('renders the currentMap if set', function() {
      var rogueScene = new RogueScene();
      var currentMap = { render: function() {}};
      sinon.spy(currentMap, 'render');
      var renderer = 'renderer';
      var ctx = 'ctx';
      rogueScene.currentMap = currentMap;
      rogueScene.hero = null;
      rogueScene.currentMenu = null;

      rogueScene.render(ctx, renderer);
      assert.ok(currentMap.render.calledWith(ctx, renderer));
    });

    it('renders the hero if set', function() {
      var rogueScene = new RogueScene();
      var hero = { render: function() {}};
      sinon.spy(hero, 'render');
      var renderer = 'renderer';
      rogueScene.currentMap = null;
      rogueScene.hero = hero;
      rogueScene.currentMenu = null;

      rogueScene.render({}, renderer);
      assert.ok(hero.render.calledWith(renderer));
    });

    it('renders the curentMenu if set', function() {
      var rogueScene = new RogueScene();
      var currentMenu = { render: function() {}};
      sinon.spy(currentMenu, 'render');
      var renderer = 'renderer';
      var ctx = 'ctx';
      rogueScene.currentMenu = currentMenu;
      rogueScene.hero = null;
      rogueScene.currentMap = null;

      rogueScene.render(ctx, renderer);
      assert.ok(currentMenu.render.calledWith(ctx, renderer));
    });
  });

  describe('setCurrentMap', function() {
    beforeEach(function(){
      sinon.stub(RogueScene.prototype, 'setUpHeroOnMap');
      sinon.stub(RogueScene.prototype, 'loadAdjacentMapsContent')
      sinon.stub(RogueMap.prototype, 'getAdjacentMaps');
      sinon.stub(Hero.prototype, 'initialise');
      sinon.stub(CollisionController, 'setCurrentMap');
    });

    afterEach(function() {
      RogueScene.prototype.setUpHeroOnMap.restore();
      RogueScene.prototype.loadAdjacentMapsContent.restore();
      RogueMap.prototype.getAdjacentMaps.restore();
      Hero.prototype.initialise.restore();
      CollisionController.setCurrentMap.restore();
    });

    it('sets the current map and places the player on it', function() {

      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      rogueScene.hero = new Hero(0,0);
      sinon.stub(RogueMap.prototype, 'initialise');
      var map = new RogueMap();
      RogueMap.prototype.initialise.restore();

      rogueScene.setCurrentMap(map);

      assert.equal(rogueScene.setUpHeroOnMap.callCount, 1);

    });

    it('passes the newly set map to the CollisionController', function() {

      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      rogueScene.hero = new Hero(0,0);
      sinon.stub(RogueMap.prototype, 'initialise');
      var map = new RogueMap();
      RogueMap.prototype.initialise.restore();

      rogueScene.setCurrentMap(map);
      assert.ok(CollisionController.setCurrentMap.calledWith(map));
    })
  });

  describe('setUpHeroOnMap', function() {
    beforeEach(function() {
      sinon.stub(Hero.prototype, 'setX');
      sinon.stub(Hero.prototype, 'setY');
      sinon.stub(Hero.prototype, 'setCurrentMap');
      sinon.stub(Hero.prototype, 'initialise');
    });

    afterEach(function() {
      Hero.prototype.setX.restore();
      Hero.prototype.setY.restore();
      Hero.prototype.setCurrentMap.restore();
      Hero.prototype.initialise.restore();
    });

    it('gets the stairs up on a map and places the hero on them', function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      rogueScene.hero = new Hero(0,0);
      RogueScene.prototype.initialise.restore();
      var entranceX = 5;
      var entranceY = 7;
      var entrance = { getX: function() { return entranceX;}, getY: function() {return entranceY;}};
      var currentMap = { current: 'Map'};

      rogueScene.setUpHeroOnMap(currentMap, entrance);

      assert.ok(rogueScene.hero.setX.calledWith(entranceX));
      assert.ok(rogueScene.hero.setY.calledWith(entranceY));
    });

    it('sets the new currentMap onto the hero', function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      rogueScene.hero = new Hero(0,0);
      RogueScene.prototype.initialise.restore();

      var entranceX = 5;
      var entranceY = 7;
      var entrance = { getX: function() { return entranceX;}, getY: function() {return entranceY;}};
      var currentMap = {current: 'map'};
      rogueScene.setUpHeroOnMap(currentMap, entrance);

      assert.ok(rogueScene.hero.setCurrentMap.calledWith(currentMap));
    });
  });

  describe('loadAdjacentMapsContent', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      sinon.stub(ContentListBuilder, 'buildListFromRogueMap');
      sinon.stub(ContentLoader, 'load');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
      ContentListBuilder.buildListFromRogueMap.restore();
      ContentLoader.load.restore();
    });

    it('checks each adjacent map to see if they have already loaded content', function() {
      var rogueMap1 = {getProcessedContent: function() {return true;}};
      var rogueMap2 = {getProcessedContent: function() {return true;}};
      sinon.spy(rogueMap1, 'getProcessedContent');
      sinon.spy(rogueMap2, 'getProcessedContent');

      var adjacentMaps = [rogueMap1, rogueMap2];

      var rogueScene = new RogueScene();
      rogueScene.loadAdjacentMapsContent(adjacentMaps);

      assert.ok(rogueMap1.getProcessedContent.callCount, 1);
      assert.ok(rogueMap2.getProcessedContent.callCount, 1);
    });

    it('for each that hasnt loaded content we call the ContentLoader with that map', function() {
      var rogueMap1 = {getProcessedContent: function() {return true;}};
      var rogueMap2 = {getProcessedContent: function() {return false;}};

      var adjacentMaps = [rogueMap1, rogueMap2];
      var contentToLoad = ['some', 'content'];
      ContentListBuilder.buildListFromRogueMap.returns(contentToLoad);

      var rogueScene = new RogueScene();
      rogueScene.loadAdjacentMapsContent(adjacentMaps);

      assert.ok(ContentListBuilder.buildListFromRogueMap.calledWith(rogueMap2));
      assert.ok(ContentLoader.load.calledWith(contentToLoad));
    });
  });

  describe('showInspectMenu', function() {
    it('it sets the currentMenu then checks it is valid', function() {
      sinon.stub(InspectMenu.prototype, 'initialise');
      sinon.stub(InspectMenu.prototype, 'setCurrentScene');

      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      sinon.stub(rogueScene, 'checkInspectMenuValid');

      rogueScene.showInspectMenu({});

      assert.equal(rogueScene.checkInspectMenuValid.callCount, 1);
      assert.ok(rogueScene.currentMenu);
      assert.ok(rogueScene.currentMenu.setCurrentScene.calledWith(rogueScene));

      InspectMenu.prototype.initialise.restore();
      InspectMenu.prototype.setCurrentScene.restore();
    });
  });

  describe('checkInspectMenuValid', function() {
    beforeEach(function() {
      sinon.stub(InspectMenu.prototype, 'hasNoActions');
      sinon.stub(InspectMenu.prototype, 'initialise');
    });

    afterEach(function() {
      InspectMenu.prototype.hasNoActions.restore();
      InspectMenu.prototype.initialise.restore();
    });

    it('It nullifies currentMenu if the menu has no actions', function() {
      InspectMenu.prototype.hasNoActions.returns(true);

      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      rogueScene.currentMenu = new InspectMenu();

      rogueScene.checkInspectMenuValid();

      assert.equal(rogueScene.currentMenu, null);
    });

    it('It sets inputState to inspect if the menu has actions', function() {
      InspectMenu.prototype.hasNoActions.returns(false);
      var currentMenu = new InspectMenu();

      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      rogueScene.currentMenu = currentMenu;

      rogueScene.checkInspectMenuValid();

      assert.equal(rogueScene.currentMenu, currentMenu);
      assert.equal(rogueScene.inputState, rogueScene.menuInputState);
    });
  });

  describe('closeCurrentMenu', function() {
    it ('Nullifies the current menu and sets the input state back to heroInputState', function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      var rogueScene = new RogueScene();
      RogueScene.prototype.initialise.restore();
      rogueScene.currentMenu = {};
      rogueScene.inputState = 'Menu';

      rogueScene.closeCurrentMenu();

      assert.equal(rogueScene.inputState, rogueScene.heroInputState);
      assert.equal(rogueScene.currentMenu, null);
    });
  });

  describe('goToMap', function() {
    beforeEach(function() {
      sinon.stub(RogueScene.prototype, 'setCurrentMap');
      sinon.stub(RogueScene.prototype, 'getMapWithId');
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.setCurrentMap.restore();
      RogueScene.prototype.getMapWithId.restore();
      RogueScene.prototype.initialise.restore();
    });


    it('calls set currentMap with the correct entrance and map objects', function() {

      var entrance = { id: '1'};
      var newCurrentMap = { id: '15', getEntranceWithId: function() {return entrance}};
      sinon.spy(newCurrentMap, 'getEntranceWithId');

      var mapId = '15';
      var entranceId = '1';
      var rogueScene = new RogueScene();
      rogueScene.getMapWithId.returns(newCurrentMap);

      rogueScene.goToMap(mapId, entranceId);
      assert.ok(rogueScene.getMapWithId.calledWith(mapId));
      assert.ok(newCurrentMap.getEntranceWithId.calledWith(entranceId));
      assert.ok(rogueScene.setCurrentMap.calledWith(newCurrentMap, entrance));
    });
  });

  describe('getMapWithId', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it('returns the first map with an id matching mapId', function() {
      var rogueScene = new RogueScene();
      var map1 = { getId: function() { return 5}};
      sinon.spy(map1, 'getId');
      var map2 = { getId: function() { return 10}};
      sinon.spy(map2, 'getId');

      var dungeon = [ map1, map2];
      rogueScene.dungeon = dungeon;

      var returnedMap = rogueScene.getMapWithId(10);
      assert.ok(map1.getId.callCount, 1);
      assert.ok(map2.getId.callCount, 1);
      assert.deepEqual(returnedMap, map2);
    });

    it('returns null if no maps match the id', function() {
      var rogueScene = new RogueScene();
      var map1 = { getId: function() { return 5}};
      sinon.spy(map1, 'getId');
      var map2 = { getId: function() { return 10}};
      sinon.spy(map2, 'getId');

      var dungeon = [ map1, map2];
      rogueScene.dungeon = dungeon;

      var returnedMap = rogueScene.getMapWithId(15);

      assert.ok(map1.getId.callCount, 1);
      assert.ok(map2.getId.callCount, 1);
      assert.deepEqual(returnedMap, null);
    });
  });

  describe('renderLoadingScreen', function() {
    beforeEach( function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      sinon.stub(RogueScene.prototype, 'renderLoadingText');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
      RogueScene.prototype.renderLoadingText.restore();
    });

    it('clears the screen with black', function() {
      var renderer = {
        clearBackground: function() {}
      };

      sinon.spy(renderer, 'clearBackground');

      var rogueScene = new RogueScene();
      rogueScene.renderLoadingScreen({}, renderer);

      assert.equal(renderer.clearBackground.callCount, 1);
    });

    it('renders loading text', function() {
       var renderer = {
        clearBackground: function() {}
      };

      sinon.spy(renderer, 'clearBackground');

      var rogueScene = new RogueScene();
      rogueScene.renderLoadingScreen({}, renderer);

      assert.equal(rogueScene.renderLoadingText.callCount, 1);
    });
  });

  describe('renderLoadingText', function() {
    beforeEach(function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it('sets text size and color', function() {
      var renderer = {
        setTextSize: function() {},
        setTextColor: function() {},
        drawText: function() {}
      };

      sinon.spy(renderer, 'setTextSize');
      sinon.spy(renderer, 'setTextColor');

      var rogueScene = new RogueScene();
      rogueScene.renderLoadingText({}, renderer);

      assert.ok(renderer.setTextSize.calledWith(30));
      assert.ok(renderer.setTextColor.calledWith('rgb(255,255,255)'));
    });

    it('renders the current rogueScene.loadingText', function() {
      var renderer = {
        setTextSize: function() {},
        setTextColor: function() {},
        drawText: function() {}
      };

      sinon.spy(renderer, 'drawText');

      var rogueScene = new RogueScene();
      rogueScene.loadingText = 'Loading...';
      rogueScene.renderLoadingText({}, renderer);

      assert.ok(renderer.drawText.calledWith(rogueScene.loadingText));
    });
  });

  describe('processContent', function() {
    beforeEach(function() {
      sinon.stub(RogueScene.prototype, 'initialise');
    });

    afterEach(function() {
      RogueScene.prototype.initialise.restore();
    });

    it('Sets the scene to have finished loading once it is finished', function() {
      var rogueScene = new RogueScene();
      rogueScene.currentMap = {processContent: function() {}};
      rogueScene.processContent({});

      assert.ok(rogueScene.loaded);
    });

    it('Calls processContent to each of the maps in the dungeon', function() {
      return assert.ok(true);

      var rogueScene = new RogueScene();
      var map = { processContent: function() {}};
      sinon.spy(map, 'processContent');

      var dungeon = [map, map, map];
      rogueScene.dungeon = dungeon;

      var content = {'some': 'content'};
      rogueScene.processContent(content);

      assert.ok(map.processContent.calledWith(content));
      assert.equal(map.processContent.callCount, dungeon.length);
    });

    it('TEMP - Calls processContent on the currentMap', function() {
      var map = {processContent: function() {}};
      sinon.spy(map, 'processContent');
      var content = {'some': 'content'};

      rogueScene = new RogueScene();
      rogueScene.currentMap = map;

      rogueScene.processContent(content);
      assert.ok(map.processContent.calledWith(content));
    });
  });

  describe('getRogueMapsFromIds', function() {
    beforeEach(function() {
      sinon.stub(RogueScene.prototype, 'initialise');
      sinon.stub(RogueScene.prototype, 'getMapWithId');
    });

    afterEach(function() {
      RogueScene.prototype.getMapWithId.restore();
      RogueScene.prototype.initialise.restore();
    });

    it('calls getMapWithId with each Id that it recieves', function() {
      var mapId1 = 1;
      var mapId2 = '15';
      var mapIds = [mapId1, mapId2];

      var map = {'A': 'Map'};
      var map2 = {'Another': 'Map'};

      var rogueScene = new RogueScene();
      rogueScene.getMapWithId.returns(map);
      rogueScene.getMapWithId.onCall(1).returns(map2);

      var expectedMaps = [map, map2];
      var maps = rogueScene.getRogueMapsFromIds(mapIds);

      assert.deepEqual(maps, expectedMaps);
      assert.ok(rogueScene.getMapWithId(mapId1));
      assert.ok(rogueScene.getMapWithId(mapId2));
    });

    it('returns an empty array if it gets invalid mapIds', function() {
      var rogueScene = new RogueScene();

      assert.deepEqual(rogueScene.getRogueMapsFromIds(), []);
      assert.deepEqual(rogueScene.getRogueMapsFromIds(null), []);
      assert.deepEqual(rogueScene.getRogueMapsFromIds(1), []);
      assert.deepEqual(rogueScene.getRogueMapsFromIds(undefined), []);
      assert.deepEqual(rogueScene.getRogueMapsFromIds({}), []);
      assert.deepEqual(rogueScene.getRogueMapsFromIds([]), []);
    });
  });
});
