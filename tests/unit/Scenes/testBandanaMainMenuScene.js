var assert = chai.assert;

describe('Bandana Scene', function() {
  describe('changeSelectedMenuOption', function() {
    it('changes the currently selected menu option by 1 if the array does not wrap', function() {

      var bandanaMainMenuScene = new BandanaMainMenuScene();

      assert.equal(bandanaMainMenuScene.mainMenuOptions[0].selected, true);
      bandanaMainMenuScene.changeSelectedMenuOption(1);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[0].selected, false);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[1].selected, true);
    });

    it('changes the currently selected menu option by -1 if the array does not wrap', function() {

      var bandanaMainMenuScene = new BandanaMainMenuScene();
      bandanaMainMenuScene.mainMenuOptions[0].selected = false;
      bandanaMainMenuScene.mainMenuOptions[1].selected = true;
      bandanaMainMenuScene.selectedMenuOption = 1;

      assert.equal(bandanaMainMenuScene.mainMenuOptions[1].selected, true);
      bandanaMainMenuScene.changeSelectedMenuOption(-1);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[1].selected, false);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[0].selected, true);
    });

    it('the currently selected menu option wraps back to zero if increase greater than array length', function() {
      var bandanaMainMenuScene = new BandanaMainMenuScene();
      bandanaMainMenuScene.mainMenuOptions[0].selected = false;
      bandanaMainMenuScene.mainMenuOptions[2].selected = true;
      bandanaMainMenuScene.selectedMenuOption = 2;
      bandanaMainMenuScene.changeSelectedMenuOption(1);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[2].selected, false);
      assert.equal(bandanaMainMenuScene.mainMenuOptions[0].selected, true);
    });
  });

  describe('handleKeyboard', function() {
    beforeEach(function() {
      sinon.stub(BandanaMainMenuScene.prototype, 'menuChoiceHandleKeyboard');
    });

    afterEach(function() {
      BandanaMainMenuScene.prototype.menuChoiceHandleKeyboard.restore();
    });

    it('passes input to menuChoiceHandleKeyboard if inputstate is MenuChoice', function() {
      var bandanaMainMenuScene = new BandanaMainMenuScene();
      bandanaMainMenuScene.inputState = 'MenuChoice';
      bandanaMainMenuScene.handleKeyboard([], []);
      assert.equal(bandanaMainMenuScene.menuChoiceHandleKeyboard.callCount, 1)
    });
  });

  describe('menuChoiceHandleKeyboard', function () {
    beforeEach(function() {
      sinon.stub(BandanaMainMenuScene.prototype, 'changeSelectedMenuOption');
    });

    afterEach(function() {
      BandanaMainMenuScene.prototype.changeSelectedMenuOption.restore();
    })
    it('calls changeSelectedMenuOption with -1 when the user presses W and wasnt in previous state', function() {

      var bandanaMainMenuScene = new BandanaMainMenuScene();
      bandanaMainMenuScene.menuChoiceHandleKeyboard([]);
    });
  });
});