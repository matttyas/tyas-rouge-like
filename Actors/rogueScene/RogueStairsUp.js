function RogueStairsUp(x, y, entranceData) {
  this.x = x;
  this.y = y;

  this.width = 100;
  this.height = 100;
  this.color = 'rgb(175, 175, 175)';
  this.inspectActions = ['Ascend Stairs'];
  this.actionMappings = {'Ascend Stairs': 'exitArea'};

  this.initialise(entranceData);
};

RogueStairsUp.prototype = new RogueEntrance();
