/*
  At the moment this is just used for debug purposes, so that I can put
  roomData information into a generated room.
*/
function RogueRoom(position, roomData) {
  this.DEFAULTCOLOR = 'rgb(0,255,255)';
  this.DEBUGTEXTCOLOR = 'rgb(0,0,0)';
  this.textSize = 18;
  if(position) {
    this.x = position.x;
    this.y = position.y;
  }

  this.color = this.DEFAULTCOLOR;
  this.roomData = roomData;
};

RogueRoom.prototype = new Actor();

RogueRoom.prototype.render = function(renderer) {
  if (DEBUG) {
    var patterns = this.roomData.patterns;
    var renderX = this.x - renderer.camera.x;
    var renderY = this.y - renderer.camera.y;

    for (var i = 0; i < patterns.length; i++) {
      renderer.setTextColor(this.DEBUGTEXTCOLOR);
      renderer.setTextSize(this.textSize);
      renderer.drawText(patterns[i].getName(), renderX + 10, renderY + 25 + (i * 50));
    }
  }
};

RogueRoom.prototype.getX = function() {
  return this.x;
};

RogueRoom.prototype.getY = function() {
  return this.y;
};
