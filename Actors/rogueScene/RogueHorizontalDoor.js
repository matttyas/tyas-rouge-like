function RogueHorizontalDoor(position, doorData) {
  this.height = 20;

  this.CLOSEDWIDTH = 100;
  this.OPENWIDTH = 10;

  this.width = this.CLOSEDWIDTH;

  this.x = position.x;
  this.y = position.y - this.height / 2;

  this.color = 'rgb(139, 69, 19)';
  this.inspectActions = ['Open'];
  this.actionMappings = {'Open': 'open'};

  this.initialise(doorData);
};

RogueHorizontalDoor.prototype = new RogueDoor();

RogueHorizontalDoor.prototype.initialise = function() {
  this.colliderObject = new ColliderObject(this, 0,0,this.width,this.height);
};

RogueHorizontalDoor.prototype.useOpenRender = function() {
  this.width = this.OPENWIDTH;
  this.colliderObject.getHitBoxes()[0].width = this.width;
};

RogueHorizontalDoor.prototype.useCloseRender = function() {
  this.width = this.CLOSEDWIDTH;
  this.colliderObject.getHitBoxes()[0].width = this.width;
};
