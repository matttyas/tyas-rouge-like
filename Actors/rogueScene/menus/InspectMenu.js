function InspectMenu (target) {
  this.target = target;
  this.inspectActions = [];
  this.initialise();
  this.currentScene = null;

  this.closeKey = 'escape';
  this.upKey = 'w';
  this.downKey = 's';
  this.selectKey = 'space';

  this.backColor = 'rgb(0,0,125)';
  this.borderColor = 'rgb(255,255,255)';
  this.textColor = 'rgb(255,255,255)';
  this.highlightColor = 'rgb(160,0,0)';

  this.textSize = 25;

  this.selectedActionIndex = 0;
};

InspectMenu.prototype = new Actor();

InspectMenu.prototype.initialise = function() {
  var targetActions = this.target.getInspectActions();
  for (var i = 0; i < targetActions.length; i++) {
    this.inspectActions.push(targetActions[i]);
  }
  this.inspectActions.push('Cancel');
  this.initialisePosition();
};

InspectMenu.prototype.initialisePosition = function() {
  this.x = 530;
  this.y = 500;
  this.width = 200;
  this.height = 150;
};

InspectMenu.prototype.handleKeyboard = function (keysDown, previousKeysDown) {
  if(this.closeKey in keysDown) {
    this.closeSelf();
  }

  if(this.upKey in keysDown && !(this.upKey in previousKeysDown)) {
    this.increaseSelectedActionIndex();
  }

  if(this.downKey in keysDown && !(this.downKey in previousKeysDown)) {
    this.decreaseSelectedActionIndex();
  }

  if(this.selectKey in keysDown && !(this.selectKey in previousKeysDown)) {
    this.selectAction();
  }
};

InspectMenu.prototype.closeSelf = function() {
  if(!this.currentScene) return console.error('Trying to close menu without having scene set!');
  this.currentScene.closeCurrentMenu();
};

InspectMenu.prototype.increaseSelectedActionIndex = function() {
  if(this.hasNoActions()) return console.error('No actions added. Cannot change selectedActionIndex');
  this.selectedActionIndex++;
  if (this.selectedActionIndex == this.inspectActions.length) this.selectedActionIndex = 0;
};

InspectMenu.prototype.decreaseSelectedActionIndex = function() {
  if(this.hasNoActions()) return console.error('No actions added. Cannot change selectedActionIndex');
  this.selectedActionIndex--;
  if (this.selectedActionIndex < 0) this.selectedActionIndex = this.inspectActions.length - 1;
};

InspectMenu.prototype.selectAction = function() {
  var selectedAction = this.inspectActions[this.selectedActionIndex];
  if (this.selectedActionIndex == this.inspectActions.length - 1) {
    this.closeSelf();
  } else {
    this.closeSelf();
    this.target.takeAction(selectedAction);
  }
};

InspectMenu.prototype.hasNoActions = function() {
  return this.inspectActions.length < 1;
};

InspectMenu.prototype.setCurrentScene = function(currentScene) {
  this.currentScene = currentScene;
};

InspectMenu.prototype.render = function(ctx, renderer) {
  this.renderBackPanel(ctx, renderer);
  this.renderInspectActions(ctx, renderer);
};

InspectMenu.prototype.renderBackPanel = function(ctx, renderer) {
  renderer.setFillStyle(this.backColor);
  renderer.fillRect(this.x, this.y, this.width, this.height);
};

InspectMenu.prototype.renderInspectActions = function(ctx, renderer) {
  this.renderSelectCursor(ctx, renderer);

  renderer.setTextColor(this.textColor);
  renderer.setTextSize(this.textSize);

  for (var i = 0; i < this.inspectActions.length; i++) {
    renderer.drawText(this.inspectActions[i], this.x + 10, this.y + 28 + (i * 35));
  }
};

InspectMenu.prototype.renderSelectCursor = function(ctx, renderer) {
  renderer.setFillStyle(this.highlightColor);

  var highlightX = this.x + 5;
  var highlightY = this.y + 5 + (this.selectedActionIndex * 35);
  var highlightWidth = this.width - 10;
  var highlightHeight = 30;

  renderer.fillRect(highlightX, highlightY, highlightWidth, highlightHeight);
};
