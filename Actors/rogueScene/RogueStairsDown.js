function RogueStairsDown(x, y, entranceData) {
  this.x = x;
  this.y = y;

  this.width = 100;
  this.height = 100;
  this.color = 'rgb(128, 128, 128)';
  this.inspectActions = ['Descend Stairs'];
  this.actionMappings = {'Descend Stairs': 'exitArea'};
  this.initialise(entranceData);
};

RogueStairsDown.prototype = new RogueEntrance();
