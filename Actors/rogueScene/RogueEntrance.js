function RogueEntrance(x, y, entranceData) {
  this.x = x;
  this.y = y;

  this.width = 50;
  this.height = 50;
  this.color = 'rgb(128, 128, 128)';
  this.inspectActions = ['Exit Area'];
  this.actionMappings = {'Exit Area': 'exitArea'};

  if (entranceData) this.initialise(entranceData);
};

RogueEntrance.prototype = new Actor();

RogueEntrance.prototype.initialise = function(entranceData) {
  this.classification = entranceData.getClassification();
  this.id = entranceData.getId();
  this.targetMapId = entranceData.getTargetMapId();
  this.targetEntranceId = entranceData.getTargetEntranceId();
  this.name = this.classification;
  this.texturePath = 'content/dungeon/Entrances/' + this.name + '.png';
};

RogueEntrance.prototype.render = function(renderer) {
  if (this.texture) return this.renderTexture(renderer);
  renderer.setFillStyle(this.color);
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();

  renderer.fillRect(renderX, renderY, this.width, this.height);
};

RogueEntrance.prototype.setWidth = function(width) {
  this.width = width;
};

RogueEntrance.prototype.setHeight = function(height) {
  this.height = height;
};

RogueEntrance.prototype.getX = function() {
  return this.x;
};

RogueEntrance.prototype.getY = function() {
  return this.y;
};

RogueEntrance.prototype.getId = function() {
  return this.id;
};

RogueEntrance.prototype.setId = function(id) {
  this.id = id;
};

RogueEntrance.prototype.getClassification = function() {
  return this.classification;
};

RogueEntrance.prototype.exitArea = function() {
  if ((!this.targetMapId && this.targetMapId !== 0) || (!this.targetEntranceId && this.targetEntranceId !== 0)) return console.log('OOPS Not Generated This Part Yet! Sorry about that.');
  this.currentScene.goToMap(this.targetMapId, this.targetEntranceId);
};

RogueEntrance.prototype.setTargetMapId = function(targetMapId) {
  this.targetMapId = targetMapId;
};

RogueEntrance.prototype.getTargetMapId = function() {
  return this.targetMapId;
};

RogueEntrance.prototype.setTargetEntranceId = function(targetEntranceId) {
  this.targetEntranceId = targetEntranceId;
};

RogueEntrance.prototype.getTargetEntranceId = function() {
  return this.targetEntranceId;
};

RogueEntrance.prototype.setCurrentScene = function(currentScene) {
  this.currentScene = currentScene;
};
