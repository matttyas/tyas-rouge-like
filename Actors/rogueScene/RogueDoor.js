function RogueDoor(position, doorData) {
  if(position) {
    this.x = position.x;
    this.y = position.y;
  }

  this.width = 50;
  this.height = 50;
  this.color = 'rgb(139, 69, 19)';
  this.inspectActions = ['Open'];
  this.actionMappings = {'Open': 'open'};
  this.passable = false;
  if (doorData) this.initialise(doorData);
};

RogueDoor.prototype = new Actor();

RogueDoor.prototype.initialise = function(entranceData) {

};

RogueDoor.prototype.render = function(ctx, renderer) {
  renderer.setFillStyle(this.color);
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();

  renderer.fillRect(renderX, renderY, this.width, this.height);
};

RogueDoor.prototype.setWidth = function(width) {
  this.width = width;
};

RogueDoor.prototype.setHeight = function(height) {
  this.height = height;
};

RogueDoor.prototype.getX = function() {
  return this.x;
};

RogueDoor.prototype.getY = function() {
  return this.y;
};

RogueDoor.prototype.getId = function() {
  return this.id;
};

RogueDoor.prototype.setId = function(id) {
  this.id = id;
};

RogueDoor.prototype.open = function() {
  this.inspectActions = ['Close'];
  this.actionMappings = {'Close': 'close'};
  this.useOpenRender();
};

RogueDoor.prototype.close = function() {
  this.inspectActions = ['Open'];
  this.actionMappings = {'Open': 'open'};
  this.useCloseRender();
};

RogueDoor.prototype.useOpenRender = function() {

};

RogueDoor.prototype.useCloseRender = function() {

};

RogueDoor.prototype.applyCollision = function (actor, collision, movement) {
  actor.alterMovementForCollision(collision, movement);
};
