function Bat(position, enemyData) {
  this.width = 50;
  this.height = 50;
  this.speed = 3;
  this.x = position.x;
  this.y = position.y;
  this.detectionRange = 350;

  if (enemyData) this.initialise(enemyData);
};

Bat.prototype = new RogueEnemy();



Bat.prototype.initialiseStats = function() {
  this.maxHp = 10;
  this.hp = 10;
  this.stats = {

  };
};

Bat.prototype.initialiseAnimations = function() {
  this.movementAnimation = new Animation([0, 1, 2, 1], 1)
  this.movementAnimation.setTimeBetweenFrames(5);
  this.movementAnimation.setCurrentFrame({x: 62, y: 42, width: 62, height: 27});

  this.restingAnimation = new Animation([0]);
  this.restingAnimation.setCurrentFrame({x: 13, y: 0, width: 20, height: 42});

  this.deathAnimation = new Animation([0]);
  this.deathAnimation.setCurrentFrame({x: 0, y: 81, width: 57, height: 23});
}

Bat.prototype.setupAI = function() {
  var restState = new State('rest');
  var combatState = new State('combat');

  var restBehaviour = new RestBehaviour();
  restState.addBehaviour(restBehaviour);

  var moveToBehaviour = new MoveToBehaviour();
  restState.addBehaviour(moveToBehaviour);

  var lookForBehaviour = new LookForBehaviour();
  lookForBehaviour.setNextState(combatState);
  restState.addBehaviour(lookForBehaviour);

  var moveToSpecificPointBehaviour = new MoveToSpecificPointBehaviour();
  moveToSpecificPointBehaviour.setNextState(restState);

  combatState.addBehaviour(moveToSpecificPointBehaviour);

  restBehaviour.setNextBehaviour(moveToBehaviour);
  moveToBehaviour.setNextBehaviour(restBehaviour);



  this.ai = new AI(this);
  this.ai.addState(restState);
  this.ai.addState(combatState);

  this.ai.setCurrentState('rest');

  moveToBehaviour.start();

};

Bat.prototype.render = function(renderer) {
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();

  renderer.drawSpriteSheet(this.texture, renderX, renderY, this.spriteSheetRectangle.x, this.spriteSheetRectangle.y, this.spriteSheetRectangle.width, this.spriteSheetRectangle.height);
};

Bat.prototype.applyCollision = function(actor, collision, movement) {
  if (this.isFoe(actor)) {
    actor.takeHit(10);
  }
};
