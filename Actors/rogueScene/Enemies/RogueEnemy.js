function RogueEnemy(position, enemyData) {
  this.width = 50;
  this.height = 50;
  this.passable = false;
  this.touchDamage = 0;

  if(position) {
    this.x = position.x;
    this.y = position.y;
  }

  this.collidedWithEnvironment = false; //Set to true when the enemy collides with a wall

  if (enemyData) this.initialise(enemyData);
};

RogueEnemy.prototype = new Actor();

RogueEnemy.prototype.initialise = function(enemyData) {

  this.name = enemyData.getProperty('name');
  this.texturePath = 'content/enemies/' + this.name + '.png';
  this.class = enemyData.getProperty('class') || 'Item';
  this.id = IdGenerator.generateId();
  this.initialiseStats();
  this.initialiseAnimations();
  this.setupAI();

  this.spriteSheetRectangle = {x: this.x, y: this.y, width: this.width, height: this.height};
};

//Override on specific enemy classes
RogueEnemy.prototype.initialiseStats = function() {
  this.maxHp = 100;
  this.hp = 100;
  this.stats = {};
};

//Override on specific enemy classes
RogueEnemy.prototype.setupAI = function() {

};

RogueEnemy.prototype.initialiseAnimations = function() {

};

RogueEnemy.prototype.update = function() {
  if (this.currentAnimation) {
    this.currentAnimation.animate();
    this.spriteSheetRectangle = this.currentAnimation.getSpriteSheetRectangle();
  }

  if (this.dontUpdate) return;
  this.ai.act();

}

RogueEnemy.prototype.isFoe = function(actor) {
  return (actor.name == 'Hero');
};

RogueEnemy.prototype.applyCollision = function(actor, collision, movement) {
  if (this.touchDamage) actor.takeDamage(this.touchDamage);
  actor.alterMovementForCollision(collision, movement);
};

RogueEnemy.prototype.setFoes = function (foes) {
  this.foes = foes;
};

RogueEnemy.prototype.addFoe = function(foe) {
  this.foes.push(foe);
};

RogueEnemy.prototype.getColliderObject = function() {
  return new ColliderObject(this, 0,0, this.spriteSheetRectangle.width, this.spriteSheetRectangle.height);
};

RogueEnemy.prototype.die = function() {
  this.dontUpdate = true;
  this.currentAnimation = this.deathAnimation;
};

RogueEnemy.prototype.lookForTargets = function() {
  var distanceToClosestFoe = 1000000;
  this.targetLocation = null;
  this.currentTarget = null;

  for (var i = 0; i < this.foes.length; i++) {
    var foe = this.foes[i];
    var foePosition = {x: foe.x, y: foe.y};

    var distanceToFoe = Physics.distanceBetweenPoints({x:this.x, y: this.y}, foePosition);
    if (distanceToFoe < this.detectionRange && distanceToFoe < distanceToClosestFoe) {
      this.currentTarget = foe;
      this.targetLocation = {x: foe.x, y: foe.y};
      distanceToClosestFoe = distanceToFoe;
    }
  };
};

RogueEnemy.prototype.getCurrentTarget = function() {
  return this.currentTarget;
};

RogueEnemy.prototype.getTargetLocation = function() {
  return this.targetLocation;
};

RogueEnemy.prototype.getFoes = function() {
  return this.foes;
};
