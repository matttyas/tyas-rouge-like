function RogueVerticalDoor(position, doorData) {

  this.width = 20;

  this.OPENHEIGHT = 10;
  this.CLOSEDHEIGHT = 100;
  this.height = this.CLOSEDHEIGHT;
  this.x = position.x - this.width / 2;
  this.y = position.y;

  this.color = 'rgb(139, 69, 19)';
  this.inspectActions = ['Open'];
  this.actionMappings = {'Open': 'open'};

  this.initialise(doorData);
};

RogueVerticalDoor.prototype = new RogueDoor();

RogueVerticalDoor.prototype.initialise = function() {
  this.colliderObject = new ColliderObject(this, 0,0,this.width,this.height);
};

RogueVerticalDoor.prototype.useOpenRender = function() {
  this.height = this.OPENHEIGHT;
  this.colliderObject.getHitBoxes()[0].height = this.height;
};

RogueVerticalDoor.prototype.useCloseRender = function() {
  this.height = this.CLOSEDHEIGHT;
  this.colliderObject.getHitBoxes()[0].height = this.height;
};
