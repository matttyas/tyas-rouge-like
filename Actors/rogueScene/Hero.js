function Hero (x, y, player) {
  this.x = x;
  this.y = y;
  this.width = 75;
  this.height = 75;
  this.handWidth = 10;
  this.color = 'rgb(255,255,255)';
  this.SLOTBORDERCOLOR = 'rgb(0,0,0)';
  this.SLOTCOLOR = 'rgb(50,50,50)';
  this.hpColor = 'rgb(255,0,0)';
  this.SLOTWIDTH = 60;
  this.SLOTHEIGHT = 100;
  this.player1 = true;
  this.player = player;
  this.sprinting = false;
  this.moveSpeed = 3;
  this.sprintSpeedModifier = 2;
  this.rogueScene = {};

  this.equipment = {
    'leftHand': null,
    'rightHand': null
  };

  this.idle = true;
  this.initialise();
};

Hero.prototype = new Actor();

Hero.prototype.initialise = function() {
  if (this.player == 'player2') {
    this.player1 = false;
  } else {
    this.player1 = true;
  }

  this.SLOTY = 630;

  if (this.player1) {
    this.SLOTLEFTX = 20;
    this.SLOTRIGHTX = 90;
    this.HEALTHBARX = 10;

  } else {
    this.SLOTLEFTX = 1050;
    this.SLOTRIGHTX = 1120;
    this.HEALTHBARX = 980;
  }

  this.LEFTITEMKEY = 'q';
  this.RIGHTITEMKEY = 'e';

  (this.player1) ? this.name = 'Player1' : this.name = 'Player2';
  this.texturePath = 'content/' + this.name + '.png';

  this.DOWNSPRITE = {x: 0, y: 0, width: 72, height: 73};
  this.LEFTSPRITE = {x: 0, y: 72, width: 72, height: 72};
  this.RIGHTSPRITE = {x: 0, y: 144, width: 72, height: 73};
  this.UPSPRITE = {x: 0, y: 222, width: 72, height: 73};

  this.HEALTHBARY = 10;
  this.healthRectangle = {x: this.HEALTHBARX, y: this.HEALTHBARY, width: 200, height: 15};

  this.movementAnimation = new Animation([0,1,2,1], 1);
  this.movementAnimation.setTimeBetweenFrames(7);
  this.deathAnimation = new Animation([0]);
  this.deathAnimation.setCurrentFrame({x: 0, y: 288, width: 72, height: 72});
  this.currentAnimation = this.movementAnimation;

  this.lastDirection = this.setLastDirection('up');

  this.id = IdGenerator.generateId();
  this.initialiseStats();
};

Hero.prototype.initialiseStats = function() {
  this.maxHp = 100;
  this.hp = 100;
  this.stats = {};
};

Hero.prototype.update = function() {
  if (this.dead) return;
  if (!this.idle) this.currentAnimation.animate();
};

Hero.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
  if (this.dead) return;
  var movementX = 0;
  var movementY = 0;
  this.sprinting = false;
  this.idle = false;

  if ('space' in keysDown && !('space' in previousKeysDown)) {
    this.inspect();
  }

  if('f' in keysDown && !('f' in previousKeysDown)) {
    this.takeHit();
    //DEBUG TO TAKE HIT
  }

  if (this.LEFTITEMKEY in keysDown && !(this.LEFTITEMKEY in previousKeysDown)) {
    if (this.equipment.leftHand) this.equipment.leftHand.use(this.rogueScene, this);
  }

  if (this.RIGHTITEMKEY in keysDown && !(this.RIGHTITEMKEY in previousKeysDown)) {
    if (this.equipment.rightHand) this.equipment.rightHand.use(this.rogueScene, this);
  }

  movementX = this.handleHorizontalMovementKeys(keysDown, previousKeysDown);
  movementY = this.handleVerticalMovementKeys(keysDown, previousKeysDown);

  if ('shift' in keysDown) this.sprinting = true;

  this.move(movementX, movementY);
};

Hero.prototype.handleHorizontalMovementKeys = function (keysDown, previousKeysDown) {
  var movementX = 0;
  if ('a' in keysDown) {
    movementX = -1;
    this.setLastDirection('left');
  } else if ('d' in keysDown) {
    movementX = 1;
    this.setLastDirection('right');
  }

  return movementX;
};

Hero.prototype.handleVerticalMovementKeys = function(keysDown, previousKeysDown) {
  var movementY = 0;
  if ('w' in keysDown) {
    movementY = -1;
    this.setLastDirection('up');
  } else if ('s' in keysDown) {
    movementY = 1;
    this.setLastDirection('down');
  }

  return movementY;
};

Hero.prototype.move = function(movementX, movementY) {
  if(movementX == 0 && movementY == 0) this.idle = true;
  var movement = this.normalize({x: movementX, y: movementY});

  movement.x = movement.x * this.moveSpeed;
  movement.y = movement.y * this.moveSpeed;

  if (this.sprinting) {
    movement.x *= this.sprintSpeedModifier;
    movement.y *= this.sprintSpeedModifier;
  }
  //Check for collision detection in moved to location and adjust movement appropriatly
  this.movement = movement;

  var xMovement = {x: movement.x, y: 0};
  var yMovement = {x: 0, y: movement.y};

  CollisionController.checkForCollision(this, xMovement);
  CollisionController.checkForCollision(this, yMovement);
  // this.adjustForCollisionWithColliderActors(movement);

  this.x += Math.round(this.movement.x);
  this.y += Math.round(this.movement.y);

  //Suggest sprite based on movementSpeed
  this.setSprite(movement, 'normal');
};

Hero.prototype.isSprinting = function() {
  return this.sprinting;
};

Hero.prototype.setX = function(x) {
  this.x = x;
};

Hero.prototype.setY = function(y) {
  this.y = y;
};

Hero.prototype.checkForNewFrame = function() {
  this.timeSinceFrameChange++;
  if(this.timeSinceFrameChange ==  this.timeBetweenFrames) {
    this.timeSinceFrameChange = 0;
    return true;
  }

  return false;
};

Hero.prototype.setCurrentMap = function (currentMap) {
  this.currentMap = currentMap;
};

Hero.prototype.setEngine = function(engine) {
  this.engine = engine;
};

Hero.prototype.setRogueScene = function(rogueScene) {
  this.rogueScene = rogueScene;
};

Hero.prototype.setLastDirection = function(lastDirection) {
  this.lastDirection = lastDirection;
  var spriteName = lastDirection.toUpperCase() +'SPRITE';
  this.movementAnimation.setCurrentFrame(this[spriteName]);
};

Hero.prototype.normalize = function(vector) {
  var normalized = {x: 0, y: 0};
  var norm = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
  if (norm != 0) { // as3 return 0,0 for a point of zero length
    normalized.x = vector.x / norm;
    normalized.y = vector.y / norm;
  }
  return normalized;
};

Hero.prototype.inspect = function() {
  var inspectableObjects = this.currentMap.getInspectableObjects();
  this.checkForInspectableObjects(inspectableObjects);
};

Hero.prototype.checkForInspectableObjects = function(collisionObjects) {
  if (!collisionObjects) return console.error('Invalid collision objects passed to checkForInspectableObjects');
  var playerCollisionRectangle = this.getInspectCollisionRectangle();

  for (var i = 0; i < collisionObjects.length; i++) {
    var collision = Physics.checkCollisionRects(playerCollisionRectangle, collisionObjects[i]);
    if (collision.collision) {
      this.rogueScene.showInspectMenu(collisionObjects[i]);
      return;
    }
  }
};

Hero.prototype.getInspectCollisionRectangle = function() {
  var inspectRectangle;
  switch (this.lastDirection) {
    case 'up':
      inspectRectangle = {x: this.x, y: this.y - this.height, width: this.width, height: this.height};
      break;
    case 'left':
      inspectRectangle = {x: this.x - this.width, y: this.y, width: this.width, height: this.height};
      break;
    case 'down':
      inspectRectangle = {x: this.x, y: this.y + this.height, width: this.width, height: this.height};
      break;
    case 'right':
      inspectRectangle = {x: this.x + this.width, y: this.y, width: this.width, height: this.height};
      break;
  }
  return inspectRectangle;
};

Hero.prototype.setSprite = function (movement, status) {

};

Hero.prototype.render = function(renderer) {
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();

  var spriteSheetRectangle = this.currentAnimation.getSpriteSheetRectangle();
  renderer.drawSpriteSheet(this.texture, renderX, renderY, spriteSheetRectangle.x, spriteSheetRectangle.y, spriteSheetRectangle.width, spriteSheetRectangle.height);

  this.renderEquipmentSlots(renderer);
  this.renderHealthBar(renderer);
};

Hero.prototype.renderEquipmentSlots = function(renderer) {
  renderer.setGlobalAlpha(0.5);
  renderer.setStrokeStyle(this.SLOTBORDERCOLOR);
  renderer.setFillStyle(this.SLOTCOLOR);

  renderer.drawBoundedBox(this.SLOTLEFTX, this.SLOTY, this.SLOTWIDTH, this.SLOTHEIGHT, 2);
  renderer.drawBoundedBox(this.SLOTRIGHTX, this.SLOTY, this.SLOTWIDTH, this.SLOTHEIGHT, 2);

  renderer.setGlobalAlpha(1.0);

  if(this.equipment.leftHand) this.equipment.leftHand.renderAsIcon(renderer, this.SLOTLEFTX, this.SLOTY);
  if(this.equipment.rightHand) this.equipment.rightHand.renderAsIcon(renderer, this.SLOTRIGHTX, this.SLOTY);
};

Hero.prototype.renderHealthBar = function(renderer) {
  renderer.renderBar(this.healthRectangle, this.hp, this.maxHp, this.hpColor, this.SLOTBORDERCOLOR);
};

Hero.prototype.processContent = function(content) {
  this.texture = content;
  this.equipment.leftHand = ItemContentLibrary.getItem('Throwing Knife');
  this.equipment.rightHand = ItemContentLibrary.getItem('Throwing Knife');
};

Hero.prototype.takeHit = function(damage) {
  this.takeDamage(15);
  var oldSpeed = this.moveSpeed;
  this.moveSpeed = 50;
  switch (this.lastDirection) {
    case 'up':
      this.move(0, 1);
      break;
    case 'left':
      this.move(1, 0);
      break;
    case 'down':
      this.move(0, -1);
      break;
    case 'right':
      this.move(-1, 0);
      break;
  }

  this.moveSpeed = oldSpeed;
};

Hero.prototype.die = function() {
  this.currentAnimation = this.deathAnimation;
};
