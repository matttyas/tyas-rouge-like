function RogueDecoration(position, decorationData) {
  this.DEFAULTCOLOR = 'rgb(0,255,255)';

  if(position) {
    this.x = position.x;
    this.y = position.y;
  }

  this.width = 50;
  this.height = 50;
  this.color = this.DEFAULTCOLOR;

  if (decorationData) this.initialise(decorationData);
};

RogueDecoration.prototype = new Actor();

RogueDecoration.prototype.initialise = function(decorationData) {
  var decorationColor = decorationData.getProperty('color');
  if (decorationColor) this.color = decorationColor;

  this.name = decorationData.getProperty('name');
  this.texturePath = 'content/dungeon/' + this.name + '.png';

  // this.width = decorationData.getProperty('width');
  // this.height = decorationData.getProperty('height');
};

RogueDecoration.prototype.render = function(renderer) {
  if (this.texture) return this.renderTexture(renderer);
  renderer.setFillStyle(this.color);
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();

  renderer.fillRect(renderX, renderY, this.width, this.height);
};

RogueDecoration.prototype.getX = function() {
  return this.x;
};

RogueDecoration.prototype.getY = function() {
  return this.y;
};

RogueDecoration.prototype.getName = function() {
  return this.name;
};

RogueDecoration.prototype.getTexturePath = function() {
  return this.texturePath;
};
