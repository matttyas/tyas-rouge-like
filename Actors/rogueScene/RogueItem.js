function RogueItem(itemData) {
  this.width = 50;
  this.height = 50;
  if (itemData) this.initialise(itemData);
};

RogueItem.prototype = new Actor();

RogueItem.prototype.initialise = function(itemData) {

  this.name = itemData.getProperty('name');
  this.texturePath = 'content/items/' + this.name + '.png';
  this.class = itemData.getProperty('class') || 'Item';
  this.type = itemData.getProperty('type');
   // this.width = decorationData.getProperty('width');
  // this.height = decorationData.getProperty('height');
};

RogueItem.prototype.render = function(renderer) {
  if (this.texture) return this.renderTexture(renderer);
};

RogueItem.prototype.getName = function() {
  return this.name;
};

RogueItem.prototype.getTexturePath = function() {
  return this.texturePath;
};

//Call this to render the icon of the item at the given location on screen
RogueItem.prototype.renderAsIcon = function(renderer, x, y) {
  renderer.drawSpriteSheet(this.texture, x, y, 0, 0, 60, 100);
};

//Basic use method. This will be overwritten by subclasses in the long run I think.
RogueItem.prototype.use = function(rogueScene, user) {
  switch (this.type) {
    case 'Projectile':
      var projectile = new this.class(rogueScene, user)
      projectile.processContent(this.texture);
      rogueScene.addProjectile(projectile);
      break;
    default:
  }
};
