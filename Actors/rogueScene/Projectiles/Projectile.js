function Projectile(rogueScene, user) {
  this.width = 50;
  this.height = 50;
  this.spriteSheetRectangle = {};
  this.initialise(rogueScene, user);
};

Projectile.prototype = new Actor();

Projectile.prototype.initialise = function(rogueScene, user) {

  this.speed = 12;
  this.movementX = 1;
  this.movementY = 0;
  this.movement = {x: 0, y: 0};
  this.userId = user.id;

  this.UPSPRITE = {x: 60, y: 12, width: 5, height: 49};
  this.RIGHTSPRITE = {x: 60, y: 0, width: 49, height: 5};
  this.DOWNSPRITE = {x: 66, y: 12, width: 5, height: 49};
  this.LEFTSPRITE = {x: 60, y: 6, width: 49, height: 5};
  this.calculateSpeedAndDirection(user);
  this.movementX = this.movement.x;
  this.movementY = this.movement.y;
  this.damage = 15; //Calculate based on user strength?
  this.id = IdGenerator.generateId();
};

Projectile.prototype.calculateSpeedAndDirection = function(user) {
  var userDirection = user.getLastDirection();

  switch (userDirection) {
    case 'up':
      this.spriteSheetRectangle = this.UPSPRITE;
      this.movement = {x: 0, y: -1};
      this.x = Math.round(user.x + (user.width / 2));
      this.y = user.y - this.spriteSheetRectangle.height;
      break;
    case 'left':
      this.spriteSheetRectangle = this.LEFTSPRITE;
      this.movement = {x: -1, y: 0};
      this.x = Math.round(user.x - this.spriteSheetRectangle.width);
      this.y = Math.round(user.y + (user.height / 2));
      break;
    case 'down':
      this.spriteSheetRectangle = this.DOWNSPRITE;
      this.movement = {x: 0, y: 1};
      this.x = Math.round(user.x + user.width / 2);
      this.y = Math.round(user.y + user.height);
      break;
    case 'right':
      this.spriteSheetRectangle = this.RIGHTSPRITE;
      this.movement = {x: 1, y: 0};
      this.x = Math.round(user.x + user.width);
      this.y = Math.round(user.y + (user.height / 2));
      break;
  }

  this.height = this.spriteSheetRectangle.height;
  this.width = this.spriteSheetRectangle.width;
};

Projectile.prototype.update = function() {
  this.move();
};

Projectile.prototype.move = function() {
  this.movement = {x: this.movementX * this.speed, y: this.movementY * this.speed};

  var movement = {x: this.movement.x, y: this.movement.y};

  var xMovement = {x: movement.x, y: 0};
  var yMovement = {x: 0, y: movement.y};

  CollisionController.checkForCollision(this, xMovement);
  CollisionController.checkForCollision(this, yMovement);

  this.x += Math.round(this.movement.x);
  this.y += Math.round(this.movement.y);
}

Projectile.prototype.render = function(renderer) {
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();
  renderer.drawSpriteSheet(this.texture, renderX, renderY, this.spriteSheetRectangle.x, this.spriteSheetRectangle.y, this.spriteSheetRectangle.width, this.spriteSheetRectangle.height);

  //RENDER SHADOW
};

//What does the projectile do to something when it hits them?
Projectile.prototype.applyCollision = function(actor, collision, movement) {
  if (actor.id == this.userId) return;
  actor.takeHit(this.damage);
  if (!this.collidedWithEnvironment) this.alterMovementForCollision(collision, movement);
};
