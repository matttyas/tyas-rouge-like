function Actor (x, y) {
  this.x = 0; //The x coordinate of the actor in the current scene.
  this.y = 0; //The y coordinate of the actor in the current scene.
  this.width = 0; //The actors width, used for collision detection and rendering if no render method is used.
  this.height = 0; //The actors height, used for collision detection and rendering if no render method is used.

  this.color = '#000000'; //The color drawn if no other rendering method is used

  this.movement = { //The amount the actor should move at the end of the update process, post physics.
      x: 0,
      y: 0
  };

  this.name = '';
  this.renderMethod = undefined; //If set then used to pick how we render an object.
  this.inspectActions = undefined; //List of actions that can be done after an actor is inspected. User presentable
  this.actionMappings = {}; //Maps presentable string to function name to call to take action
  this.passable = true;

  this.animationOrder = [];
  this.animationIndex = 0;
  this.timeBetweenFrames = 7;
  this.timeSinceFrameChange = 0;
  this.resting = false;
  this.dontUpdate = false;
  this.hp = -100000; //IF health less than -99999 then we assume we don't have to worry about death
  this.dead = false;
}

Actor.prototype.update = function() {
  this.x += this.movement.x;
  this.y += this.movement.y;
};

Actor.prototype.setX = function (x) {
  this.x = x;
};

Actor.prototype.setY = function (y) {
  this.y = y;
};

Actor.prototype.setWidth = function (width) {
  this.width = width;
};

Actor.prototype.setHeight = function (height) {
  this.height = height;
};

Actor.prototype.getInspectActions = function() {
  return this.inspectActions || [];
};

Actor.prototype.takeAction = function(actionString) {
  var actionFunctionName = this.actionMappings[actionString];
  this[actionFunctionName]();
};

Actor.prototype.getColliderObject = function() {
  if (this.passable) return null;
  if (this.colliderObject) return this.colliderObject;

  return new ColliderObject(this, 0,0, this.width, this.height);
}

Actor.prototype.processContent = function(content) {
  this.texture = content;
};

Actor.prototype.renderTexture = function(renderer) {
  var camera = renderer.getCamera();
  var renderX = this.x - camera.getX();
  var renderY = this.y - camera.getY();
  renderer.drawImage(this.texture, renderX, renderY)
};

Actor.prototype.getName = function() {
  return this.name;
};

Actor.prototype.getTexturePath = function() {
  return this.texturePath;
};

//Overwrite this method on a child class to implement a consequence of collision,
//with this class.
Actor.prototype.applyCollision = function(actor, collision, movement) {

};

//To do collision right we check twice once with x movement and once with y.
//This takes the single movement(either x or y) adjusts it for collision,
//then sets it on this.movement, for the actual movement.
Actor.prototype.alterMovementForCollision = function(collision, movement) {
  if (movement.x < 0) {
    this.movement.x = movement.x + Math.round(collision.xOverlap);
    movement.x = this.movement.x;
  } else if (movement.x > 0) {
    this.movement.x = movement.x - Math.round(collision.xOverlap);
    movement.x = this.movement.x
  }

  if (movement.y < 0) {
    this.movement.y = movement.y + Math.round(collision.yOverlap);
    movement.y = this.movement.y;
  } else if (movement.y > 0) {
    this.movement.y = movement.y - Math.round(collision.yOverlap);
    movement.y = this.movement.y;
  }

  this.collidedWithEnvironment = true;
};

Actor.prototype.getLastDirection = function() {
  return this.lastDirection || 'left';
};

Actor.prototype.rest = function() {
  this.resting = true;
  if(this.restingAnimation) this.currentAnimation = this.restingAnimation;
};

Actor.prototype.stopResting = function() {
  this.resting = false;
};

Actor.prototype.moveTowards = function(targetPosition) {
  if (this.movementAnimation) this.currentAnimation = this.movementAnimation;

  this.collidedWithEnvironment = false;
  var position = {x: this.x, y: this.y};

  var movement = Physics.calculateMovementVector(position, targetPosition, this.speed);
  this.movement = {x: movement.x, y: movement.y};
  var xMovement = {x: movement.x, y: 0};
  var yMovement = {x: 0, y: movement.y};

  CollisionController.checkForCollision(this, xMovement);
  CollisionController.checkForCollision(this, yMovement);

  this.x += Math.round(this.movement.x);
  this.y += Math.round(this.movement.y);
};

Actor.prototype.closeTo = function(targetPosition) {
  var speed = this.speed || 0;
  return (Math.abs(Physics.distanceBetweenPoints({x: this.x, y: this.y}, targetPosition)) <= speed );
};

Actor.prototype.takeHit = function(damage) {
  this.takeDamage(damage);
};

Actor.prototype.takeDamage = function(damage) {
  if (!damage || this.hp < -100000 || this.dead) return;
  this.hp -= damage;
  Math.round(this.hp);
  if (this.hp < 0) {
    this.hp = 0;
    this.dead = true;
    this.die();
  }
};

Actor.prototype.die = function() {

};

//Should be overwritten by the npc type I think
Actor.prototype.lookForTargets = function() {

};

Actor.prototype.getCurrentTarget = function() {

};

Actor.prototype.getFoes = function() {
  return [];
};
