function MainMenuOption(x, y, width, height, text) {
  if (!x || !y || !width || !height || !text) return console.error('Invalid params for MainMenuOption');

  this.renderMethod = 'self';
  this.gathered = false;
  this.name = 'MainMenuOption';

  this.hide = false;
  this.leanLeft = true;

  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.text = text;

  this.textOffSetX = 0;

  this.selected = false;
}

MainMenuOption.prototype = new Actor();

MainMenuOption.prototype.render = function(ctx, renderer) {
  if (this.hide) return;

  //Border
  renderer.setFillStyle('rgb(0,0,0)');

  if(!this.selected) {
    renderer.fillRect(this.x, this.y, this.width, this.height);  
  } else {
    var scaledUpRectangle = renderer.scaleUpRectangle(this, 20);
    renderer.fillRect(scaledUpRectangle.x, scaledUpRectangle.y, scaledUpRectangle.width, scaledUpRectangle.height);
  }
  

  //Background
  !this.selected ? renderer.setFillStyle('rgb(255,255,0)') : renderer.setFillStyle('rgb(255,140,0)');

  if (!this.selected) {
    renderer.fillRect(this.x + 5, this.y + 5, this.width - 10, this.height - 10);  
  } else {
    var scaledUpRectangle = renderer.scaleUpRectangle({x: this.x + 5, y: this.y + 5, width: this.width - 10, height: this.height - 10}, 20);
    renderer.fillRect(scaledUpRectangle.x, scaledUpRectangle.y, scaledUpRectangle.width, scaledUpRectangle.height);
  }
  

  //Text
  renderer.setTextColor('rgb(0,0,0)');
  renderer.setTextSize(30);
  renderer.setFont('Impact');
  renderer.drawText(this.text, this.x + this.textOffSetX, this.y + 37);
};

MainMenuOption.prototype.setIsLeanLeft = function(leanLeft) {
  this.isLeanLeft = leanLeft;
};

MainMenuOption.prototype.setText = function(text) {
  this.text = text;
};

MainMenuOption.prototype.setTextOffSetX = function(textOffSetX) {
  this.textOffSetX = textOffSetX;
};

MainMenuOption.prototype.removeSelection = function() {
  this.selected = false;
};

MainMenuOption.prototype.addSelection = function () {
  this.selected = true;
};
