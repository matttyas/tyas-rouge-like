function Editor (x, y) {
  this.x = x; //The x coordinate of the actor in the current scene.
  this.y = y; //The y coordinate of the actor in the current scene.
  this.camera; //Reference to camera should be set to use editor.
  this.cameraSpeed = 5; //Speed at which camera moves
  this.cameraInputState = 'Camera';
  this.inputState = this.cameraInputState;
}

Editor.prototype = new Actor();

Editor.prototype.update = function() {

};

Editor.prototype.render = function(ctx, renderer) {

};

Editor.prototype.handleKeyboard = function(keysDown, previousKeysDown) {

  switch (this.inputState) {
    case this.cameraInputState:
      this.handleCameraInput(keysDown, previousKeysDown);
      break;
  }

  if ('q' in keysDown && !('q' in previousKeysDown)) { //Q
    if(!this.currentScene) return console.error('Current scene for handle input in Editor');
    this.currentScene.goUpAFloor();
  }

  if ( 'e' in keysDown  && ! ('e' in previousKeysDown)) { //E
    if(!this.currentScene) return console.error('Current scene for handle input in Editor');
    this.currentScene.goDownAFloor();
  }
};

Editor.prototype.handleCameraInput = function (keysDown, previousKeysDown) {
  if ('w' in keysDown) { //W
    this.camera.y -= this.cameraSpeed;
  }

  if ( 's' in keysDown ) { //S
    this.camera.y += this.cameraSpeed;
  }

  if ( 'a' in keysDown) { //A
    this.camera.x -= this.cameraSpeed;
  }

  if ('d' in keysDown) { //D
    this.camera.x += this.cameraSpeed;
  }
};

Editor.prototype.setCamera = function(camera) {
  this.camera = camera;
};

Editor.prototype.setCurrentScene = function(currentScene) {
  this.currentScene = currentScene;
};
