function CharacterSelect() {
  this.renderMethod = 'self';
  this.gathered = false;
  this.name = 'CharacterSelect';
  this.x = 470;
  this.y = 300;

  this.hide = false;
}

CharacterSelect.prototype = new Actor();

CharacterSelect.prototype.render = function(ctx, renderer) {
  if (this.hide) return;
 //Background

  renderer.setFillStyle('rgb(0,0,0)');
  ctx.fillRect(this.x, this.y, 340, 180);
  renderer.setFillStyle('rgb(255,255,0)');
  ctx.fillRect(this.x + 10, this.y + 10, 320, 160 );

  //Bandana Man

  renderer.setFillStyle('rgb(0,0,0)');
  ctx.fillRect(this.x + 30, this.y + 40, 100, 130);

  //Body

  renderer.setFillStyle('rgb(0,255,0)');
  ctx.fillRect(this.x + 35, this.y + 45, 90, 125);

  //Bandana
  renderer.setFillStyle('rgb(255,0,0)');
  ctx.fillRect(this.x + 35, this.y + 55, 90, 25);

  //Eye
  renderer.setFillStyle('rgb(0,0,255)');
  ctx.fillRect(this.x + 105, this.y + 60, 15, 15);

  //Lady Bandana


  //Divider Line
  renderer.setStrokeStyle('rgb(0,0,0)');
  renderer.setLineWidth(10);
  renderer.drawLine({x: this.x + 180, y: this.y}, {x: this.x + 160, y: this.y + 175});

  renderer.setFillStyle('rgb(0,0,0)');
  ctx.fillRect(this.x + 210, this.y + 40, 100, 130);

  //Body

  renderer.setFillStyle('rgb(0,255,0');
  ctx.fillRect(this.x + 215, this.y + 45, 90, 125);

  //Bandana
  renderer.setFillStyle('rgb(255,255,0)');
  ctx.fillRect(this.x + 215, this.y + 55, 90, 25);

    //Eye
  renderer.setFillStyle('rgb(0,0,255)');
  ctx.fillRect(this.x + 220, this.y + 60, 15, 15);
};

CharacterSelect.prototype.handleInput = function(keysDown, previousKeysDown) {
  if ( 65 in keysDown) { //A

  } else if (68 in keysDown) { //D

  }
};