function RogueTile (x, y, width, height) {
  this.x = x; //The x coordinate of the actor in the current scene.
  this.y = y; //The y coordinate of the actor in the current scene.
  this.width = width; //The actors width, used for collision detection and rendering if no render method is used.
  this.height = height; //The actors height, used for collision detection and rendering if no render method is used.

  this.color = 'rgb(0,0,0)'; //The color drawn if no other rendering method is used

  this.renderX = 0; //x coordinate to render on screen
  this.renderY = 0; //y coordinate to render on screen

  this.renderBorder = DEBUG; //If true draw a solid pixel border around this tile.

  this.hidden = false; //If true then display as fog of war
  this.revealed = true; //If false then show as fully unexplored (black)
  this.unrevealedColor = 'rgb(0,0,0)';
  this.hiddenColor = 'rgb(125,125,125,30)';
  this.borderColor = 'rgb(255,255,255)';
  this.label = null; //If in debug mode and label is set this will be drawn on the tile.
  //Object that describes gameplay related properties of the tile
  //Most values in here will be overriden by child tiles to define their function
  this.properties = {};

  this.movement = { //The amount the actor should move at the end of the update process, post physics.
      x: 0,
      y: 0
  };

  this.name = '';
  this.renderMethod = undefined; //If set then used to pick how we render an object.
}

RogueTile.prototype = new Actor();

RogueTile.prototype.update = function() {

};

RogueTile.prototype.render = function(renderer) {
  this.renderX = this.x - renderer.camera.x;
  this.renderY = this.y - renderer.camera.y;

  if (!this.revealed) {
    return this.renderUnrevealed(renderer);
  }

  if (this.texture) {
    this.renderTexture(renderer);
  } else {
    renderer.setFillStyle(this.color);
    renderer.fillRect(this.renderX, this.renderY, this.width, this.height);
  }

  if (this.hidden) {
    this.renderHidden(renderer);
  }

  if(this.renderBorder) {
    renderer.setStrokeStyle(this.borderColor);
    renderer.strokeRect(this.renderX, this.renderY, this.width, this.height);
  }

  if(DEBUG) this.debugRender(renderer);
};

//If unrevealed then show the unrevealedColor instead of the tile color

RogueTile.prototype.renderUnrevealed = function(renderer) {
  renderer.setFillStyle(this.unrevealedColor);
  renderer.fillRect(this.renderX, this.renderY, this.width, this.height);
};

//If hidden render a greyed out effect over the tile
RogueTile.prototype.renderHidden = function(renderer) {
  renderer.setFillStyle(this.hiddenColor);
  renderer.fillRect(this.renderX, this.renderY, this.width, this.height);
};

RogueTile.prototype.buildRogueTile = function(tile) {
  this.setProperties(tile.getProperties());
  var label = tile.getLabel();
  if (label) this.label = label;
}

//Sets properties of the tile. May get moved out to seperate class later.
RogueTile.prototype.setProperties = function(properties) {
  this.properties = properties;

  if (this.properties.classification) {
    this.name = this.properties.classification;
    this.texturePath = 'content/dungeon/tiles/' + this.properties.classification + '.png';
    this.passable = this.properties.passable;
    if(this.name.indexOf('Tall') > 0) {
      this.y -= 100;
      this.height += 100;
    }
  }
};

RogueTile.prototype.getProperty = function(property) {
  return this.properties[property];
}

RogueTile.prototype.setLabel = function(label) {
  this.label = label;
};

//Show if hook or not
RogueTile.prototype.debugRender = function(renderer) {
  if (this.properties.hook) {
    renderer.setFillStyle('rgb(255,0,0');
    renderer.fillRect(this.renderX, this.renderY, this.width, this.height);
  }

  if (this.label) {
    renderer.drawText(this.label, this.renderX, this.renderY + this.height / 2);
  }
};

RogueTile.prototype.applyCollision = function (actor, collision, movement) {
  actor.alterMovementForCollision(collision, movement);
};
