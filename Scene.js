function Scene (engine) {
  this.actors = [];
  this.name = 'Scene';
}

Scene.prototype.update = function() {

};

Scene.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
    if ( 87 in keysDown) { //W

  }

  if ( 83 in keysDown) { //S

  }

  if ( 65 in keysDown) { //A

  }

  if (68 in keysDown) { //D

  }
};

//e is the mouse click. x is pos within canvas as is y.
Scene.prototype.handleClick = function(e, x, y) {

};

Scene.prototype.render = function(ctx, renderer) { };

Scene.prototype.getName = function() {
  return this.name;
}

Scene.prototype.handleRenderer = function() {
  this.setCamera();
};

Scene.prototype.setCamera = function() {
  if (this.engine) this.camera = this.engine.camera;
};

Scene.prototype.getLoaded = function() {
  return true;
};